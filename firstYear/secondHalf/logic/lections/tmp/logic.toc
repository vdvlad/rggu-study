\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {russian}{}
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {1}Необходимость чёткого языка в математике}{3}{chapter.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1.1}Причины появления}{3}{section.1.1}%
\defcounter {refsection}{0}\relax 
\contentsline {chapter}{\numberline {2}Высказывания}{5}{chapter.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.1}Свойства}{5}{section.2.1}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.2}Логические операции}{5}{section.2.2}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.3}Формулы алгебры высказываний}{8}{section.2.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.4}Булевы функции}{9}{section.2.4}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.4.1}Конкретные булевы функции}{11}{subsection.2.4.1}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.4.2}Равносильность (эквивалентность) формул}{12}{subsection.2.4.2}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.4.3}Понятие суперпозиции}{13}{subsection.2.4.3}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.5}Двойственные функции}{16}{section.2.5}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2.6}Фиктивные и существенные переменные}{17}{section.2.6}%
