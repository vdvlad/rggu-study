{-# LANGUAGE QuasiQuotes, FlexibleContexts, BangPatterns #-}
import Prelude
import Data.Maybe (fromJust,isNothing)

import Graphics.Rendering.Chart.Easy (def,plot,points,opaque,liftEC,(.=))
import Graphics.Rendering.Chart.Easy (blue,red,magenta,cyan,setColors)
import Graphics.Rendering.Chart.Backend.Cairo (toFile)
import Graphics.Rendering.Chart.Plot.Points
import Graphics.Rendering.Chart.Drawing

import Text.Regex.PCRE.Heavy (gsub,re,scan)

import Data.List.Split (splitOn)

type Input   = String
type Points  = ((Double,Double),(Double,Double),(Double,Double))
type Points2 = ((Double,Double),(Double,Double))
type Point   = (Double,Double)
type Vector  = (Double,Double)

stringDot :: String -> String
stringDot string =
  if (last string == '.')
  then string ++ "0"
  else string

splitDots3 :: [Double] -> Points
splitDots3 (ax:ay:bx:by:cx:cy:_) = ((ax, ay), (bx, by), (cx, cy))

makeDots3 :: Input -> Maybe Points
makeDots3 input =
  let
    p = [re| *A *\( *(-?\d+\.?\d*) *[,;] *(-?\d+\.?\d*) *\) *[;,]? *B *\( *(-?\d+\.?\d*) *[,;] *(-?\d+\.?\d*) *\) *[;,]? *C *\( *(-?\d+\.?\d*) *[,;] *(-?\d+\.?\d*) *\) *|]
    s = scan p input
  in
    if (null s)
    then Nothing
    else
      let str = snd $ last s
          j   = splitDots3 $ map (\x -> read (stringDot x) :: Double) str
      in
        Just j

readDots3 :: IO(Points)
readDots3 =
  do
    input <- getLine
    let m = makeDots3 input
    if (isNothing m)
    then do
      putStrLn "You are wrong. Please, try again."
      r <- readDots3
      return r
    else return (fromJust m)

splitDots2 :: [Double] -> Points2
splitDots2 (ax:ay:bx:by:_) = ((ax, ay), (bx, by))

makeDots2 :: Input -> Maybe Points2
makeDots2 input =
  let
    p = [re| *A *\( *(-?\d+\.?\d*) *[,;] *(-?\d+\.?\d*) *\) *[;,]? *B *\( *(-?\d+\.?\d*) *[,;] *(-?\d+\.?\d*) *\) *|]
    s = scan p input
  in
    if (null s)
    then Nothing
    else
      let str = snd $ last s
          j   = splitDots2 $ map (\x -> read (stringDot x) :: Double) str
      in
        Just j

readDots2 :: IO(Points2)
readDots2 =
  do
    input <- getLine
    let m = makeDots2 input
    if (isNothing m)
    then do
      putStrLn "You are wrong. Please, try again."
      r <- readDots2
      return r
    else return (fromJust m)

splitDot :: [Double] -> Point
splitDot (ax:ay:_) = (ax, ay)

makeDot :: Input -> Maybe Point
makeDot input =
  let
    p = [re| *[PO] *\((-?\d+\.?\d*) *[,;] *(-?\d+\.?\d*)\) *|]
    s = scan p input
  in
    if (null s)
    then Nothing
    else
      let str = snd $ last s
          j   = splitDot $ map (\x -> read (stringDot x) :: Double) str
      in
        Just j

readDot :: IO(Point)
readDot =
  do
    input <- getLine
    let m = makeDot input
    if (isNothing m)
    then do
      putStrLn "You are wrong. Please, try again."
      r <- readDot
      return r
    else return (fromJust m)

getNumber :: IO(Double)
getNumber =
  do
    a <- getLine
    let !r = readNumber a
    if (r < 0 || r >= 1)
    then do
      putStrLn "Sorry, try again, please."
      getNumber
    else return r

readNumber :: Input -> Double
readNumber input =
  let s = scan [re|^(\d+\.?\d*)$|] input
  in if (null s)
     then -1
     else let str = fst $ head s
          in if (last str == '.')
             then (read (str ++ "0") :: Double)
             else (read str :: Double)

number1 :: Points -> IO((Double,Double)) -- return O(x,y)
number1 ((ax, ay), (bx, by), (cx, cy)) =
  do
    let ka = (by - ay)/(bx - ax)
        kb = (cy - by)/(cx - bx)
        x = (ka*kb*(ay - cy) + kb*(ax + bx) - ka*(bx + cx)) / (2*(kb - ka))
        y = -(x - (bx + cx)/2)/kb + (by + cy)/2
    putStrLn ("The center of circle has coordinates ("
              ++ show x ++ ", " ++ show y ++ ").")
    toFile def "number1.png" $ do
      plot $ liftEC $ do
        plot_points_title  .= "  O"
        plot_points_style  .= PointStyle (opaque red) (opaque red) 4 4 PointShapeCircle
        plot_points_values .= (x,y)
                             :[]
      plot $ liftEC $ do
        plot_points_title  .= "A, B, C"
        plot_points_style  .= PointStyle (opaque magenta) (opaque magenta) 3 3 PointShapeCircle
        plot_points_values .= (ax,ay)
                             :(bx,by)
                             :(cx,cy)
                             :[]
    return (x,y)

number2 :: Points -> Point -> IO(Double) -- return R
number2 ((ax, ay), (bx, by), (cx, cy)) (x, y) =
  do
    let r = sqrt $ (ax - x)^2 + (ay - y)^2
    putStrLn $ "Radius of circle is " ++ show r ++ "."
    let signal = [ (r*sin t + x,r*cos t + y) | t <- [0,0.001..2*pi]]
    toFile def "number2.png" $ do
      setColors [opaque blue]
      plot (points "y = f(x)" (signal))
      plot $ liftEC $ do
        plot_points_title  .= "  O"
        plot_points_style  .= PointStyle (opaque red) (opaque red) 4 4 PointShapeCircle
        plot_points_values .= (x,y)
                             :[]
      plot $ liftEC $ do
        plot_points_title  .= "A, B, C"
        plot_points_style  .= PointStyle (opaque magenta) (opaque magenta) 3 3 PointShapeCircle
        plot_points_values .= (ax,ay)
                             :(bx,by)
                             :(cx,cy)
                             :[]
    return r


--      :: point with tangents -> center -> radius -> nothing
number3 :: Point -> Point -> Double -> IO()
number3 (px, py) (x, y) r =
  do
    let (!pox,!poy) = (x-px,y-py)
    let !lp = sqrt $ pox^2 + poy^2

    let !angle = asin $ r / lp
    let angle2 = 2 * angle -- double angle for answer

    putStrLn $ "Angle with line from P("
             ++ show px ++ "," ++ show py ++ ")"
             ++ " is " ++ show angle2 ++ "."
    let circle  = [ (r*sin t + x,r*cos t + y) | t <- [0,0.001..2*pi]]
    -- ph - directional segment between point P and one of points of tangency
    -- with d (discriminant) we can to find phx
    -- (it is get by resolve equation of circle and scalar of PH and PO)

    let !k  = lp^2 - r^2
        !k1  = k / pox
        !k2  = poy / pox
        !otl = k*(k2^2) - k1^2 + k
        !dPh = sqrt otl

        !k3 = 1+k2^2
        !k4 = k*poy/(lp^2)
        !ph1y = k4 - dPh / k3
        !ph2y = k4 + dPh / k3

        !ph1x = k1 - ph1y * k2
        !ph2x = k1 - ph2y * k2

    let phSig = [ (ph1x*t + px, ph1y*t + py) | t <- [0,0.001..1]]
             ++ [ (ph2x*t + px, ph2y*t + py) | t <- [0,0.001..1]]
    toFile def "number3.png" $ do
      setColors [opaque blue, opaque cyan]
      plot (points "y = f(x)" (circle))
      plot (points "tangents" (phSig))
      plot $ liftEC $ do
        plot_points_title  .= "  O"
        plot_points_style  .= PointStyle (opaque red) (opaque red) 3 3 PointShapeCircle
        plot_points_values .= (x,y)
                             :[]

--      :: center -> point O -> radius -> nothing
number4 :: Point -> Point -> Double -> IO()
number4 (xc, yc) (x, y) r =
  do
    let (!cox,!coy) = (x-xc,y-yc) -- beetwin center and O
        !lo = sqrt $ cox^2 + coy^2
        !k  = lo^2
        !k1  = k / cox
        !k2  = coy / cox
        !otl = (r*k2)^2 - k1^2 + r^2
        !dPh = sqrt otl

        !k3 = 1+k2^2
        !k4 = coy
        !ph1y = k4 - dPh / k3
        !ph2y = k4 + dPh / k3

        !ph1x = k1 - ph1y * k2
        !ph2x = k1 - ph2y * k2

        (chorx, chory) =  (ph2x-ph1x,ph2y-ph1y)

    putStrLn $ "Length of chord is " ++ show (sqrt $ chorx^2 + chory^2)

    let circle  = [ (r*sin t + xc,r*cos t + yc) | t <- [0,0.001..2*pi]]

    let phSig = [ (chorx*t + x, chory*t + y) | t <- [-0.5,-0.49..0.5]]
    toFile def "number4.png" $ do
      setColors [opaque blue, opaque cyan]
      plot (points "y = f(x)" (circle))
      plot (points "Chord" (phSig))
      plot $ liftEC $ do
        plot_points_title  .= "  O"
        plot_points_style  .= PointStyle (opaque red) (opaque red) 3 3 PointShapeCircle
        plot_points_values .= (x,y)
                             :[]
      plot $ liftEC $ do
        plot_points_title  .= "  C"
        plot_points_style  .= PointStyle (opaque magenta) (opaque magenta) 3 3 PointShapeCircle
        plot_points_values .= (xc,yc)
                             :[]

--      :: center -> point O -> radius -> nothing
number5 :: Point -> Point -> Double -> IO()
number5 (xc, yc) (x, y) r =
  do
    let (!ocx,!ocy) = (xc-x,yc-y) -- beetwin center and O
        !loc = sqrt $ ocx^2 + ocy^2
        !lob = loc + r
        !k   = lob / loc
        (!obx,!oby) = (ocx*k,ocy*k)

    putStrLn $ "Max length from null to circle (length vector CB) is "
             ++ show lob

    let circle  = [ (r*sin t + xc,r*cos t + yc) | t <- [0,0.001..2*pi]]

    let phSig = [ (x + obx*t, y + oby*t) | t <- [1,0.99..0]]
    toFile def "number5.png" $ do
      setColors [opaque blue, opaque cyan]
      plot (points "y = f(x)" (circle))
      plot (points "OB" (phSig))
      plot $ liftEC $ do
        plot_points_title  .= "  C"
        plot_points_style  .= PointStyle (opaque red) (opaque red) 3 3 PointShapeCircle
        plot_points_values .= (xc,yc)
                             :[]

--      :: two points -> e -> (bias x, semi major axis, semi minor axis)
number6 :: Points2 -> Double -> IO(Double,Double,Double)
number6 ((x1,y1),(x2,y2)) e =
  do
    let !k0 = 1 - e^2
        !k1 = y1^2 / k0
        !k2 = y2^2 / k0

        !x0  = -(x2^2 - x1^2 + k2 - k1)/(2*(x1-x2))
        -- l --- major axis
        !l   = sqrt $ (x2 - x0)^2 + k2
        -- h --- minor axis
        !h   = l*sqrt k0

    putStrLn $ "The center is (" ++ show x0 ++ ", 0)."

    let ellipse = [ (l*sin t + x0,h*cos t) | t <- [0.00,0.01..2*pi]]
    toFile def "number6.png" $ do
      setColors [opaque blue, opaque cyan]
      plot (points "ellipse" (ellipse))
      plot $ liftEC $ do
        plot_points_title  .= "  O"
        plot_points_style  .= PointStyle (opaque red) (opaque red) 3 3 PointShapeCircle
        plot_points_values .= (x0,0)
                             :[]
      plot $ liftEC $ do
        plot_points_title  .= "  A"
        plot_points_style  .= PointStyle (opaque red) (opaque red) 3 3 PointShapeCircle
        plot_points_values .= (x1,y1)
                             :[]
      plot $ liftEC $ do
        plot_points_title  .= "  B"
        plot_points_style  .= PointStyle (opaque red) (opaque red) 3 3 PointShapeCircle
        plot_points_values .= (x2,y2)
                             :[]
    return (x0,l,h)

--      :: bias x -> semi major axis -> semi minor axis -> e -> nothing
number7 :: Double -> Double -> Double -> Double -> IO()
number7 x0 l h e =
  do
    let !f1x =  l*e + x0
        !f2x = -l*e + x0

    putStrLn $ "Distance between focuses = " ++ show (f1x - f2x) ++ "."

    let ellipse = [ (l*sin t + x0,h*cos t) | t <- [0.00,0.01..2*pi]]
    toFile def "number7.png" $ do
      setColors [opaque blue, opaque cyan]
      plot (points "ellipse" (ellipse))
      plot $ liftEC $ do
        plot_points_title  .= "  O"
        plot_points_style  .= PointStyle (opaque red) (opaque red) 3 3 PointShapeCircle
        plot_points_values .= (x0,0)
                             :[]
      plot $ liftEC $ do
        plot_points_title  .= "  F1"
        plot_points_style  .= PointStyle (opaque red) (opaque red) 3 3 PointShapeCircle
        plot_points_values .= (f1x,0)
                             :[]
      plot $ liftEC $ do
        plot_points_title  .= "  F2"
        plot_points_style  .= PointStyle (opaque red) (opaque red) 3 3 PointShapeCircle
        plot_points_values .= (f2x,0)
                             :[]

--      :: bias x -> semi major axis -> semi minor axis -> nothing
number8 :: Double -> Double -> Double -> IO()
number8 x0 l h =
  do
    putStrLn $ "Minor axis length = " ++ show (h*2) ++ "."
    let ellipse = [ (l*sin t + x0,h*cos t) | t <- [0.00,0.01..2*pi]]
    let semi = [ (x0,h*t) | t <- [-1.00,-0.99..1]]
    toFile def "number8.png" $ do
      setColors [opaque blue, opaque cyan]
      plot (points "ellipse" (ellipse))
      plot (points "semi minor axis" (semi))
      plot $ liftEC $ do
        plot_points_title  .= "  O"
        plot_points_style  .= PointStyle (opaque red) (opaque red) 3 3 PointShapeCircle
        plot_points_values .= (x0,0)
                             :[]

--      :: bias x -> semi major axis -> semi minor axis -> e -> nothing
number9 :: Double -> Double -> Double -> Double -> IO()
number9 x0 l h e =
  do
    let !d = l/e
    let !dir1 = d + x0
    let !dir2 = -d + x0
    putStrLn $ "Distance directris = " ++ show (dir1-dir2) ++ "."
    let ellipse = [ (l*sin t + x0,h*cos t) | t <- [0.00,0.01..2*pi]]
    let !dia  = h*1.3
    let dirs = [ (dir1,t) | t <- [-dia,(-dia+0.01)..dia]]
            ++ [ (dir2,t) | t <- [-dia,(-dia+0.01)..dia]]
    toFile def "number9.png" $ do
      setColors [opaque blue, opaque cyan]
      plot (points "ellipse" (ellipse))
      plot (points "directris" (dirs))
      plot $ liftEC $ do
        plot_points_title  .= "  O"
        plot_points_style  .= PointStyle (opaque red) (opaque red) 3 3 PointShapeCircle
        plot_points_values .= (x0,0)
                             :[]

--      :: two points -> bias x -> semi major axis -> semi minor axis -> e -> nothing
number10 :: Points2 -> Double -> Double -> Double -> Double -> IO()
number10 ((xa,ya),(xb,yb)) x0 l h eks =
  do
    let (!xab,!yab) = (xb-xa,yb-ya)

        !k     = (yab/xab)

        !l2 = l^2
        !h2 = h^2
        !k1 = l2*k
        !x = -k1 / sqrt(k1*k + h2)
        !ktg = -h2/k1
        !y = ktg*x

        !alpha0 = atan ktg
        !alpha = if (alpha0 >= pi)
                 then alpha0 - pi
                 else if (alpha0 < 0)
                      then alpha0 + pi
                      else alpha0
    putStrLn $ "The angle is " ++ show alpha ++ " radian."

    let ellipse  = [ (l*sin t + x0,h*cos t) | t <- [0.00,0.01..2*pi]]
        chord    = [ (xab*t+xa, yab*t+ya) | t <- [0.00,0.01..1]]
        diameter = [ (x*t+x0, y*t) | t <- [-1.00,-0.99..1]]
    toFile def "number10.png" $ do
      setColors [opaque blue, opaque cyan, opaque magenta]
      plot (points "ellipse" (ellipse))
      plot (points "chord" (chord))
      plot (points "diameter" (diameter))

main :: IO ()
main = do
  putStrLn "Put your points A, B, C for numbers 1,2."
  putStrLn "In format like:"
  putStrLn "A( -4 ,2), B ( -1,5) ; C(6,-4)"
  ---
  first <- readDots3
  ---
  o <- number1 first
  r <- number2 first o

  putStrLn "Put your points P for tangents, number 3."
  putStrLn "In format like:"
  putStrLn "P(15,2)"
  ---
  third <- readDot
  ---
  number3 third o r

  putStrLn "Put your points O for chord, number 4 and number 5."
  putStrLn "In format like:"
  putStrLn "O(0,0)"
  ---
  cdot <- readDot
  ---
  number4 o cdot r
  number5 o cdot r

  putStrLn "Put your points A, B for number 6."
  putStrLn "In format like:"
  putStrLn "A( -4 ,2), B ( -1,5)"
  ---
  six <- readDots2
  ---
  putStrLn "Put your points eccentricity (number >= 0, and < 1)."
  ---
  eks <- getNumber
  ---
  (x0,l,h) <- number6 six eks
  number7 x0 l h eks
  number8 x0 l h
  number9 x0 l h eks
  number10 six x0 l h eks
