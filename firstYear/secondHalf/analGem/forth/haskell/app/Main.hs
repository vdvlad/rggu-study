{-# LANGUAGE QuasiQuotes, FlexibleContexts, BangPatterns #-}
import Prelude
import Data.Maybe (fromJust,isNothing)

import Graphics.Rendering.Chart.Easy (def,plot,points,opaque,liftEC,(.=))
import Graphics.Rendering.Chart.Easy (blue,red,magenta,cyan,setColors)
import Graphics.Rendering.Chart.Backend.Cairo (FileFormat(PNG),toFile)
import Graphics.Rendering.Chart.Backend.Cairo (FileOptions (..))
import Graphics.Rendering.Chart.Plot.Points
import Graphics.Rendering.Chart.Drawing

import Text.Regex.PCRE.Heavy (gsub,re,scan)

import Data.List.Split (splitOn)

type Input   = String
type Point   = (Double,Double)
type Points  = ((Double,Double),(Double,Double),(Double,Double))

stringDot :: String -> String
stringDot string =
  if (last string == '.')
  then string ++ "0"
  else string

splitDots :: [Double] -> Points
splitDots (ax:ay:bx:by:cx:cy:_) = ((ax, ay), (bx, by), (cx, cy))

makeDots :: Input -> Maybe Points
makeDots input =
  let
    p = [re| *A *\( *(-?\d+\.?\d*) *[,;] *(-?\d+\.?\d*) *\) *[;,]? *B *\( *(-?\d+\.?\d*) *[,;] *(-?\d+\.?\d*) *\) *[;,]? *C *\( *(-?\d+\.?\d*) *[,;] *(-?\d+\.?\d*) *\) *|]
    s = scan p input
  in
    if (null s)
    then Nothing
    else
      let str = snd $ last s
          j   = splitDots $ map (\x -> read (stringDot x) :: Double) str
      in
        Just j

readDots :: IO(Points)
readDots =
  do
    input <- getLine
    let m = makeDots input
    if (isNothing m)
    then do
      putStrLn "You are wrong. Please, try again."
      r <- readDots
      return r
    else return (fromJust m)

number1 :: Points -> IO(Point) -- return O(ox,oy)
number1 ((ax, ay), (bx, by), (cx, cy)) =
  do
    let (ox,oy) = ((cx+bx)/2,(cy+by)/2)
    putStrLn ("The center of hyperbola has coordinates ("
              ++ show ox ++ ", " ++ show oy ++ ").")
    toFile def "number1.png" $ do
      plot $ liftEC $ do
        plot_points_title  .= "  O"
        plot_points_style  .= PointStyle (opaque red) (opaque red) 4 4 PointShapeCircle
        plot_points_values .= (ox,oy)
                             :[]
      plot $ liftEC $ do
        plot_points_title  .= "A, B, C"
        plot_points_style  .= PointStyle (opaque magenta) (opaque magenta) 3 3 PointShapeCircle
        plot_points_values .= (ax,ay)
                             :(bx,by)
                             :(cx,cy)
                             :[]
    return (ox,oy)

number2 :: Points -> Point -> IO((Double,Double,Double,Double)) -- return a,b,c,angle
number2 ((ax, ay), (bx, by), (cx, cy)) (ox, oy) =
  do
    let (!bcx,!bcy) = ((cx-bx),(cy-by))
        !angle      = atan $ bcy/bcx

        !cosa = cos angle
        !sina = sin angle

        !rx = ax - ox
        !ry = ay - oy
        !x' = rx * cosa + ry * sina
        !y' = rx * sina - ry * cosa

        !c2 = ((bcx)^2 + (bcy)^2)/4
        !y2 = y'^2
        !x2 = x'^2

        !d  = (x2 + y2 - c2)^2 + 4*y2*c2
        !b2 = (c2 - x2 - y2 + sqrt d)/2
        !a  = sqrt $ c2 - b2

        b   = sqrt b2
        c   = sqrt c2
    putStrLn ("The minimum distance of hyperbola is "
              ++ show (2*a) ++ ".")
    let signal = [ (bcx*t + bx, bcy*t + by)  | t <- [-0.200,-0.195..1.200]]
              ++ [ (-bcy*t + ox, bcx*t + oy) | t <- [-0.800,-0.795..0.800]]
        !prop    = (c-a)/(2*c)
        (!vertexx,!vertexy) = (bcx*prop,bcy*prop)
    toFile def "number2.png" $ do
      plot (points "simmetries" (signal))
      plot $ liftEC $ do
        plot_points_title  .= "  O"
        plot_points_style  .= PointStyle (opaque red) (opaque red) 4 4 PointShapeCircle
        plot_points_values .= (ox,oy)
                             :[]
      plot $ liftEC $ do
        plot_points_title  .= "vertexes"
        plot_points_style  .= PointStyle (opaque magenta) (opaque magenta) 3 3 PointShapeCircle
        plot_points_values .= (vertexx+bx,vertexy+by)
                             :(cx-vertexx,cy-vertexy)
                             :[]
    return (a,b,c,angle)

number3 :: Points -> Point -> Double -> Double -> Double -> IO()
number3 ((ax, ay), (bx, by), (cx, cy)) (ox, oy) a b angle =
  do
    let !a2    = a^2
        !b2    = b^2
        !alpha' = acos $ (a2 - b2)/(a2 + b2)
        !alpha = if alpha' < pi/2
                 then alpha'
                 else pi - alpha'

        !k1    = b/a
        !k2    = -k1

        (!bcx,!bcy) = ((cx-bx),(cy-by))
    putStrLn ("The angle is "
              ++ show alpha ++ " radian.")
    let simm = [ (bcx*t + bx, bcy*t + by)  | t <- [-0.200,-0.195..1.200]]
            ++ [ (-bcy*t + ox, bcx*t + oy) | t <- [-0.800,-0.795..0.800]]

        !assAngle = atan k1
        ass = [ (t + ox, k1*t + oy) | t <- [-5,-4.99..5.00]]
           ++ [ (t + ox, k2*t + oy) | t <- [-5,-4.99..5.00]]
    let angAss = if (abs(alpha - alpha') > 0.001)
                 then [ (2*cos t + ox,2*sin t + oy) | t <- [assAngle,assAngle+0.01.. assAngle+alpha]]
                 else [ (2*cos t + ox,2*sin t + oy) | t <- [assAngle,assAngle-0.01.. assAngle-alpha']]
    let !c       = sqrt $ a2 + b2
        !prop    = (c-a)/(2*c)
        (!vertexx,!vertexy) = (bcx*prop,bcy*prop)
    toFile def "number3.png" $ do
      plot (points "simmetries"  (simm))
      plot (points "assymptotes" (ass))
      plot (points "ass. angle" (angAss))
      plot $ liftEC $ do
        plot_points_title  .= "  O"
        plot_points_style  .= PointStyle (opaque cyan) (opaque cyan) 4 4 PointShapeCircle
        plot_points_values .= (ox,oy)
                             :[]
      plot $ liftEC $ do
        plot_points_title  .= "vertexes"
        plot_points_style  .= PointStyle (opaque magenta) (opaque magenta) 3 3 PointShapeCircle
        plot_points_values .= (vertexx+bx,vertexy+by)
                             :(cx-vertexx,cy-vertexy)
                             :[]

number4 :: Points -> Point -> Double -> Double -> Double -> IO()
number4 ((ax, ay), (bx, by), (cx, cy)) (ox, oy) a b angle =
  do
    putStrLn ("The equation is x^2/("
              ++ show a ++ ")^2 -"
              ++ " y^2/(" ++ show b ++ ")^2"
              ++ " = 1.")
    let (!bcx,!bcy) = ((cx-bx),(cy-by))
        simm = [ (0, t) | t <- [-10.00,-9.99..10.00]]
            ++ [ (t, 0) | t <- [-10.00,-9.99..10.00]]
        func  = \x -> b*sqrt(x^2/a^2 - 1)
        hyper = [ (x, func x) | x <- [-10.000,-9.999..10.000]]
             ++ [ (x,-func x) | x <- [-10.000,-9.999..10.000]]
    toFile def "number4.png" $ do
      plot (points "simmetries"  (simm))
      plot (points "hyperbola"  (hyper))

number5 :: Points -> Point -> Double -> Double -> Double -> IO()
number5 ((ax, ay), (bx, by), (cx, cy)) (ox, oy) a b angle =
  do
    let !cosa = cos angle
        !sina = sin angle
    putStrLn ("The equation is ((x - ("
              ++ show ox ++ "))*("
              ++ show cosa
              ++ ") + (y - ("
              ++ show oy ++ "))*("
              ++ show sina
              ++ "))^2/(" ++ show a ++ ")^2 -"
              ++ " ((x - ("
              ++ show ox ++ "))*("
              ++ show sina
              ++ ") - (y - ("
              ++ show oy ++ "))*("
              ++ show cosa
              ++ "))^2/(" ++ show b ++ ")^2"
              ++ " = 1.")
    let (!bcx,!bcy) = ((cx-bx),(cy-by))
        simm = [ (bcx*t + bx, bcy*t + by)  | t <- [-0.200,-0.195..1.200]]
            ++ [ (-bcy*t + ox, bcx*t + oy) | t <- [-0.800,-0.795..0.800]]
        hyper = [ (x,y) | y <- [-20.000,-19.995..20.000],
                          x <- [-20.000,-19.995..20.000],
                          abs(((x-ox)*cosa + (y-oy)*sina)^2/(a^2)
                          - ((x-ox)*sina - (y-oy)*cosa)^2/(b^2) - 1) < 0.01]
    toFile def "number5.png" $ do
      plot (points "simmetries"  (simm))
      plot (points "hyperbola"  (hyper))
      plot $ liftEC $ do
        plot_points_title  .= "A, B, C"
        plot_points_style  .= PointStyle (opaque magenta) (opaque magenta) 3 3 PointShapeCircle
        plot_points_values .= (ax,ay)
                             :(bx,by)
                             :(cx,cy)
                             :[]

number6 :: Points -> IO(Point) -- return vertex
number6 ((ax, ay), (bx, by), (cx, cy)) =
  do
    let (!vecCAx,!vecCAy) = (ax-cx,ay-cy)

        !a   = ay - cy
        !b   = cx - ax
        !c   = ax*cy - cx*ay

        !k1  = b*bx - a*by
        !k2  = a^2 + b^2

        (!hx,!hy) = ((b*k1-a*c)/k2,(-a*k1-b*c)/k2)
        (!vx,!vy) = ((hx+bx)/2,(hy+by)/2)

    putStrLn ("The vertex of parabola has coordinates ("
              ++ show vx ++ ", " ++ show vy ++ ").")
    let signal = [ (vecCAx*t + cx, vecCAy*t + cy)  | t <- [-0.200,-0.195..1.200]]
    toFile def "number6.png" $ do
      plot (points "directrix"  (signal))
      plot $ liftEC $ do
        plot_points_title  .= "Vertex"
        plot_points_style  .= PointStyle (opaque red) (opaque red) 3 3 PointShapeCircle
        plot_points_values .= (vx,vy)
                             :[]
      plot $ liftEC $ do
        plot_points_title  .= "A, B, C"
        plot_points_style  .= PointStyle (opaque magenta) (opaque magenta) 3 3 PointShapeCircle
        plot_points_values .= (ax,ay)
                             :(bx,by)
                             :(cx,cy)
                             :[]
    return (vx,vy)

number7 :: Points -> Point -> IO(Double) -- return angle
number7 ((ax, ay), (bx, by), (cx, cy)) (vx,vy) =
  do
    let !angle = (by-vy)/(bx-vx)
    putStrLn ("The tangens of angle between ox and simmetry = "
              ++ show angle ++ ".")
    let (acx,acy) = (cx - ax, cy - ay)
        signal = [ (acx*t+ax,acy*t+ay) | t <- [-0.50,-0.49..1.50]]
    toFile def "number7.png" $ do
      plot (points "simmetry"  (signal))
      plot $ liftEC $ do
        plot_points_title  .= "A, B, C"
        plot_points_style  .= PointStyle (opaque magenta) (opaque magenta) 3 3 PointShapeCircle
        plot_points_values .= (ax,ay)
                             :(bx,by)
                             :(cx,cy)
                             :[]
    return angle

number8 :: Points -> Point -> IO(Double) -- return p
number8 ((ax, ay), (bx, by), (cx, cy)) (vx, vy) =
  do
    let !p = 2* sqrt((bx-vx)^2 + (by-vy)^2)
    putStrLn ("The equation is y^2 = ("
              ++ show (2*p) ++ ")*x.")
    let func  = \x -> sqrt $ 2*p*x
        signal = [ (x, func x) | x <- [-10.000,-9.999..10.000]]
              ++ [ (x,-func x) | x <- [-10.000,-9.999..10.000]]
    toFile def "number8.png" $ do
      plot (points "parabola"  (signal))
    return p

number910 :: Points
        -> Point
        -> Double
        -> Double
        -> IO((Double->Double,Double->Double)) -- return y(x)
number910 ((ax, ay), (bx, by), (cx, cy)) (vx, vy) p tgD =
  do
    let !alpha = atan tgD - pi
        !sina = sin(alpha)
        !cosa = cos(alpha)
    -- chack, make simple; add vx, vy
    putStrLn ("The equation is (x*("
              ++ show (sina) ++ ") + y*("
              ++ show (cosa) ++ ") - ("
              ++ show vy     ++ "))^2 = "
              ++ show (2*p)     ++"*(x*("
              ++ show (cosa) ++ ") - y*("
              ++ show (sina) ++ ") - ("
              ++ show vx     ++ ")).")
    let !k1   = p^2*sina^2
        !k2   = 2*p*cosa
        !sqD  = \x -> sqrt $ k1 + k2*x
        !k3   =  sina*cosa
        !k4   =  p*sina
        !notB = \x -> x*k3 + k4
        !k5   =  cosa^2
        !y1   = \x -> ( notB x - sqD x)/k5
        !y2   = \x -> ( notB x + sqD x)/k5
        signal = [ (x+vx,y1 x + vy) | x <- [-20.000,-19.999..20.000]]
              ++ [ (x+vx,y2 x + vy) | x <- [-20.000,-19.999..20.000]]

    -- num10
    let !c1     = y1 (-vx) + vy
        !c2     = y2 (-vx) + vy
        !length = c1 - c2
        chorde  = [ (0,c1-length*t) | t <- [0.000,0.001..1.000]]
    putStrLn $ "The length of chord is " ++ show (abs length) ++ "."
    -- num10
    toFile (FileOptions (350,1000) PNG) "number9.png" $ do
      plot (points "parabola"  (signal))
      plot (points "chorde"    (chorde))
      plot $ liftEC $ do
        plot_points_title  .= "Vertex"
        plot_points_style  .= PointStyle (opaque cyan) (opaque cyan) 5 5 PointShapeCircle
        plot_points_values .= (vx,vy)
                             :[]
      plot $ liftEC $ do
        plot_points_title  .= "A, B, C"
        plot_points_style  .= PointStyle (opaque magenta) (opaque magenta) 3 3 PointShapeCircle
        plot_points_values .= (ax,ay)
                             :(bx,by)
                             :(cx,cy)
                             :[]
      plot $ liftEC $ do
        plot_points_title  .= "Chorde points"
        plot_points_style  .= PointStyle (opaque red) (opaque red) 3 3 PointShapeCircle
        plot_points_values .= (0,c1)
                             :(0,c2)
                             :[]
    return (y1,y2)

main :: IO ()
main = do
  putStrLn "Put your points A, B, C for numbers 1-5."
  putStrLn "In format like:"
  putStrLn "A( -4 ,2), B ( -1,5) ; C(6,4)"
  ---
  input <- readDots
  ---
  o             <- number1 input
  (a,b,c,angle) <- number2 input o
  ---
  number3 input o a b angle
  number4 input o a b angle
  number5 input o a b angle
  ---
  putStrLn ""
  ---
  v   <- number6 input
  tgD <- number7 input v
  ---
  p     <- number8 input v
  funcs <- number910 input v p tgD
  putStrLn ""
