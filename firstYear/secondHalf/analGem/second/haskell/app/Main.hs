{-# LANGUAGE QuasiQuotes, FlexibleContexts #-}
import Prelude
import Data.Maybe (fromJust)

import Graphics.Rendering.Chart.Easy (def,plot,points)
import Graphics.Rendering.Chart.Backend.Cairo (toFile)

import Numeric.Tools.Differentiation
import Numeric.Tools.Integration

import Text.Regex.PCRE.Heavy (gsub,re)

type Input = String
type Coefficient = Double
type Square = Double
type Points = [(Double,Double)]
type Equation = Coefficient -> Double

extract :: Input -> Coefficient
extract input =
  read $
  gsub [re|.+=(.+)|] (\(a:b:_) -> b ++ "") input

square2 :: Equation -> Square
square2 r =
  (fromJust . quadRes) (quadRomberg defQuad (0,pi) (\t -> (r t**2)/2))
  + (fromJust . quadRes)(quadRomberg defQuad (pi,0) (\t -> (r t**2)/(-2)))

square4 :: Double -> Double -> Equation -> Square
square4 l r y =
  2*(fromJust . quadRes)(quadRomberg defQuad (l+0.001,r-0.001) (\x -> y x))

crossOX :: Coefficient -> Coefficient -> Points
crossOX a b = [ (10 * cos (a*t),0) | t <- [-10.000010,-10.000009..10.000010]
            , (abs(sin (b*t)) < 0.000001)]

number5 :: Coefficient -> Coefficient -> IO()
number5 b a =
  do
    putStrLn $ "For number 5 (equation '" ++ show b ++ "^2*x^2 - " ++ show a ++ "^2*y^2 = 100'"
    let y = \x -> sqrt $ ((b**2)*(x**2)-100)/(a**2)
    let signal = [ (x,y(x))  | x <- [-10.00,-9.9995..10.00]]
              ++ [ (x,-y(x)) | x <- [-10.00,-9.9995..10.00]]
    toFile def "number5.png" $ plot (points "y = f(x)" (signal))
    putStrLn "Your plot in file 'number5.png'"
    putStrLn $ "k of Asyptotes is " ++ show (b/a) ++ " and " ++ show (-b/a)

number4 :: Coefficient -> Coefficient -> IO()
number4 b a =
  do
    putStrLn $ "For number 4 (equation '" ++ show b ++ "^2*x^2 + " ++ show a ++ "^2*y^2 = 100'"
    let y = \x -> sqrt $ (100-(b**2)*(x**2))/(a**2)
    let signal = [ (x,y(x))  | x <- [-10.00,-9.9995..10.00]]
              ++ [ (x,-y(x)) | x <- [-10.00,-9.9995..10.00]]
    toFile def "number4.png" $ plot (points "y = f(x)" (signal))
    putStrLn "Your plot in file 'number4.png'"
    putStrLn $ "S = "
      ++ show (square4 (-10/b) (10/b) y)

number3 :: Coefficient -> Coefficient -> IO()
number3 a b =
  do
    putStrLn $ "For number 3 (equations 'x = 10 * cos (" ++ show a ++ "*t)' and 'y = 7 * sin(" ++ show b ++ "*t)')"
    let x = \t -> 10 * cos (a*t)
    let y = \t -> 7 * sin (b*t)
    let signal = [ (x t,y t) | t <- [0,0.005..16*pi]]
    toFile def "number3.png" $ plot (points "y = f(x)" (signal))
    putStrLn "Your plot in file 'number3.png'"
    putStrLn $ "cross OX  in " ++ show (crossOX a b)

number2 :: Coefficient -> Coefficient -> IO()
number2 a b =
  do
    putStrLn $ "For number 2 (equation '" ++ show a ++ " + " ++ show b ++ "cos(3*t)'"
    let f = \t -> a + b*cos(3*t)
    let signal = [ (f t * cos t,f t * sin t) | t <- [0.000,0.0005..2*pi]]
    toFile def "number2.png" $ plot (points "y = f(x)" (signal))
    putStrLn "Your plot in file 'number2.png'"
    putStrLn $ "S = " ++ show (square2 f)

number1 :: Coefficient -> IO()
number1 a =
  do
    putStrLn $ "For number 1 (equation 'x^2*y - 4*(" ++ show a ++ "^2)*(2*" ++ show a ++ "-y))'"
    let f = \x -> 8*(a**3)/(x**2 + 4*(a**2))
    let signal = [ (x,f x) | x <- [-140.00,-139.98..180]]
    toFile def "number1.png" $ plot (points "y = f(x)" (signal))
    putStrLn "Your plot in file 'number1.png'"
    putStrLn $ "y''(x) = (" ++ show (2*a/(sqrt 3)) ++ "," ++ show (3*a/2) ++ "), ("
      ++ show (-2*a/(sqrt 3)) ++ "," ++ show (3*a/2) ++ ")"

main :: IO ()
main = do
  putStrLn "Put yout 'a' coefficient for number 1"
  input <- getLine
  let firstCo = extract input

  putStrLn ""
  number1 firstCo
  putStrLn ""

  putStrLn "Put yout 'a' coefficient for numbers 2-5"
  input <- getLine
  let firstCo = extract input
  putStrLn "Put yout 'b' coefficient for numbers 2-5"
  input <- getLine
  let secondCo = extract input

  putStrLn ""
  number2 firstCo secondCo
  putStrLn ""
  number3 firstCo secondCo
  putStrLn ""
  number4 firstCo secondCo
  putStrLn ""
  number5 firstCo secondCo
