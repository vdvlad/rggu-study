{-# LANGUAGE QuasiQuotes, FlexibleContexts #-}

module Main where

-- import Prelude as P
import Prelude (IO,Int,Float,String,($),(++),(.),(^),(/),(*),(+),(-))
import Prelude (fst,map,take,last,head,abs,sqrt,signum,atan)
import Prelude (fromEnum,fromIntegral,putStr,putStrLn,show)

import Text.Regex.PCRE.Heavy (gsub,re)

import Data.ByteString (ByteString)
import Data.ByteString (empty,split,getLine)
import Data.ByteString.Char8 (readInt)
import Data.Maybe (fromMaybe)

type Input = ByteString
type Output = String
type Equation = [Float]
type Answer = Float
type Dots = [[Float]]
type Dot = [Float]
type Vector = [Float]

second :: [a] -> a
second a = last $ take 2 a

makeEquation :: Input -> Equation
makeEquation input =
  map (fromIntegral . fst . fromMaybe (0::Int,empty) . readInt) $
  split (fromIntegral $ fromEnum ' ') $
  gsub [re|(-?\d+) *\*? *x *((-)|\+) *(\d+) *\*? *?y *((-)|\+) *(\d+)|] (\(a:_:m2:b:_:m3:c:_) -> a ++ " " ++ m2 ++ b ++ " " ++ m3 ++ c) input

splitDots :: [Float] -> Dots
splitDots (ax:ay:bx:by:cx:cy:_) =  (ax : ay : []) : (bx : by : []) : (cx : cy : []) : []

makeDots :: Input -> Dots
makeDots input =
  splitDots $
  map (fromIntegral . fst . fromMaybe (0::Int,empty) . readInt) $
  split (fromIntegral $ fromEnum ' ') $
  gsub [re|A\((-?\d+) *[,;] *(-?\d+)\) *[;,]? *B\((-?\d+) *[,;] *(-?\d+)\) *[;,]? *C\((-?\d+) *[,;] *(-?\d+)\)|] (\(ax:ay:bx:by:cx:cy:_) -> ax ++ " " ++ ay ++ " " ++ bx ++ " " ++ by ++ " " ++ cx ++ " " ++ cy) input


sqrtOfSquare :: Float -> Float -> Float
sqrtOfSquare a b = sqrt $ a^2 + b^2

distance :: Equation -> Answer
distance equation =
  abs $
  (last equation) /
  (sqrtOfSquare (head equation) (second equation))

number1 :: Equation -> (Equation,Answer)
number1 equation =
  let sqr = sqrtOfSquare (head equation) (second equation)
  in
    (
        map (\x -> (x / sqr) * (-(signum $ last equation))) equation
    ,
        distance equation
    )

number1Show :: (Equation,Answer) -> Output
number1Show ([a,b,c],x) =
  "(" ++ show a ++ ")x + (" ++ show b ++ ")y + (" ++ show c
  ++ ") ; distanse = " ++ show x

makeLastNeg :: Equation -> Equation
makeLastNeg equation = (take 2 equation) ++ [-last equation]

reverseMap :: Equation -> Equation
reverseMap equation = map (\x -> 1/x) equation

number2 :: Equation -> (Equation,Answer)
number2 equation =
  (
    (reverseMap $ makeLastNeg $
    (map (/ (-last equation)) equation))
  ,
    ((last equation * (-1)) / (head equation))
  )

number2Show :: (Equation,Answer) -> Output
number2Show ([a,b,c],x) =
  "x/(" ++ show a ++ ") + y/(" ++ show b ++ ") = " ++ show c
  ++ " ; x = " ++ show x

makeYNeg :: Equation -> Equation
makeYNeg (a:b:c) = (a:(-b):c)

number3 :: Equation -> (Equation,Answer)
number3 equation =
  let newEq = makeYNeg $ map (/ (-1 * second equation)) equation
  in
    (
        newEq
    ,
        (1 / head newEq)
    )

number3Show :: (Equation,Answer) -> Output
number3Show ([a,b,c],x) =
  "y = " ++ show a ++ "x + (" ++ show c
  ++ ") ; tan with OY = " ++ show x

number4 :: Equation -> Answer
number4 equation =
  abs $
  ((last equation * (-1)) / head equation)
  *
  ((last equation * (-1)) / second equation)
  / 2

newCoors :: Equation -> Float -> Float -> Equation
newCoors (x:y:c:_) a b = x : y : (c + x*a + y*b) : []

number5 :: Equation -> Answer
number5 equation =
  distance $ newCoors equation (-5) 3

newCoorsDots :: Dots -> Float -> Float -> Dots
newCoorsDots ((ax:ay:_):(bx:by:_):(cx:cy:_):_) a b =
  (ax-a : ay-b : []) : (bx-a : by-b : []) : (cx-a : cy-b : []) : []

scalar :: Vector -> Vector -> Float
scalar (ax:ay:_) (bx:by:_) =
  ax * bx + ay * by

length :: Vector -> Float
length vector = sqrt $ (head vector)^2 + (second vector)^2

cos2tan :: Float -> Float
cos2tan cos = (sqrt $ 1 / (cos^2) - 1) * (signum cos)

dots2vec :: Dot -> Dot -> Vector
dots2vec a b =
  head   a - head   b :
  second a - second b : []

number6 :: Dots -> Answer
number6 dots =
  let vecBA = dots2vec (head dots) (second dots)
      vecBC = dots2vec (last dots) (second dots)
  in
  cos2tan $
    (scalar vecBA vecBC)
    /
    (length vecBA * length vecBC)

number6Show :: Answer -> Output
number6Show b =
  "∠B = " ++ show (atan b) ++ " rad; tan ∠B = " ++ show b

average :: Float -> Float -> Float
average x1 x2 = (x1 + x2) / 2

number7 :: Dots -> Answer
number7 dots =
  let vecCM =
        dots2vec ( average (head $ head $ dots) (head $ second $ dots)
                 :
                   average (second $ head $ dots) (second $ second $ dots) : []
                 )
        (last $ dots)
  in
    length vecCM

number8 :: Dots -> Answer
number8 dots =
  (square dots)
  /
  (length $ dots2vec (head dots) (second dots))
  *2

square :: Dots -> Answer
square dots =
  let ndots = newCoorsDots dots (head $ last  dots) (second $ last dots)
  in
    abs $
    (head (head ndots) *
    second (second ndots) -
    second (head ndots) *
    head (second ndots))
    / 2

number9 :: Dots -> Answer
number9 dots = square dots

number10 :: Dots -> Answer
number10 dots =
  let vecBA = dots2vec (head dots) (second dots)
      vecBC = dots2vec (last dots) (second dots)
      vecAC = dots2vec (last dots) (head dots)
  in
    (length vecBA * length vecBC * length vecAC)
    /
    (square dots * 4)

main :: IO ()
main = do
  putStrLn "┌─────────────────────────────────────────────────────────────┐"
  putStrLn "│                       Print equation                        │"
  putStrLn "│Such syntaxes or thair combinates supported:                 │"
  putStrLn "│4x + 3y - 15                                                 │"
  putStrLn "│4*x + 3  * y -15                                             │"
  putStrLn "└─────────────────────────────────────────────────────────────┘"
  input <- getLine
  let equation = makeEquation input
  putStrLn "┌─────────────────────────────────────────────────────────────┐"
  putStrLn "│                           Answers                           │"
  putStrLn "└─────────────────────────────────────────────────────────────┘"
  putStr   "  number1 : "
  putStrLn $ number1Show $ number1 equation
  putStrLn "  ───────────────────────────────────────────────────────────  "
  putStr   "  number2 : "
  putStrLn $ number2Show $ number2 equation
  putStrLn "  ───────────────────────────────────────────────────────────  "
  putStr   "  number3 : "
  putStrLn $ number3Show $ number3 equation
  putStrLn "  ───────────────────────────────────────────────────────────  "
  putStr   "  number4 : "
  putStrLn $ "S = " ++ (show $ number4 equation)
  putStrLn "  ───────────────────────────────────────────────────────────  "
  putStr   "  number5 : "
  putStrLn $ "Distance = " ++ (show $ number5 equation)
  putStrLn "  ───────────────────────────────────────────────────────────  "
  putStrLn "┌─────────────────────────────────────────────────────────────┐"
  putStrLn "│                    Print dots                               │"
  putStrLn "│Such syntaxes or thair combinates supported:                 │"
  putStrLn "│A(2,-50) ;  B(-20,3)  ;  C(1  , 6)                           │"
  putStrLn "│A(2;-50)  B(-20;3) ,C(1;6)                                   │"
  putStrLn "└─────────────────────────────────────────────────────────────┘"
  input <- getLine
  let dots = makeDots input
  putStrLn "┌─────────────────────────────────────────────────────────────┐"
  putStrLn "│                           Answers                           │"
  putStrLn "└─────────────────────────────────────────────────────────────┘"
  putStr   "  number6 : "
  putStrLn $ number6Show $ number6 dots
  putStrLn "  ───────────────────────────────────────────────────────────  "
  putStr   "  number7 : "
  putStrLn $ "CM = " ++ (show $ number7 dots)
  putStrLn "  ───────────────────────────────────────────────────────────  "
  putStr   "  number8 : "
  putStrLn $ "CH = " ++ (show $ number8 dots)
  putStrLn "  ───────────────────────────────────────────────────────────  "
  putStr   "  number9 : "
  putStrLn $ "S = " ++ (show $ number9 dots)
  putStrLn "  ───────────────────────────────────────────────────────────  "
  putStr   "  number10: "
  putStrLn $ "R = " ++ (show $ number10 dots)
  putStrLn "  ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━  "

-- Equation --- universal type of data which bound with essense of equations
