; ModuleID = 'a.c'
source_filename = "a.c"
target datalayout = "e-m:e-p270:32:32-p271:32:32-p272:64:64-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"

%struct._IO_FILE = type { i32, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, %struct._IO_marker*, %struct._IO_FILE*, i32, i32, i64, i16, i8, [1 x i8], i8*, i64, %struct._IO_codecvt*, %struct._IO_wide_data*, %struct._IO_FILE*, i8*, i64, i32, [20 x i8] }
%struct._IO_marker = type opaque
%struct._IO_codecvt = type opaque
%struct._IO_wide_data = type opaque
%struct.real_pcre8_or_16 = type opaque
%struct.pcre_extra = type { i64, i8*, i64, i8*, i8*, i64, i8**, i8* }

@stdin = external local_unnamed_addr global %struct._IO_FILE*, align 8
@.str = private unnamed_addr constant [41 x i8] c"Iterations give %f after %d iterations.\0A\00", align 1
@.str.2 = private unnamed_addr constant [40 x i8] c"Dichotomy give %f after %d iterations.\0A\00", align 1
@.str.3 = private unnamed_addr constant [37 x i8] c"Newton give %f after %d iterations.\0A\00", align 1
@__const.main.reAcc = private unnamed_addr constant [14 x i8] c"^-?\\d+\\.?\\d*$\00", align 1
@pcre_free = external local_unnamed_addr global void (i8*)*, align 8
@str = private unnamed_addr constant [35 x i8] c"In this interval dichotomy is bad.\00", align 1
@str.12 = private unnamed_addr constant [43 x i8] c"We have equation like \22f(x) = x - cos(x)\22.\00", align 1
@str.13 = private unnamed_addr constant [39 x i8] c"We can find one root use the interval.\00", align 1
@str.14 = private unnamed_addr constant [36 x i8] c"Please, put left border (rational).\00", align 1
@str.15 = private unnamed_addr constant [37 x i8] c"Please, put right border (rational).\00", align 1
@str.16 = private unnamed_addr constant [24 x i8] c"Please, print accuracy.\00", align 1
@str.17 = private unnamed_addr constant [41 x i8] c"You are wrong. Try again input accuracy.\00", align 1
@str.18 = private unnamed_addr constant [45 x i8] c"You are wrong. Try again input right border.\00", align 1
@str.19 = private unnamed_addr constant [44 x i8] c"You are wrong. Try again input left border.\00", align 1

; Function Attrs: nounwind sspstrong uwtable
define dso_local i8* @getLine() local_unnamed_addr #0 {
  %1 = tail call noalias dereferenceable_or_null(100) i8* @malloc(i64 100) #11
  br label %2

2:                                                ; preds = %19, %0
  %3 = phi i8* [ %1, %0 ], [ %20, %19 ]
  %4 = phi i64 [ 100, %0 ], [ %21, %19 ]
  %5 = phi i64 [ 100, %0 ], [ %22, %19 ]
  %6 = phi i8* [ %1, %0 ], [ %25, %19 ]
  %7 = load %struct._IO_FILE*, %struct._IO_FILE** @stdin, align 8, !tbaa !4
  %8 = tail call i32 @fgetc(%struct._IO_FILE* %7)
  switch i32 %8, label %9 [
    i32 -1, label %26
    i32 10, label %26
  ]

9:                                                ; preds = %2
  %10 = add i64 %5, -1
  %11 = icmp eq i64 %10, 0
  br i1 %11, label %12, label %19

12:                                               ; preds = %9
  %13 = shl i64 %4, 1
  %14 = tail call i8* @realloc(i8* %3, i64 %13) #11
  %15 = ptrtoint i8* %6 to i64
  %16 = ptrtoint i8* %3 to i64
  %17 = sub i64 %15, %16
  %18 = getelementptr inbounds i8, i8* %14, i64 %17
  br label %19

19:                                               ; preds = %12, %9
  %20 = phi i8* [ %14, %12 ], [ %3, %9 ]
  %21 = phi i64 [ %13, %12 ], [ %4, %9 ]
  %22 = phi i64 [ %4, %12 ], [ %10, %9 ]
  %23 = phi i8* [ %18, %12 ], [ %6, %9 ]
  %24 = trunc i32 %8 to i8
  %25 = getelementptr inbounds i8, i8* %23, i64 1
  store i8 %24, i8* %23, align 1, !tbaa !8
  br label %2

26:                                               ; preds = %2, %2
  store i8 0, i8* %6, align 1, !tbaa !8
  ret i8* %3
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.start.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nofree nounwind
declare noalias i8* @malloc(i64) local_unnamed_addr #2

; Function Attrs: nofree nounwind
declare i32 @fgetc(%struct._IO_FILE* nocapture) local_unnamed_addr #2

; Function Attrs: nounwind
declare noalias i8* @realloc(i8* nocapture, i64) local_unnamed_addr #3

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.lifetime.end.p0i8(i64 immarg, i8* nocapture) #1

; Function Attrs: nounwind readnone sspstrong uwtable
define dso_local double @fx(double %0) local_unnamed_addr #4 {
  %2 = tail call fast double @llvm.cos.f64(double %0)
  %3 = fsub fast double %0, %2
  ret double %3
}

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.cos.f64(double) #5

; Function Attrs: nounwind readnone sspstrong uwtable
define dso_local double @fD(double %0) local_unnamed_addr #4 {
  %2 = tail call fast double @llvm.sin.f64(double %0)
  %3 = fadd fast double %2, 1.000000e+00
  ret double %3
}

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.sin.f64(double) #5

; Function Attrs: norecurse nounwind readnone sspstrong uwtable
define dso_local double @absD(double %0) local_unnamed_addr #6 {
  %2 = tail call fast double @llvm.fabs.f64(double %0)
  ret double %2
}

; Function Attrs: nofree nounwind sspstrong uwtable
define dso_local void @iteration(double %0, double %1, double %2) local_unnamed_addr #7 {
  %4 = fadd fast double %1, %0
  %5 = fmul fast double %4, 5.000000e-01
  %6 = tail call fast double @llvm.cos.f64(double %5) #11
  %7 = fsub fast double %5, %6
  %8 = tail call fast double @llvm.fabs.f64(double %7) #11
  %9 = fcmp fast ogt double %8, %2
  br i1 %9, label %10, label %19

10:                                               ; preds = %3, %10
  %11 = phi i32 [ %14, %10 ], [ 0, %3 ]
  %12 = phi double [ %13, %10 ], [ %5, %3 ]
  %13 = tail call fast double @llvm.cos.f64(double %12)
  %14 = add nuw nsw i32 %11, 1
  %15 = tail call fast double @llvm.cos.f64(double %13) #11
  %16 = fsub fast double %13, %15
  %17 = tail call fast double @llvm.fabs.f64(double %16) #11
  %18 = fcmp fast ogt double %17, %2
  br i1 %18, label %10, label %19

19:                                               ; preds = %10, %3
  %20 = phi double [ %5, %3 ], [ %13, %10 ]
  %21 = phi i32 [ 0, %3 ], [ %14, %10 ]
  %22 = tail call i32 (i8*, ...) @printf(i8* nonnull dereferenceable(1) getelementptr inbounds ([41 x i8], [41 x i8]* @.str, i64 0, i64 0), double %20, i32 %21)
  ret void
}

; Function Attrs: nofree nounwind
declare i32 @printf(i8* nocapture readonly, ...) local_unnamed_addr #2

; Function Attrs: norecurse nounwind readnone sspstrong uwtable
define dso_local i32 @eqSign(double %0, double %1) local_unnamed_addr #6 {
  %3 = fmul fast double %1, %0
  %4 = fcmp fast oge double %3, 0.000000e+00
  %5 = zext i1 %4 to i32
  ret i32 %5
}

; Function Attrs: nofree nounwind sspstrong uwtable
define dso_local void @dichotomy(double %0, double %1, double %2) local_unnamed_addr #7 {
  %4 = tail call fast double @llvm.cos.f64(double %0) #11
  %5 = fsub fast double %0, %4
  %6 = tail call fast double @llvm.cos.f64(double %1) #11
  %7 = fsub fast double %1, %6
  %8 = fmul fast double %7, %5
  %9 = fcmp fast ult double %8, 0.000000e+00
  br i1 %9, label %10, label %13

10:                                               ; preds = %3
  %11 = tail call fast double @llvm.fabs.f64(double %5) #11
  %12 = fcmp fast ogt double %11, %2
  br i1 %12, label %15, label %34

13:                                               ; preds = %3
  %14 = tail call i32 @puts(i8* nonnull dereferenceable(1) getelementptr inbounds ([35 x i8], [35 x i8]* @str, i64 0, i64 0))
  br label %38

15:                                               ; preds = %10, %15
  %16 = phi double [ %28, %15 ], [ %0, %10 ]
  %17 = phi i32 [ %29, %15 ], [ 0, %10 ]
  %18 = phi double [ %27, %15 ], [ %1, %10 ]
  %19 = fadd fast double %16, %18
  %20 = fmul fast double %19, 5.000000e-01
  %21 = tail call fast double @llvm.cos.f64(double %20) #11
  %22 = fsub fast double %20, %21
  %23 = tail call fast double @llvm.cos.f64(double %18) #11
  %24 = fsub fast double %18, %23
  %25 = fmul fast double %22, %24
  %26 = fcmp fast ult double %25, 0.000000e+00
  %27 = select i1 %26, double %18, double %20
  %28 = select i1 %26, double %20, double %16
  %29 = add nuw nsw i32 %17, 1
  %30 = tail call fast double @llvm.cos.f64(double %28) #11
  %31 = fsub fast double %28, %30
  %32 = tail call fast double @llvm.fabs.f64(double %31) #11
  %33 = fcmp fast ogt double %32, %2
  br i1 %33, label %15, label %34

34:                                               ; preds = %15, %10
  %35 = phi i32 [ 0, %10 ], [ %29, %15 ]
  %36 = phi double [ %0, %10 ], [ %28, %15 ]
  %37 = tail call i32 (i8*, ...) @printf(i8* nonnull dereferenceable(1) getelementptr inbounds ([40 x i8], [40 x i8]* @.str.2, i64 0, i64 0), double %36, i32 %35)
  br label %38

38:                                               ; preds = %34, %13
  ret void
}

; Function Attrs: nofree nounwind sspstrong uwtable
define dso_local void @newton(double %0, double %1, double %2) local_unnamed_addr #7 {
  %4 = fadd fast double %1, %0
  %5 = fmul fast double %4, 5.000000e-01
  %6 = tail call fast double @llvm.cos.f64(double %5) #11
  %7 = fsub fast double %5, %6
  %8 = tail call fast double @llvm.fabs.f64(double %7) #11
  %9 = fcmp fast ogt double %8, %2
  br i1 %9, label %10, label %23

10:                                               ; preds = %3, %10
  %11 = phi double [ %20, %10 ], [ %7, %3 ]
  %12 = phi i32 [ %18, %10 ], [ 0, %3 ]
  %13 = phi double [ %17, %10 ], [ %5, %3 ]
  %14 = tail call fast double @llvm.sin.f64(double %13) #11
  %15 = fadd fast double %14, 1.000000e+00
  %16 = fdiv fast double %11, %15
  %17 = fsub fast double %13, %16
  %18 = add nuw nsw i32 %12, 1
  %19 = tail call fast double @llvm.cos.f64(double %17) #11
  %20 = fsub fast double %17, %19
  %21 = tail call fast double @llvm.fabs.f64(double %20) #11
  %22 = fcmp fast ogt double %21, %2
  br i1 %22, label %10, label %23

23:                                               ; preds = %10, %3
  %24 = phi double [ %5, %3 ], [ %17, %10 ]
  %25 = phi i32 [ 0, %3 ], [ %18, %10 ]
  %26 = tail call i32 (i8*, ...) @printf(i8* nonnull dereferenceable(1) getelementptr inbounds ([37 x i8], [37 x i8]* @.str.3, i64 0, i64 0), double %24, i32 %25)
  ret void
}

; Function Attrs: nounwind sspstrong uwtable
define dso_local i32 @main() local_unnamed_addr #0 {
  %1 = alloca [14 x i8], align 1
  %2 = alloca [14 x i8], align 1
  %3 = alloca i8*, align 8
  %4 = alloca i32, align 4
  %5 = tail call i32 @puts(i8* nonnull dereferenceable(1) getelementptr inbounds ([43 x i8], [43 x i8]* @str.12, i64 0, i64 0))
  %6 = tail call i32 @puts(i8* nonnull dereferenceable(1) getelementptr inbounds ([39 x i8], [39 x i8]* @str.13, i64 0, i64 0))
  %7 = getelementptr inbounds [14 x i8], [14 x i8]* %1, i64 0, i64 0
  call void @llvm.lifetime.start.p0i8(i64 14, i8* nonnull %7) #11
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* nonnull align 1 dereferenceable(14) %7, i8* nonnull align 1 dereferenceable(14) getelementptr inbounds ([14 x i8], [14 x i8]* @__const.main.reAcc, i64 0, i64 0), i64 14, i1 false)
  %8 = getelementptr inbounds [14 x i8], [14 x i8]* %2, i64 0, i64 0
  call void @llvm.lifetime.start.p0i8(i64 14, i8* nonnull %8) #11
  call void @llvm.memcpy.p0i8.p0i8.i64(i8* nonnull align 1 dereferenceable(14) %8, i8* nonnull align 1 dereferenceable(14) getelementptr inbounds ([14 x i8], [14 x i8]* @__const.main.reAcc, i64 0, i64 0), i64 14, i1 false)
  %9 = bitcast i8** %3 to i8*
  call void @llvm.lifetime.start.p0i8(i64 8, i8* nonnull %9) #11
  %10 = bitcast i32* %4 to i8*
  call void @llvm.lifetime.start.p0i8(i64 4, i8* nonnull %10) #11
  %11 = call %struct.real_pcre8_or_16* @pcre_compile(i8* nonnull %7, i32 0, i8** nonnull %3, i32* nonnull %4, i8* null) #11
  %12 = call %struct.real_pcre8_or_16* @pcre_compile(i8* nonnull %8, i32 0, i8** nonnull %3, i32* nonnull %4, i8* null) #11
  call void @llvm.lifetime.end.p0i8(i64 4, i8* nonnull %10) #11
  call void @llvm.lifetime.end.p0i8(i64 8, i8* nonnull %9) #11
  %13 = call i32 @puts(i8* nonnull dereferenceable(1) getelementptr inbounds ([36 x i8], [36 x i8]* @str.14, i64 0, i64 0))
  %14 = call noalias dereferenceable_or_null(100) i8* @malloc(i64 100) #11
  br label %15

15:                                               ; preds = %32, %0
  %16 = phi i8* [ %14, %0 ], [ %33, %32 ]
  %17 = phi i64 [ 100, %0 ], [ %34, %32 ]
  %18 = phi i64 [ 100, %0 ], [ %35, %32 ]
  %19 = phi i8* [ %14, %0 ], [ %38, %32 ]
  %20 = load %struct._IO_FILE*, %struct._IO_FILE** @stdin, align 8, !tbaa !4
  %21 = call i32 @fgetc(%struct._IO_FILE* %20) #11
  switch i32 %21, label %22 [
    i32 -1, label %39
    i32 10, label %39
  ]

22:                                               ; preds = %15
  %23 = add i64 %18, -1
  %24 = icmp eq i64 %23, 0
  br i1 %24, label %25, label %32

25:                                               ; preds = %22
  %26 = shl i64 %17, 1
  %27 = call i8* @realloc(i8* %16, i64 %26) #11
  %28 = ptrtoint i8* %19 to i64
  %29 = ptrtoint i8* %16 to i64
  %30 = sub i64 %28, %29
  %31 = getelementptr inbounds i8, i8* %27, i64 %30
  br label %32

32:                                               ; preds = %25, %22
  %33 = phi i8* [ %27, %25 ], [ %16, %22 ]
  %34 = phi i64 [ %26, %25 ], [ %17, %22 ]
  %35 = phi i64 [ %17, %25 ], [ %23, %22 ]
  %36 = phi i8* [ %31, %25 ], [ %19, %22 ]
  %37 = trunc i32 %21 to i8
  %38 = getelementptr inbounds i8, i8* %36, i64 1
  store i8 %37, i8* %36, align 1, !tbaa !8
  br label %15

39:                                               ; preds = %15, %15
  store i8 0, i8* %19, align 1, !tbaa !8
  %40 = call i64 @strlen(i8* nonnull dereferenceable(1) %16) #12
  %41 = trunc i64 %40 to i32
  %42 = call i32 @pcre_exec(%struct.real_pcre8_or_16* %11, %struct.pcre_extra* null, i8* %16, i32 %41, i32 0, i32 0, i32* null, i32 0) #11
  %43 = icmp slt i32 %42, 0
  br i1 %43, label %44, label %76

44:                                               ; preds = %39, %71
  %45 = call i32 @puts(i8* nonnull dereferenceable(1) getelementptr inbounds ([44 x i8], [44 x i8]* @str.19, i64 0, i64 0))
  %46 = call noalias dereferenceable_or_null(100) i8* @malloc(i64 100) #11
  br label %47

47:                                               ; preds = %64, %44
  %48 = phi i8* [ %46, %44 ], [ %65, %64 ]
  %49 = phi i64 [ 100, %44 ], [ %66, %64 ]
  %50 = phi i64 [ 100, %44 ], [ %67, %64 ]
  %51 = phi i8* [ %46, %44 ], [ %70, %64 ]
  %52 = load %struct._IO_FILE*, %struct._IO_FILE** @stdin, align 8, !tbaa !4
  %53 = call i32 @fgetc(%struct._IO_FILE* %52) #11
  switch i32 %53, label %54 [
    i32 -1, label %71
    i32 10, label %71
  ]

54:                                               ; preds = %47
  %55 = add i64 %50, -1
  %56 = icmp eq i64 %55, 0
  br i1 %56, label %57, label %64

57:                                               ; preds = %54
  %58 = shl i64 %49, 1
  %59 = call i8* @realloc(i8* %48, i64 %58) #11
  %60 = ptrtoint i8* %51 to i64
  %61 = ptrtoint i8* %48 to i64
  %62 = sub i64 %60, %61
  %63 = getelementptr inbounds i8, i8* %59, i64 %62
  br label %64

64:                                               ; preds = %57, %54
  %65 = phi i8* [ %59, %57 ], [ %48, %54 ]
  %66 = phi i64 [ %58, %57 ], [ %49, %54 ]
  %67 = phi i64 [ %49, %57 ], [ %55, %54 ]
  %68 = phi i8* [ %63, %57 ], [ %51, %54 ]
  %69 = trunc i32 %53 to i8
  %70 = getelementptr inbounds i8, i8* %68, i64 1
  store i8 %69, i8* %68, align 1, !tbaa !8
  br label %47

71:                                               ; preds = %47, %47
  store i8 0, i8* %51, align 1, !tbaa !8
  %72 = call i64 @strlen(i8* nonnull dereferenceable(1) %48) #12
  %73 = trunc i64 %72 to i32
  %74 = call i32 @pcre_exec(%struct.real_pcre8_or_16* %11, %struct.pcre_extra* null, i8* %48, i32 %73, i32 0, i32 0, i32* null, i32 0) #11
  %75 = icmp slt i32 %74, 0
  br i1 %75, label %44, label %76

76:                                               ; preds = %71, %39
  %77 = phi i8* [ %16, %39 ], [ %48, %71 ]
  %78 = call fast double @strtod(i8* nocapture nonnull %77, i8** null) #11
  %79 = call i32 @puts(i8* nonnull dereferenceable(1) getelementptr inbounds ([37 x i8], [37 x i8]* @str.15, i64 0, i64 0))
  %80 = call noalias dereferenceable_or_null(100) i8* @malloc(i64 100) #11
  br label %81

81:                                               ; preds = %98, %76
  %82 = phi i8* [ %80, %76 ], [ %99, %98 ]
  %83 = phi i64 [ 100, %76 ], [ %100, %98 ]
  %84 = phi i64 [ 100, %76 ], [ %101, %98 ]
  %85 = phi i8* [ %80, %76 ], [ %104, %98 ]
  %86 = load %struct._IO_FILE*, %struct._IO_FILE** @stdin, align 8, !tbaa !4
  %87 = call i32 @fgetc(%struct._IO_FILE* %86) #11
  switch i32 %87, label %88 [
    i32 -1, label %105
    i32 10, label %105
  ]

88:                                               ; preds = %81
  %89 = add i64 %84, -1
  %90 = icmp eq i64 %89, 0
  br i1 %90, label %91, label %98

91:                                               ; preds = %88
  %92 = shl i64 %83, 1
  %93 = call i8* @realloc(i8* %82, i64 %92) #11
  %94 = ptrtoint i8* %85 to i64
  %95 = ptrtoint i8* %82 to i64
  %96 = sub i64 %94, %95
  %97 = getelementptr inbounds i8, i8* %93, i64 %96
  br label %98

98:                                               ; preds = %91, %88
  %99 = phi i8* [ %93, %91 ], [ %82, %88 ]
  %100 = phi i64 [ %92, %91 ], [ %83, %88 ]
  %101 = phi i64 [ %83, %91 ], [ %89, %88 ]
  %102 = phi i8* [ %97, %91 ], [ %85, %88 ]
  %103 = trunc i32 %87 to i8
  %104 = getelementptr inbounds i8, i8* %102, i64 1
  store i8 %103, i8* %102, align 1, !tbaa !8
  br label %81

105:                                              ; preds = %81, %81
  store i8 0, i8* %85, align 1, !tbaa !8
  %106 = call i64 @strlen(i8* nonnull dereferenceable(1) %82) #12
  %107 = trunc i64 %106 to i32
  %108 = call i32 @pcre_exec(%struct.real_pcre8_or_16* %11, %struct.pcre_extra* null, i8* %82, i32 %107, i32 0, i32 0, i32* null, i32 0) #11
  %109 = call fast double @strtod(i8* nocapture nonnull %82, i8** null) #11
  %110 = icmp slt i32 %108, 0
  %111 = fcmp fast ole double %109, %78
  %112 = or i1 %111, %110
  br i1 %112, label %113, label %148

113:                                              ; preds = %105, %140
  %114 = call i32 @puts(i8* nonnull dereferenceable(1) getelementptr inbounds ([45 x i8], [45 x i8]* @str.18, i64 0, i64 0))
  %115 = call noalias dereferenceable_or_null(100) i8* @malloc(i64 100) #11
  br label %116

116:                                              ; preds = %133, %113
  %117 = phi i8* [ %115, %113 ], [ %134, %133 ]
  %118 = phi i64 [ 100, %113 ], [ %135, %133 ]
  %119 = phi i64 [ 100, %113 ], [ %136, %133 ]
  %120 = phi i8* [ %115, %113 ], [ %139, %133 ]
  %121 = load %struct._IO_FILE*, %struct._IO_FILE** @stdin, align 8, !tbaa !4
  %122 = call i32 @fgetc(%struct._IO_FILE* %121) #11
  switch i32 %122, label %123 [
    i32 -1, label %140
    i32 10, label %140
  ]

123:                                              ; preds = %116
  %124 = add i64 %119, -1
  %125 = icmp eq i64 %124, 0
  br i1 %125, label %126, label %133

126:                                              ; preds = %123
  %127 = shl i64 %118, 1
  %128 = call i8* @realloc(i8* %117, i64 %127) #11
  %129 = ptrtoint i8* %120 to i64
  %130 = ptrtoint i8* %117 to i64
  %131 = sub i64 %129, %130
  %132 = getelementptr inbounds i8, i8* %128, i64 %131
  br label %133

133:                                              ; preds = %126, %123
  %134 = phi i8* [ %128, %126 ], [ %117, %123 ]
  %135 = phi i64 [ %127, %126 ], [ %118, %123 ]
  %136 = phi i64 [ %118, %126 ], [ %124, %123 ]
  %137 = phi i8* [ %132, %126 ], [ %120, %123 ]
  %138 = trunc i32 %122 to i8
  %139 = getelementptr inbounds i8, i8* %137, i64 1
  store i8 %138, i8* %137, align 1, !tbaa !8
  br label %116

140:                                              ; preds = %116, %116
  store i8 0, i8* %120, align 1, !tbaa !8
  %141 = call i64 @strlen(i8* nonnull dereferenceable(1) %117) #12
  %142 = trunc i64 %141 to i32
  %143 = call i32 @pcre_exec(%struct.real_pcre8_or_16* %11, %struct.pcre_extra* null, i8* %117, i32 %142, i32 0, i32 0, i32* null, i32 0) #11
  %144 = call fast double @strtod(i8* nocapture nonnull %117, i8** null) #11
  %145 = icmp slt i32 %143, 0
  %146 = fcmp fast ole double %144, %78
  %147 = or i1 %146, %145
  br i1 %147, label %113, label %148

148:                                              ; preds = %140, %105
  %149 = phi i8* [ %82, %105 ], [ %117, %140 ]
  %150 = phi double [ %109, %105 ], [ %144, %140 ]
  %151 = call i32 @puts(i8* nonnull dereferenceable(1) getelementptr inbounds ([24 x i8], [24 x i8]* @str.16, i64 0, i64 0))
  %152 = call noalias dereferenceable_or_null(100) i8* @malloc(i64 100) #11
  br label %153

153:                                              ; preds = %170, %148
  %154 = phi i8* [ %152, %148 ], [ %171, %170 ]
  %155 = phi i64 [ 100, %148 ], [ %172, %170 ]
  %156 = phi i64 [ 100, %148 ], [ %173, %170 ]
  %157 = phi i8* [ %152, %148 ], [ %176, %170 ]
  %158 = load %struct._IO_FILE*, %struct._IO_FILE** @stdin, align 8, !tbaa !4
  %159 = call i32 @fgetc(%struct._IO_FILE* %158) #11
  switch i32 %159, label %160 [
    i32 -1, label %177
    i32 10, label %177
  ]

160:                                              ; preds = %153
  %161 = add i64 %156, -1
  %162 = icmp eq i64 %161, 0
  br i1 %162, label %163, label %170

163:                                              ; preds = %160
  %164 = shl i64 %155, 1
  %165 = call i8* @realloc(i8* %154, i64 %164) #11
  %166 = ptrtoint i8* %157 to i64
  %167 = ptrtoint i8* %154 to i64
  %168 = sub i64 %166, %167
  %169 = getelementptr inbounds i8, i8* %165, i64 %168
  br label %170

170:                                              ; preds = %163, %160
  %171 = phi i8* [ %165, %163 ], [ %154, %160 ]
  %172 = phi i64 [ %164, %163 ], [ %155, %160 ]
  %173 = phi i64 [ %155, %163 ], [ %161, %160 ]
  %174 = phi i8* [ %169, %163 ], [ %157, %160 ]
  %175 = trunc i32 %159 to i8
  %176 = getelementptr inbounds i8, i8* %174, i64 1
  store i8 %175, i8* %174, align 1, !tbaa !8
  br label %153

177:                                              ; preds = %153, %153
  store i8 0, i8* %157, align 1, !tbaa !8
  %178 = call i64 @strlen(i8* nonnull dereferenceable(1) %154) #12
  %179 = trunc i64 %178 to i32
  %180 = call i32 @pcre_exec(%struct.real_pcre8_or_16* %12, %struct.pcre_extra* null, i8* %154, i32 %179, i32 0, i32 0, i32* null, i32 0) #11
  %181 = call fast double @strtod(i8* nocapture nonnull %154, i8** null) #11
  %182 = icmp slt i32 %180, 0
  %183 = fcmp fast ole double %181, 1.000000e-08
  %184 = or i1 %183, %182
  br i1 %184, label %185, label %220

185:                                              ; preds = %177, %212
  %186 = call i32 @puts(i8* nonnull dereferenceable(1) getelementptr inbounds ([41 x i8], [41 x i8]* @str.17, i64 0, i64 0))
  %187 = call noalias dereferenceable_or_null(100) i8* @malloc(i64 100) #11
  br label %188

188:                                              ; preds = %205, %185
  %189 = phi i8* [ %187, %185 ], [ %206, %205 ]
  %190 = phi i64 [ 100, %185 ], [ %207, %205 ]
  %191 = phi i64 [ 100, %185 ], [ %208, %205 ]
  %192 = phi i8* [ %187, %185 ], [ %211, %205 ]
  %193 = load %struct._IO_FILE*, %struct._IO_FILE** @stdin, align 8, !tbaa !4
  %194 = call i32 @fgetc(%struct._IO_FILE* %193) #11
  switch i32 %194, label %195 [
    i32 -1, label %212
    i32 10, label %212
  ]

195:                                              ; preds = %188
  %196 = add i64 %191, -1
  %197 = icmp eq i64 %196, 0
  br i1 %197, label %198, label %205

198:                                              ; preds = %195
  %199 = shl i64 %190, 1
  %200 = call i8* @realloc(i8* %189, i64 %199) #11
  %201 = ptrtoint i8* %192 to i64
  %202 = ptrtoint i8* %189 to i64
  %203 = sub i64 %201, %202
  %204 = getelementptr inbounds i8, i8* %200, i64 %203
  br label %205

205:                                              ; preds = %198, %195
  %206 = phi i8* [ %200, %198 ], [ %189, %195 ]
  %207 = phi i64 [ %199, %198 ], [ %190, %195 ]
  %208 = phi i64 [ %190, %198 ], [ %196, %195 ]
  %209 = phi i8* [ %204, %198 ], [ %192, %195 ]
  %210 = trunc i32 %194 to i8
  %211 = getelementptr inbounds i8, i8* %209, i64 1
  store i8 %210, i8* %209, align 1, !tbaa !8
  br label %188

212:                                              ; preds = %188, %188
  store i8 0, i8* %192, align 1, !tbaa !8
  %213 = call i64 @strlen(i8* nonnull dereferenceable(1) %189) #12
  %214 = trunc i64 %213 to i32
  %215 = call i32 @pcre_exec(%struct.real_pcre8_or_16* %12, %struct.pcre_extra* null, i8* %189, i32 %214, i32 0, i32 0, i32* null, i32 0) #11
  %216 = call fast double @strtod(i8* nocapture nonnull %189, i8** null) #11
  %217 = icmp slt i32 %215, 0
  %218 = fcmp fast ole double %216, 1.000000e-08
  %219 = or i1 %218, %217
  br i1 %219, label %185, label %220

220:                                              ; preds = %212, %177
  %221 = phi i8* [ %154, %177 ], [ %189, %212 ]
  %222 = phi double [ %181, %177 ], [ %216, %212 ]
  %223 = fadd fast double %150, %78
  %224 = fmul fast double %223, 5.000000e-01
  %225 = call fast double @llvm.cos.f64(double %224) #11
  %226 = fsub fast double %224, %225
  %227 = call fast double @llvm.fabs.f64(double %226) #11
  %228 = fcmp fast ogt double %227, %222
  br i1 %228, label %229, label %238

229:                                              ; preds = %220, %229
  %230 = phi i32 [ %233, %229 ], [ 0, %220 ]
  %231 = phi double [ %232, %229 ], [ %224, %220 ]
  %232 = call fast double @llvm.cos.f64(double %231) #11
  %233 = add nuw nsw i32 %230, 1
  %234 = call fast double @llvm.cos.f64(double %232) #11
  %235 = fsub fast double %232, %234
  %236 = call fast double @llvm.fabs.f64(double %235) #11
  %237 = fcmp fast ogt double %236, %222
  br i1 %237, label %229, label %238

238:                                              ; preds = %229, %220
  %239 = phi double [ %224, %220 ], [ %232, %229 ]
  %240 = phi i32 [ 0, %220 ], [ %233, %229 ]
  %241 = call i32 (i8*, ...) @printf(i8* nonnull dereferenceable(1) getelementptr inbounds ([41 x i8], [41 x i8]* @.str, i64 0, i64 0), double %239, i32 %240) #11
  %242 = call fast double @llvm.cos.f64(double %78) #11
  %243 = fsub fast double %78, %242
  %244 = call fast double @llvm.cos.f64(double %150) #11
  %245 = fsub fast double %150, %244
  %246 = fmul fast double %245, %243
  %247 = fcmp fast ult double %246, 0.000000e+00
  br i1 %247, label %248, label %251

248:                                              ; preds = %238
  %249 = call fast double @llvm.fabs.f64(double %243) #11
  %250 = fcmp fast ogt double %249, %222
  br i1 %250, label %253, label %272

251:                                              ; preds = %238
  %252 = call i32 @puts(i8* nonnull dereferenceable(1) getelementptr inbounds ([35 x i8], [35 x i8]* @str, i64 0, i64 0)) #11
  br label %276

253:                                              ; preds = %248, %253
  %254 = phi double [ %266, %253 ], [ %78, %248 ]
  %255 = phi i32 [ %267, %253 ], [ 0, %248 ]
  %256 = phi double [ %265, %253 ], [ %150, %248 ]
  %257 = fadd fast double %256, %254
  %258 = fmul fast double %257, 5.000000e-01
  %259 = call fast double @llvm.cos.f64(double %258) #11
  %260 = fsub fast double %258, %259
  %261 = call fast double @llvm.cos.f64(double %256) #11
  %262 = fsub fast double %256, %261
  %263 = fmul fast double %260, %262
  %264 = fcmp fast ult double %263, 0.000000e+00
  %265 = select i1 %264, double %256, double %258
  %266 = select i1 %264, double %258, double %254
  %267 = add nuw nsw i32 %255, 1
  %268 = call fast double @llvm.cos.f64(double %266) #11
  %269 = fsub fast double %266, %268
  %270 = call fast double @llvm.fabs.f64(double %269) #11
  %271 = fcmp fast ogt double %270, %222
  br i1 %271, label %253, label %272

272:                                              ; preds = %253, %248
  %273 = phi i32 [ 0, %248 ], [ %267, %253 ]
  %274 = phi double [ %78, %248 ], [ %266, %253 ]
  %275 = call i32 (i8*, ...) @printf(i8* nonnull dereferenceable(1) getelementptr inbounds ([40 x i8], [40 x i8]* @.str.2, i64 0, i64 0), double %274, i32 %273) #11
  br label %276

276:                                              ; preds = %251, %272
  br i1 %228, label %277, label %290

277:                                              ; preds = %276, %277
  %278 = phi double [ %287, %277 ], [ %226, %276 ]
  %279 = phi i32 [ %285, %277 ], [ 0, %276 ]
  %280 = phi double [ %284, %277 ], [ %224, %276 ]
  %281 = call fast double @llvm.sin.f64(double %280) #11
  %282 = fadd fast double %281, 1.000000e+00
  %283 = fdiv fast double %278, %282
  %284 = fsub fast double %280, %283
  %285 = add nuw nsw i32 %279, 1
  %286 = call fast double @llvm.cos.f64(double %284) #11
  %287 = fsub fast double %284, %286
  %288 = call fast double @llvm.fabs.f64(double %287) #11
  %289 = fcmp fast ogt double %288, %222
  br i1 %289, label %277, label %290

290:                                              ; preds = %277, %276
  %291 = phi double [ %224, %276 ], [ %284, %277 ]
  %292 = phi i32 [ 0, %276 ], [ %285, %277 ]
  %293 = call i32 (i8*, ...) @printf(i8* nonnull dereferenceable(1) getelementptr inbounds ([37 x i8], [37 x i8]* @.str.3, i64 0, i64 0), double %291, i32 %292) #11
  call void @free(i8* %77) #11
  call void @free(i8* %149) #11
  call void @free(i8* %221) #11
  %294 = load void (i8*)*, void (i8*)** @pcre_free, align 8, !tbaa !4
  %295 = bitcast %struct.real_pcre8_or_16* %11 to i8*
  call void %294(i8* %295) #11
  call void @llvm.lifetime.end.p0i8(i64 14, i8* nonnull %8) #11
  call void @llvm.lifetime.end.p0i8(i64 14, i8* nonnull %7) #11
  ret i32 0
}

; Function Attrs: argmemonly nounwind willreturn
declare void @llvm.memcpy.p0i8.p0i8.i64(i8* noalias nocapture writeonly, i8* noalias nocapture readonly, i64, i1 immarg) #1

declare %struct.real_pcre8_or_16* @pcre_compile(i8*, i32, i8**, i32*, i8*) local_unnamed_addr #8

declare i32 @pcre_exec(%struct.real_pcre8_or_16*, %struct.pcre_extra*, i8*, i32, i32, i32, i32*, i32) local_unnamed_addr #8

; Function Attrs: argmemonly nofree nounwind readonly
declare i64 @strlen(i8* nocapture) local_unnamed_addr #9

; Function Attrs: nounwind
declare void @free(i8* nocapture) local_unnamed_addr #3

; Function Attrs: nofree nounwind
declare double @strtod(i8* readonly, i8** nocapture) local_unnamed_addr #2

; Function Attrs: nofree nounwind
declare i32 @puts(i8* nocapture readonly) local_unnamed_addr #10

; Function Attrs: nounwind readnone speculatable willreturn
declare double @llvm.fabs.f64(double) #5

attributes #0 = { nounwind sspstrong uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "denormal-fp-math"="preserve-sign,preserve-sign" "denormal-fp-math-f32"="ieee,ieee" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="true" "no-jump-tables"="false" "no-nans-fp-math"="true" "no-signed-zeros-fp-math"="true" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="true" "use-soft-float"="false" }
attributes #1 = { argmemonly nounwind willreturn }
attributes #2 = { nofree nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "denormal-fp-math"="preserve-sign,preserve-sign" "denormal-fp-math-f32"="ieee,ieee" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="true" "no-nans-fp-math"="true" "no-signed-zeros-fp-math"="true" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="true" "use-soft-float"="false" }
attributes #3 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "denormal-fp-math"="preserve-sign,preserve-sign" "denormal-fp-math-f32"="ieee,ieee" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="true" "no-nans-fp-math"="true" "no-signed-zeros-fp-math"="true" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="true" "use-soft-float"="false" }
attributes #4 = { nounwind readnone sspstrong uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "denormal-fp-math"="preserve-sign,preserve-sign" "denormal-fp-math-f32"="ieee,ieee" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="true" "no-jump-tables"="false" "no-nans-fp-math"="true" "no-signed-zeros-fp-math"="true" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="true" "use-soft-float"="false" }
attributes #5 = { nounwind readnone speculatable willreturn }
attributes #6 = { norecurse nounwind readnone sspstrong uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "denormal-fp-math"="preserve-sign,preserve-sign" "denormal-fp-math-f32"="ieee,ieee" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="true" "no-jump-tables"="false" "no-nans-fp-math"="true" "no-signed-zeros-fp-math"="true" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="true" "use-soft-float"="false" }
attributes #7 = { nofree nounwind sspstrong uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "denormal-fp-math"="preserve-sign,preserve-sign" "denormal-fp-math-f32"="ieee,ieee" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "min-legal-vector-width"="0" "no-infs-fp-math"="true" "no-jump-tables"="false" "no-nans-fp-math"="true" "no-signed-zeros-fp-math"="true" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="true" "use-soft-float"="false" }
attributes #8 = { "correctly-rounded-divide-sqrt-fp-math"="false" "denormal-fp-math"="preserve-sign,preserve-sign" "denormal-fp-math-f32"="ieee,ieee" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="true" "no-nans-fp-math"="true" "no-signed-zeros-fp-math"="true" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="true" "use-soft-float"="false" }
attributes #9 = { argmemonly nofree nounwind readonly "correctly-rounded-divide-sqrt-fp-math"="false" "denormal-fp-math"="preserve-sign,preserve-sign" "denormal-fp-math-f32"="ieee,ieee" "disable-tail-calls"="false" "frame-pointer"="none" "less-precise-fpmad"="false" "no-infs-fp-math"="true" "no-nans-fp-math"="true" "no-signed-zeros-fp-math"="true" "no-trapping-math"="true" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+cx8,+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="true" "use-soft-float"="false" }
attributes #10 = { nofree nounwind }
attributes #11 = { nounwind }
attributes #12 = { nounwind readonly }

!llvm.module.flags = !{!0, !1, !2}
!llvm.ident = !{!3}

!0 = !{i32 1, !"wchar_size", i32 4}
!1 = !{i32 7, !"PIC Level", i32 2}
!2 = !{i32 7, !"PIE Level", i32 2}
!3 = !{!"clang version 11.1.0"}
!4 = !{!5, !5, i64 0}
!5 = !{!"any pointer", !6, i64 0}
!6 = !{!"omnipotent char", !7, i64 0}
!7 = !{!"Simple C/C++ TBAA"}
!8 = !{!6, !6, i64 0}
