#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include <pcre.h>

char* getLine(void) {
  char *line = (char*)malloc(100), *linep = line;
  size_t lenmax = 100, len = lenmax;
  int c;

  c = fgetc(stdin);
  while (c != EOF && c != '\n') {
    if(--len == 0) {
      len = lenmax;
      char * linen = (char*)realloc(linep, lenmax *= 2);
      line = linen + (line - linep);
      linep = linen;
    }

    *line++ = c;
    c = fgetc(stdin);
  }
  *line = '\0';
  return linep;
}

// our function
double fx (const double x) {
  return x - cos(x);
}

// derivate of out function
double fD (const double x) {
  return 1 + sin(x);
}

double absD (const double x) {
  if (x >= 0) {
    return x;
  } else
    return -x;
}

// method of iterations
void iteration(const double l, const double r, const double acc) {
  double x = (r+l)/2;
  int    i = 0; // for count iterations

  /* printf("Iter %f %f %f.\n", x, absD(fx(x)), acc); */
  /* printf("Iter %f %f %f.\n", x, acc, absD(fx(x))); */
  for (; absD(fx(x)) > acc; ++i) {
    x = cos(x);
  }
  printf("Iterations give %f after %d iterations.\n", x, i);
}

int eqSign(double x, double y) {
  return (x*y >= 0);
}

// method of dichotomy
void dichotomy(double l, double r, double acc) {
  int i = 0; // for count iterations
  if (eqSign(fx(l),fx(r))) {
    printf("In this interval dichotomy is bad.\n");
    return;
  }
  for (; absD(fx(l)) > acc; ++i) {
    double newBorder = (l+r)/2;
    if (!eqSign(fx(newBorder),fx(r))) {
      l = newBorder;
    } else {
      r = newBorder;
    }
  }
  printf("Dichotomy give %f after %d iterations.\n", l, i);
}

// method of newton
void newton(const double l, const double r, const double acc) {
  double x = (r+l)/2;
  int    i = 0; // for count iterations
  for (; absD(fx(x)) > acc; ++i) {
    x = x - fx(x)/fD(x);
  }
  printf("Newton give %f after %d iterations.\n", x, i);
}

int main() {
  printf("We have equation like \"f(x) = x - cos(x)\".\n");
  printf("We can find one root use the interval.\n");

  double l,r;
  double acc;
  char* lS;
  char* rS;
  char* accS;

  char re[]  = "^-?\\d+\\.?\\d*$";
  char reAcc[]  = "^-?\\d+\\.?\\d*$";
  pcre* pcRe;
  pcre* pcReAcc;
  {
    const char* error;
    int erroroffset;
    pcRe = pcre_compile(re, 0, &error, &erroroffset, NULL);
    pcReAcc = pcre_compile(reAcc, 0, &error, &erroroffset, NULL);
  }

  printf("Please, put left border (rational).\n");
  lS = getLine();
  int count = pcre_exec(pcRe, NULL, lS, strlen(lS), 0, 0, NULL, 0);
  while (count < 0) {
    printf("You are wrong. Try again input left border.\n");
    lS = getLine();
    count = pcre_exec(pcRe, NULL, lS, strlen(lS), 0, 0, NULL, 0);
  }
  l = atof(lS);


  printf("Please, put right border (rational).\n");
  rS = getLine();
  count = pcre_exec(pcRe, NULL, rS, strlen(rS), 0, 0, NULL, 0);
  r = atof(rS);
  while (count < 0 || r <= l) {
    printf("You are wrong. Try again input right border.\n");
    rS = getLine();
    count = pcre_exec(pcRe, NULL, rS, strlen(rS), 0, 0, NULL, 0);
    r = atof(rS);
  }

  printf("Please, print accuracy.\n");
  accS = getLine();
  count = pcre_exec(pcReAcc, NULL, accS, strlen(accS), 0, 0, NULL, 0);
  acc = atof(accS);
  while (count < 0 || acc <= 0.00000001) {
    printf("You are wrong. Try again input accuracy.\n");
    accS = getLine();
    count = pcre_exec(pcReAcc, NULL, accS, strlen(accS), 0, 0, NULL, 0);
    acc = atof(accS);
  }

  iteration(l,r,acc);
  dichotomy(l,r,acc);
  newton(l,r,acc);

  free(lS);
  free(rS);
  free(accS);
  pcre_free(pcRe);

  return 0;
}
