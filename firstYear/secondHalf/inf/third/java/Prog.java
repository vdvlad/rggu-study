package progJava;

import java.util.regex.Pattern;
import java.util.Scanner;

import progJava.Information;

class Prog {
  public static Information input() {
    Scanner line = new Scanner(System.in);
    String s = "";
    Information i = new Information();

    do {
      do {
        System.out.println("Print left border.");
        s = line.nextLine();
      }
      while (!Pattern.matches("^-?\\d+(\\.\\d)?\\d*$",s));
      i.left_ = Integer.parseInt(s);

      do {
        System.out.println("Print right border (more then left).");
        s = line.nextLine();
      }
      while (!Pattern.matches("^-?\\d+(\\.\\d)?\\d*$",s));
      i.right_ = Integer.parseInt(s);
    } while (i.right_ <= i.left_);

    do {
      do {
        System.out.println("Print accuracy (more then 0.00000001).");
        s = line.nextLine();
      }
      while (!Pattern.matches("^\\d+(\\.\\d)?\\d*$",s));
      i.acc_ = Double.parseDouble(s);
    }
    while (i.acc_ <= 0.00000001);

    return i;
  }

  // our function
  public static double fx (final double x) {
    return x - Math.cos(x);
  }

  // derivate of out function
  public static double fD (final double x) {
    return 1 + Math.sin(x);
  }

  // method of iterations
  public static void iteration(final Information inf) {
    double x = (inf.right_ + inf.left_) / 2;
    int    i = 0; // for count iterations
    for (; Math.abs(fx(x)) > inf.acc_; ++i) {
        x = Math.cos(x);
    }
    System.out.println("Iterations give " +
                     x + " after " + i
                     + " iterations.");
  }

  // method of dichotomy
  static void dichotomy(final Information infOld) {
    Information inf = new Information(infOld);
    int    i = 0; // for count iterations

    if (fx(inf.left_)*fx(inf.right_) >= 0) {
      System.out.println("In this interval dichotomy is bad.");
      return;
    }
    for (; Math.abs(fx(inf.left_)) > inf.acc_; ++i) {
      double newBorder = (inf.left_+inf.right_)/2;
      if (fx(newBorder)*fx(inf.right_) < 0) {
        inf.left_ = newBorder;
      } else {
        inf.right_ = newBorder;
      }
    }
    System.out.println("Dichotomy give " + inf.left_
                     + " after " + i
                     + " iterations.");
  }

  // method of newton
  static void newton(final Information inf) {
    double x = (inf.right_+inf.left_)/2;
    int    i = 0; // for count iterations

    for (; Math.abs(fx(x)) > inf.acc_; ++i) {
      x = x - fx(x)/fD(x);
    }
    System.out.println("Newton give " + x
                      + " after " + i + " iterations.");
  }

  public static void main(String[] args) {
    System.out.println("We have equation like \"f(x) = x - cos(x)\".");
    System.out.println("We can find one root use the interval.\n");
    System.out.println("Put the borders for dichotomy method.");
    System.out.println("(for another will be used half sum of left and right border)\n");

    Information inf = input();

    iteration(inf);
    dichotomy(inf);
    newton(inf);
  }
}
