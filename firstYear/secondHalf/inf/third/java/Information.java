package progJava;

class Information {
  public double left_;
  public double right_;
  public double acc_; // accuracy

  public Information() {
    this.left_     = 0;
    this.right_    = 0;
    this.acc_      = 0;
  }

  public Information(final Information d) {
    this.left_     = d.left_;
    this.right_    = d.right_;
    this.acc_      = d.acc_;
  }
}
