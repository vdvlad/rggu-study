#include <math.h>
#include <string>
#include <iostream>
#include <pcrecpp.h>

// our function
double fx (double x) {
  return x - cos(x);
}

// derivate of out function
double fD (double x) {
  return 1 + sin(x);
}

// method of iterations
void iteration(const double l, const double r, const double acc) {
  double x = (r+l)/2;
  int    i = 0; // for count iterations
  for (; abs(fx(x)) > acc; ++i) {
    x = cos(x);
  }
  std::cout << "Iterations give " << x
            << " after " << i << " iterations." << std::endl;
}

inline int eqSign(double x, double y) {
  return (x*y >= 0);
}

// method of dichotomy
void dichotomy(double l, double r, double acc) {
  int    i = 0; // for count iterations
  if (eqSign(fx(l),fx(r))) {
    std::cout << "In this interval dichotomy is bad." << std::endl;
    return;
  }
  for (; abs(fx(l)) > acc; ++i) {
    double newBorder = (l+r)/2;
    if (!eqSign(fx(newBorder),fx(r))) {
      l = newBorder;
    } else {
      r = newBorder;
    }
  }
  std::cout << "Dichotomy give " << l
            << " after " << i << " iterations." << std::endl;
}

// method of newton
void newton(const double l, const double r, const double acc) {
  double x = (r+l)/2;
  int    i = 0; // for count iterations
  for (; abs(fx(x)) > acc; ++i) {
    x = x - fx(x)/fD(x);
  }
  std::cout << "Newton give " << x
            << " after " << i << " iterations." << std::endl;
}

int main() {
  std::cout << "We have equation like \"f(x) = x - cos(x)\"." << std::endl;
  std::cout << "We can find one root use the interval.\n" << std::endl;
  std::cout << "Put the borders for dichotomy method." << std::endl;
  std::cout << "(for another will be used half sum of left and right border)\n" << std::endl;

  double l,r;
  double acc;
  std::string lS,rS,accS;
  pcrecpp::RE re("(-?\\d+\\.?\\d*)");
  pcrecpp::RE reAcc("(\\d+\\.?\\d*)");

  std::cout << "Please, put left border (rational)." << std::endl;
  std::getline(std::cin, lS);
  while (!re.FullMatch(lS, &l)) {
    std::cout << "You are wrong. Try again input left border." << std::endl;
    std::getline(std::cin, lS);
  }

  std::cout << "Please, put right border (rational)." << std::endl;
  std::getline(std::cin, rS);
  while (!re.FullMatch(rS, &r) || r <= l) {
    std::cout << "You are wrong. Try again input right border." << std::endl;
    std::getline(std::cin, rS);
  }

  std::cout << "Print accuracy, please. (not longer then 0.00000001)" << std::endl;
  std::getline(std::cin, accS);
  while (!reAcc.FullMatch(accS, &acc) || acc < 0.00000001) {
    std::cout << "You are wrong. Try again input accuracy." << std::endl;
    std::getline(std::cin, accS);
  }

  iteration(l,r,acc);
  dichotomy(l,r,acc);
  newton(l,r,acc);

  return 0;
}
