use 5.010;

sub fx {
  my $x = shift;
  return $x - cos $x;
}

# derivate of out function
sub fD {
  my $x = shift;
  return 1 + sin $x;
}

# method of iterations
sub iteration {
  my $acc = @_[2];
  my $x   = (@_[0]+@_[1])/2; # (l+r)/2
  my $i   = 0; # for count iterations
  for (; abs(fx($x)) > $acc; ++$i) {
    $x = cos $x;
  }
  say "Iterations give ", $x, " after ", $i, " iterations.";
}

sub eqSign {
  return (@_[0]*@_[1] >= 0);
}

# method of dichotomy
sub dichotomy {
  my $l   = shift;
  my $r   = shift;
  my $acc = shift;
  my $i   = 0; # for count iterations
  if (eqSign(fx($l),fx($r))) {
    say "In this interval dichotomy is bad.";
    return;
  }
  for (; abs(fx($l)) > $acc; ++$i) {
    my $newBorder = ($l+$r)/2;
    if (!eqSign(fx($newBorder),fx($r))) {
      $l = $newBorder;
    } else {
      $r = $newBorder;
    }
  }
  say "Dichotomy give ", $l, " after ", $i, " iterations.";
}

# method of newton
sub newton {
  my $acc = @_[2];
  my $x   = (@_[0]+@_[1])/2; # (l+r)/2
  my $i   = 0; # for count iterations
  for (; abs(fx($x)) > $acc; ++$i) {
    $x = $x - fx($x)/fD($x);
  }
  say "Newton give ", $x, " after ", $i, " iterations.";
}

say "We have equation like \"f(x) = x - cos(x)\".";
say "We can find one root use the interval.";

my $l,$r;
my $acc;

say "Please, put left border (rational).";
$l = <>;
while ($l !~ /^-?\d+\.?\d*$/) {
  say "You are wrong. Try again input right border.";
  $l = <>;
}
chomp $l;

say "Please, put right border (rational).";
$r = <>;
chop($r);
while ($r !~ /^-?\d+\.?\d*$/ || $r <= $l) {
  say "You are wrong. Try again input left border.";
  $r = <>;
}

say "Please, put accuracy (rational).";
$acc = <>;
chop($acc);
while ($r !~ /^\d+\.?\d*$/ || $acc <= 0.000001) {
  say "You are wrong. Try again input accuracy.";
  $acc = <>;
}

iteration($l,$r,$acc);
dichotomy($l,$r,$acc);
newton($l,$r,$acc);
