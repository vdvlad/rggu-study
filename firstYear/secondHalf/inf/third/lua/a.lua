rex = require "rex_pcre"

function input()
  print("We have equation like \"f(x) = x - cos(x)\".")
  print("We can find one root use the interval.\n")
  local left
  local right
  local acc

  repeat
    print("Please enter left border.")
    left = io.read()
  until (rex.match(left, "^-?\\d+\\.?\\d*$") ~= nil)
  repeat
    print("Please enter right border.")
    right = io.read()
  until ((rex.match(right, "^-?\\d+\\.?\\d*$") ~= nil) and (tonumber(left) < tonumber(right)) )
  repeat
    print("Please enter accuracy.")
    acc = io.read()
  until ((rex.match(acc, "^\\d+\\.?\\d*$") ~= nil) and (tonumber(acc) > 0.000001))

  return tonumber(left), tonumber(right), tonumber(acc)
end

function fx(x) -- our function
  return x - math.cos(x)
end

function fD(x) -- derivate our function
  return 1 + math.sin(x)
end

-- for method of iteration
function iteration(l,r,acc)
  local x = (r+l)/2;
  local i = 0; -- for count iterations
  while (math.abs(fx(x)) > acc) do
    x = math.cos(x)
    i = i + 1
  end
  print("Iterations give " .. tostring(x)
        .. " after " .. tostring(i) .. " iterations.")
end

function eqSign(x,y)
  return (x*y >= 0)
end

-- for method of dichotomy
function dichotomy(l,r,acc)
  local i = 0 -- for count iterations
  if (eqSign(fx(l),fx(r))) then
    print("In this interval dichotomy is bad.")
    return
  end
  while (math.abs(fx(l)) > acc) do
    local newBorder = (l+r)/2
    if (not eqSign(fx(newBorder),fx(r))) then
      l = newBorder
    else
      r = newBorder
    end
    i = i + 1
  end
  print("Dichotomy give " .. tostring(l)
        .. " after " .. tostring(i) .. " iterations.")
end

-- for method of newton
function newton(l,r,acc)
  local x = (r+l)/2;
  local i = 0; -- for count iterations
  while (math.abs(fx(x)) > acc) do
    x = x - fx(x)/fD(x)
    i = i + 1
  end
  print("Newton give " .. tostring(x)
        .. " after " .. tostring(i) .. " iterations.")
end


local l, r, acc = input()
iteration(l,r,acc)
dichotomy(l,r,acc)
newton(l,r,acc)
