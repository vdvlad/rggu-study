module progr
  use, intrinsic :: iso_c_binding
  use pcre_module
  use iso_fortran_env, only : IOSTAT_EOR

contains

  subroutine readNumber(inp)
    Integer :: io_number

    Character(len = 1) :: buffer
    Character(len = :), allocatable :: inp

    inp = ""
    do
        read(unit = *, fmt = '(a)', advance = "no", iostat = io_number) buffer
        select case (io_number)
        case(0)
          inp = inp // buffer
        case(IOSTAT_EOR)
          exit
        end select
    end do

  end subroutine

  Real function checkNumber(pcre)
    Type(pcre_type) :: pcre
    Character(len = :), allocatable :: inp

    integer, dimension(0:1) :: ovector

    do
        call readNumber(inp)
        if (pcre_exec(pcre, c_null_ptr, inp, 0, 0, ovector) >= 0) exit
        write (*,*) "Sorry you are wrong."
    end do

    read (inp,*) checkNumber
  end function

  subroutine input(left,right,acc)
    Type(pcre_type) :: regex
    Character(len=*), parameter :: commonDigit &
        =  "^-?\d+\.?\d* *$"
    Character(len=*), parameter :: unsignDigit &
        =  "^\d+\.?\d* *$"

    Real :: left
    Real :: right
    Real :: acc

    write (*,*) "We have equation like 'f(x) = x - cos(x)'."
    write (*,*) "We can find one root use the interval."
    write (*,*) ""

    regex = pcre_compile(commonDigit, 0)

    write (*,*) "Please enter left border."
    left = checkNumber(regex)

    write (*,*) "Please enter right border."
    do
      right = checkNumber(regex)
      if (right > left) exit
      write (*,*) "Sorry, but right border must grater then left."
      write (*,*) "Please, enter right border again."
    end do

    regex = pcre_compile(unsignDigit, 0)
    write (*,*) "Please enter accuracy (more then 0.000001)."
    do
      acc = checkNumber(regex)
      if (acc > 0.000001) exit
      write (*,*) "Sorry, but accuracy must be grater then 0.000001."
      write (*,*) "Please, enter accuracy again."
    end do
  end subroutine

  Real function fx(x) ! our function
    Real :: x
    fx =  x - cos(x)
  end function

  Real function fD(x) ! derivate our function
    Real :: x
    fD =  1 + sin(x)
  end function

  ! for method of iteration
  subroutine iteration(l,r,acc)
    Real    :: l,r,acc
    Real    :: x
    Integer :: i

    x = (r+l)/2;
    i = 0; ! for count iterations
    do
      if (abs(fx(x)) <= acc) exit
      x = cos(x)
      i = i + 1
    end do
    write (*,*) "Iterations give ", x, " after ", i, " iterations."
  end subroutine

  Logical function eqSign(x,y)
    Real :: x,y
    eqSign  = (x*y >= 0)
  end function

  ! for method of iteration
  subroutine dichotomy(left,right,acc)
    Real    :: left,right,acc
    Integer :: i

    Real    :: l,r,newBorder
    l = left
    r = right
    i = 0; ! for count iterations

    if (eqSign(fx(l),fx(r))) then
      write (*,*) "In this interval dichotomy is bad."
      return
    end if
    do
      if (abs(fx(l)) <= acc) exit
        newBorder = (l+r)/2
      if (.not. eqSign(fx(newBorder),fx(r))) then
        l = newBorder
      else
        r = newBorder
      end if
      i = i + 1
    end do
    write (*,*) "Dichotomy give ", l, " after ", i, " iterations."
  end subroutine

  ! for method of newton
  subroutine newton(l,r,acc)
    Real    :: l,r,acc
    Real    :: x
    Integer :: i

    x = (r+l)/2;
    i = 0; ! for count iterations
    do
      if (abs(fx(x)) <= acc) exit
      x = x - fx(x)/fD(x)
      i = i + 1
    end do
    write (*,*) "Newton give ", x, " after ", i, " iterations."
  end subroutine
end module

program Date
  use progr
  Real :: left
  Real :: right
  Real :: acc

  call input(left,right,acc)

  call iteration(left,right,acc)
  call dichotomy(left,right,acc)
  call newton(left,right,acc)

end program
