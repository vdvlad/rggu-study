{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE FlexibleContexts #-}
module Main where

import Lens.Micro
import Lens.Micro.TH
import qualified Graphics.Vty as V

import qualified Brick.Main as M
import qualified Brick.Types as T
import Brick.Widgets.Core
  ( (<+>)
  , (<=>)
  , hLimit
  , vLimit
  , str
  , hBox
  , vBox
  )
import qualified Brick.Widgets.Center as C
import qualified Brick.Widgets.Edit as E
import qualified Brick.Widgets.Border as B
import qualified Brick.AttrMap as A
import qualified Brick.Focus as F
import Brick.Util (on)

import Data.Maybe (fromMaybe)
import Data.Either

import Text.Regex.PCRE.Heavy (scan,re)

data Name = EdL
          | EdR
          | EdAcc -- Accuracy
          deriving (Ord, Show, Eq)

data St =
    St { _focusRing :: F.FocusRing Name
       , _l   :: E.Editor String Name
       , _r   :: E.Editor String Name
       , _acc :: E.Editor String Name
       }

makeLenses ''St

parseAccuracy :: String -> String
parseAccuracy input
  | (null input) = ""
  | otherwise =
    let s = scan [re|(^\d+\.?\d*$)|] input
    in if (null s)
       then ""
       else let a = fst $ head s
            in if (last a == '.')
               then a ++ "0"
               else a

parse :: String -> String
parse input
  | (null input) = ""
  | otherwise =
    let s = scan [re|(^-?\d+\.?\d*$)|] input
    in if (null s)
       then ""
       else let a = fst $ head s
            in if (last a == '.')
               then a ++ "0"
               else a

-- our function
fx :: Double -> Double
fx x = x - cos x

-- f'(x) of our function
fD :: Double -> Double
fD x = 1 + sin x

-- method of iterations
iteration :: Either String (Double,Double,Double) -> String
iteration input = either id prCalc input
  where
    calc :: (Double,Double,Double) -> (Int,Double,Double)
    calc = \ (l,r,a) ->
                  let
                    f = scanl (\ x y -> cos x) ((l+r)/2) [1..]
                  in
                    head $ filter (\ (y,x,z) -> abs x < a) $
                    zipWith (\ x y -> (x,fx y,y)) [1..] f

    prin :: (Int,Double,Double) -> String
    prin = \ (i,b,c) ->
            "Iteration method gives " ++ show c
             ++ " with " ++ show i ++ " iterations."

    prCalc lr = prin $ calc lr

-- method of dichotomy
dichotomy :: Either String (Double,Double,Double) -> String
dichotomy input = either id prCalc input
  where
    sign l r = signum(fx l) * signum(fx r)
    f l r acc i =
              if (abs(fx r) <= acc)
              then "Dichotomy method gives " ++ show r
                   ++ " with " ++ show i ++ " iterations."
              else
                let new = (l+r)/2
                in
                  if (sign l new == -1)
                  then f l new acc (i+1)
                  else f new r acc (i+1)
    prCalc = \ (l,r,a)  -> if (sign l r == -1)
                           then f l r a 0
                           else "This method does not work good with this interwal"

-- method of newton
newton :: Either String (Double,Double,Double) -> String
newton input = either id prCalc input
  where
    calc :: (Double,Double,Double) -> (Int,Double,Double)
    calc = \ (l,r,a) ->
                  let
                    f = scanl (\ x y -> x - fx x / fD x) ((l+r)/2) [1..]
                  in
                    head $ filter (\ (y,x,z) -> abs x < a) $
                    zipWith (\ x y -> (x,fx y,y)) [1..] f

    prin :: (Int,Double,Double) -> String
    prin = \ (i,b,c) ->
            "Newton method gives " ++ show c
             ++ " with " ++ show i ++ " iterations."

    prCalc lr = prin $ calc lr

readEntries :: String -> String -> String
                      -> Either String (Double,Double,Double)
readEntries l r a =
  case (l,r,a) of
    ([],[],[]) -> Left "All borders and accuracy are wrong."
    ([],[],_)  -> Left "All borders are wrong."
    (_,[],[])  -> Left "Rigth border and accuracy are wrong."
    ([],_,[])  -> Left "Left border and accuracy are wrong."
    ([],_,_)   -> Left "The left border is wrong."
    (_,[],_)   -> Left "The right border is wrong."
    (_,_,[])   -> Left "Accuracy is wrong."
    (_,_,_)    -> let
                    lN = (read l :: Double)
                    rN = (read r :: Double)
                    aN = (read a :: Double)
                  in if (lN >= rN)
                     then Left "Left border not smaller then right."
                     else if (aN < 0.0000000000001)
                          then Left "Accuracy too small."
                          else Right (lN, rN,aN)

drawUI :: St -> [T.Widget Name]
drawUI st = [ui]
    where
        e1 = F.withFocusRing (st^.focusRing) (E.renderEditor (str . unlines)) (st^.l)
        e2 = F.withFocusRing (st^.focusRing) (E.renderEditor (str . unlines)) (st^.r)
        e3 = F.withFocusRing (st^.focusRing) (E.renderEditor (str . unlines)) (st^.acc)
        ui = C.center $ hLimit 80 $ vBox
          [            hBox [ corner   , B.hBorder, corner   ]
          , vLimit 1 $ hBox [ B.vBorder, prompt1   , B.vBorder]
          , vLimit 4 $ hBox [ B.vBorder, prompt2   , B.vBorder]
          , vLimit 4 $ hBox [ B.vBorder, ed1      , B.vBorder]
          , vLimit 4 $ hBox [ B.vBorder, ed2      , B.vBorder]
          , vLimit 4 $ hBox [ B.vBorder, ed3      , B.vBorder]
          , vLimit 4 $ hBox [ B.vBorder, iter     , B.vBorder]
          , vLimit 4 $ hBox [ B.vBorder, dich     , B.vBorder]
          , vLimit 4 $ hBox [ B.vBorder, newt     , B.vBorder]
          ,            hBox [ corner   , B.hBorder, corner   ]
          ]
          where
          prompt1 = C.center (str "We can to find one root of \"f(x) = x - cos(x)\".")
          prompt2 = C.center (str "Press 'tab' to switch border, 'Esc' to quit.")
          ed1 = C.center (str "Print left border, please " <+> (hLimit 15 $ vLimit 1 e1))
          ed2 = C.center (str "Print right border, please " <+> (hLimit 15 $ vLimit 1 e2))
          ed3 = C.center (str "Print accuracy, please " <+> (hLimit 15 $ vLimit 1 e3))
          iter = C.center $ str $ iteration $ readEntries
            (parse $ head $ E.getEditContents(st^.l))
            (parse $ head $ E.getEditContents(st^.r))
            (parseAccuracy $ head $ E.getEditContents(st^.acc))
          dich = C.center $ str $ dichotomy $ readEntries
            (parse $ head $ E.getEditContents(st^.l))
            (parse $ head $ E.getEditContents(st^.r))
            (parseAccuracy $ head $ E.getEditContents(st^.acc))
          newt = C.center $ str $ newton $ readEntries
            (parse $ head $ E.getEditContents(st^.l))
            (parse $ head $ E.getEditContents(st^.r))
            (parseAccuracy $ head $ E.getEditContents(st^.acc))
          corner = B.joinableBorder (pure False)

appEvent :: St -> T.BrickEvent Name e -> T.EventM Name (T.Next St)
appEvent st (T.VtyEvent ev) =
  case ev of
    V.EvKey V.KEsc [] -> M.halt st
    V.EvKey (V.KChar '\t') [] -> M.continue $ st & focusRing %~ F.focusNext
    V.EvKey V.KBackTab [] -> M.continue $ st & focusRing %~ F.focusPrev

    _ -> M.continue =<< case F.focusGetCurrent (st^.focusRing) of
         Just EdL -> T.handleEventLensed st l E.handleEditorEvent ev
         Just EdR -> T.handleEventLensed st r E.handleEditorEvent ev
         Just EdAcc -> T.handleEventLensed st acc E.handleEditorEvent ev
         Nothing -> return st
appEvent st _ = M.continue st

initialState :: St
initialState =
    St (F.focusRing [EdL,EdR,EdAcc])
       (E.editor EdL Nothing "")
       (E.editor EdR Nothing "")
       (E.editor EdAcc Nothing "")

theMap :: A.AttrMap
theMap = A.attrMap V.defAttr
  [
    (E.editAttr,                   V.white `on` V.blue)
  , (E.editFocusedAttr,            V.black `on` V.yellow)
  ]

appCursor :: St -> [T.CursorLocation Name] -> Maybe (T.CursorLocation Name)
appCursor = F.focusRingCursor (^.focusRing)

theApp :: M.App St e Name
theApp =
  M.App { M.appDraw = drawUI
        , M.appChooseCursor = appCursor
        , M.appHandleEvent = appEvent
        , M.appStartEvent = return
        , M.appAttrMap = const theMap
        }

main :: IO ()
main = do
  st <- M.defaultMain theApp initialState
  putStr ""
