(load "/usr/lib/quicklisp/setup")
(declaim (sb-ext:muffle-conditions cl:warning))
(proclaim '(optimize (speed 3) (debug 0)))

(ql:quickload "cl-ppcre" :silent t)
(ql:quickload "parse-float" :silent t)

(defpackage Alg
  (:use :cl :cl-ppcre :parse-float))

(in-package Alg)

(defun match (in)
  (not (null (ppcre:scan "^-?\\d+\\.?\\d*$" in)))) ; for float number

(defun matchAcc (in)
  (not (null (ppcre:scan "^\\d+\\.?\\d*$" in)))) ; for float number

(defun putStrLn (str)
  (progn (princ str) (terpri))
)

(defun input ()
  (progn
    (putStrLn "We have equation like \"f(x) = x - cos(x)\".")
    (putStrLn "We can find one root use the interval.")
    (terpri)
    (setq l 0)
    (setq r 0)
    (loop while (>= l r)
          do ( progn
               (setq l (progn
                 (putStrLn "Please, put left border (rational).")
                 (parse-float:parse-float
                   (loop for in = (read-line)
                         unless (null (match in))
                           return in
                         do (putStrLn "You are mistaken, try again.")))))
               (setq r (progn
                 (putStrLn "Please, put right border (rational).")
                 (parse-float:parse-float
                   (loop for in = (read-line)
                         unless (null (match in))
                           return in
                         do (putStrLn "You are mistaken, try again.")))))))
    (setq acc 0)
    (loop while (<= acc 0.0000001)
          do (setq acc (progn
               (putStrLn "Please, put accuracy (rational).")
               (parse-float:parse-float
                 (loop for in = (read-line)
                       unless (null (matchAcc in))
                         return in
                       do (putStrLn "You are mistaken, try again."))))))
    (terpri)
    (list l r acc)
  )
)

(defun fx (x) ; our function
  (- x (cos x)))

; 1+sin(x) --- F'(x)
(defun fD (x) ; derivate of our function
  (+ 1 (sin x)))

; cos(x) --- function from this method
; from input F(x) = (x - cos(x))
(defun iterations (in) ; for method of iterations
  (let ((l (car in)) (r (cadr in)) (acc (caddr in)))
    (setq x (/ (+ r l) 2))
    (setq i 0)
    (loop while (>= (abs (fx x)) acc)
          do ( progn
               (setq x (cos x))
               (setq i (+ i 1))
             )
    )
    (putStrLn (concatenate 'string
                           "Iterations, answer: "
                           (write-to-string x)
                           ", number of iteration: "
                           (write-to-string i)))
  )
)

(defun dichotomy (in) ; for method of dichotomy
  (let ((l (car in)) (r (cadr in)) (acc (caddr in)))
    (setq i 0)
    (cond
      ((= (* (signum (fx l)) (signum (fx r))) 1) (putStrLn "Dichotomy method work bad in this interval."))
      (t
      (loop while (>= (abs (fx l)) acc)
            do ( progn (cond
                 ((/= (* (signum (fx l)) (signum (fx r))) 1)
                   (setq r (/ (+ r l) 2)))
                 (t
                   (progn
                     (setq r (- (* 2 r) l))
                     (setq l (/ (+ r l) 2))
                   ))
                 )
                 (setq i (+ i 1))
               )
      )
      (putStrLn (concatenate 'string
                             "Dichotomy, Answer: "
                             (write-to-string l)
                             ", number of iteration: "
                             (write-to-string i)))))
  )
)

(defun newton (in) ; for method of Newton
  (let ((l (car in)) (r (cadr in)) (acc (caddr in)))
    (setq x-old (+ x 0.002)) ; old x for optimisation check in while
    (setq x (/ (+ r l) 2))
    (setq i 0)
    (loop while (>= (abs (fx x)) acc)
          do ( progn
               (setq x-old x)
               (setq x (- x (/ (fx x) (fD x))))
               (setq i (+ i 1))
             )
    )
    (putStrLn (concatenate 'string
                           "Newton, Answer: "
                           (write-to-string x)
                           ", number of iteration: "
                           (write-to-string i)))
  )
)

(setq in (input))

(iterations in)
(terpri)
(dichotomy in)
(terpri)
(newton in)
