(load "/usr/lib/quicklisp/setup")
(proclaim '(optimize (speed 3) (debug 0)))

(ql:quickload "cl-ppcre" :silent t)
(ql:quickload "parse-float" :silent t)

(defpackage Quadratic
  (:use :cl :cl-ppcre :parse-float))

(in-package Quadratic)

(defun match (in)
  (ppcre:scan "^-?\\d+\\.?\\d*$" in))

(defun putStrLn (str)
  (progn (princ str) (terpri))
)

(defun input ()
  (progn
    (putStrLn "Please, input coeffs for \"Ax² + Bx + C\".")
    (let
      ((a (progn
        (putStrLn "Please, put coeff \"A\".")
        (loop for in = (read-line)
              unless (eq (null (match in)) t)
                return in
              do (putStrLn "You are mistaken, try again."))))
      (b (progn
        (putStrLn "Please, put coeff \"B\".")
        (loop for in = (read-line)
              unless (eq (null (match in)) t)
                return in
              do (putStrLn "You are mistaken, try again."))))
      (c (progn
        (putStrLn "Please, put coeff \"C\".")
        (loop for in = (read-line)
              unless (eq (null (match in)) t)
                return in
              do (putStrLn "You are mistaken, try again.")))))
      ;; (putStrLn (+ (parse-float:parse-float a)
      ;;             (parse-float:parse-float b)
      ;;             (parse-float:parse-float c)
      ;;             ))
      (list
       (parse-float:parse-float a)
       (parse-float:parse-float b)
       (parse-float:parse-float c))
    )
  )
)

(defun calculate (a b c)
  (let ((D (- (expt b 2) (* 4 a c))))
    (cond
      ((>= D 0) (concatenate 'string "Answer are "
        (write-to-string (/ (+ b D) -2 a)) " and "
        (write-to-string (/ (+ b (- D)) -2 a))))
      (t "There are only complex numbers in answer.")
    )
  )
)

(defun output (in)
  (let ((a (car in) ) (b (cadr in) ) (c (caddr in) ))
    (cond
      ((and (= a 0) (= b 0)) (cond ((= c 0) (putStrLn "∀x: the x will be answer.")) (t (putStrLn "There are not answers."))))
      ((= a 0) (putStrLn
               (concatenate 'string "Answer is " (write-to-string (/ c b -1)))))
      (t (putStrLn (calculate a b c)))
    )
  )
)

(output (input))
