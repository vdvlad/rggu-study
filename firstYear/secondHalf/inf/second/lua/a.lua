rex = require "rex_pcre"

function input()
  print("Print coefficients for equation is Ax² + Bx + C = 0\n")
  local first
  local second
  local third
  repeat
    print("Please enter first coeff(A).")
    first = io.read()
  until (rex.match(first, "^-?\\d+\\.?\\d*$") ~= nil)
  repeat
    print("Please enter second coeff(B).")
    second = io.read()
  until (rex.match(first, "^-?\\d+\\.?\\d*$") ~= nil)
  repeat
    print("Please enter third coeff(C).")
    third = io.read()
  until (rex.match(first, "^-?\\d+\\.?\\d*$") ~= nil)

  print("Your equation is "
        .. tostring(first)
        .. "x² + "
        .. tostring(second)
        .. "x + "
        .. tostring(third)
        .. " = 0")
  return tonumber(first), tonumber(second), tonumber(third)
end

function resolve(a,b,c)
  local D = b^2 -4*a*c
  if (D >= 0) then
    print("Answer: " .. tostring((-b-math.sqrt(D))/(2*a)) .. " and " .. tostring((-b+math.sqrt(D))/(2*a)))
    return
  end
  print("Answer is only in complex numbers.")
  return
end

function output(first,second,third)
  if (first == 0 and second == 0) then
    if (third == 0) then
      print("∀x: the x will be answer.")
      return
    end
    print("There are not answers.")
    return
  end
  if (first == 0) then
    print("Answer: "
          .. tostring(-third/second)
          .. ".")
    return
  end
  if (second == 0) then
    local answ = -third/first
    if (answ >= 0) then
      print("Answers: "
          .. tostring(math.sqrt(answ))
          .. " and "
          .. tostring(-math.sqrt(answ))
          .. ".")
      return
    else
      print("The are only complex answers.")
      return
    end
  end
  resolve(first,second,third)
end

output(input())
