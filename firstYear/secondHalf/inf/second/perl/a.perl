use Curses::UI;

$cui = Curses::UI->new( -clear_on_exit => 1
                       ,-color_support => 1);
$win = $cui->add('window_1', 'Window');

$continue = 1;
sub finish {
  $continue = 0;
}
sub equation {
  my $a = @_[0];
  my $b = @_[1];
  my $c = @_[2];

  my $D = $b**2 - 4*$a*$c;
  if ($D >= 0) {
    return "Answers : " . (-$b-sqrt($D))/(2*$a) . " and " . (-$b+sqrt($D))/(2*$a) . ".";
  }
  return "The are only complex answers.";
}
sub resolve {
  my $a = $editor1->text();
  my $b = $editor2->text();
  my $c = $editor3->text();
     $a =~ s/A : -/-/;
     $b =~ s/B : -/-/;
     $c =~ s/C : -/-/;
     $a =~ s/A : /0/;
     $b =~ s/B : /0/;
     $c =~ s/C : /0/;

  if ($a == 0 and $b == 0) {
    if ($c == 0) {
      $text = "Forall x: the x will be answer.";
    }
    else {
      $text = "There are no answer.";
    }
  } elsif ($a == 0) {
    $text = "Answer: " . (-$c/$b) . ".";
  } else {
    $text = equation($a,$b,$c);
  }

  $label2->text($text);
  $label2->draw;
  $buttons2->focus;
}

$dialog1 = $win->add(
  'message1', 'Dialog::Basic',
  -message   => "Enter coeffs for quadratic equation.\n ('a' to A, 'b' to B, 'c' to C, press 'r' to focus buttons).",
  -buttons   => undef
);
$buttons1 = $dialog1->add(
  'boxed1', 'Buttonbox',
  -y => 4,
  -buttonalignment => 'right',
  -buttons => [
    {
      -label => '< Resolve >',
      -onpress => sub {resolve;},
      -shortcut => 'R',
    },{
      -label => '< Quit >',
      -onpress => sub {finish;},
      -shortcut => 'Q'
      }
  ]
);

$editor1 = $dialog1->add(
    'editor1', 'TextEditor',
    -y => 4,
    -maxlines => 1,
    -maxlength => 10,
    -text => 'A : ',
    -pos => 4,
    -regexp   => '/^A : -?\d+\.?\d*$/',
    -width => 10,
    );
$editor2 = $dialog1->add(
    'editor2', 'TextEditor',
    -y => 4,
    -maxlines => 1,
    -maxlength => 10,
    -text => 'B : ',
    -pos => 4,
    -regexp   => '/^B : -?\d+\.?\d*$/',
    -width => 10,
    );
$editor3 = $dialog1->add(
    'editor3', 'TextEditor',
    -y => 4,
    -maxlines => 1,
    -maxlength => 10,
    -text => 'C : ',
    -pos => 4,
    -regexp   => '/^C : -?\d+\.?\d*$/',
    -width => 10,
    );

$editor1->focus();

$cui->set_binding(sub {$buttons1->focus()}, "r");
$cui->set_binding(sub {$editor1->focus()}, "a");
$cui->set_binding(sub {$editor2->focus()}, "b");
$cui->set_binding(sub {$editor3->focus()}, "c");

$win2 = $cui->add('window_2', 'Window');
$dialog2 = $win2->add(
  'message2', 'Dialog::Basic',
  -message   => "Your answers:                              ",
  -buttons   => undef
);
$buttons2 = $dialog2->add(
 'boxed2', 'Buttonbox',
 -y => 4,
 -buttonalignment => 'right',
 -buttons => [
   {
     -label => '< Again >',
     -onpress => sub {$buttons1->focus},
     -shortcut => 'A',
   },{
     -label => '< Quit >',
     -onpress => sub {$continue = 0;},
     -shortcut => 'Q'
     }
 ]
);

$label2 = $dialog2->add(
    'label2', 'Label',
    -y         => 3,
    -text      => "Chuck",
    -bold      => 1,
    -width     => 50
    );

$editor1->focus;

while ($continue) {
    $cui->focus(undef, 1);
    $cui->draw;

    $cui->do_one_event;
}
