#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <pcre.h>

char* getline(void) {
  char *line = malloc(100), *linep = line;
  size_t lenmax = 100, len = lenmax;
  int c;

  c = fgetc(stdin);
  while (c != EOF && c != '\n') {
    if(--len == 0) {
      len = lenmax;
      char * linen = realloc(linep, lenmax *= 2);
      line = linen + (line - linep);
      linep = linen;
    }

    *line++ = c;
    c = fgetc(stdin);
  }
  *line = '\0';
  return linep;
}

void resolve(const int a,const int b,const int c) {
  double D = b*b - 4*a*c;
  if (D >= 0) {
    printf("Answer : %f and %f\n"
                , (-b-sqrt(D))/(2*a)
                , (-b+sqrt(D))/(2*a));
  } else {
    printf("Only complex\n");
  }
}

int main() {
  printf("Print coefficients for equation is Ax² + Bx + C = 0\n");

  double a,b,c;
  char* as;
  char* bs;
  char* cs;

  char re[]  = "^-?\\d+\\.?\\d*$";
  pcre* pcRe;
  {
    const char* error;
    int erroroffset;
    pcRe = pcre_compile(re, 0, &error, &erroroffset, NULL);
  }

  printf("Print coeff A\n");
  as = getline();
  int count = pcre_exec(pcRe, NULL, as, strlen(as), 0, 0, NULL, 0);
  while (count < 0) {
    printf("You are wrong. Try again input A.\n");
    as = getline();
    count = pcre_exec(pcRe, NULL, as, strlen(as), 0, 0, NULL, 0);
  }
  a = atof(as);

  printf("Print coeff B\n");
  bs = getline();
  count = pcre_exec(pcRe, NULL, bs, strlen(bs), 0, 0, NULL, 0);
  while (count < 0) {
    printf("You are wrong. Try again input B.\n");
    bs = getline();
    count = pcre_exec(pcRe, NULL, as, strlen(as), 0, 0, NULL, 0);
  }
  b = atof(bs);

  printf("Print coeff C\n");
  cs = getline();
  count = pcre_exec(pcRe, NULL, cs, strlen(cs), 0, 0, NULL, 0);
  while (count < 0) {
    printf("You are wrong. Try again input C.\n");
    cs = getline();
    count = pcre_exec(pcRe, NULL, cs, strlen(cs), 0, 0, NULL, 0);
  }
  c = atof(cs);

  if (a == 0 && b == 0) {
    if (c == 0) {
      printf("This is any numbers.\n");
    } else {
      printf("This are no answers.\n");
    }
  } else if (a == 0) {
    printf("Answer: %f\n", (-c/b));
  } else {
    resolve(a,b,c);
  }

  free(as);
  free(bs);
  free(cs);
  pcre_free(pcRe);

  return 0;
}
