package progJava;

import java.util.regex.Pattern;
import java.util.Scanner;

import progJava.Quadratic;

class Prog {
  public static Quadratic input() {
    Scanner line = new Scanner(System.in);
    String s = "";
    Quadratic q = new Quadratic();

    do {
      System.out.println("Print coeff \"a\".");
      s = line.nextLine();
    }
    while (!Pattern.matches("^-?\\d+(\\.\\d)?\\d*$",s));
    q.a_ = Integer.parseInt(s);

    do {
      System.out.println("Print coeff \"b\".");
      s = line.nextLine();
    }
    while (!Pattern.matches("^-?\\d+(\\.\\d)?\\d*$",s));
    q.b_ = Integer.parseInt(s);

    do {
      System.out.println("Print coeff \"c\".");
      s = line.nextLine();
    }
    while (!Pattern.matches("^-?\\d+(\\.\\d)?\\d*$",s));
    q.c_ = Integer.parseInt(s);

    return q;
  }

  public static void resolve(final Quadratic q) {
    double D = q.b_*q.b_ - 4*q.a_*q.c_;
    if (D >= 0) {
      System.out.println(
                "Answer : "
                + (-q.b_-Math.sqrt(D))/(2*q.a_)
                + " and "
                + (-q.b_+Math.sqrt(D))/(2*q.a_));
    } else {
      System.out.println("Only complex");
    }
  }

  public static void main(String[] args) {
    System.out.println("Print coefficients for equation is Ax² + Bx + C = 0");
    Quadratic q = input();

    if (q.a_ == 0 && q.b_ == 0) {
      if (q.c_ == 0) {
        System.out.println("This is any numbers.");
      } else {
        System.out.println("This are no numbers.");
      }
    } else if (q.a_ == 0) {
      System.out.println("Answer: " + (-q.c_ / q.b_));
    } else {
      resolve(q);
    }

  }
}
