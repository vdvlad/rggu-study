package progJava;

class Quadratic {
  public double a_;
  public double b_;
  public double c_;

  public Quadratic() {
    this.a_ = 0;
    this.b_ = 0;
    this.c_ = 0;
  }

  public Quadratic(final Quadratic d) {
    this.a_ = d.a_;
    this.b_ = d.b_;
    this.c_ = d.c_;
  }
}
