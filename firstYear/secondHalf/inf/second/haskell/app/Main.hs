{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE FlexibleContexts #-}
module Main where

import Lens.Micro
import Lens.Micro.TH
import qualified Graphics.Vty as V

import qualified Brick.Main as M
import qualified Brick.Types as T
import Brick.Widgets.Core
  ( (<+>)
  , (<=>)
  , hLimit
  , vLimit
  , str
  , hBox
  , vBox
  )
import qualified Brick.Widgets.Center as C
import qualified Brick.Widgets.Edit as E
import qualified Brick.Widgets.Border as B
import qualified Brick.AttrMap as A
import qualified Brick.Focus as F
import Brick.Util (on)

import Data.Maybe (fromMaybe)

import Text.Regex.PCRE.Heavy (scan,re)

data Name = EdA
          | EdB
          | EdC
          deriving (Ord, Show, Eq)

data St =
    St { _focusRing :: F.FocusRing Name
       , _a :: E.Editor String Name
       , _b :: E.Editor String Name
       , _c :: E.Editor String Name
       }

makeLenses ''St

parse :: String -> String
parse input
  | (null input) = ""
  | otherwise =
    let s = scan [re|(^-?\d+\.?\d*$)|] input
    in if (null s)
       then ""
       else let a = fst $ head s
            in if (last a == '.')
               then a ++ "0"
               else a

quadratic :: Float -> Float -> Float -> String
quadratic a b c =
  let d = b**2 - 4*a*c
  in if (d >= 0)
     then "Answers: " ++ show ((-b-sqrt d)/(2*a))
          ++ " and " ++ show ((-b+sqrt d)/(2*a)) ++ "."
     else "The are only complex answers"

resolve :: Float -> Float -> Float -> String
resolve a b c
  | (a == 0 && b == 0) = if (c == 0)
                         then "∀x: the x will be answer." ++ show c
                         else "There are not answers."
  | (a == 0) = "Answer: " ++ show (-c/b)
  | otherwise = quadratic a b c

calculate :: String -> String -> String -> String
calculate a b c =
  case (a,b,c) of
    ([],[],[]) -> "All coeffs are wrong."
    ([],[],_)  -> "The first and the second coeffs are wrong."
    ([],_,[])  -> "The first and the third coeffs are wrong."
    (_,[],[])  -> "The second and the third coeffs are wrong."
    (_,_,[])   -> "The third coeff is wrong."
    (_,[],_)   -> "The second coeff is wrong."
    ([],_,_)   -> "The first coeff is wrong."
    (_,_,_)    -> resolve
                    (read a :: Float)
                    (read b :: Float)
                    (read c :: Float)

drawUI :: St -> [T.Widget Name]
drawUI st = [ui]
    where
        e1 = F.withFocusRing (st^.focusRing) (E.renderEditor (str . unlines)) (st^.a)
        e2 = F.withFocusRing (st^.focusRing) (E.renderEditor (str . unlines)) (st^.b)
        e3 = F.withFocusRing (st^.focusRing) (E.renderEditor (str . unlines)) (st^.c)
        ui = C.center $ hLimit 80 $ vBox
          [            hBox [ corner   , B.hBorder, corner   ]
          , vLimit 1 $ hBox [ B.vBorder, prompt1   , B.vBorder]
          , vLimit 4 $ hBox [ B.vBorder, prompt2   , B.vBorder]
          , vLimit 4 $ hBox [ B.vBorder, ed1      , B.vBorder]
          , vLimit 4 $ hBox [ B.vBorder, ed2      , B.vBorder]
          , vLimit 4 $ hBox [ B.vBorder, ed3      , B.vBorder]
          , vLimit 4 $ hBox [ B.vBorder, func     , B.vBorder]
          ,            hBox [ corner   , B.hBorder, corner   ]
          ]
          where
          prompt1 = C.center (str "Print coeffs for Ax² + Bx + C = 0.")
          prompt2 = C.center (str "Press 'tab' to switch coeff, 'Esc' to quit.")
          ed1 = C.center (str "Print coeff A, please " <+> (hLimit 10 $ vLimit 1 e1))
          ed2 = C.center (str "Print coeff B, please " <+> (hLimit 10 $ vLimit 1 e2))
          ed3 = C.center (str "Print coeff C, please " <+> (hLimit 10 $ vLimit 1 e3))
          func = C.center $ str $ calculate
            (parse $ head $ E.getEditContents(st^.a))
            (parse $ head $ E.getEditContents(st^.b))
            (parse $ head $ E.getEditContents(st^.c))
          corner = B.joinableBorder (pure False)

appEvent :: St -> T.BrickEvent Name e -> T.EventM Name (T.Next St)
appEvent st (T.VtyEvent ev) =
  case ev of
    V.EvKey V.KEsc [] -> M.halt st
    V.EvKey (V.KChar '\t') [] -> M.continue $ st & focusRing %~ F.focusNext
    V.EvKey V.KBackTab [] -> M.continue $ st & focusRing %~ F.focusPrev

    _ -> M.continue =<< case F.focusGetCurrent (st^.focusRing) of
         Just EdA -> T.handleEventLensed st a E.handleEditorEvent ev
         Just EdB -> T.handleEventLensed st b E.handleEditorEvent ev
         Just EdC -> T.handleEventLensed st c E.handleEditorEvent ev
         Nothing -> return st
appEvent st _ = M.continue st

initialState :: St
initialState =
    St (F.focusRing [EdA,EdB,EdC])
       (E.editor EdA Nothing "")
       (E.editor EdB Nothing "")
       (E.editor EdC Nothing "")

theMap :: A.AttrMap
theMap = A.attrMap V.defAttr
  [
    (E.editAttr,                   V.white `on` V.blue)
  , (E.editFocusedAttr,            V.black `on` V.yellow)
  ]

appCursor :: St -> [T.CursorLocation Name] -> Maybe (T.CursorLocation Name)
appCursor = F.focusRingCursor (^.focusRing)

theApp :: M.App St e Name
theApp =
  M.App { M.appDraw = drawUI
        , M.appChooseCursor = appCursor
        , M.appHandleEvent = appEvent
        , M.appStartEvent = return
        , M.appAttrMap = const theMap
        }

main :: IO ()
main = do
  st <- M.defaultMain theApp initialState
  putStr ""
