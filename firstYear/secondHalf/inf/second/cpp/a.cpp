#include <math.h>
#include <string>
#include <iostream>
#include <pcrecpp.h>

void resolve(const int a,const int b,const int c) {
  double D = b*b - 4*a*c;
  if (D >= 0) {
    std::cout << "Answer : "
              << (-b-std::sqrt(D))/(2*a)
              << " and "
              << (-b+std::sqrt(D))/(2*a)
              << std::endl;
  } else {
    std::cout << "Only complex" << std::endl;
  }
}

int main() {
  std::cout << "Print coefficients for equation is Ax² + Bx + C = 0" << std::endl;

  double a,b,c;
  std::string as,bs,cs;
  pcrecpp::RE re("(-?\\d+\\.?\\d*)");

  std::cout << "Print coeff A" << std::endl;
  std::getline(std::cin, as);
  while (!re.FullMatch(as, &a)) {
    std::cout << "You are wrong. Try again input A." << std::endl;
    std::getline(std::cin, as);
  }

  std::cout << "Print coeff B" << std::endl;
  std::getline(std::cin, bs);
  while (!re.FullMatch(bs, &b)) {
    std::cout << "You are wrong. Try again input B." << std::endl;
    std::getline(std::cin, bs);
  }

  std::cout << "Print coeff C" << std::endl;
  std::getline(std::cin, cs);
  while (!re.FullMatch(cs, &c)) {
    std::cout << "You are wrong. Try again input C." << std::endl;
    std::getline(std::cin, cs);
  }

  if (a == 0 && b == 0) {
    if (c == 0) {
      std::cout << "This is any numbers." << std::endl;
    } else {
      std::cout << "This are no numbers." << std::endl;
    }
  } else if (a == 0) {
    std::cout << "Answer: " << (-c/b) << std::endl;
  } else {
    resolve(a,b,c);
  }
  return 0;
}
