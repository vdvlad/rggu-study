module disc

contains

subroutine input (first, second, third)
  Real :: first, second, third
  Character (len = 68) :: form

  write (*,*) "Print coefficients for equation is Ax² + Bx + C = 0"
  write (*,*)
  write (*,*) "Please enter first coeff(A)."
  read  (*,*) first
  write (*,*) "Please enter first coeff(B)."
  read  (*,*) second
  write (*,*) "Please enter first coeff(C)."
  read  (*,*) third

  form =  '("Your equation is ", F12.3, "x² + ", F12.3, "x + ", F12.3, " = 0")'
  write (*,form) first, second, third
  write (*,*)
end subroutine

subroutine resolve (a, b, c)
  Real :: a, b, c
  Real :: D

  D = b**2 - 4*a*c
  if (D >= 0) then
     write (*,*) "Answer: ", ((-b-sqrt(D))/(2*a)), " and ", ((-b+sqrt(D))/(2*a))
     return
  end if
  write (*,*) "Answer is only in complex numbers."
  return
end subroutine

subroutine equation (first, second, third)
  Real :: first, second, third
  Real :: answ

  if (first == 0 .and. second == 0) then
    if (third == 0) then
      write (*,*) "∀x: the x will be answer."
      return
    end if
    write (*,*) "There are not answers."
    return
  end if
  if (first == 0) then
    write (*,*) "Answer: ", (-third/second)
    return
  end if
  call resolve(first,second,third)
end subroutine

end module

program Date

use disc;

Real            :: first,second,third

call input(first, second, third)
call equation (first, second, third)

end program
