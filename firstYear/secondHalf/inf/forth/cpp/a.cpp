#include <math.h>
#include <string>
#include <iostream>
#include <pcrecpp.h>

double myPow (const double n, const double o = 1) {
  return exp(log(n) * o);
}

// n in i
double fastPow (const double n, const int i) {
  return (i>0) ? n*fastPow(n,i-1) : 1;
}

// calculate element

inline double naive(const int n) {
  return myPow((sqrt(n)+2)/(sqrt(n)+3),
               myPow(n,3./2));
}

inline double optimazed(const int n) {
  const double l = sqrt(n);
  return myPow(1 - 1/(l + 3),fastPow(l,3));
}

// calculate sum

double summ(const int n, double (*f)(int) ) {
  double sum = 0;
  for (int i = 1; i <= n; ++i) {
    sum += (*f)(i);
  }
  return sum;
}

inline double sumNaive(const int n) {
  return summ(n,&naive);
}

inline double sumOptimazed(const int n) {
  return summ(n,&optimazed);
}

inline double sumApprox(const int n, const int o = -1) {
  if (o == -1 && n > 4)
    return summ(int(n/2),&optimazed);
  if (n < o || o == -1)
    return summ(n,&optimazed);
  return summ(o,&optimazed);
}

int read() {
  int i;
  std::string s;
  pcrecpp::RE re("(-?\\d+)");

  std::cout << "Please, put \"n\" for particular sum." << std::endl;
  std::getline(std::cin, s);
  while (!re.FullMatch(s, &i)) {
    std::cout << "You are wrong. Try again input \"n\"." << std::endl;
    std::getline(std::cin, s);
  }
  std::cout << std::endl;
  return i;
}

void calc() {
  int i = read();

  std::cout.precision(20);
  std::cout << "Naive calculate." << std::endl;
  std::cout << sumNaive(i) << std::endl;
  std::cout << std::endl;
  std::cout << "Optimazed calculation." << std::endl;
  std::cout << sumOptimazed(i) << std::endl;
  std::cout << std::endl;
  std::cout << "Approximated calculation." << std::endl;
  std::cout << sumApprox(i) << std::endl;
  std::cout << std::endl;
}

int main() {
  std::cout << "We have series like \"((sqrt(n)+2)/(sqrt(n)+3))^(n^(3/2))\"." << std::endl;
  std::cout << "We can find particular sum." << std::endl;
  std::cout << "You can input three times.\n" << std::endl;

  calc();
  calc();
  calc();

  return 0;
}
