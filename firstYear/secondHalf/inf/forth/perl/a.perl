use 5.010;

sub myPow {
  my $n = shift;
  my $o = shift;
  $o = 1 if !defined($o);
  return exp(log($n) * $o);
}

# n in i
sub fastPow {
  my $n = shift;
  my $i = shift;
  return ($i>0) ? $n*fastPow($n,$i-1) : 1;
}

# calculate element

sub naive {
  my $n = shift;
  return myPow((sqrt($n)+2)/(sqrt($n)+3),
               myPow($n,3/2));
}

sub optimazed {
  my $l = sqrt(shift);
  return myPow(1 - 1/($l + 3),fastPow($l,3));
}

# calculate sum

sub summ {
  my $n = shift;
  my $f = shift;

  my $sum = 0;
  for (my $i = 1; $i <= $n; ++$i) {
    $sum += $f->($i);
  }
  return $sum;
}

sub sumNaive {
  my $n = shift;
  return summ($n,sub{naive @_[0]});
}

sub sumOptimazed {
  my $n = shift;
  return summ($n,sub{optimazed @_[0]});
}

sub sumApprox {
  my $n = shift;
  my $o = shift;

  my $f = sub{optimazed @_[0]};

  if ((!defined $o) && $n > 4) {
    return summ(($n - ($n % 2))/2,$f);
  }
  if ($n < $o || (!defined $o)) {
    return summ($n,$f);
  }
  return summ($o,$f);
}

sub readInp {
  $s = <>;
  while ($s !~ /\d+/) {
    say "You are wrong. Try again input \"n\".\n";
    $s = <>;
  }
  return $s;
}

sub calc {
  say "Input number please.";
  my $i = readInp();

  say "Naive calculate.";
  say sumNaive($i);
  say;
  say "Optimazed calculation.";
  say sumOptimazed($i);
  say;
  say "Approximated calculation.";
  say sumApprox($i);
  say;
}

sub main {
  say "We have series like \"((sqrt(n)+2)/(sqrt(n)+3))^(n^(3/2))\".";
  say "We can find particular sum.";
  say "You can input three times.\n";

  calc();
  calc();
  calc();
}

main();
