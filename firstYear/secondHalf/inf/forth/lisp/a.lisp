(load "/usr/lib/quicklisp/setup")
(declaim (sb-ext:muffle-conditions cl:warning))
(proclaim '(optimize (speed 3) (debug 0)))

(ql:quickload "cl-ppcre" :silent t)
(ql:quickload "parse-float" :silent t)

(defpackage Alg
  (:use :cl :cl-ppcre :parse-float))

(in-package Alg)

(defun match (in)
  (not (null (ppcre:scan "^\\d+" in))))

(defun putStrLn (str)
  (princ str) (terpri)
)

(defun input ()
  (putStrLn "We have series like \"((sqrt(n)+2)/(sqrt(n)+3))^(n^(3/2))\".")
  (putStrLn "We can find particular sum.")
  (putStrLn "You can input three times.")
  (terpri)
  (progn
    (putStrLn "Please, put \"n\" for particular sum.")
    (parse-float:parse-float
      (loop for in = (read-line)
            unless (null (match in))
              return in
            do (putStrLn "You are mistaken, try again."))))
)

(defun fastPow(x n)
  (cond
    ((= n 0) 1)
    (t (* x (fastPow x (- n 1))))))

(defun naive(n)
  (expt (/ (+ (sqrt n) 2) (+ (sqrt n) 3))
        (expt n (/ 3 2))))

(defun optimized(n)
  (setq x (sqrt n))
  (expt (- 1 (/ 1 (+ x 3)))
        (fastPow x 3)))

(defun summ (n f)
  (cond
    ((= n 0) 0)
    (t (+ (funcall f n) (summ (- n 1) f)))))

(defun sumNaive(n)
  (summ n #'naive))

(defun sumOptimazed(n)
  (summ n #'optimized))

(defun sumApprox(n &optional o)
  (cond
    ((and (null o) (> n 4)) (summ (floor n 2) #'optimized))
    ((and (not (null o)) (< o n)) (summ o #'optimized))
    (t (summ n #'optimized))
  )
)

(defun calc(i)
  (terpri)
  (putStrLn "Naive calculate.")
  (putStrLn (sumNaive i))
  (terpri)
  (putStrLn "Optimazed calculation.")
  (putStrLn (sumOptimazed i))
  (terpri)
  (putStrLn "Approximated calculation.")
  (putStrLn (sumApprox i))
  (terpri)
)

(calc (input))
(calc (input))
(calc (input))
