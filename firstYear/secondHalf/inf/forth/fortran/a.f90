module progr
  use, intrinsic :: iso_c_binding
  use pcre_module
  use iso_fortran_env, only : IOSTAT_EOR

  abstract interface
     function allSum(n) result(res)
       Integer :: n
       Real    :: res
     end function
  end interface

contains

  subroutine readNumber(inp)
    Integer :: io_number

    Character(len = 1) :: buffer
    Character(len = :), allocatable :: inp

    inp = ""
    do
        read(unit = *, fmt = '(a)', advance = "no", iostat = io_number) buffer
        select case (io_number)
        case(0)
          inp = inp // buffer
        case(IOSTAT_EOR)
          exit
        end select
    end do

  end subroutine

  Real function checkNumber(pcre)
    Type(pcre_type) :: pcre
    Character(len = :), allocatable :: inp

    integer, dimension(0:1) :: ovector

    do
        call readNumber(inp)
        if (pcre_exec(pcre, c_null_ptr, inp, 0, 0, ovector) >= 0) exit
        write (*,*) "Sorry you are wrong."
    end do

    read (inp,*) checkNumber
  end function

  Integer function input() result(n)
    Type(pcre_type) :: regex
    Character(len=*), parameter :: pattern &
        =  "^\d+$"


    regex = pcre_compile(pattern, 0)

    write (*,*) "Please enter 'n'."
    n = checkNumber(regex)
  end function


  ! Pows
  Real function myPow (n, o)
    Real           :: n
    Real           :: o
    myPow = exp(log(n) * o)
  end function

  recursive function fastPow (n,i) result(fp)
    Real           :: n
    Integer        :: i

    Real           :: fp

    if (i>0) then
      fp = n*fastPow(n,i-1)
    else
      fp = 1
    end if
  end function

  ! calculate element
  Real function naive(n)
    Integer :: n
    Real    :: l
    l = n
    naive = myPow((sqrt(l)+2)/(sqrt(l)+3),myPow(l,3/2.))
  end function

  ! calculate element
  Real function optimized(n)
    Integer :: n
    Real    :: l
    l = sqrt(Real(n))
    optimized = myPow(1-1./(l+3),fastPow(l,3))
  end function


  Real function summ(n,f)
    Integer                    :: n
    Procedure(allSum), Pointer :: f

    Integer                    :: i

    i  = 1
    summ = 0
    do
      if (i > n) exit
      summ = summ + f(i)
      i = i + 1
    end do
  end function

  Real function sumNaive(n)
    Integer :: n
    Procedure(allSum), Pointer :: f
    f => naive
    sumNaive = summ(n,f)
  end function

  Real function sumOptimized(n)
    Integer :: n
    Procedure(allSum), Pointer :: f
    f => optimized
    sumOptimized = summ(n,f)
  end function

  Real function sumApproximated(n,o)
    Integer           :: n
    Integer, optional :: o
    Procedure(allSum), Pointer :: f

    Integer :: opt
    if (present(o)) then
       opt = o
    else
       opt = Int(n/2)
    end if

    f => optimized

    if (n < 5 .or. n > opt) then
      sumApproximated = summ(n,f)
    else
      sumApproximated = summ(opt,f)
    end if
  end function

  subroutine calc()
    Integer :: n
    Procedure(allSum), Pointer :: f

    n = input()

    write (*,*) "Naive calculate."
    write (*,*) sumNaive(n)
    write (*,*)
    write (*,*) "Optimazed calculation."
    write (*,*) sumOptimized(n)
    write (*,*)
    write (*,*) "Approximated calculation."
    write (*,*) sumApproximated(n)
    write (*,*)
  end subroutine

end module

program Date
  use progr

  write (*,*) 'We have series like "((sqrt(n)+2)/(sqrt(n)+3))^(n^(3/2))".'
  write (*,*) "We can particular sum."
  write (*,*) "You can input three times"

  call calc()
  call calc()
  call calc()

end program
