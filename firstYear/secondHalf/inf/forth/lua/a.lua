rex = require "rex_pcre"

function input()
  print("Please, enter \"n\".")
  local n = io.read()

  while (rex.match(n, "^\\d+$") == nil) do
    print("You are wrong, please, try again.")
    n = io.read()
  end

  return tonumber(n)
end


function myPow (n, o)
  return math.exp(math.log(n) * o)
end

function fastPow (n,i)
  if (i>0) then
    return n*fastPow(n,i-1)
  end
  return 1
end

function naive(n)
  return myPow((math.sqrt(n)+2)/(math.sqrt(n)+3),myPow(n,3/2))
end

-- calculate element
function optimized(n)
  local l = math.sqrt(n)
  return myPow(1-1./(l+3),fastPow(l,3))
end


function summ(n,f)
  local i  = 1
  local sum = 0
  while (i <= n) do
    sum = sum + f(i)
    i = i + 1
  end
  return sum
end

function sumNaive(n)
   return summ(n,naive)
end

function sumOptimized(n)
  return summ(n,optimized)
end

function sumApproximated(n,o)
  if (o ~= nil) then
     opt = o
  else
     opt = math.floor(n,2)
  end

  if (n < 5 or n > opt) then
    return summ(n,optimized)
  else
    return summ(opt,optimized)
  end
end

function calc()
  local n = input()

  print("Naive calculate.")
  print(sumNaive(n))
  print()
  print("Optimazed calculation.")
  print(sumOptimized(n))
  print()
  print("Approximated calculation.")
  print(sumApproximated(n))
  print()
end

print('We have series like "((sqrt(n)+2)/(sqrt(n)+3))^(n^(3/2))".')
print("We can particular sum.")
print("You can input three times")

calc()
calc()
calc()
