#include <math.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>

#include <pcre.h>

char* getLine() {
  char *line = (char*)malloc(100), *linep = line;
  size_t lenmax = 100, len = lenmax;
  int c;

  c = fgetc(stdin);
  while (c != EOF && c != '\n') {
    if(--len == 0) {
      len = lenmax;
      char * linen = (char*)realloc(linep, lenmax *= 2);
      line = linen + (line - linep);
      linep = linen;
    }

    *line++ = c;
    c = fgetc(stdin);
  }
  *line = '\0';
  return linep;
}

double myPow (const double n, const double o) {
  return exp(log(n) * o);
}

// n in i
double fastPow (const double n, const int i) {
  return (i>0) ? n*fastPow(n,i-1) : 1;
}

// calculate element

inline double naive(const int in) {
  double n = (double)in;
  return myPow((sqrt(n)+2)/(sqrt(n)+3),
               myPow(n,3./2));
}

inline double optimazed(const int n) {
  const double l = sqrt((double)n);
  return myPow(1 - 1/(l + 3),fastPow(l,3));
}

// calculate sum

double summ(const int n, double (*f)(int) ) {
  double sum = 0;
  for (int i = 1; i <= n; ++i) {
    /* sum += (*f)(i); */
    sum += naive(i);
  }
  return sum;
}

inline double sumNaive(const int n) {
  return summ(n,&naive);
}

inline double sumOptimazed(const int n) {
  return summ(n,&optimazed);
}

inline double sumApprox(const int n, const int o) {
  if (o == -1 && n > 4)
    return summ((int)(n/2),&optimazed);
  if (n < o || o == -1)
    return summ(n,&optimazed);
  return summ(o,&optimazed);
}

int read() {
  int i;
  char* s;
  char re[]  = "^\\d+$";

  pcre* pcRe;
  {
    const char* error;
    int erroroffset;
    pcRe = pcre_compile(re, 0, &error, &erroroffset, NULL);
  }

  printf("Please, put \"n\" for particular sum.\n");
  s = getLine();
  int count = pcre_exec(pcRe, NULL, s, strlen(s), 0, 0, NULL, 0);
  while (count < 0) {
    printf("You are wrong. Try again input \"n\"\n.");
    free(s);
    s = getLine();
    count = pcre_exec(pcRe, NULL, s, strlen(s), 0, 0, NULL, 0);
  }

  i = atof(s);
  free(s);
  return i;
}

void calc() {
  int i = read();

  printf("Naive calculate.\n");
  printf("%f\n\n",sumNaive(i));
  printf("Optimazed calculation.\n");
  printf("%f\n\n",sumOptimazed(i));
  printf("Approximated calculation.\n");
  printf("%f\n\n",sumApprox(i,-1));
}

int main() {
  printf("We have series like \"((sqrt(n)+2)/(sqrt(n)+3))^(n^(3/2))\".\n");
  printf("We can find particular sum.\n");
  printf("You can input three times.\n\n");

  calc();
  calc();
  calc();

  return 0;
}
