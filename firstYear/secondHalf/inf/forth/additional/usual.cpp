#include <sys/ioctl.h>
#include <unistd.h>
#include <iostream>
#include <sstream>

#include <string>
#include <vector>
#include <pcrecpp.h>

struct winsize size;
typedef std::vector<std::vector<long double> > Matrix;

std::string duplicate(const char c, int n) {
  std::string s = std::string(1,c);
  for (; n > 1; --n) {
    s += c;
  }
  return s;
}

std::string center(const std::string s) {
  return duplicate(' ',(size.ws_col/2)-(s.size()/2)) + s;
}

void centerWr(const std::string s) {
  std::cout << center(s) << std::endl;
}

void getMatrix (Matrix& matrix, int n) {
  matrix.resize(n);
  for (int i = 0; i < matrix.size(); ++i) {
    matrix[i].resize(n);

    if (matrix[i].size()) {
      matrix[i][0] = 1;
    }

    if (i) {
      matrix[i][0] = matrix[i-1][0]/(i+1);
    } else {
      matrix[0][0] = 1;
    }
    for (int j = 1; j < matrix[i].size(); ++j) {
      matrix[i][j] = matrix[i][j-1] * matrix[i][0];
    }
  }
}

void printMatrix (Matrix& matrix, int n) {
  std::cout.setf(std::ios::scientific);
  std::cout.precision((size.ws_col - 2.-n)/(n)-6);

  for (int i = 0; i < matrix.size(); ++i) {
    std::cout << ' ';
    for (int j = 0; j < matrix[i].size(); ++j) {
      std::cout << matrix[i][j];
      std::cout << ' ';
    }
    std::cout << std::endl;
  }
}

int read() {
  pcrecpp::RE re(" *(\\d+) *");
  std::string s;
  int result;

  do {
    centerWr("Please input natural number.");
    std::cout << center("");
  std::getline(std::cin, s);
  }
  while (!re.FullMatch(s, &result));

  return result;
}

int main() {
  ioctl(STDOUT_FILENO, TIOCGWINSZ, &size);

  centerWr("Enter size of quadratic matrix, please.");

  int n = read();
  while (n == 0) {
    n = read();
  }

  Matrix matrix;
  getMatrix(matrix,n);

  if ((size.ws_col - 2.-n)/(n)-6 >= -1) {
    printMatrix(matrix,n);
  } else {
    centerWr("Error, too wide matrix.");
  }

  return 0;
}
