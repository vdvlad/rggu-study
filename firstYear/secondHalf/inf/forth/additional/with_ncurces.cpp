#include <ncurses.h>
#include <vector>

#include <string>
#include <sstream>

typedef std::vector<std::vector<double> > Matrix;

const int maxNumberAcc() {
  return 5;
}

int fact (const int n) {
  int acc = 1;
  for (int i = 2; i <= n; ++i) {
    acc *= i;
  }
  return acc;
}

void getMatrix (Matrix& matrix, int n) {
  matrix.resize(n);
  for (int i = 0; i < matrix.size(); ++i) {
    matrix[i].resize(n);

    double acc = 1.;
    if (matrix[i].size()) {
      matrix[i][0] = 1;
    }
    for (int j = 0; j < matrix[i].size(); ++j) {
      acc = acc / fact(i+1);
      matrix[i][j] = acc;
    }
  }
}

void printMenu (WINDOW*& win, const int& height, const int& width, const Matrix& matrix) {
  if (win == nullptr) {
    win = newwin(height,width,2,5);
  } else {
    resize_term(height+2+10,width+10);
    delwin(win);
    clear();
    win = newwin(height,width,2,5);
  }
  box(win,0,0);
  refresh();

  box(win,0,0);
  touchwin(win);
  wrefresh(win);

  std::string s = "Enter size of quadratic matrix, please.";
  wmove(win, height/4, width/2 - (int)(s.size()/2));
  wprintw(win,s.c_str());
  s = "Press numbers to input, 'q' to quit, arrows for move from numbers.";
  wmove(win, height/4 + 1, width/2 - (int)(s.size()/2));
  wprintw(win,s.c_str());

  wrefresh(win);
}

void clear (WINDOW*& win, const int& height, const int& width) {
  std::string tmp = "";
  for (int i = 0; i < width-4; ++i) tmp += " ";
  for (int i = 0; i < height*3/8 - 4; ++i) {
    wmove(win, height/4 + 8 + 2*i, -1 +width - tmp.size());
    wprintw(win,tmp.c_str());
  }
}

void printMatrix (WINDOW*& win, const int& height, const int& width, const Matrix& matrix) {
  clear(win,height,width);
  std::string raw = "";

  int large = (int)((width-4)/(double)(matrix.size())) - 6;

  for (int i = 0; i < matrix.size(); ++i) {
    raw = "";
    for (int j = 0; j < matrix[i].size(); ++j) {
      std::ostringstream strS;
      strS.precision(large);
      strS << matrix[i][j];
      raw = strS.str();
      wmove(win, height/4 + 8 + 2*i, 4+j*(large+6));
      wprintw(win,raw.c_str());
    }
  }
}

void printError (WINDOW*& win, const int& height, const int& width) {
  clear(win,height,width);
  std::string error = "This dimention too large to display on this monitor.";
  wmove(win, height/4 + 8, width/2 - (int)(error.size()/2.));
  wprintw(win,error.c_str());
}

void getChar (WINDOW*& win, Matrix& matrix) {
  int height;
  int width;
  getmaxyx(stdscr,height,width);
  int heightWin = height-2-4;
  int widthWin  = width-10;
  printMenu(win,heightWin,widthWin,matrix);
  printMatrix(win,heightWin,widthWin,matrix);
  int terY = 0;
  int terX = 0;

  std::vector<int> matSize {0,0};
  wmove(win, heightWin/4 + 5, widthWin/2 - 2);
  wprintw(win,std::to_string(matSize[0]).c_str());
  wmove(win, heightWin/4 + 5, widthWin/2 - 1);
  wprintw(win,std::to_string(matSize[1]).c_str());

  wmove(win, heightWin/4 + 5, widthWin/2 - 2);

  unsigned int pos = 0;
  int ch;
  refresh();
  wrefresh(win);
  while((ch = getch()) != 'q') {
    getmaxyx(stdscr,terY,terX);
    if (terX != width || terY != height) {
      height = terY;
      width  = terX;
      printMenu(win,heightWin,widthWin,matrix);
    }

    switch(ch) {
    case KEY_LEFT:
      if (pos == 1) {
        pos = 0;
      }
      break;
    case KEY_RIGHT:
      if (pos == 0) {
        pos = 1;
      }
      break;
    case '0':
      matSize[pos] = 0;
      break;
    case '1':
      matSize[pos] = 1;
      break;
    case '2':
      matSize[pos] = 2;
      break;
    case '3':
      matSize[pos] = 3;
      break;
    case '4':
      matSize[pos] = 4;
      break;
    case '5':
      matSize[pos] = 5;
      break;
    case '6':
      matSize[pos] = 6;
      break;
    case '7':
      matSize[pos] = 7;
      break;
    case '8':
      matSize[pos] = 8;
      break;
    case '9':
      matSize[pos] = 9;
      break;
    }

    int matrSize = matSize[0]*10 + matSize[1];
    // if not error
    if (heightWin*3/4 - 7 > 2*matrSize && widthWin - 2 > 9*matrSize ) {
       getMatrix(matrix,matrSize);
       printMatrix(win,heightWin,widthWin,matrix);
    } else {
      printError(win,heightWin,widthWin);
    }

    wmove(win, heightWin/4 + 5, widthWin/2 - 2);
    wprintw(win,std::to_string(matSize[0]).c_str());
    wmove(win, heightWin/4 + 5, widthWin/2 - 1);
    wprintw(win,std::to_string(matSize[1]).c_str());

    wmove(win, heightWin/4 + 5, widthWin/2 - 2 + pos);

    refresh();
    wrefresh(win);
  }
}

int main() {
  initscr();
  keypad(stdscr, TRUE);
  refresh();

  noecho();

  WINDOW *win = nullptr;

  Matrix matrix;
  getChar(win,matrix);

  endwin();
  return 0;
}
