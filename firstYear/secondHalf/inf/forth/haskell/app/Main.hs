{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE FlexibleContexts #-}
module Main where

import Lens.Micro
import Lens.Micro.TH
import qualified Graphics.Vty as V

import qualified Brick.Main as M
import qualified Brick.Types as T
import Brick.Widgets.Core
  ( (<+>)
  , (<=>)
  , hLimit
  , vLimit
  , str
  , hBox
  , vBox
  )
import qualified Brick.Widgets.Center as C
import qualified Brick.Widgets.Edit as E
import qualified Brick.Widgets.Border as B
import qualified Brick.AttrMap as A
import qualified Brick.Focus as F
import Brick.Util (on)

import Data.Maybe (fromMaybe)
import Data.Either

import Text.Regex.PCRE.Heavy (scan,re)

data Name = Ed
          deriving (Ord, Show, Eq)

data St =
    St { _focusRing :: F.FocusRing Name
       , _n   :: E.Editor String Name
       }

makeLenses ''St

parse :: String -> Either String Int
parse input
  | (null input) = Left "You did not write anything."
  | otherwise =
    let s = scan [re|(^\d+$)|] input
    in if (null s)
       then Left "This is not positive integer (not \"n\")."
       else Right (read (fst $ head s)::Int)

-- pows
myPow :: Double -> Double -> Double
myPow n o = exp $ log n * o

fastPow :: Double -> Int -> Double
fastPow n i
  | (i > 0)   = n * fastPow n (i-1)
  | otherwise = 1

-- calculate element
naive :: Int -> Double
naive n =
  let l = fromIntegral n :: Double
  in myPow ((sqrt(l)+2)/(sqrt(l)+3)) (myPow l (3/2))

optimized :: Int -> Double
optimized n =
  let l = sqrt $ fromIntegral n :: Double
  in myPow (1 - 1/(l+3)) (fastPow l 3)

-- summs
summ :: (Int -> Double)
     -> Either String Int
     -> String
summ f inp
  | (isLeft inp) = fromLeft "" inp
  | otherwise      = let n = fromRight 0 inp
                     in show $ sum [ f(i) | i <- [1..n]]

-- summ approximated
summAppr :: (Int -> Double)
     -> Either String Int
     -> String
summAppr f inp
  | (isLeft inp) = fromLeft "" inp
  | otherwise      = let n = fromRight 0 inp
                     in if (n > 5)
                        then show $ sum [ f(i) | i <- [1..(div n 2)]]
                        else show $ sum [ f(i) | i <- [1..n]]

drawUI :: St -> [T.Widget Name]
drawUI st = [ui]
    where
        e = F.withFocusRing (st^.focusRing) (E.renderEditor (str . unlines)) (st^.n)
        ui = C.center $ hLimit 80 $ vBox
          [            hBox [ corner   , B.hBorder, corner   ]
          , vLimit 1 $ hBox [ B.vBorder, prompt1   , B.vBorder]
          , vLimit 4 $ hBox [ B.vBorder, prompt2   , B.vBorder]
          , vLimit 4 $ hBox [ B.vBorder, ed        , B.vBorder]
          , vLimit 4 $ hBox [ B.vBorder, naiveBox  , B.vBorder]
          , vLimit 4 $ hBox [ B.vBorder, optBox    , B.vBorder]
          , vLimit 4 $ hBox [ B.vBorder, approxBox , B.vBorder]
          ,            hBox [ corner   , B.hBorder , corner   ]
          ]
          where
            prompt1  = C.center (str "We have series like \"((sqrt(n)+2)/(sqrt(n)+3))^(n^(3/2))\".")
            prompt2  = C.center (str "We can find particular sum. (Esc to quit)")
            ed       = C.center (str "Print \"n\", please " <+> (hLimit 15 $ vLimit 1 e))
            naiveBox = C.center $ str $ summ naive
              (parse $ head $ E.getEditContents(st^.n))
            optBox   = C.center $ str $ summ optimized
              (parse $ head $ E.getEditContents(st^.n))
            approxBox= C.center $ str $ summAppr optimized
              (parse $ head $ E.getEditContents(st^.n))
            -- dich = C.center $ str $ dichotomy $ readEntries
            --   (parse $ head $ E.getEditContents(st^.l))
            --   (parse $ head $ E.getEditContents(st^.r))
            --   (parseAccuracy $ head $ E.getEditContents(st^.acc))
            -- newt = C.center $ str $ newton $ readEntries
            --   (parse $ head $ E.getEditContents(st^.l))
            --   (parse $ head $ E.getEditContents(st^.r))
            --   (parseAccuracy $ head $ E.getEditContents(st^.acc))
            corner = B.joinableBorder (pure False)

appEvent :: St -> T.BrickEvent Name e -> T.EventM Name (T.Next St)
appEvent st (T.VtyEvent ev) =
  case ev of
    V.EvKey V.KEsc [] -> M.halt st
    V.EvKey (V.KChar '\t') [] -> M.continue $ st & focusRing %~ F.focusNext
    V.EvKey V.KBackTab [] -> M.continue $ st & focusRing %~ F.focusPrev

    _ -> M.continue =<< case F.focusGetCurrent (st^.focusRing) of
         Just Ed -> T.handleEventLensed st n E.handleEditorEvent ev
         Nothing -> return st
appEvent st _ = M.continue st

initialState :: St
initialState =
    St (F.focusRing [Ed])
       (E.editor Ed Nothing "")

theMap :: A.AttrMap
theMap = A.attrMap V.defAttr
  [
    (E.editAttr,                   V.white `on` V.blue)
  , (E.editFocusedAttr,            V.black `on` V.yellow)
  ]

appCursor :: St -> [T.CursorLocation Name] -> Maybe (T.CursorLocation Name)
appCursor = F.focusRingCursor (^.focusRing)

theApp :: M.App St e Name
theApp =
  M.App { M.appDraw = drawUI
        , M.appChooseCursor = appCursor
        , M.appHandleEvent = appEvent
        , M.appStartEvent = return
        , M.appAttrMap = const theMap
        }

main :: IO ()
main = do
  st <- M.defaultMain theApp initialState
  putStr ""
