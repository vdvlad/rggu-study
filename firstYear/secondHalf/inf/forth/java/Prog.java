package progJava;

import java.util.regex.Pattern;
import java.util.Scanner;
import java.util.function.*;

class Prog {
  public static Integer input() {
    Scanner line = new Scanner(System.in);
    String s     = "";
    Integer n        = 0;

    do {
      System.out.println("Print \"n\", please.");
      s = line.nextLine();
    }
    while (!Pattern.matches("^\\d+$",s));
    n = Integer.parseInt(s);

    return n;
  }

  public static Double myPow (final Double n, final Double o) {
    return Math.exp(Math.log(n) * o);
  }

  // n in i
  public static Double fastPow (final Double n, final Integer i) {
    return (i>0) ? n*fastPow(n,i-1) : 1;
  }

  // calculate element

  public static Double naive(final Integer nOld) {
    final double n = nOld;
    return myPow((Math.sqrt(n)+2.)/(Math.sqrt(n)+3),
                 myPow(n,3./2));
  }

  public static double optimazed(final Integer nOld) {
    final double n = nOld;
    final double l = Math.sqrt(n);
    return myPow(1 - 1/(l + 3),fastPow(l,3));
  }

  // calculate sum

  public static Double summ(final Integer n, Function< Integer, Double > f) {
    Double sum = 0.;
    for (Integer i = 1; i <= n; ++i) {
      sum += f.apply(i);
    }
    return sum;
  }

  public static Double sumNaive(final Integer n) {
    Function <Integer, Double> f = x -> naive(x);
    return summ(n,f);
  }

  public static double sumOptimazed(final Integer n) {
    Function <Integer, Double> f = x -> optimazed(x);
    return summ(n,f);
  }

  public static double sumApprox(final Integer n) {
    Function <Integer, Double> f = x -> optimazed(x);
    if (n > 4)
      return summ(n/2,f);
    return summ(n,f);
  }

  public static void calc() {
    Integer i = input();

    System.out.println("Naive calculate.");
    System.out.println(sumNaive(i) + "\n");
    System.out.println("Optimazed calculation.");
    System.out.println(sumOptimazed(i) + "\n");
    System.out.println("Approximated calculation.");
    System.out.println(sumApprox(i) + "\n");
  }

  public static void main(String[] args) {
    System.out.println("We have series like \"((sqrt(n)+2)/(sqrt(n)+3))^(n^(3/2))\".");
    System.out.println("We can find particular sum.");
    System.out.println("You can input three times.\n");

    calc();
    calc();
    calc();
  }
}
