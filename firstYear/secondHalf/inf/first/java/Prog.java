package progJava;

import java.util.regex.Pattern;
import java.util.Scanner;

import progJava.Date;

class Prog {
  public static Date input() {
    Scanner line = new Scanner(System.in);
    String s = "";
    do {
      System.out.println("Print date, please (like \"21:02\").");
      s = line.nextLine();
    } while (!Pattern.matches("^(([01]\\d)|(2[0-3])):[0-5]\\d$",s));
    Date d = new Date(s);
    return d;
  }
  public static String hour(final int hours) {
    if (hours == 1 || hours == 13) {
      return " hour";
    }
    return " hours";
  }

  public static void output(final Date d) {
    System.out.println("Your time will be the next:");
    if (d.hours_ >= 12) {
      if (d.hours_ >= 18) {
        if (d.minutes_ == 0) {
          System.out.println("The time is " + (d.hours_-12) + hour(d.hours_) + " of evening equaly.");
        } else {
          System.out.println("The time is " + (d.hours_-12) + hour(d.hours_) +  " of evening, " + (d.minutes_) + " miuntes.");
        }
      } else {
        if (d.minutes_ == 0) {
          System.out.println("The time is " + (d.hours_-12) + hour(d.hours_) + " of afternoon equaly.");
        } else {
          System.out.println("The time is " + (d.hours_-12) + hour(d.hours_) + " of afternoon, " + (d.minutes_) + " miuntes.");
        }
      }
    } else {
      if (d.hours_ >= 6) {
        if (d.minutes_ == 0) {
          System.out.println("The time is " + (d.hours_) + " of morning equaly.");
        } else {
          System.out.println("The time is " + (d.hours_) + " of morning, " + (d.minutes_) + " miuntes.");
        }
      } else {
        if (d.minutes_ == 0) {
          System.out.println("The time is " + (d.hours_) + " of night equaly.");
        } else {
          System.out.println("The time is " + (d.hours_) + " of night, " + (d.minutes_) + " miuntes.");
        }
      }
    }
  }
  public static void main(String[] args) {
    Date d = input();
    output(d);
  }
}
