package progJava;

class Date {
  public int hours_;
  public int minutes_;

  public Date(final String hours_minutes) {
    String [] s = hours_minutes.split(":");
    this.hours_   = Integer.parseInt(s[0]);
    this.minutes_ = Integer.parseInt(s[1]);
  }

  public Date(final int hours, final int minutes) {
    this.hours_   = hours;
    this.minutes_ = minutes;
  }

  public Date() {
    this.hours_   = 0;
    this.minutes_ = 0;
  }

  public Date(final Date d) {
    this.hours_   = d.hours_;
    this.minutes_ = d.minutes_;
  }
}
