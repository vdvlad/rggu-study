module func

contains

subroutine input (hours, minutes)
  Integer            :: hours, minutes
  Character          :: delimeter
  Character(len = 10):: form

  form =  "(I2,A1,I2)"
  do
    write (*,*)    'Please enter time in format ":"'
    read  (*,form) hours, delimeter, minutes

    if ( hours >= 0 .and. hours < 24 &
       .and. minutes >= 0 .and. minutes < 60 &
       .and. delimeter == ':') then
      exit
    end if

    if ( hours < 0 .or. hours >= 24) then
      write (*,*) 'Hours wrong.'
    end if
    if ( minutes < 0 .or. minutes >= 60) then
      write (*,*) 'Minutes wrong.'
    end if
    if ( delimeter /= ':' ) then
      write (*,*) 'Delimeter wrong.'
    end if
    write (*,*) 'You have a mistake in your input. Please enter again.'
  end do
end subroutine

subroutine setPart (hours, partDay)
  Integer            :: hours
  Character(len = 9) :: partDay

  if ( hours >= 12) then
    hours = hours - 12
    if ( hours >= 6) then
      partDay = "morning"
    else
      partDay = "afternoon"
    end if
  else
    if ( hours >= 6) then
      partDay = "evening"
    else
      partDay = "midnight"
    end if
  end if
end subroutine

subroutine output (hours, minutes, partDay)
  Integer             :: hours, minutes
  Character(len = 9)  :: partDay
  Character(len = 40) :: formWith
  Character(len = 36) :: formWithout
  Character(len = 40) :: formWith1
  Character(len = 36) :: formWithout1

  formWith    = '(I2," hours of the ",A9,", minutes ",I2)'
  ! for minutes /= 00
  formWithout = '(I2," hours of the ",A9,", equaly ")'
  ! for minutes 00
  formWith1    = '(I2," hour of the ",A9,", minutes ",I2)'
  ! for minutes /= 00
  formWithout1 = '(I2," hour of the ",A9,", equaly ")'
  ! for minutes 00
  if ( hours /= 1 ) then
    if ( minutes == 00 ) then
        write (*,formWithout) hours, partDay
    else
        write (*,formWith)    hours, partDay, minutes
    end if
  else
    if ( minutes == 00 ) then
        write (*,formWithout1) hours, partDay
    else
        write (*,formWith1)    hours, partDay, minutes
    end if
  end if

end subroutine

end module

program Date

use func;

Integer            :: hours, minutes
Character(len = 9) :: partDay

hours = -1
minutes = -1

call input  (hours,minutes)
call setPart(hours,partDay)
call output (hours,minutes,partDay)

end program
