use Curses::UI;

$cui = Curses::UI->new( -clear_on_exit => 1
                          ,-color_support => 1);
$win = $cui->add('window_1', 'Window');

$continue = 1;
sub finish {
  $continue = 0;
}

$dialog1 = $win->add(
  'message1', 'Dialog::Basic',
  -message   => "Enter time in format '00:12'.\n (press 'c' to focus buttons or 'e' to focus entry).",
  -buttons   => undef
);
$buttons1 = $dialog1->add(
  'boxed1', 'Buttonbox',
  -y => 4,
  -buttonalignment => 'right',
  -buttons => [
    {
      -label => '< Calculate >',
      -onpress =>
        sub { ##########################################################
          my $text = $editor1->text();
          if ($text =~ /(([01]\d)|(2[0-3])):([0-5]\d)/) {
            my $hours   = $1;
            my $minutes = ", " . $4 . " minutes";
            $minutes = " equaly" if $4 eq '00';
            if ($hours > 12) {
              $hours -= 12;
              if ($hours <= 6) {
                  $text = "The time is " . $hours . (($hours == 1 or $hours == 13)?" hour":" hours") . " of afternoon" . $minutes;
              } else {
                  $text = "The time is " . $hours . (($hours == 1 or $hours == 13)?" hour":" hours") . " of evening" . $minutes;
              }
            } else {
              if ($hours <= 6) {
                  $text = "The time is " . $hours . (($hours == 1 or $hours == 13)?" hour":" hours") . " of midnight" . $minutes;
              } else {
                  $text = "The time is " . $hours . (($hours == 1 or $hours == 13)?" hour":" hours") . " of morning" . $minutes;
              }
            }
          } else {
            $text = "Sorry, try again";
          }
          $label2->text($text);
          $label2->draw;
          $buttons2->focus;
        }, #############################################################
      -shortcut => 'C',
    },{
      -label => '< Quit >',
      -onpress => sub {$continue = 0;},
      -shortcut => 'Q'
      }
  ]
);

$editor1 = $dialog1->add(
    'editor', 'TextEditor',
    -y => 4,
    -maxlines => 1,
    -maxlength => 5,
    -text => '00:00',
    -regexp   => '/^\d{0,2}:\d{0,2}$/',
    -width => 8,
    );

$editor1->focus();

$cui->set_binding(sub {$buttons1->focus()}, "c");
$cui->set_binding(sub {$editor1->focus()}, "e");

$win2 = $cui->add('window_2', 'Window');
$dialog2 = $win2->add(
  'message2', 'Dialog::Basic',
  -message   => "Your time in text format:                              ",
  -buttons   => undef
);
$buttons2 = $dialog2->add(
 'boxed2', 'Buttonbox',
 -y => 4,
 -buttonalignment => 'right',
 -buttons => [
   {
     -label => '< Again >',
     -onpress => sub {$buttons1->focus},
     -shortcut => 'A',
   },{
     -label => '< Quit >',
     -onpress => sub {$continue = 0;},
     -shortcut => 'Q'
     }
 ]
);

$label2 = $dialog2->add(
    'label2', 'Label',
    -y         => 3,
    -text      => "Chuck",
    -bold      => 1,
    -width     => 50
    );

$editor1->focus;

while ($continue) {
    $cui->focus(undef, 1);
    $cui->draw;

    $cui->do_one_event;
}
