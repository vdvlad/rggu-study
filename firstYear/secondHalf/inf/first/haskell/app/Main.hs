{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE FlexibleContexts #-}
module Main where

import Lens.Micro
import Lens.Micro.TH
import qualified Graphics.Vty as V

import qualified Brick.Main as M
import qualified Brick.Types as T
import Brick.Widgets.Core
  ( (<+>)
  , (<=>)
  , hLimit
  , vLimit
  , str
  , hBox
  , vBox
  )
import qualified Brick.Widgets.Center as C
import qualified Brick.Widgets.Edit as E
import qualified Brick.Widgets.Border as B
import qualified Brick.AttrMap as A
import qualified Brick.Focus as F
import Brick.Util (on)

import Data.Maybe (fromMaybe)

import Text.Regex.PCRE.Heavy (scan,re)

data Name = Edit
          deriving (Ord, Show, Eq)

data St =
    St { _focusRing :: F.FocusRing Name
       , _edit :: E.Editor String Name
       }

makeLenses ''St

mins :: Int -> String
mins a
  | (a == 0)   = "equaly."
  | otherwise  = show a ++ " minutes."

hour :: Int -> String
hour 1  = " hour"
hour 13 = " hour"
hour _  = " hours"

out :: Int -> Int -> String
out hours minutes
  | (hours >= 18) = "The time is " ++ show (hours - 12) ++ hour hours ++ " of evening, " ++ mins minutes
  | (hours == 12) = "The time is " ++ show (hours     ) ++ hour hours ++ " of midday, "  ++ mins minutes
  | (hours >  12) = "The time is " ++ show (hours - 12) ++ hour hours ++ " of midday, "  ++ mins minutes
  | (hours >= 6 ) = "The time is " ++ show (hours     ) ++ hour hours ++ " of morning, " ++ mins minutes
  | otherwise     = "The time is " ++ show (hours     ) ++ hour hours ++ " of night, "   ++ mins minutes

parse :: String -> String
parse input =
  let s = scan [re|(([01]\d)|(2[0-3])):([0-5]\d)|] input
  in
    if (null s)
    then "You did not put correct time."
    else out
      (read (head $ snd $ head $ s) :: Int)
      (read (last $ snd $ head $ s) :: Int)

drawUI :: St -> [T.Widget Name]
drawUI st = [ui]
    where
        e1 = F.withFocusRing (st^.focusRing) (E.renderEditor (str . unlines)) (st^.edit)
        ui = C.center $ hLimit 80 $ vBox
          [            hBox [ corner   , B.hBorder, corner   ]
          , vLimit 4 $ hBox [ B.vBorder, mid1     , B.vBorder]
          , vLimit 4 $ hBox [ B.vBorder, mid2     , B.vBorder]
          , vLimit 4 $ hBox [ B.vBorder, mid3     , B.vBorder]
          ,            hBox [ corner   , B.hBorder, corner   ]
          ]
          where
          mid1 = C.center (str "Print your time, please " <+> (hLimit 6 $ vLimit 1 e1))
          mid2 = C.center (str "'q' to quit.")
          mid3 = C.center (str (parse $ head $ E.getEditContents(st^.edit)))
          corner = B.joinableBorder (pure False)

appEvent :: St -> T.BrickEvent Name e -> T.EventM Name (T.Next St)
appEvent st (T.VtyEvent ev) =
  case ev of
    V.EvKey V.KEsc [] -> M.halt st
    V.EvKey (V.KChar '\t') [] -> M.continue $ st & focusRing %~ F.focusNext
    V.EvKey V.KBackTab [] -> M.continue $ st & focusRing %~ F.focusPrev

    V.EvKey V.KLeft [] -> M.continue =<< T.handleEventLensed st edit E.handleEditorEvent ev
    V.EvKey V.KRight [] -> M.continue =<< T.handleEventLensed st edit E.handleEditorEvent ev
    V.EvKey V.KDel [] -> M.continue =<< T.handleEventLensed st edit E.handleEditorEvent ev
    V.EvKey V.KBS [] -> M.continue =<< T.handleEventLensed st edit E.handleEditorEvent ev
    _ ->
      let
        ed =
             head $
             E.getEditContents(st^.edit)
      in
      M.continue =<<
        if (length ed < 5)
        then T.handleEventLensed st edit E.handleEditorEvent ev
        else return st
appEvent st _ = M.continue st

initialState :: St
initialState =
    St (F.focusRing [Edit])
       (E.editor Edit Nothing "")

theMap :: A.AttrMap
theMap = A.attrMap V.defAttr
  [
  (E.editFocusedAttr,            V.black `on` V.yellow)
  ]

appCursor :: St -> [T.CursorLocation Name] -> Maybe (T.CursorLocation Name)
appCursor = F.focusRingCursor (^.focusRing)

theApp :: M.App St e Name
theApp =
  M.App { M.appDraw = drawUI
        , M.appChooseCursor = appCursor
        , M.appHandleEvent = appEvent
        , M.appStartEvent = return
        , M.appAttrMap = const theMap
        }

main :: IO ()
main = do
  st <- M.defaultMain theApp initialState
  putStrLn ""
