rex = require "rex_pcre"

function input()
  repeat
    print("Please enter time, make sure it is in right format \"12:34\".")
    line = io.read()
  until (rex.match(line, "(([01]\\d)|(2[0-3])):[0-5]\\d") ~= nil and string.len(line) == 5)

  local arr = {}
  for s in string.gmatch(line, "%d+") do
    arr[#arr+1] = s
  end
  local hours = tonumber(arr[1])
  local minutes = tonumber(arr[2])
  return hours,minutes
end

function hour(hours)
  if (hours == 1 or hours == 13) then
    return " hour"
  else
    return " hours"
  end
end

function output(hours, minutes)
  print("Your time will be the next:")
  if (hours >= 12) then
    if (hours >= 18) then
      if (minutes == 0) then
        print("The time is " .. tostring(hours-12) .. hour(hours) .. " of evening equaly.")
      else
        print("The time is " .. tostring(hours-12) .. hour(hours) ..  " of evening, " .. tostring(minutes) .. " miuntes.")
      end
    else
      if (minutes == 0) then
        print("The time is " .. tostring(hours-12) .. hour(hours) .. " of afternoon equaly.")
      else
        print("The time is " .. tostring(hours-12) .. hour(hours) .. " of afternoon, " .. tostring(minutes) .. " miuntes.")
      end
    end
  else
    if (hours >= 6) then
      if (minutes == 0) then
        print("The time is " .. tostring(hours) .. " of morning equaly.")
      else
        print("The time is " .. tostring(hours) .. " of morning, " .. tostring(minutes) .. " miuntes.")
      end
    else
      if (minutes == 0) then
        print("The time is " .. tostring(hours) .. " of night equaly.")
      else
        print("The time is " .. tostring(hours) .. " of night, " .. tostring(minutes) .. " miuntes.")
      end
    end
  end
end

output(input())
