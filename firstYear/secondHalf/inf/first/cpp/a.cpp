#include <ncurses.h>
#include <vector>
#include <string>

inline void dateTo (const std::vector<int> & date, int& hours, int& minutes) {
  hours   = 10* date[0] + date[1];
  minutes = 10* date[2] + date[3];
}

std::string getTime (const std::vector<int> & date) {
  std::string result;

  int hours;
  int minutes;
  dateTo(date,hours,minutes);

  result  = "The time is ";
  result += (hours > 12) ? std::to_string(hours - 12) : std::to_string(hours);
  if (hours == 1 || hours == 13) {
    result += " hour";
  } else {
    result += " hours";
  }
  result += " of ";
  if (hours > 18) {
    result += "evening";
  } else if (hours > 12) {
    result += "day";
  } else if (hours > 6) {
    result += "morning";
  } else {
    result += "night";
  }
  result += ", ";

  if (minutes != 0) {
  result += std::to_string(minutes);
  result += " minutes.";
  } else {
  result += "equaly.";
  }

  return result;
}

void printMenu (WINDOW*& win, const int& height, const int& width, const std::vector<int>& date) {
  if (win == nullptr) {
    win = newwin(height,width,2,5);
  } else {
    resize_term(height+2+10,width+10);
    delwin(win);
    clear();
    win = newwin(height,width,2,5);
  }
  box(win,0,0);
  refresh();

  box(win,0,0);
  touchwin(win);
  wrefresh(win);

  wmove(win, height/4, width/2 - 22);
  wprintw(win,"Enter your time, please. (in format \"12:34\")");
  wmove(win, height/4 + 1, width/2 - 33);
  wprintw(win,"Press numbers to input, 'q' to quit, arrows for move from numbers.");

  wmove(win, height/4 + 8, width/2 - 6);
  wprintw(win,"Your time is:");

  wrefresh(win);
}

void printTime (WINDOW*& win, const int& height, const int& width, const std::vector<int>& date) {
  wmove(win, height/4 + 5, width/2 - 2);
  wprintw(win,"%d%d:%d%d",date[0],date[1],date[2],date[3]);

  std::string answer = getTime(date);
  wmove(win, height/4 + 8, width/2 - 26);
  wprintw(win, "                                          ");
  wmove(win, height/4 + 8, width/2 - answer.size()/2);
  wprintw(win, answer.c_str());
}

void getChar (WINDOW*& win, std::vector<int>& date) {
  int height;
  int width;
  getmaxyx(stdscr,height,width);
  int heightWin = height-2-10;
  int widthWin  = width-10;
  printMenu(win,heightWin,widthWin,date);
  printTime(win,heightWin,widthWin,date);
  int terY = 0;
  int terX = 0;

  unsigned int pos = 0;
  int ch;

  wmove(win, heightWin/4 + 5, widthWin/2 - 2);
  refresh();
  wrefresh(win);
  while((ch = getch()) != 'q') {
    getmaxyx(stdscr,terY,terX);
    if (terX != width || terY != height) {
      height = terY;
      width  = terX;
      printMenu(win,heightWin,widthWin,date);
    }

    switch(ch) {
    case KEY_LEFT:
      if (pos > 0) {
        --pos;
      }
      break;
    case KEY_RIGHT:
      if (pos < 3) {
        ++pos;
      }
      break;
    case '0':
      date[pos] = 0;
      break;
    case '1':
      date[pos] = 1;
      break;
    case '2':
      date[pos] = 2;
      if (pos == 0 && date[1] > 3) {
        date[1] = 0;
      }
      break;
    case '3':
      if (pos != 0) {
        date[pos] = 3;
      }
      break;
    case '4':
      if (pos != 0 && !(pos == 1 && date[0] == 2)) {
        date[pos] = 4;
      }
      break;
    case '5':
      if (pos != 0 && !(pos == 1 && date[0] == 2)) {
        date[pos] = 5;
      }
      break;
    case '6':
      if (pos != 0
          && !(pos == 1 && date[0] == 2)
          && pos != 2) {
        date[pos] = 6;
      }
      break;
    case '7':
      if (pos != 0
          && !(pos == 1 && date[0] == 2)
          && pos != 2) {
        date[pos] = 7;
      }
      break;
    case '8':
      if (pos != 0
          && !(pos == 1 && date[0] == 2)
          && pos != 2) {
        date[pos] = 8;
      }
      break;
    case '9':
      if (pos != 0
          && !(pos == 1 && date[0] == 2)
          && pos != 2) {
        date[pos] = 9;
      }
      break;
    }

    printTime(win,heightWin,widthWin,date);

    if (pos > 1) { // right from ':'
      wmove(win, heightWin/4 + 5, widthWin/2 - 2 + pos + 1);
    } else {
      wmove(win, heightWin/4 + 5, widthWin/2 - 2 + pos);
    }
    refresh();
    wrefresh(win);
  }
}

int main() {
  initscr();
  keypad(stdscr, TRUE);
  refresh();

  noecho();

  WINDOW *win = nullptr;

  std::vector<int> date(5,0);

  getChar(win,date);

  endwin();
  return 0;
}
