(load "/usr/lib/quicklisp/setup")
(proclaim '(optimize (speed 3) (debug 0)))

(ql:quickload "cl-ppcre" :silent t)

(defpackage Date
  (:use :cl :cl-ppcre))

(in-package Date)

(defun match (in)
  (ppcre:scan "(([01]\\d)|(2[0-3])):([0-5]\\d)" in))

(defun input ()
  (progn
    (progn (princ "Please, put time in format \"12:23\"") (terpri))
    (loop for in = (read-line)
          unless (cond ((/= (length in) 5) t) ((null (match in)) t) (t nil))
            return in
          do (progn (print "You are mistaken, try again.") (terpri)))
  ))

(defun extract (in)
  (ppcre:register-groups-bind
    (hours minutes)
    ("(\\d\\d):(\\d\\d)" in :sharedp t)
    (list hours minutes)))

(defun hour (hours)
  (concatenate 'string
  (cond
    ((>= hours 12 ) (write-to-string (- hours 12)))
    (t              (write-to-string hours)))
  (cond
    ((= hours 1 ) " hour")
    ((= hours 13) " hour")
    (t            " hours"))
  )
)

(defun output (in)
  (progn
    (progn (princ "Your time will be:") (terpri))
    (let ((ex (extract in)))
      (let ((hours (parse-integer (car ex)))
            (minutes (parse-integer (cadr ex)))
            (minutesS (cadr ex)))
        (cond
          ((>= hours 12)
            (cond
              ((>= hours 18) (cond
                ((= minutes 0) (princ (concatenate 'string "The time is " (hour hours) " of evening equaly.")))
                (t (princ (concatenate 'string "The time is " (hour hours) " of evening " minutesS " minutes.")))))
              (t (cond
                ((= minutes 0) (princ (concatenate 'string "The time is " (hour hours) " of afternoon equaly.")))
                (t (princ (concatenate 'string "The time is " (hour hours) " of afternoon " minutesS " minutes."))))
              )
            ))
          (t
            (cond
              ((>= hours 6) (cond
                ((= minutes 0) (princ (concatenate 'string "The time is " (hour hours) " of morning equaly.")))
                (t (princ (concatenate 'string "The time is " (hour hours) " of morning " minutesS " minutes.")))))
              (t (cond
                ((= minutes 0) (princ (concatenate 'string "The time is " (hour hours) " of night equaly.")))
                (t (princ (concatenate 'string "The time is " (hour hours) " of night " minutesS " minutes."))))
              )
            )
          )
        )
      )
    )
    (terpri)
  )
)

(output (input))
