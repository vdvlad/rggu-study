#include <ncurses.h>
#include <string.h>
#include <stdlib.h>

void dateTo (const int* date, int* hours, int* minutes) {
  *hours   = 10* (*(date)) + (*(date + 1));
  *minutes = 10* (*(date + 2)) + (*(date + 3));
}

void getTime (const int* date, char* answer, int* length) {
  int hours;
  int minutes;
  dateTo(date,&hours,&minutes);

  memcpy(answer, "The time is " ,12);
  *length = 12;
  if (hours > 12) {
    if (hours > 21) {
      char a[2];
      sprintf(a, "%d", hours-12);
      memcpy(answer+(*length), a, 2);
      *length += 2;
    } else {
      char a[1];
      sprintf(a, "%d", hours-12);
      memcpy(answer+(*length), a, 1);
      *length += 1;
    }
  } else {
    if (hours > 9) {
      char a[2];
      sprintf(a, "%d", hours);
      memcpy(answer+(*length), a, 2);
      *length += 2;
    } else {
      char a[1];
      sprintf(a, "%d", hours);
      memcpy(answer+(*length), a, 1);
      *length += 1;
    }
  }
  if (hours == 1 || hours == 13) {
    memcpy(answer+(*length), " hours" ,5);
    *length += 5;
  } else {
    memcpy(answer+(*length), " hours" ,6);
    *length += 6;
  }

  memcpy(answer+(*length), " of ", 4);
  *length += 4;
  if (hours > 18) {
    memcpy(answer+(*length), "evening", 7);
    *length += 7;
  } else if (hours > 12) {
    memcpy(answer+(*length), "day", 3);
    *length += 3;
  } else if (hours > 6) {
    memcpy(answer+(*length), "morning", 7);
    *length += 7;
  } else {
    memcpy(answer+(*length), "night", 5);
    *length += 5;
  }
  memcpy(answer+(*length), ", ", 2);
  *length += 2;

  if (minutes != 0) {
    if (minutes > 9) {
      char a[2];
      sprintf(a, "%d", minutes);
      memcpy(answer+(*length), a, 2);
      *length += 2;
    } else {
      char a[1];
      sprintf(a, "%d", minutes);
      memcpy(answer+(*length), a, 1);
      *length += 1;
    }
    memcpy(answer+(*length), " minutes.", 9);
    *length += 9;
  } else {
    memcpy(answer+(*length), "equaly.", 8);
    *length += 8;
  }
}

void printMenu (WINDOW** win, const int* height, const int* width, const int* date) {
  if (win == NULL) {
    *win = newwin(*height,*width,2,5);
  } else {
    resize_term(*height+10,*width+2+10);
    delwin(*win);
    clear();
    *win = newwin(*height,*width,2,5);
  }
  box(*win,0,0);
  refresh();

  box(*win,0,0);
  touchwin(*win);
  wrefresh(*win);

  wmove(*win, (*height)/4, (*width)/2 - 22);
  wprintw(*win,"Enter your time, please. (in format \"12:34\")");
  wmove(*win, (*height)/4 + 1, (*width)/2 - 33);
  wprintw(*win,"Press numbers to input, 'q' to quit, arrows for move from numbers.");

  wmove(*win, (*height)/4 + 8, (*width/2) - 6);
  wprintw(*win,"Your time is:");

  wrefresh(*win);
}

void printTime (WINDOW** win, const int* height, const int* width, const int* date) {
  wmove(*win, (*height)/4 + 5, (*width)/2 - 2);
  wprintw(*win,"%d%d:%d%d",(*date),*(date + 1),*(date + 2),*(date + 3));

  int length = 0;
  char* answer = (char*)malloc(44*sizeof(char));
  getTime(date,answer,&length);
  wmove(*win, (*height)/4 + 8, (*width)/2 - length/2);
  wprintw(*win, answer);
  free(answer);
}

void getChar (WINDOW* win) {
  int* date = (int*)malloc(4*sizeof(int));
  *date       = 0;
  *(date + 1) = 0;
  *(date + 2) = 0;
  *(date + 3) = 0;

  int height;
  int width;
  getmaxyx(stdscr,height,width);
  int heightWin = height-2-10;
  int widthWin  = width-10;
  printMenu(&win,&heightWin,&widthWin,date);
  printTime(&win,&heightWin,&widthWin,date);
  int terY = 0;
  int terX = 0;

  unsigned int pos = 0;
  int ch;

  wmove(win, heightWin/4 + 5, widthWin/2 - 2);
  refresh();
  wrefresh(win);
  while((ch = getch()) != 'q') {
    getmaxyx(stdscr,terY,terX);
    if (terX != width || terY != height) {
      height = terY;
      width  = terX;
      printMenu(&win,&heightWin,&widthWin,date);
    }

    switch(ch) {
    case KEY_LEFT:
      if (pos > 0) {
        --pos;
      }
      break;
    case KEY_RIGHT:
      if (pos < 3) {
        ++pos;
      }
      break;
    case '0':
      *(date + pos) = 0;
      break;
    case '1':
      *(date + pos) = 1;
      break;
    case '2':
      *(date + pos) = 2;
      if (pos == 0 && date[1] > 3) {
        date[1] = 3;
      }
      break;
    case '3':
      if (pos != 0) {
        *(date + pos) = 3;
      }
      break;
    case '4':
      if (pos != 0
         && !(pos == 1 && (*date) == 2)){
        *(date + pos) = 4;
      }
      break;
    case '5':
      if (pos != 0
         && !(pos == 1 && (*date) == 2)){
        *(date + pos) = 5;
      }
      break;
    case '6':
      if (pos != 0
          && !(pos == 1 && (*date) == 2)
          && pos != 2) {
        *(date + pos) = 6;
      }
      break;
    case '7':
      if (pos != 0
          && !(pos == 1 && (*date) == 2)
          && pos != 2) {
        *(date + pos) = 7;
      }
      break;
    case '8':
      if (pos != 0
          && !(pos == 1 && (*date) == 2)
          && pos != 2) {
        *(date + pos) = 8;
      }
      break;
    case '9':
      if (pos != 0
          && !(pos == 1 && (*date) == 2)
          && pos != 2) {
        *(date + pos) = 9;
      }
      break;
    }

    printTime(&win,&heightWin,&widthWin,date);

    if (pos > 1) { // right from ':'
      wmove(win, heightWin/4 + 5, widthWin/2 - 2 + pos + 1);
    } else {
      wmove(win, heightWin/4 + 5, widthWin/2 - 2 + pos);
    }
    refresh();
    wrefresh(win);
  }
  free(date);
}

int main() {
  initscr();
  keypad(stdscr, TRUE);
  refresh();

  noecho();

  WINDOW *win = NULL;

  getChar(win);

  endwin();
  return 0;
}
