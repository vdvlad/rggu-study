\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\babel@toc {russian}{}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {1}{Понятие устаревших слов}{2}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {2}{Архаизмы}{6}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {2}{1}{Архаизмы. Основные типы}{6}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {2}{2}{Субстракты}{8}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {2}{3}{Практическая часть}{9}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {2}{4}{Становление лексических архаизмов}{13}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {3}{Наше время}{14}{0}{3}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {3}{1}{Изменение значения}{14}{0}{3}
\defcounter {refsection}{0}\relax 
\beamer@subsectionintoc {3}{2}{Словари архаизмов и историзмов}{16}{0}{3}
