\babel@toc {russian}{}
\beamer@sectionintoc {1}{Системы координат на двумерном пространстве}{2}{0}{1}
\beamer@subsectionintoc {1}{1}{Прямоугольная система координат}{3}{0}{1}
\beamer@subsectionintoc {1}{2}{Полярная система координат}{7}{0}{1}
\beamer@subsectionintoc {1}{3}{Параметрическое представление}{11}{0}{1}
\beamer@sectionintoc {2}{Системы координат на трёхмерном пространстве}{12}{0}{2}
\beamer@subsectionintoc {2}{1}{Декартова трёхмерная система координат}{12}{0}{2}
\beamer@subsectionintoc {2}{2}{Цилиндрическая система координат}{13}{0}{2}
\beamer@subsectionintoc {2}{3}{Сферическая система координат}{15}{0}{2}
\beamer@sectionintoc {3}{Многомерные системы координат}{18}{0}{3}
\beamer@subsectionintoc {3}{1}{Аффинная система координат}{18}{0}{3}
