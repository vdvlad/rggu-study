\newpage
\chapter{Основные понятия}
\Section{Определение множества}
\word{Множество} (set) --- совокупность объектов любой природы, рассматриваемых как одно целое. Не определяемое понятие в полном смысле слова.

Множество (определение Кантора)~--- это многое, мыслимое как единое.

Обычно множества обозначаются заглавными латинскими буквами. $A, B, C \hdots$~--- множества. \par
Элементы множества обозначаются заглавными латинскими буквами. $a, b, c \hdots$~--- элементы множества.

$a \in A$ --- элемент $a$ принадлежит $A$. \par
$a \notin A$ --- элемент $a$  не принадлежит $A$.

Для графического представления отношений между множествами пользуются \word[?диаграммами Эйлера-Венна]{Диаграмма Эйлера-Венна} (Диаграмма \ref{Пример диаграммы Эйлера-Венна})

\begin{mydiagram}[ptb]
\def\nullRectangle{(-4,-3) rectangle (4,3)}
\def\firstcircle{(0,0) circle (1.5cm)}
\begin{center}
\begin{tikzpicture}
    \begin{scope}[fill opacity=0.5]
        \fill[red] \firstcircle;

        \draw \nullRectangle;
        \draw \nullRectangle node[node font=\Large] at (-3,2) {$\mathbb U$};

        \draw \firstcircle node[below] {$A$};
    \end{scope}
\end{tikzpicture}
\end{center}
\caption{Пример диаграммы Эйлера-Венна}
\label{Пример диаграммы Эйлера-Венна}
\end{mydiagram}

\Section{Способы задания множеств}
\begin{itemize}
\item пересечение элементов. \par
    Пример: $A=\{1,2,3\}$
\item описание с помощью предикатов:
    $A=\{a \in \mathbb{U} \mid P(a)\}$, где $\mathbb{U}$~--- универсальное множество (содержащее все объекты и множества) \par
        Пример: $A=\{b \in \mathbb{Z} \mid b=2k+1\}$~--- нечётные целые числа.
\end{itemize}

\Section{Основные свойства множества}
\begin{defi}{Принадлежность}
Множество $A \subseteq B$ ($A$ содержится в $B$), если любой элемент из $A$ является элементом из $B$.
\end{defi}
На диаграмме \ref{dia:Принадлежность} принадлежность отображена графически.

$A \subset B, \text{если } A \subseteq B, A \neq B$

$A \subseteq A$

\begin{mydiagram}[ptb]
\def\nullRectangle{(-4,-3) rectangle (4,3)}
\def\firstcircle{(0,0) circle (2.5cm)}
\def\secondcircle{(45:1.5cm) circle (0.75cm)}
\begin{center}
\begin{tikzpicture}
    \begin{scope}[fill opacity=0.5]
        \fill[red] \firstcircle;
        \fill[blue] \secondcircle;

        \draw \nullRectangle;
        \draw \nullRectangle node[node font=\Large] at (-3,2) {$\mathbb U$};

        \draw \firstcircle node[below] {$B$};
        \draw \secondcircle node[below] {$A$};
    \end{scope}
\end{tikzpicture}
\end{center}
\caption{Принадлежность}
\label{dia:Принадлежность}
\end{mydiagram}

\index{Множество}
Множества делятся на два основных типа: конечные и бесконечные. В курсе рассматриваются преимущественно конечные.
\begin{gr}{Типы множеств}
"Множества" ->
    {"Конечные"
    ,"Бесконечные"}
\end{gr}

\index{Множество}
$|A| = Card\:A$ --- мощность множества

$         \underset{\text{Натуральные}}{\mathbb{N}}
\subseteq \underset{\text{Целые}}{\mathbb{Z}}
\subseteq \underset{\text{Рациональные}}{\mathbb{Q}}
\subseteq \underset{\text{Действительные}}{\mathbb{R}}
$

\vspace*{2cm}

\begin{exer}{$\sqrt{2} \notin \mathbb{Q}$.}
    \text{Пусть НОД}(p,q) = 1,
    \sqrt{2} & = \frac{p}{q} \\
    2 & = \frac{p^2}{q^2} \\
    p^2 & = 2q^2 \\
    p & = 2 \cdot \frac{q^2}{p} \\
    & \Downarrow \\
    p & = 2k \\
    & \Downarrow \\
    4k^2 & = 2q^2 \\
    2k^2 & = q^2 \\
    q & = 2m \\
    & \Downarrow \\
    \text{НОД}&(p,q) \neq 1
\end{exer}

\section{Алеф}
\index{\aleph}
$|\mathbb{N}| = \aleph_0$ \par
$\aleph_0$ является мощностью $\mathbb{N}$. Существуют большие мощности, которые обозначаются как $\underset{i \in \mathbb{N}}{\aleph_i}$. Причём для любого $i:\:\aleph_0 < \aleph_1 < \aleph_2 \hdots$

Множество $\mathbb{Z}$ обладает той же мощностью, что множество $\mathbb{N}$, действительно: \par
\begin{center}
\begin{math}
\begin{NiceArray}[columns-width=1cm]{rrrrrrrrrr}
    & \hdots & -3         & -2         & -1         & 0          & 1          & 2          & 3          & \hdots \\
    &        & \downarrow & \downarrow & \downarrow & \downarrow & \downarrow & \downarrow & \downarrow & \\
    & \hdots & 7          & 5          & 3          & 1          & 2          & 4          & 6          & \hdots
\end{NiceArray}
\end{math}
\end{center}

$\mathbb{Q} = \mathbb{N} = \mathbb{Z} = \aleph_0$

Множества, мощность которых равна $\aleph_0$, называются счётными. Не все бесконечные множества являются счётными, например $\aleph_0 < |\mathbb{R}|$.

\begin{defi}{Булеан}
$P(A) = 2^A = \{ b \mid b \subseteq A\}$ \\
Множество $P(A)$ называется множеством всех подмножеств или \textbf{булеан}.
\end{defi}
$|A| < |2^A|$ --- \word{Теорема Кантора}

$|2^A| = |R| = \aleph_1$ --- \word{Мощность континиума}
\index{Множество}

\section{Особые множества}
\Subsection{Пустое множество}
$\varnothing$ --- пустое множество

Свойства пустого множества:
\begin{itemize}
\item $\varnothing \subseteq A$
\item $\varnothing \subseteq \varnothing$
\item $\varnothing \notin \varnothing$
\end{itemize}

\Subsection{Универсальное множество}
\index{Множество}
$\mathbb{U}$ --- универсальное множество, то есть множество в которое входят все элементы, возможные в этой задаче.

\section{Операции над множествами}
\index{Множество}
\Subsection{Основная символика}
$\& = \land = and$ \par
$\lor = or$ \par
$\forall$ --- любой \par
$\exists$ --- существует \par
$\exists!$ --- существует ровно 1\par

\Subsection{Операции над множествами}
\index{Множество}
\begin{itemize}
\item Пересечение множеств (Диаграмма \ref{dia:Пересечение множеств}, Таблица  \ref{myt:Пересечение множеств}) \par
    $A \cAp B = \{ x \in \mathbb{U} \mid (x \in A) \land (x \in B)\}$ \par

    \begin{mydiagram}[ptb]
    \def\nullRectangle{(-4,-3) rectangle (6,3)}
    \def\firstcircle{(0,0) circle (1.5cm)}
    \def\secondcircle{(0:2cm) circle (1.5cm)}
    \begin{center}
    \begin{tikzpicture}
        \begin{scope}[fill opacity=0.5]
            \fill[red] \firstcircle;
            \begin{scope}
            \clip \firstcircle;
            \fill[pattern=north west lines] \secondcircle;
            \end{scope}
            \fill[blue] \secondcircle;

            \draw \nullRectangle;
            \draw \nullRectangle node[node font=\Large] at (-3,2) {$\mathbb U$};

            \draw \firstcircle node[below] {$A$};
            \draw \secondcircle node[below] {$B$};
        \end{scope}
    \end{tikzpicture}
    \end{center}
    \caption{Пересечение множеств}
    \label{dia:Пересечение множеств}
    \end{mydiagram}

    $x \in A \Leftrightarrow x=1$ \par
    $x \notin A \Leftrightarrow x=0$

    \begin{myt}[b;code-before = columncolor{blue!10}{4}]{m |m m n}{Пересечение множеств}
    Число  & A & B & A \cAp B \h
    0 & 0 & 0 & 0 \\
    1 & 0 & 1 & 0 \\
    2 & 1 & 0 & 0 \\
    3 & 1 & 1 & 1
    \end{myt}

\item Объединение множеств (Диаграмма \ref{dia:Объединение множеств}, Таблица  \ref{myt:Объединение множеств}) \par
    \index{Множество}
    $A \cUp B = \{ x \in \mathbb{U} \mid (x \in A) \lor (x \in B)\}$ \par

    \begin{mydiagram}[ptb]
    \def\nullRectangle{(-4,-3) rectangle (6,3)}
    \def\firstcircle{(0,0) circle (1.5cm)}
    \def\secondcircle{(0:2cm) circle (1.5cm)}
    \begin{center}
    \begin{tikzpicture}
        \begin{scope}[fill opacity=0.5]
            \fill[pattern=north west lines] \firstcircle;
            \fill[red] \firstcircle;
            \fill[pattern=north west lines] \secondcircle;
            \fill[blue] \secondcircle;

            \draw \nullRectangle;
            \draw \nullRectangle node[node font=\Large] at (-3,2) {$\mathbb U$};

            \draw \firstcircle node[below] {$A$};
            \draw \secondcircle node[below] {$B$};
        \end{scope}
    \end{tikzpicture}
    \end{center}
    \caption{Объединение множеств}
    \label{dia:Объединение множеств}
    \end{mydiagram}

    \begin{myt}[ptb;code-before = columncolor{blue!10}{4}]{m |m m n}{Объединение множеств}
    Число  & A & B & A \cUp B \h
    0 & 0 & 0 & 0 \\
    1 & 0 & 1 & 1 \\
    2 & 1 & 0 & 1 \\
    3 & 1 & 1 & 1
    \end{myt}

\item Вычитание множеств (Диаграмма \ref{dia:Вычитание множеств}, Таблица  \ref{myt:Вычитание множеств}) \par
    \index{Множество}
    $A \sEtminus B = \{ x \in \mathbb{U} \mid (x \in A) \land (x \notin B)\}$ \par

    \begin{mydiagram}[ptb]
    \def\nullRectangle{(-4,-3) rectangle (6,3)}
    \def\firstcircle{(0,0) circle (1.5cm)}
    \def\secondcircle{(0:2cm) circle (1.5cm)}
    \begin{center}
    \begin{tikzpicture}
        \begin{scope}[fill opacity=0.5]
            \fill[red] \firstcircle;
            \fill[pattern=north west lines] \secondcircle;
            \begin{scope}
            \clip \firstcircle;
            \fill[white,fill opacity=1] \secondcircle;
            \fill[red] \secondcircle;
            \end{scope}
            \fill[blue] \secondcircle;

            \draw \nullRectangle;
            \draw \nullRectangle node[node font=\Large] at (-3,2) {$\mathbb U$};

            \draw \firstcircle node[below] {$A$};
            \draw \secondcircle node[below] {$B$};
        \end{scope}
    \end{tikzpicture}
    \end{center}
    \caption{Вычитание множеств}
    \label{dia:Вычитание множеств}
    \end{mydiagram}

    \begin{myt}[b;code-before = columncolor{blue!10}{4}]{m |m m n}{Вычитание множеств}
    Число  & A & B & A \sEtminus B \h
    0 & 0 & 0 & 0 \\
    1 & 0 & 1 & 0 \\
    2 & 1 & 0 & 1 \\
    3 & 1 & 1 & 0
    \end{myt}

\item Дополнение множеств (Диаграмма \ref{dia:Дополнение множеств}) \par
    \index{Множество}
    $\bar A = \mathbb{U} \sEtminus A$

    \begin{mydiagram}[ptb]
    \def\nullRectangle{(-4,-3) rectangle (6,3)}
    \def\firstcircle{(0,0) circle (1.5cm)}
    \begin{center}
    \begin{tikzpicture}
        \begin{scope}[fill opacity=0.5]
            \fill[pattern=north west lines] \nullRectangle;
            \begin{scope}
            \clip \nullRectangle;
            \fill[white,fill opacity=1] \firstcircle;
            \end{scope}
            \fill[red] \firstcircle;

            \draw \nullRectangle;
            \draw \nullRectangle node[node font=\Large] at (-3,2) {$\mathbb U$};

            \draw \firstcircle node[below] {$A$};
        \end{scope}
    \end{tikzpicture}
    \end{center}
    \caption{Дополнение множеств}
    \label{dia:Дополнение множеств}
    \end{mydiagram}
\end{itemize}

\section{Пара и Декартово произведение}
\subsection{Пара}
\begin{defi}{Упорядоченная пара}
Два числа, порядок которых важен, называются упорядоченной парой и обозначаются $(x,y)$.
\end{defi}
Три числа, порядок которых важен, называются кортежем и обозначаются $(x,y,z)$.

\subsection{Декартово произведение (прямое произведения)}
\begin{defi}{Декартово произведение (прямое произведения)}
    $A \times B = \{ (x,y) \mid x \in A, y \in B)\}$
\end{defi}
$\mathbb{N}^2 = \mathbb{N} \times \mathbb{N}$

\Section{Разбиение множеств}
\index{Множество}
\begin{defi}{Разбиение множеств}
    Разбиением множества $A$ называется система подмножеств $A_1,A_2,\hdots,A_n$
\end{defi}

Свойства разбиения:
\begin{itemize}
    \item $A_i \neq \varnothing, i = \overline{1,m}$
    \item $A_i \cAp A_j = \varnothing, i \neq j$
    \item $\underset{i=\overline{1,m}}{\cUp A_i} = A_1 \cUp A_2 \cUp \hdots \cUp A_m = A$
\end{itemize}
\index{Множество}

\Section{Свойства операций на множествах}
\begin{itemize}
\item идемпотентность ($A \cUp A = A,\; A \cAp A = A$)
\item коммутативность ($A \cUp B = B \cUp A,\; A \cAp B = B \cAp B$)
\item ассоциативность ($(A \cUp B) \cUp C = A \cUp (B \cUp C),\;(A \cAp B) \cAp C = A \cAp (B \cAp C)$)
\item дистрибутивность ($(A \cUp B) \cAp C = (A \cAp C) \cUp (B \cAp C),\;(A \cAp B) \cUp C = (A \cUp C) \cAp (B \cUp C)$)
\item $\bar{\bar{A}} = A$
\item $\overline{A \cUp B} = \bar{A} \cAp \bar{B}$
$\overline{A \cAp B} = \bar{A} \cUp \bar{B}$~--- \word{Законы Де Моргана}
\item $A \cUp \bar A = \mathbb U$
\item $A \cAp \bar A = \varnothing$
\end{itemize}
\index{Множество}