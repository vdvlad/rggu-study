BASE_DIR=$$(readlink -e "$$(pwd)")
BIN_DIR=$(BASE_DIR)/../bin/
LOG_DIR=$(BASE_DIR)/../log/
SRC_DIR=$(BASE_DIR)/../src/
TEX_DIR=$(BASE_DIR)/../tex/
XDY_DIR=$(BASE_DIR)/../lib/xdy/
SCRIPTS_DIR=$(BASE_DIR)/../lib/scripts/
TMP_DIR=$(BASE_DIR)/../tmp/
TMP_DIR2=$(BASE_DIR)/tmp/

TEX_FILENAME=finite

TEX=perltex -latex=$(SCRIPTS_DIR)colorlualatex
TEXDEBUGFLAGS= --nosafe --permit=:browse --permit=:filesys_open --permit=open -shell-escape
TEXFLAGS=--interaction=batchmode $(TEXDEBUGFLAGS)

ARGS=($(MAKECMDGOALS))

.ONESHELL:
latex-debug:
	@echo "Compiling report in debug mode started"
	@ls $(TMP_DIR2) || mkdir $(TMP_DIR2)
	@cd $(TMP_DIR2)
	$(TEX) $(TEXDEBUGFLAGS) "$(TEX_DIR)"$(TEX_FILENAME).tex
	texindy -L russian -C utf8 -M $(XDY_DIR)alph.xdy $(TEX_FILENAME).idx
	perl $(SCRIPTS_DIR)/makeglossaries -L russian -C utf8 -M $(XDY_DIR)alph.xdy $(TEX_FILENAME).glo
	biber $(TEX_FILENAME)
	$(TEX) $(TEXDEBUGFLAGS) "$(TEX_DIR)"$(TEX_FILENAME).tex
	$(TEX) $(TEXDEBUGFLAGS) "$(TEX_DIR)"$(TEX_FILENAME).tex
	@mv "$(TMP_DIR)"$(TEX_FILENAME).pdf "$(BIN_DIR)"
	@echo "Compiling report in debug mode ended"

latex-full:
	@echo "Compiling report in full mode started"
	@ls $(TMP_DIR2) || mkdir $(TMP_DIR2)
	@cd $(TMP_DIR2)
	$(TEX) $(TEXDEBUGFLAGS) "$(TEX_DIR)"$(TEX_FILENAME).tex
	texindy -L russian -C utf8 -M $(XDY_DIR)alph.xdy $(TEX_FILENAME).idx
	perl $(SCRIPTS_DIR)/makeglossaries -L russian -C utf8 -M $(XDY_DIR)alph.xdy $(TEX_FILENAME).glo
	biber $(TEX_FILENAME)
	$(TEX) $(TEXDEBUGFLAGS) "$(TEX_DIR)"$(TEX_FILENAME).tex
	$(TEX) $(TEXDEBUGFLAGS) "$(TEX_DIR)"$(TEX_FILENAME).tex
	$(TEX) $(TEXDEBUGFLAGS) "$(TEX_DIR)"$(TEX_FILENAME).tex
	@mv "$(TMP_DIR)"$(TEX_FILENAME).pdf "$(BIN_DIR)"
	perl -apE 's/(\\input.+homework)/\% \1/' -i "$(TEX_DIR)"$(TEX_FILENAME).tex
	$(TEX) $(TEXDEBUGFLAGS) "$(TEX_DIR)"$(TEX_FILENAME).tex
	texindy -L russian -C utf8 -M $(XDY_DIR)alph.xdy $(TEX_FILENAME).idx
	perl $(SCRIPTS_DIR)/makeglossaries -L russian -C utf8 -M $(XDY_DIR)alph.xdy $(TEX_FILENAME).glo
	biber $(TEX_FILENAME)
	$(TEX) $(TEXDEBUGFLAGS) "$(TEX_DIR)"$(TEX_FILENAME).tex
	$(TEX) $(TEXDEBUGFLAGS) "$(TEX_DIR)"$(TEX_FILENAME).tex
	$(TEX) $(TEXDEBUGFLAGS) "$(TEX_DIR)"$(TEX_FILENAME).tex
	perl -apE 's/% (\\input.+homework)/\1/' -i "$(TEX_DIR)"$(TEX_FILENAME).tex
	@mv "$(TMP_DIR)"$(TEX_FILENAME).pdf "$(BIN_DIR)"$(TEX_FILENAME)_without_homework.pdf
	perl -apE 's/(\\printindex)/\% \1/; $$_ = "% ". $$_ if m/^\\input/ and not m/(homework|title)/' -i "$(TEX_DIR)"$(TEX_FILENAME).tex
	$(TEX) $(TEXDEBUGFLAGS) "$(TEX_DIR)"$(TEX_FILENAME).tex
	texindy -L russian -C utf8 -M $(XDY_DIR)alph.xdy $(TEX_FILENAME).idx
	perl $(SCRIPTS_DIR)/makeglossaries -L russian -C utf8 -M $(XDY_DIR)alph.xdy $(TEX_FILENAME).glo
	biber $(TEX_FILENAME)
	$(TEX) $(TEXDEBUGFLAGS) "$(TEX_DIR)"$(TEX_FILENAME).tex
	$(TEX) $(TEXDEBUGFLAGS) "$(TEX_DIR)"$(TEX_FILENAME).tex
	$(TEX) $(TEXDEBUGFLAGS) "$(TEX_DIR)"$(TEX_FILENAME).tex
	perl -apE 's/% (\\printindex)/\1/; $$_ = substr($$_,2) if m/^% \\input/ and not m/(homework|title)/' -i "$(TEX_DIR)"$(TEX_FILENAME).tex
	@mv "$(TMP_DIR)"$(TEX_FILENAME).pdf "$(BIN_DIR)"$(TEX_FILENAME)_only_homework.pdf
	@echo "Compiling report in full mode ended"

latex-clean:
	@echo "Removing compiled report started"
	@rm -f "$(TMP_DIR)"$(TEX_FILENAME)
	@rm -f "$(BIN_DIR)"$(TEX_FILENAME).pdf
	@echo "Removing compiled report ended"

all: latex-debug

clean: latex-clean
