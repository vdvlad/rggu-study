#!/usr/bin/perl -CS
use Data::Frame;
use Data::Frame::Indexer;
use Data::Frame::IO::CSV;
use 5.10.0;
use PDL::Ufunc /:all/;
use PDL::Stats /:all/;
use PDL;
use utf8;
# use Statistics::Descriptive::PDL /mean new add_data/;
# use Statistics::Descriptive::PDL;
# use Statistics::Descriptive /mean new/;

$df = Data::Frame->from_csv("../NEO2.csv",header=>true);

sub fun1 {
  my $a = $df->select_columns([qw/ЗОДИАК C4_Целеустремленность/])->drop_bad;
  my %b = $a->split($a->{ЗОДИАК});
  for $key(keys %b) {
      my $tmp = average($b{$key}->{C4_Целеустремленность});
      say "Поле $key mean: ", $tmp if $tmp > 25;
  }
}
sub fun2 {
  my $a = $df->select_columns([qw/ЗОДИАК C4_Целеустремленность/])->drop_bad;
  my %b   = $a->split($a->{ЗОДИАК});
  my @kmax = "undef";
  my $max  = -1;
  for $key(keys %b) {
      my $tmp = var_unbiased($b{$key}->{C4_Целеустремленность});
      if ($tmp > $max) {
          $max = $tmp;
          @kmax = ($key);
      } elsif ($tmp == $max) {
          @kmax[$#kmax+1] = $key;
      }
  }
  say "Поле $_ var: ", $max for @kmax;
}
sub fun3 {
  my $a = $df->select_columns([qw/C4_Целеустремленность/])->drop_bad;
  say "Поле C4_Целеустремленность skewness: "
    , skew($a->{C4_Целеустремленность});
}
sub fun4 {
  my $a = $df->select_columns([qw/ЛИЧ_ДОХ/])->drop_bad->uniq();
  say $a;
}

# fun5  -- no lillie

sub fun6 {
  my $a = $df->select_columns([qw/ОБР_РОД/])->drop_bad;
  my @b = $a->{ОБР_РОД};
  my @c = map {$_ == "БОЛ1_ВЫС"} @b;
  say "Пропорция: ", sum(@c)/double(nelems(@b));
}

sub fun7 {
  my $a = $df->select_columns([qw/ЛИЧ_ДОХ ВОЗРАСТ/]);
  my $b1 = $a->{ВОЗРАСТ} > 18;
  my $b2 = $a->{ЛИЧ_ДОХ} == "низкий";
  say "fun7: ", sum($b1 & $b2);
}

sub fun8 {
  my $a = $df->{ЗОДИАК};
  say "fun8: ", sum($a == "РАК");
}

sub fun9 {
  my $a = $df->select_columns([qw/СТЕП_РЕЛ E1_Доброжелательность/]);
  my $b = $a->transform({СТЕП_РЕЛ => sub{shift=="СИЛЬНАЯ"}});
  my $c = $b->split($b->{СТЕП_РЕЛ})->{1}->{E1_Доброжелательность};
  say "fun9: ", average($c);
}

# fun10 -- dont have ks.test (but have another)

fun1();
