#!/usr/bin/python
import pandas as pd
import scipy  as sc
import numpy  as np
import scipy.stats  as scs
import statsmodels.stats.diagnostic

def fun1() :
  r = pd.read_csv("../NEO2.csv")
  s = r[["C4_Целеустремленность","ЗОДИАК"]].dropna()
  t = s.groupby("ЗОДИАК").mean()
  return list(t[t > 25].dropna().index)

def fun2() :
  r = pd.read_csv("../NEO2.csv")
  s = r[["C4_Целеустремленность","ЗОДИАК"]].dropna()
  t = s.groupby("ЗОДИАК").var()
  return list(t[t == max(t.values)].dropna().index)

def fun3() :
  r = pd.read_csv("../NEO2.csv")
  s = r[["C4_Целеустремленность"]].dropna()
  # return sc.stats.moment(s,moment=3)
  return sc.stats.skew(s)

def fun4() :
  r = pd.read_csv("../NEO2.csv")
  s = r["ЛИЧ_ДОХ"].dropna()
  return list(np.unique(s.values))

def fun5() :
  r = pd.read_csv("../NEO2.csv")
  name0 = ["N_Нейротизм", "N1_Тревожность", "N2_Враждебность",  "N3_Депрессивность", "N4_Застенчивость", "N5_Импульсивность",  "N6_Уязвимость", "E_Экстраверсия", "E1_Доброжелательность" , "E2_Общительность", "E3_Настойчивость", "E4_Активность",  "E5_Непоседливость", "E6_Жизнерадостность", "O_Открытость_опыту",  "O1_Фантазия", "O2_Эстетичность", "O3_Чувства",  "O4_Действия", "O5_Идеи", "O6_Ценности",  "A_Согласие", "A1_Доверие", "A2_Прямота",  "A3_Альтруизм", "A4_Уступчивость", "A5_Скромность",  "A6_Отзывчивость", "C_Сознательность", "C1_Компетентность",  "C2_Организованность", "C3_Ответственность", "C4_Целеустремленность" , "C5_Самодисциплина", "C6_Осмотрительность"]
  res = []
  for i in range(0,len(name0)) :
    if (statsmodels.stats.diagnostic.lilliefors(r[name0[i]].dropna().to_numpy())[1] > 0.05) :
      res.append(name0[i])
  return list(res)

def fun6() :
  r = pd.read_csv("../NEO2.csv")
  s = r["ОБР_РОД"].dropna()
  return len(s[s=="БОЛ1_ВЫС"])/len(s)

def fun7() :
  r = pd.read_csv("../NEO2.csv")
  s = r[["ЛИЧ_ДОХ","ВОЗРАСТ"]].dropna()
  s1 = s[s['ВОЗРАСТ'] > 18]
  s2 = s1[s1['ЛИЧ_ДОХ'] == 'низкий']
  return len(s2)

def fun8() :
  r = pd.read_csv("../NEO2.csv")
  s = r["ЗОДИАК"].dropna()
  return len(s[s=="РАК"])

def fun9() :
  r = pd.read_csv("../NEO2.csv")
  s = r[["E1_Доброжелательность","СТЕП_РЕЛ"]].dropna()
  s1 = s[s['СТЕП_РЕЛ']  == "СИЛЬНАЯ"]
  s2 = s1['E1_Доброжелательность']
  return s2.mean()

def fun10() :
  r = pd.read_csv("../NEO2.csv")
  name0 = ["N_Нейротизм", "N1_Тревожность", "N2_Враждебность",  "N3_Депрессивность", "N4_Застенчивость", "N5_Импульсивность",  "N6_Уязвимость", "E_Экстраверсия", "E1_Доброжелательность" , "E2_Общительность", "E3_Настойчивость", "E4_Активность",  "E5_Непоседливость", "E6_Жизнерадостность", "O_Открытость_опыту",  "O1_Фантазия", "O2_Эстетичность", "O3_Чувства",  "O4_Действия", "O5_Идеи", "O6_Ценности",  "A_Согласие", "A1_Доверие", "A2_Прямота",  "A3_Альтруизм", "A4_Уступчивость", "A5_Скромность",  "A6_Отзывчивость", "C_Сознательность", "C1_Компетентность",  "C2_Организованность", "C3_Ответственность", "C4_Целеустремленность" , "C5_Самодисциплина", "C6_Осмотрительность"]
  res = []
  for i in range(0,len(name0)) :
    d = r[name0[i]].dropna().to_numpy()
    d = (d - d.mean())/d.std()
    if sc.stats.kstest(d,"norm")[1] > 0.05 :
      res.append(name0[i])
  return list(res)

print(fun3())
