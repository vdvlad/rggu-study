using CSV, DataFramesMeta, DataFrames, Chain, StatsBase, Statistics, HypothesisTests

function num1()
  a = CSV.read("../NEO2.csv",DataFrame;missingstring=["","NA"])
  b = a[:,[:C4_Целеустремленность,:ЗОДИАК]] |> dropmissing
  c = combine(groupby(b, :ЗОДИАК), :C4_Целеустремленность => mean)
  @show @subset(c, :C4_Целеустремленность_mean .> 25)
end

function num2()
  a = CSV.read("../NEO2.csv",DataFrame;missingstring=["","NA"])
  b = a[:,[:C4_Целеустремленность,:ЗОДИАК]] |> dropmissing
  c = combine(groupby(b, :ЗОДИАК), :C4_Целеустремленность => var)
  @show @subset(c, :C4_Целеустремленность_var .>= maximum(:C4_Целеустремленность_var))
end

function num3()
  a = CSV.read("../NEO2.csv",DataFrame;missingstring=["","NA"])
  @show skewness(a[:,:C4_Целеустремленность] |> skipmissing |> collect)
end

function num4()
  a = CSV.read("../NEO2.csv",DataFrame;missingstring=["","NA"])
  @show (a[:,:ЛИЧ_ДОХ] |> skipmissing |> unique)
end

# lillie test doest not implemented

function num6()
  a = CSV.read("../NEO2.csv",DataFrame;missingstring=["","NA"])
  b = a[:,:ОБР_РОД] |> skipmissing |> collect
  @show sum(b .== "БОЛ1_ВЫС")/length(b)
end

function num7()
  a = CSV.read("../NEO2.csv",DataFrame;missingstring=["","NA"])
  b = a[:,[:ЛИЧ_ДОХ,:ВОЗРАСТ]] |> dropmissing
  @show sum(b[!,:ЛИЧ_ДОХ] .== "низкий" .&& b[!,:ВОЗРАСТ] .> 18)
end

function num8()
  a = CSV.read("../NEO2.csv",DataFrame;missingstring=["","NA"])
  @show sum(a[!,:ЗОДИАК] |> skipmissing  .== "РАК")
end

function num9()
  a = CSV.read("../NEO2.csv",DataFrame;missingstring=["","NA"])
  b = @subset(a,:СТЕП_РЕЛ  .== "СИЛЬНАЯ",skipmissing=true)
  @show mean(b[!,:E1_Доброжелательность])
end

function num10()
  function fun(a)
    b = skipmissing(a) |> collect
    mx = mean(b)
    mu = std(b)
    return(pvalue(ExactOneSampleKSTest(b,Normal(mx,mu))))
  end
  a = CSV.read("../NEO2.csv",DataFrame;missingstring=["","NA"])
  b = a[:,25:59]
  c = mapcols(a -> fun(a),b) |> stack
  @show @subset(c, :value .> 0.05)
end
# julia> c = mapcols(a -> pvalue(ExactOneSampleKSTest((skipmissing(a) |> collect),Normal(0,1))),b)

num1()
