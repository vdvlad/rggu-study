R version 4.2.0 (2022-04-22) -- "Vigorous Calisthenics"
> library(bootstrap)
> 
> # №33
> s <- function(N_var) {
+   set.seed(N_var); load("NEO")
+   x=sample(names(NEO)[19:53][-c(1,8,15,22,29)],1)
+ 
+   s <- na.omit(subset(NEO
+                     , select = eval(parse(text=x))
+                     , subset = (ПОЛ == "ЖЕН")))[,1]
+ 
+   theta <- function(x) ks.test(x,"pnorm")$p.value
+   B1 <- bootstrap::bootstrap(s, 10000, theta)$thetastar
+   sum(B1 < 0.05)/length(B1)
+ }
> s(1)
[1] 1
> s(2)
[1] 1
> s(3)
[1] 1
> s(4)
[1] 1
> s(5)
[1] 1
> s(6)
[1] 1
> s(7)
[1] 1
> s(8)
[1] 1
> s(9)
[1] 1
> s(10)
[1] 1
> s(11)
[1] 1
> 
