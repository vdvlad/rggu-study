> library(bootstrap)
> 
> # №31
> s &lt;- function(N_var) {
<span foreground="red">+   set.seed(N_var); load("NEO")</span>
<span foreground="red">+   x=sample(names(NEO)[19:53][-c(1,8,15,22,29)],1)</span>
<span foreground="red">+ </span>
<span foreground="red">+   s &lt;- na.omit(subset(NEO</span>
<span foreground="red">+                     , select = eval(parse(text=x))</span>
<span foreground="red">+                     , subset = (ПОЛ == "ЖЕН")))[,1]</span>
<span foreground="red">+ </span>
<span foreground="red">+   theta &lt;- function(x) shapiro.test(x)$p.value</span>
<span foreground="red">+   B1 &lt;- bootstrap::bootstrap(s, 10000, theta)$thetastar</span>
<span foreground="red">+   sum(B1 &lt; 0.05)/length(B1)</span>
<span foreground="red">+ }</span>
> s(1)
[1] 0.9607
> s(2)
[1] 0.9975
> s(3)
[1] 0.9767
> s(4)
[1] 0.9999
> s(5)
[1] 0.9874
> s(6)
[1] 0.9981
> s(7)
[1] 0.557
> s(8)
[1] 0.8969
> s(9)
[1] 0.917
> s(10)
[1] 0.9053
> s(11)
[1] 0.7493
> 
