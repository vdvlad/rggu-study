for i in *
do
  if [ -d $i ]
  then
    cp $i/homework.R $i/home.R
    cat $i/home.R | R --vanilla | grep "[>+\[\"]" > $i/out0.txt
    sed -n -e 2,1000p $i/out0.txt > $i/out.txt
    perl -pi -e 's/\&/&amp;/g;s/</&lt;/g;s%^(\+.*)$%<span foreground="red">\1</span>%' $i/out.txt
    magick -background grey -font consolas -pointsize 12 pango:"@$i/out.txt" -bordercolor grey -trim -border 10x10 $i/output$i.png
    rm $i/out*.txt $i/home.R
  fi
done;
