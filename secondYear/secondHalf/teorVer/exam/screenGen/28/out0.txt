R version 4.2.0 (2022-04-22) -- "Vigorous Calisthenics"
> # №28
> s <- function(N_var) {
+   set.seed(N_var); load("NEO")
+   x <-sample(names(NEO)[54:293],1)
+ 
+   t <- table(eval(parse(text=paste("NEO$",x,sep=""))), NEO$ФАКУЛЬТ)
+   t <- t[,1:2]
+ 
+   t.s <- t[c(2,5),]
+   t.s <- t.s[1,] + t.s[2,]
+   t.n <- colSums(t)
+ 
+   prop.test(t.s,t.n)$p.value
+ }
> 
> s(1)
[1] 0.03947158
> s(2)
[1] 0.2195671
> s(3)
[1] 0.1844232
> s(4)
[1] 0.03787673
> s(5)
[1] 0.01895418
> s(6)
[1] 0.7500178
> s(7)
[1] 0.01719088
> s(8)
[1] 0.001249577
> s(9)
[1] 0.3442347
> s(10)
[1] 0.09982268
> s(11)
[1] 0.3037563
> 
