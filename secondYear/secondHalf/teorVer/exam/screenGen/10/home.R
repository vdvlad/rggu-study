# №10
s <- function(N_var) {
  set.seed(N_var)
  p <- sample(20:39,1)/1000; N <- sample(301:499,1)
  x <- rbinom(N,1000,p)
  sqrt(2*mean(x)*(1-mean(x)/1000))
}

s(1)
s(2)
s(3)
s(4)
s(5)
s(6)
s(7)
s(8)
s(9)
s(10)
s(11)
