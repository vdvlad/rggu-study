> library(bootstrap)
> 
> # №20
> s &lt;- function(N_var) {
<span foreground="red">+   set.seed(20*N_var); load("NEO")</span>
<span foreground="red">+   v &lt;- sample(names(NEO)[19:53][-c(1,8,15,22,29)],2)</span>
<span foreground="red">+   x &lt;- v[1]</span>
<span foreground="red">+   y &lt;- v[2]</span>
<span foreground="red">+ </span>
<span foreground="red">+   s &lt;- na.omit(subset(NEO</span>
<span foreground="red">+              , select = c(eval(parse(text=x)), eval(parse(text=y)))))</span>
<span foreground="red">+ </span>
<span foreground="red">+   s1 &lt;- s[1][,1]</span>
<span foreground="red">+   s2 &lt;- s[2][,1]</span>
<span foreground="red">+ </span>
<span foreground="red">+   s.cor = cor(s1,s2,method="kendal")</span>
<span foreground="red">+   theta &lt;- function(ind) cor(s1[ind],s2[ind],method="kendal")</span>
<span foreground="red">+   B1 &lt;- bootstrap::bootstrap(seq(length(s1)), 10000, theta)</span>
<span foreground="red">+   ci1 &lt;- c(estimate=s.cor, quantile(B1$thetastar, c(.025, .975)))</span>
<span foreground="red">+   as.numeric(ci1[3]-ci1[2])</span>
<span foreground="red">+ }</span>
> 
> s(1)
[1] 0.1440695
> s(2)
[1] 0.1516049
> s(3)
[1] 0.1519068
> s(4)
[1] 0.1552053
> s(5)
[1] 0.1382756
> s(6)
[1] 0.1561316
> s(7)
[1] 0.1554743
> s(8)
[1] 0.1538574
> s(9)
[1] 0.1455999
> s(10)
[1] 0.1498753
> s(11)
[1] 0.1364475
> 
