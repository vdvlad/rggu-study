R version 4.2.0 (2022-04-22) -- "Vigorous Calisthenics"
> library(bootstrap)
> 
> # №20
> s <- function(N_var) {
+   set.seed(20*N_var); load("NEO")
+   v <- sample(names(NEO)[19:53][-c(1,8,15,22,29)],2)
+   x <- v[1]
+   y <- v[2]
+ 
+   s <- na.omit(subset(NEO
+              , select = c(eval(parse(text=x)), eval(parse(text=y)))))
+ 
+   s1 <- s[1][,1]
+   s2 <- s[2][,1]
+ 
+   s.cor = cor(s1,s2,method="kendal")
+   theta <- function(ind) cor(s1[ind],s2[ind],method="kendal")
+   B1 <- bootstrap::bootstrap(seq(length(s1)), 10000, theta)
+   ci1 <- c(estimate=s.cor, quantile(B1$thetastar, c(.025, .975)))
+   as.numeric(ci1[3]-ci1[2])
+ }
> 
> s(1)
[1] 0.1440695
> s(2)
[1] 0.1516049
> s(3)
[1] 0.1519068
> s(4)
[1] 0.1552053
> s(5)
[1] 0.1382756
> s(6)
[1] 0.1561316
> s(7)
[1] 0.1554743
> s(8)
[1] 0.1538574
> s(9)
[1] 0.1455999
> s(10)
[1] 0.1498753
> s(11)
[1] 0.1364475
> 
