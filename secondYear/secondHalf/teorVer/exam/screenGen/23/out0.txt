R version 4.2.0 (2022-04-22) -- "Vigorous Calisthenics"
> # №23
> s <- function(N_var) {
+   set.seed(3*N_var); load("NEO")
+   x <- sample(names(NEO)[19:53][-c(1,8,15,22,29)],1)
+ 
+   s <- na.omit(subset(NEO
+                     , select = eval(parse(text=x))
+                     , subset = (ФАКУЛЬТ == "А")))[,1]
+   t.test(s,mu=28)$p.value
+ }
> 
> s(1)
[1] 1.389237e-14
> s(2)
[1] 0.034464
> s(3)
[1] 0.02003865
> s(4)
[1] 1.333357e-37
> s(5)
[1] 1.389237e-14
> s(6)
[1] 0.04711905
> s(7)
[1] 1.284087e-14
> s(8)
[1] 6.752525e-11
> s(9)
[1] 1.389237e-14
> s(10)
[1] 0.04711905
> s(11)
[1] 0.04711905
> 
