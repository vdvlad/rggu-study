library(bootstrap)

# №19
s <- function(N_var) {
  set.seed(19*N_var); load("NEO")
  v <- sample(names(NEO)[19:53][-c(1,8,15,22,29)],2)
  x <- v[1]
  y <- v[2]

  s <- na.omit(subset(NEO
             , select = c(eval(parse(text=x)), eval(parse(text=y)))))

  s1 <- s[1][,1]
  s2 <- s[2][,1]

  s.cor = cor(s1,s2,method="pearson")
  theta <- function(ind) cor(s1[ind],s2[ind],method="pearson")
  B1 <- bootstrap::bootstrap(seq(length(s1)), 10000, theta)
  ci1 <- c(estimate=s.cor, quantile(B1$thetastar, c(.025, .975)))
  as.numeric(ci1[3]-ci1[2])
}

s(1)
s(2)
s(3)
s(4)
s(5)
s(6)
s(7)
s(8)
s(9)
s(10)
s(11)
