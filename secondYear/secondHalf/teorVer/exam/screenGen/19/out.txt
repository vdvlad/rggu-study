> library(bootstrap)
> 
> # №19
> s &lt;- function(N_var) {
<span foreground="red">+   set.seed(19*N_var); load("NEO")</span>
<span foreground="red">+   v &lt;- sample(names(NEO)[19:53][-c(1,8,15,22,29)],2)</span>
<span foreground="red">+   x &lt;- v[1]</span>
<span foreground="red">+   y &lt;- v[2]</span>
<span foreground="red">+ </span>
<span foreground="red">+   s &lt;- na.omit(subset(NEO</span>
<span foreground="red">+              , select = c(eval(parse(text=x)), eval(parse(text=y)))))</span>
<span foreground="red">+ </span>
<span foreground="red">+   s1 &lt;- s[1][,1]</span>
<span foreground="red">+   s2 &lt;- s[2][,1]</span>
<span foreground="red">+ </span>
<span foreground="red">+   s.cor = cor(s1,s2,method="pearson")</span>
<span foreground="red">+   theta &lt;- function(ind) cor(s1[ind],s2[ind],method="pearson")</span>
<span foreground="red">+   B1 &lt;- bootstrap::bootstrap(seq(length(s1)), 10000, theta)</span>
<span foreground="red">+   ci1 &lt;- c(estimate=s.cor, quantile(B1$thetastar, c(.025, .975)))</span>
<span foreground="red">+   as.numeric(ci1[3]-ci1[2])</span>
<span foreground="red">+ }</span>
> 
> s(1)
[1] 0.2154844
> s(2)
[1] 0.2139278
> s(3)
[1] 0.1956256
> s(4)
[1] 0.1643818
> s(5)
[1] 0.1986083
> s(6)
[1] 0.1876286
> s(7)
[1] 0.1658142
> s(8)
[1] 0.1783414
> s(9)
[1] 0.1450768
> s(10)
[1] 0.1546016
> s(11)
[1] 0.2091835
> 
