fx <- function(x,x0) {
  1/length(x0)*sum(x-x0 >= 0)
}

# №8
s <- function(N_var) {
  set.seed(N_var)
  m <- sample(1715:1725,1)/10; s <- sample(601:799,1)/100
  N <- sample(301:499,1); x <- round(rnorm(N,m,s),1)
  print(1-fx(190,x))
}


s(1)
s(2)
s(3)
s(4)
s(5)
s(6)
s(7)
s(8)
s(9)
s(10)
s(11)
