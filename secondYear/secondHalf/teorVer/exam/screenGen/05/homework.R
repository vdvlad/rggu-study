## N_var <- 1

# №5
s <- function(N_var) {
  set.seed(N_var)
  m <- sample(21:39,1); N <- sample(201:399,1)
  x <- round(rexp(N,1/m),2)
  as.numeric(quantile(x,probs=0.25))
}

s(1)
s(2)
s(3)
s(4)
s(5)
s(6)
s(7)
s(8)
s(9)
s(10)
s(11)
