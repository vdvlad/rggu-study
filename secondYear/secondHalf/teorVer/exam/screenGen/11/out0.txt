R version 4.2.0 (2022-04-22) -- "Vigorous Calisthenics"
> # №11
> s <- function(N_var) {
+   set.seed(6*N_var); load("NEO")
+   x <- sample(names(NEO)[19:53][-c(1,8,15,22,29)],1)
+   sum(na.omit(NEO[x]) > 30)
+ }
> 
> s(1)
[1] 131
> s(2)
[1] 26
> s(3)
[1] 79
> s(4)
[1] 195
> s(5)
[1] 79
> s(6)
[1] 131
> s(7)
[1] 102
> s(8)
[1] 46
> s(9)
[1] 195
> s(10)
[1] 111
> s(11)
[1] 115
> 
