> # №11
> s &lt;- function(N_var) {
<span foreground="red">+   set.seed(6*N_var); load("NEO")</span>
<span foreground="red">+   x &lt;- sample(names(NEO)[19:53][-c(1,8,15,22,29)],1)</span>
<span foreground="red">+   sum(na.omit(NEO[x]) > 30)</span>
<span foreground="red">+ }</span>
> 
> s(1)
[1] 131
> s(2)
[1] 26
> s(3)
[1] 79
> s(4)
[1] 195
> s(5)
[1] 79
> s(6)
[1] 131
> s(7)
[1] 102
> s(8)
[1] 46
> s(9)
[1] 195
> s(10)
[1] 111
> s(11)
[1] 115
> 
