# №21
s <- function(N_var) {
  set.seed(N_var); load("NEO")
  x <- sample(names(NEO)[19:53][-c(1,8,15,22,29)],1)

  s <- na.omit(subset(NEO
                    , select = eval(parse(text=x))
                    , subset = (ПОЛ == "ЖЕН")))[,1]
  shapiro.test(s)$p.value
}

s(1)
s(2)
s(3)
s(4)
s(5)
s(6)
s(7)
s(8)
s(9)
s(10)
s(11)
