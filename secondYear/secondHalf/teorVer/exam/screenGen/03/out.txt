> # №3
> s &lt;- function(N_var) {
<span foreground="red">+   set.seed(N_var)</span>
<span foreground="red">+   a &lt;- sample(31:49,1)/10; N &lt;- 10*sample(25:39,1)</span>
<span foreground="red">+   x &lt;- rpois(N,a); head(x)</span>
<span foreground="red">+   print(mean(x))</span>
<span foreground="red">+ }</span>
> 
> s(1)
[1] 3.329032
> s(2)
[1] 4.516667
> s(3)
[1] 3.497059
> s(4)
[1] 4.214815
> s(5)
[1] 3.151429
> s(6)
[1] 4.191892
> s(7)
[1] 4.148148
> s(8)
[1] 3.210256
> s(9)
[1] 3.674286
> s(10)
[1] 4.063636
> s(11)
[1] 3.071875
> 
