> library(bootstrap)
> 
> # №37
> s &lt;- function(N_var) {
<span foreground="red">+   set.seed(N_var); load("NEO")</span>
<span foreground="red">+   x=sample(names(NEO)[19:53][-c(1,8,15,22,29)],1)</span>
<span foreground="red">+ </span>
<span foreground="red">+   s &lt;- na.omit(subset(NEO</span>
<span foreground="red">+                     , select = c(eval(parse(text=x)), ПОЛ)))</span>
<span foreground="red">+   s1 &lt;- s[1][s[2] == "ЖЕН"]</span>
<span foreground="red">+   s2 &lt;- s[1][s[2] == "МУЖ"]</span>
<span foreground="red">+ </span>
<span foreground="red">+   theta &lt;- function(i) ks.test(s1[i],s2[i])$p.value</span>
<span foreground="red">+   B1 &lt;- bootstrap::bootstrap(seq(length(s1)), 10000, theta)$thetastar</span>
<span foreground="red">+   sum(B1 &lt; 0.05)/length(B1)</span>
<span foreground="red">+ }</span>
> s(1)
[1] 0.2522
> s(2)
[1] 0.9453
> s(3)
[1] 0.8345
> s(4)
[1] 0.9986
> s(5)
[1] 0.7085
> s(6)
[1] 0.9445
> s(7)
[1] 0.1917
> s(8)
[1] 0.9143
> s(9)
[1] 0.2627
> s(10)
[1] 0.5158
> s(11)
[1] 0.2355
> 
