> # №30
> s &lt;- function(N_var) {
<span foreground="red">+   set.seed(N_var); load("NEO")</span>
<span foreground="red">+   x=sample(names(NEO)[19:53][-c(1,8,15,22,29)],1)</span>
<span foreground="red">+ </span>
<span foreground="red">+   oneway.test(eval(parse(text=paste("NEO$",x,sep="")))</span>
<span foreground="red">+               ~</span>
<span foreground="red">+               NEO$СТЕП_РЕЛ)$p.value</span>
<span foreground="red">+ }</span>
> 
> s(1)
[1] 0.6869603
> s(2)
[1] 0.7320882
> s(3)
[1] 0.4816705
> s(4)
[1] 0.02581175
> s(5)
[1] 0.08808197
> s(6)
[1] 0.7320882
> s(7)
[1] 0.5734439
> s(8)
[1] 0.03064652
> s(9)
[1] 0.6223111
> s(10)
[1] 0.0403288
> s(11)
[1] 0.2818539
> 
