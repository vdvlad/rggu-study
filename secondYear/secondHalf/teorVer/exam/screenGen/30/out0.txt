R version 4.2.0 (2022-04-22) -- "Vigorous Calisthenics"
> # №30
> s <- function(N_var) {
+   set.seed(N_var); load("NEO")
+   x=sample(names(NEO)[19:53][-c(1,8,15,22,29)],1)
+ 
+   oneway.test(eval(parse(text=paste("NEO$",x,sep="")))
+               ~
+               NEO$СТЕП_РЕЛ)$p.value
+ }
> 
> s(1)
[1] 0.6869603
> s(2)
[1] 0.7320882
> s(3)
[1] 0.4816705
> s(4)
[1] 0.02581175
> s(5)
[1] 0.08808197
> s(6)
[1] 0.7320882
> s(7)
[1] 0.5734439
> s(8)
[1] 0.03064652
> s(9)
[1] 0.6223111
> s(10)
[1] 0.0403288
> s(11)
[1] 0.2818539
> 
