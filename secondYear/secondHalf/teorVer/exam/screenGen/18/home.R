library(bootstrap)

# №18
s <- function(N_var) {
  set.seed(18*N_var); load("NEO")
  x <- sample(names(NEO)[19:53][-c(1,8,15,22,29)],1)

  s <- na.omit(subset(NEO
             , select = eval(parse(text=x))))[,1]

  s.q = quantile(s,0.25)
  theta <- function(x) quantile(x,0.25)
  B1 <- bootstrap::bootstrap(s, 10000, theta)
  ci1 <- c(estimate=s.q, quantile(B1$thetastar, c(.025, .975)))
  as.numeric(ci1[3]-ci1[2])
}

s(1)
s(2)
s(3)
s(4)
s(5)
s(6)
s(7)
s(8)
s(9)
s(10)
s(11)
