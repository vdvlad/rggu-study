get_mode <- function(c) {
  a <- table(c)
  as.numeric(names(a[a == max(a)]))
}

# №4
s <- function(N_var) {
  set.seed(N_var)
  a <- sample(31:49,1)/10; N <- 10*sample(25:39,1)
  x <- rpois(N,a)
  print(get_mode(x))
}

s(1)
s(2)
s(3)
s(4)
s(5)
s(6)
s(7)
s(8)
s(9)
s(10)
s(11)
