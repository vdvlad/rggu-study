> library(bootstrap)
> 
> # №39
> 
> izba &lt;- function(a,b) {
<span foreground="red">+   t &lt;- table(a,b)</span>
<span foreground="red">+   t &lt;- t[,1:2]</span>
<span foreground="red">+ </span>
<span foreground="red">+   t.s &lt;- t[c(2,5),]</span>
<span foreground="red">+   t.s &lt;- t.s[1,] + t.s[2,]</span>
<span foreground="red">+   t.n &lt;- colSums(t)</span>
<span foreground="red">+   prop.test(t.s,t.n)$p.value</span>
<span foreground="red">+ }</span>
> 
> s &lt;- function(N_var) {
<span foreground="red">+   set.seed(N_var); load("NEO")</span>
<span foreground="red">+   x=sample(names(NEO)[19:53][-c(1,8,15,22,29)],1)</span>
<span foreground="red">+ </span>
<span foreground="red">+   s &lt;- na.omit(subset(NEO,</span>
<span foreground="red">+              , select = c(eval(parse(text=x)),ФАКУЛЬТ)))</span>
<span foreground="red">+   s1 &lt;- s[1][,1]</span>
<span foreground="red">+   s2 &lt;- s[2][,1]</span>
<span foreground="red">+   theta &lt;- function(i) izba(s1[i],s2[i])</span>
<span foreground="red">+   B1 &lt;- bootstrap::bootstrap(seq(length(s1)), 10000, theta)$thetastar</span>
<span foreground="red">+   sum(B1 &lt; 0.05)/length(B1)</span>
<span foreground="red">+ }</span>
> s(1)
[1] 0.1227
> s(2)
[1] 0.0781
> s(3)
[1] 0.0697
> s(4) # Этот ответ не верный, но тебе не повезло
[1] NA
> s(5)
[1] 0.0197
> s(6)
[1] 0.0765
> s(7)
[1] 0.1242
> s(8)
[1] 0.0372
> s(9)
[1] NA
> s(10)
[1] 0.3788
> s(11)
[1] 0.1272
> 
