library(bootstrap)

# №39

izba <- function(a,b) {
  t <- table(a,b)
  t <- t[,1:2]

  t.s <- t[c(2,5),]
  t.s <- t.s[1,] + t.s[2,]
  t.n <- colSums(t)
  prop.test(t.s,t.n)$p.value
}

s <- function(N_var) {
  set.seed(N_var); load("NEO")
  x=sample(names(NEO)[19:53][-c(1,8,15,22,29)],1)

  s <- na.omit(subset(NEO,
             , select = c(eval(parse(text=x)),ФАКУЛЬТ)))
  s1 <- s[1][,1]
  s2 <- s[2][,1]
  theta <- function(i) izba(s1[i],s2[i])
  B1 <- bootstrap::bootstrap(seq(length(s1)), 10000, theta)$thetastar
  sum(B1 < 0.05)/length(B1)
}
s(1)
s(2)
s(3)
s(4) # Этот ответ не верный, но тебе не повезло
s(5)
s(6)
s(7)
s(8)
s(9)
s(10)
s(11)
