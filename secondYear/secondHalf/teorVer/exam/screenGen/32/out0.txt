R version 4.2.0 (2022-04-22) -- "Vigorous Calisthenics"
> library(bootstrap)
> 
> # №32
> s <- function(N_var) {
+   set.seed(N_var); load("NEO")
+   x=sample(names(NEO)[19:53][-c(1,8,15,22,29)],1)
+ 
+   s <- na.omit(subset(NEO
+                     , select = eval(parse(text=x))
+                     , subset = (ПОЛ == "МУЖ")))[,1]
+ 
+   theta <- function(x) shapiro.test(x)$p.value
+   B1 <- bootstrap::bootstrap(s, 10000, theta)$thetastar
+   sum(B1 < 0.05)/length(B1)
+ }
> s(1)
[1] 0.9164
> s(2)
[1] 0.981
> s(3)
[1] 0.4325
> s(4)
[1] 0.9241
> s(5)
[1] 0.5868
> s(6)
[1] 0.9784
> s(7)
[1] 0.7185
> s(8)
[1] 0.6887
> s(9)
[1] 0.7974
> s(10)
[1] 0.9083
> s(11)
[1] 0.9561
> 
