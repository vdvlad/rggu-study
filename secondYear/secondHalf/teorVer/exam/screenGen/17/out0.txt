R version 4.2.0 (2022-04-22) -- "Vigorous Calisthenics"
> library(bootstrap)
> 
> # №17
> s <- function(N_var) {
+   set.seed(17*N_var); load("NEO")
+   x <- sample(names(NEO)[19:53][-c(1,8,15,22,29)],1)
+ 
+   s <- na.omit(subset(NEO
+              , select = eval(parse(text=x))))[,1]
+ 
+   s.var = var(s)
+   theta <- function(x) var(x)
+   B1 <- bootstrap::bootstrap(s, 10000, theta)
+   ci1 <- c(estimate=s.var, quantile(B1$thetastar, c(.025, .975)))
+   as.numeric(ci1[3]-ci1[2])
+ }
> 
> s(1)
[1] 4.371374
> s(2)
[1] 6.039883
> s(3)
[1] 4.72518
> s(4)
[1] 5.116752
> s(5)
[1] 6.093253
> s(6)
[1] 6.085846
> s(7)
[1] 6.319169
> s(8)
[1] 4.436623
> s(9)
[1] 6.369406
> s(10)
[1] 7.029431
> s(11)
[1] 4.076203
> 
