R version 4.2.0 (2022-04-22) -- "Vigorous Calisthenics"
> library(bootstrap)
> 
> # №36
> s <- function(N_var) {
+   set.seed(N_var); load("NEO")
+   x=sample(names(NEO)[19:53][-c(1,8,15,22,29)],1)
+ 
+   s <- na.omit(subset(NEO
+                     , select = c(eval(parse(text=x)), ПОЛ)))
+   s1 <- s[1][s[2] == "ЖЕН"]
+   s2 <- s[1][s[2] == "МУЖ"]
+ 
+   theta <- function(i) wilcox.test(s1[i],s2[i])$p.value
+   B1 <- bootstrap::bootstrap(seq(length(s1)), 10000, theta)$thetastar
+   sum(B1 < 0.05)/length(B1)
+ }
> s(1)
[1] 0.3097
> s(2)
[1] 0.9774
> s(3)
[1] 0.9366
> s(4)
[1] 0.9993
> s(5)
[1] 0.753
> s(6)
[1] 0.9794
> s(7)
[1] 0.2948
> s(8)
[1] 0.9092
> s(9)
[1] 0.3221
> s(10)
[1] 0.2516
> s(11)
[1] 0.2227
> 
