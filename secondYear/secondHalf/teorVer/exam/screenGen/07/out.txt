> fx &lt;- function(x,x0) {
<span foreground="red">+   1/length(x0)*sum(x-x0 >= 0)</span>
<span foreground="red">+ }</span>
> 
> # №7
> s &lt;- function(N_var) {
<span foreground="red">+   set.seed(N_var)</span>
<span foreground="red">+   m &lt;- sample(1715:1725,1)/10; s &lt;- sample(601:799,1)/100</span>
<span foreground="red">+   N &lt;- sample(301:499,1); x &lt;- round(rnorm(N,m,s),1)</span>
<span foreground="red">+   print(fx(170,x))</span>
<span foreground="red">+ }</span>
> 
> s(1)
[1] 0.3683084
> s(2)
[1] 0.4248366
> s(3)
[1] 0.4227273
> s(4)
[1] 0.4074074
> s(5)
[1] 0.414433
> s(6)
[1] 0.3678647
> s(7)
[1] 0.3523573
> s(8)
[1] 0.4168865
> s(9)
[1] 0.3300654
> s(10)
[1] 0.3529412
> s(11)
[1] 0.3698347
> 
