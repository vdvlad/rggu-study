R version 4.2.0 (2022-04-22) -- "Vigorous Calisthenics"
> # №27
> s <- function(N_var) {
+   set.seed(4*N_var); load("NEO")
+   x <- sample(names(NEO)[19:53][-c(1,8,15,22,29)],1)
+ 
+   s <- na.omit(subset(NEO
+                     , select = c(eval(parse(text=x)), ПОЛ)))
+   s1 <- s[1][s[2] == "ЖЕН"]
+   s2 <- s[1][s[2] == "МУЖ"]
+   var.test(s1,s2)$p.value
+ }
> 
> s(1)
[1] 0.5953427
> s(2)
[1] 0.1314067
> s(3)
[1] 0.3203712
> s(4)
[1] 0.06593282
> s(5)
[1] 0.4079949
> s(6)
[1] 0.05001885
> s(7)
[1] 0.03845265
> s(8)
[1] 0.4079949
> s(9)
[1] 0.6611129
> s(10)
[1] 0.184727
> s(11)
[1] 0.03845265
> 
