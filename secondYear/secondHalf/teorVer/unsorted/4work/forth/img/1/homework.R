# приготовления
n <- 330
set.seed(2022003)

# №1
x <- rbinom(n, size = 610, prob = 0.04)
p <- shapiro.test(x)$p.value
# H0 = закон распределения не отличается от нормального закона
# H1 = закон распределения отличается от нормального закона
round(p,3)
if (p > 0.05) {print("Принимается H0")} else {print("Принимается H1")}

stat.test <- function (i, sig.level=0.05) {
  s <- sapply(1:i,tau)
  sum(s < sig.level)/i
}

# a) Без ресамплинга
tau <- function(i) {
  r <- rbinom(n, size = 610, prob = 0.04)
  shapiro.test(r)$p.value
}
stat.test(10000)

# b) Классический бутстреп
tau <- function(i) {
  s <- sample(x,n,replace=TRUE)
  shapiro.test(s)$p.value
}
stat.test(10000)
