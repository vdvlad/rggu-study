for i in *
do
  if [ -d $i ]
  then
    cp $i/homework.R $i/home.R
    # Master
    # export NVAR=420
    # export NI=Эстетичность
    # export NIFULL=O2_Эстетичность
    # export SEED=2022012
    # My
    export NVAR=310
    export NI=Тревожность
    export NIFULL=N1_Тревожность
    export SEED=2022001
    # Alex
    # export NVAR=370
    # export NI=Общительность
    # export NIFULL=E2_Общительность
    # export SEED=2022007
    # Lexail
    # export NVAR=330
    # export NI=N3_Депрессивность
    # export NIFULL=Депрессивность
    # export SEED=2022003
    perl -pi -e "s/N3_Депрессивность/$NIFULL/g;s/Депрессивность/$NI/g;s/(n\s*<-) 330/\1 $NVAR/g;s/set.seed\(.*\)/set.seed($SEED)/g;" $i/home.R
    cat $i/home.R | R --vanilla | grep "[>+\[\"]" > $i/out.txt
    perl -pi -e 's/</&lt;/g;s%^(\+.*)$%<span foreground="red">\1</span>%' $i/out.txt
    # convert -size 1200x1200 xc:grey -font "Source-Code-Pro" -pointsize 12 -fill black -annotate +15+15 "@$i/out.txt" -trim +repage -bordercolor grey -border 10x10 $i/output$i.png
    # convert -size 1200x1200 xc:grey -font "Source-Code-Pro" -pointsize 12 -fill black pango: "@$i/out.txt" -trim +repage -bordercolor grey -border 10x10 $i/output$i.png
    magick -background grey -font consolas -pointsize 12 pango:"@$i/out.txt" -bordercolor grey -trim -border 10x10 $i/output$i.png
    rm $i/out.txt $i/home.R
  fi
done;
