# приготовления
n <- 330
set.seed(2022003)

# №3
x   <- rbinom(n, size = 100, prob = 0.4)
y   <- rnorm(200, mean = 43, sd = 5)
p   <- ks.test(x,y)$p.value
# H0 = две выборки из ген. совокупностей с не отличающимися законами распределения
# H1 = две выборки из ген. совокупностей с отличающимися законами распределения
round(p,3)
if (p > 0.05) {print("Принимается H0")} else {print("Принимается H1")}

stat.test <- function (i, sig.level=0.05) {
  s <- sapply(1:i,tau)
  sum(s < sig.level)/i
}

# a) Без ресамплинга
tau <- function(i) {
  r1  <- rbinom(n, size = 100, prob = 0.4)
  r2  <- rnorm(200, mean = 43, sd = 5)
  ks.test(r1,r2)$p.value
}
stat.test(10000)

# b) Классический бутстреп
tau <- function(i) {
  s1   <- sample(x,n  ,replace=TRUE)
  s2   <- sample(y,200,replace=TRUE)
  ks.test(s1,s2)$p.value
}
stat.test(10000)
