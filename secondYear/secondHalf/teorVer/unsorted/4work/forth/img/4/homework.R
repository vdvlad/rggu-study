# приготовления
n <- 330
set.seed(2022003)

# №4
x   <- rnorm(n, mean = 160, sd = 9)
p   <- t.test(x,mu=164)$p.value
# H0 = мат. ожидание не отличается от 164
# H1 = мат. ожидание отличается от 164
round(p,3)
if (p > 0.05) {print("Принимается H0")} else {print("Принимается H1")}

stat.test <- function (i, sig.level=0.05) {
  s <- sapply(1:i,tau)
  sum(s < sig.level)/i
}

# a) Без ресамплинга
tau <- function(i) {
  r <- rnorm(n, mean = 160, sd = 9)
  t.test(r,mu=164)$p.value
}
stat.test(10000)

# b) Классический бутстреп
tau <- function(i) {
  s <- sample(x,n  ,replace=TRUE)
  t.test(s,mu=164)$p.value
}
stat.test(10000)
