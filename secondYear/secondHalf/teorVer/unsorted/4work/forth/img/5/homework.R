# приготовления
n <- 330
set.seed(2022003)

# №5
x <- rbinom(n, size = 200, prob = 0.04)
p <- wilcox.test(x,mu=10)$p.value
# H0 = положение случайной величины не отличается от 10
# H1 = положение случайной величины отличается от 10
round(p,3)
if (p > 0.05) {print("Принимается H0")} else {print("Принимается H1")}

stat.test <- function (i, sig.level=0.05) {
  s <- sapply(1:i,tau)
  sum(s < sig.level)/i
}

# a) Без ресамплинга
tau <- function(i) {
  r <- rbinom(n, size = 200, prob = 0.04)
  wilcox.test(r,mu=10)$p.value
}
stat.test(10000)

# b) Классический бутстреп
tau <- function(i) {
  s <- sample(x,n  ,replace=TRUE)
  wilcox.test(s,mu=10)$p.value
}
stat.test(10000)
