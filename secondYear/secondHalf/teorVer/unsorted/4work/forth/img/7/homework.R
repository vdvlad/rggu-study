# приготовления
load("NEO")
n  <- 330
Ni <- "N3_Депрессивность"
set.seed(2022003)

# №7
x <- na.omit(subset(NEO, select = N3_Депрессивность,
           , subset = (ПОЛ == "ЖЕН")))[,1]
y <- na.omit(subset(NEO, select = N3_Депрессивность,
           , subset = (ПОЛ == "МУЖ")))[,1]
p <- t.test(x,y)$p.value
# H0 = мат. ожидания N3_Депрессивность парней и девушек не отличаются
# H1 = мат. ожидания N3_Депрессивность парней и девушек отличаются
round(p,3)
if (p > 0.05) {print("Принимается H0")} else {print("Принимается H1")}

stat.test <- function (i, sig.level=0.05) {
  s <- sapply(1:i,tau)
  sum(s < sig.level)/i
}

# Классический бутстреп
tau <- function(i) {
  s1 <- sample(x,length(x),replace=TRUE)
  s2 <- sample(y,length(y),replace=TRUE)
  p  <- t.test(s1,s2)$p.value
}

stat.test(10000)
stat.test(10000)
