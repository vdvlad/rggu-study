for i in *
do
  cp $i/homework.R $i/home.R
  perl -pi -e 's/N3_Депрессивность/N4_Застенчивость/g;s/Депрессивность/Застенчивость/g;s/n <- 330/n <- 340/g;s/set.seed(2022003)/set.seed(2022004)/g' $i/home.R
  # sed -n -e 14,1000p $i/home.R > $i/pr.R
  convert -size 1200x1200 xc:white -font "Times-New-Roman" -pointsize 12 -fill black -annotate +15+15 "@$i/pr.R" -trim +repage -bordercolor white -border 10x10 $i/code.png
  Rscript $i/home.R > $i/out.R
  convert -size 1200x1200 xc:white -font "Times-New-Roman" -pointsize 12 -fill black -annotate +15+15 "@$i/out.R" -trim +repage -bordercolor white -border 10x10 $i/output.png
  rm $i/pr.R $i/home.R $i/out.R
done;
