#!/usr/bin/Rscript

n     <- 330
Ni    <- 'N3_Депрессивность'
set.seed(2022003)
load("NEO")
encForR <- function(x){if (!is.numeric(x)) {iconv(x,to="utf8",from="cp1251")} else {x}}
names(NEO) <- encForR(names(NEO))

for (i in 1:length(names(NEO))) {
  NEO[i] = lapply(NEO[i],encForR)
}

# №7
x <- subset(NEO
           , select = N3_Депрессивность,
           , subset = (ПОЛ == "ЖЕН"))
x <- na.omit(x)[,1]

y <- subset(NEO
           , select = N3_Депрессивность,
           , subset = (ПОЛ == "МУЖ"))
y <- na.omit(y)[,1]

p <- t.test(x,y)$p.value
"P-value, H1 = 'мат. ожидания равны':"
round(p,3)
if (p > 0.05) {
    print("Принимается")
} else {
    print("Не Принимается")
}
