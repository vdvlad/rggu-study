#!/usr/bin/Rscript

n     <- 330
Ni    <- 'N3_Депрессивность'
set.seed(2022003)
load("NEO")
encForR <- function(x){if (!is.numeric(x)) {iconv(x,to="utf8",from="cp1251")} else {x}}
names(NEO) <- encForR(names(NEO))

for (i in 1:length(names(NEO))) {
  NEO[i] = lapply(NEO[i],encForR)
}

# №5
x <- rbinom(n, size = 200, prob = 0.04)
p <- wilcox.test(x, mu = 10)$p.value
"P-value, H1 = 'M(x)=10':"
round(p,3)
if (p > 0.05) {
    print("Принимается")
} else {
    print("Не принимается")
}
