#!/usr/bin/Rscript

n     <- 400
Ni    <- 'E5_Непоседливость'
set.seed(2022010)
load("NEO")
encForR <- function(x){if (!is.numeric(x)) {iconv(x,to="utf8",from="cp1251")} else {x}}
names(NEO) <- encForR(names(NEO))

for (i in 1:length(names(NEO))) {
  NEO[i] = lapply(NEO[i],encForR)
}

# №4
x <- rnorm(n, mean = 160, sd = 8)
p <- shapiro.test(x)$p.value
"Проверим применимость критерия Стьюдента"
round(p,3)
if (p > 0.05) {
  print("Применим")
  p2 <- t.test(x,mu=164)$p.value
  print("P-value")
  round(p2,3)
  if (p2 > 0.05) {
    print("H1= (Мат Ожидание = 164)")
    print("Принимается")
  } else {
    print("H1= (Мат Ожидание = 164)")
    print("Не Принимается")
  }
} else {
  print("Не Применим")
}
