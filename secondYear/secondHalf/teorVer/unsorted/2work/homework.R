#!/usr/bin/Rscript

if (length(commandArgs(trailingOnly=TRUE))) {
  n  = commandArgs(trailingOnly=TRUE)[1]
  Ni = commandArgs(trailingOnly=TRUE)[2]
} else {
  n     <<- 400
  Ni    <<- 'E5_Непоседливость'
  seedy <<- 2022010
}

k <- 1:1000;

work.number <- "№0"
work.number.set <- function(x) {
  work.number <<- x
  set.seed(seedy)
  print("")
  print(x)
}

pr <- function(x,num,y=3) {
  if (length(num) > 1) {
    print(x)
    print(round(num,y))
  } else {
    if (length(x) < 35) {
      x <- paste(x,strrep(" ",35-nchar(x)), "");
    }
    if (is.numeric(num)) {
      print(paste(x,toString(round(num,y))))
    } else {
      print(paste(x,num))
    }
  }
}

load("NEO")
encForR <- function(x){if (!is.numeric(x)) {iconv(x,to="utf8",from="cp1251")} else {x}}
names(NEO) <- encForR(names(NEO))

for (i in 1:length(names(NEO))) {
  NEO[i] = lapply(NEO[i],encForR)
}

# №1
work.number.set("№1")

n1.x <- subset(NEO, select = eval(parse(text = Ni)))

n1.Fn <- ecdf(n1.x[,1])
n1.name <- "Гистограмма эмпирической функции распределения"
jpeg(paste(work.number,".", n1.name,".jpeg",sep=""))
plot(n1.Fn, main = n1.name)
dev.off()

# №2
work.number.set("№2")

n2.d <- density(n1.x[,1], na.rm = TRUE)
n2.name <- "Полигон эмпирической плотности вероятности"
jpeg(paste(work.number,".", n2.name,".jpeg",sep=""))
plot(n2.d, type= "l", main = n2.name)
dev.off()

# №3
work.number.set("№3")

jpeg(paste(work.number,". ", Ni, " ~ ФАКУЛЬТ", ".jpeg",sep=""))
boxplot(eval(parse(text = Ni)) ~ ФАКУЛЬТ, data = NEO,
             main = paste("boxplot ", Ni, " ~ ФАКУЛЬТ"),
             xlab = "ФАКУЛЬТ", ylab = Ni)
dev.off()

# №4
work.number.set("№4")

n4 <- subset(NEO, select = eval(parse(text = Ni)))
n4 <- na.omit(n4)
jpeg(paste(work.number,". ", Ni, " Q-Q plot", ".jpeg",sep=""))
qqnorm(n4[,1], main = paste("Q-Q plot ", names(n4)))
qqline(n4[,1])
dev.off()

# №5
work.number.set("№5")

n5.db <- subset(NEO, select = c(eval(parse(text = Ni))
                               ,N6_Уязвимость))
n5.db <- na.omit(n5.db)

jpeg(paste(work.number,". Диаграмма рассеивания.jpeg",sep=""))
plot(n5.db)
n5.x  <- n5.db[,1]; n5.y <- n5.db[,2]; m <- lm(n5.y ~ n5.x); abline(m)
dev.off()

n5.cor <- cor(n5.x, n5.y)
pr("Корреляция по Пирсону", n5.cor)

# №6
work.number.set("№6")
n6.x <- rnorm(n, mean = 172, sd = 6.4)
n6.p <- 0.95
n6.n <- length(n6.x)

n6.tau <- function(v) mean(v)
n6.tau.s <- 0
for (i in k) n6.tau.s[i] <- n6.tau(n6.x[ceiling(n6.n*runif(n6.n))])
n6.ciMX <- quantile(n6.tau.s, c((1-n6.p)/2,(1+n6.p)/2))
pr("Доверительный интервал по среднему", n6.ciMX)

n6.tau2 <- function(v) var(v)
n6.tau.s <- 0
for (i in k) n6.tau.s[i] <- n6.tau2(n6.x[ceiling(n6.n*runif(n6.n))])
n6.ciDX <- quantile(n6.tau.s, c((1-n6.p)/2,(1+n6.p)/2))
pr("Доверительный интервал по дисперсии", n6.ciDX)

# №7
work.number.set("№7")

n7.x <- rnorm(n, mean = 172, sd = 6.4)

n7.tau <- function(v) sum(v >= 170 & v <= 176)/n6.n
n7.tau.s <- 0
for (i in k) n7.tau.s[i] <- n7.tau(n7.x[ceiling(n6.n*runif(n6.n))])
n7.ciPA <- quantile(n7.tau.s, c((1-n6.p)/2,(1+n6.p)/2))
pr("Доверительный интервал, k<=1000, [170,176]", n7.ciPA)

# №8
work.number.set("№8")
n8.x <- rpois(n, lambda = 8.1)
n8.p <- 0.95
n8.n <- length(n8.x)
n8.tau.s <- 0
for (i in k) n8.tau.s[i] <- n6.tau(n8.x[ceiling(n8.n*runif(n8.n))])
n8.ciMX <- quantile(n8.tau.s, c((1-n8.p)/2,(1+n8.p)/2))
pr("Доверительный интервал по среднему", n8.ciMX)

# №9
work.number.set("№9")
n9.p <- 0.95

n9.tau <- function(v) cor(v[,1], v[,2])
n9.n   <- nrow(n5.db)
n9.tau.s <- 0
for (i in k) n9.tau.s[i] <- n9.tau(n5.db[ceiling(n9.n*runif(n9.n)),])
n9.cir <- quantile(n9.tau.s, c((1-n9.p)/2,(1+n9.p)/2))
pr("Коэффициент корреляции по Пирсону", n9.cir)

# №10
work.number.set("№10")
n10.p <- 0.95

n10.tau <- function(v) cor(v[,1], v[,2], method = "spearman")
n10.tau.s <- 0
for (i in k) n10.tau.s[i] <- n10.tau(n5.db[ceiling(n9.n*runif(n9.n)),])
n10.cir <- quantile(n10.tau.s, c((1-n10.p)/2,(1+n10.p)/2))
pr("Коэффициент корреляции по Спирману", n10.cir)
