#!/usr/bin/Rscript

if (length(commandArgs(trailingOnly=TRUE))) {
  n  = commandArgs(trailingOnly=TRUE)[1]
  Ni = commandArgs(trailingOnly=TRUE)[2]
} else {
  n  <<- 310
  Ni <<- 'N1_Тревожность'
}

work.number <- "№0"
work.number.set <- function(x) {
  work.number <<- x
  set.seed(2022001)
  print("")
  print(x)
}

wri <- function(x,name,type="poly") {
  if (type == "poly") {
    # in pdf troubles with cyrrilic
    name <- paste("Полигон ", name,sep="")
    jpeg(paste(work.number,".", name,".jpeg",sep=""))
    plot(x, type= "l", main = name)
    dev.off()
  } else if (type == "hist") {
    # in pdf troubles with cyrrilic
    name <- paste("Гистограмма ", name,sep="")
    jpeg(paste(work.number,".", name,".jpeg",sep=""))
    plot(x, main = name)
    dev.off()
  } else if (type == "plot_dot") {
    # in pdf troubles with cyrrilic
    jpeg(paste(work.number,".", name,".jpeg",sep=""))
    plot(x, main = name)
    dev.off()
  }
}

pr <- function(x,num,y=3) {
  if (length(num) > 1) {
    print(x)
    print(round(num,y))
  } else {
    if (length(x) < 35) {
      x <- paste(x,strrep(" ",35-nchar(x)), "");
    }
    if (is.numeric(num)) {
      print(paste(x,toString(round(num,y))))
    } else {
      print(paste(x,num))
    }
  }
}

# №1
work.number.set("№1")

x <- rbinom(n, size=100, prob=0.517)

x.m <- mean(x);
pr("Среднее Арифметическое: ", x.m)

x.var <- var(x)
pr("Дисперсия: ", x.var)

x.sd  <- sd(x)
pr("Среднее квадратическое отклонение: ", x.sd)

pr("Наибольшее: ", max(x))
pr("Наименьшее: ", min(x))

x.median <- median(x)
pr("Медиана: ", x.median)

# №2
work.number.set("№2")

y <- rpois(n, lambda=8.1)

y.var <- table(y);
pr("Вариационный ряд:",y.var)

y.var.cum <- cumsum(y.var)
pr("Накопленные частоты выборки:", y.var.cum)

y.var.length   <- sum(y.var)
y.var.relative <- y.var/y.var.length
pr("Относительные частоты выборки:", y.var.relative)

y.var.relative.cum <- cumsum(y.var.relative)
pr("Накопленные относительные частоты выборки:", y.var.relative.cum)

wri(y.var, "частот", "poly")
wri(y.var, "частот", "hist")

wri(y.var.cum, "накопленных частот", "poly")
wri(y.var.cum, "накопленных частот", "hist")

# №3
work.number.set("№3")

z <- rexp(n, 0.4)

z.probs <- c(0,0.05,0.25,0.5,0.75,0.95,1)
z.qua <- quantile(z,z.probs)

pr("Квантили уровней", z.qua, 2)

pr("Межквартильный размах", IQR(z))

pr("Относительная частота при < 1", sum(z<1)/length(z))

# №4
work.number.set("№4")

w <- rnorm(n, 172, 6.4)

w.cum_func    <- ecdf(w)
w.cum_density <- density(w)

wri(w.cum_func,    "Эмпирическая функция распределения", "plot_dot")
wri(w.cum_density, "Эмпирическая плотность распределения", "plot_dot")

w.A <- w.cum_func(176) - w.cum_func(170)
pr("Событие A (на отрезке [170,176])",w.A)

w.B <- w.cum_func(182) - w.cum_func(176)
pr("Событие B (на отрезке [176,182])",w.B)

w.C <- 1 - w.cum_func(190)
pr("Событие C (> 190)", w.C)

# №5
work.number.set("№5")
v <- runif(n, 12, 19)

# min and max in calculated becouse real min and max
# will be little different with ideal
jpeg(paste(work.number
         , "Гистограмма равномерного распределения.jpeg"
         , sep = "."))
hist(v, breaks = seq(min(v),max(v), length.out = 6)
      , main = "Гистограмма равномерного распределения"
      , ylab = "Частоты"
     )
dev.off()


### for second part
load("NEO")

# №6
work.number.set("№6")

a <- subset(NEO
         , select = c(ПОЛ, eval(parse(text = Ni)))
         , subset = (ПОЛ == "МУЖ" & eval(parse(text = Ni)) > 28))
pr("Число мужчин с параметром > 28", nrow(a))

# №7
work.number.set("№7")

b <- subset(NEO
         , select = eval(parse(text = Ni))
         , subset = (ПОЛ == "ЖЕН" & ФАКУЛЬТ == "Б" & ВОЗРАСТ < 19))

b.max <- max(b[,1])
if (is.na(b.max)) {
  print("Данные не заполнены корректно (присутствует NA), вычисления будет производиться без учёта неизвестных полей")
  bt <- b[,1]
  pr(paste("Максимальная",Ni),max(bt[!is.na(bt)]))
} else {
  pr(paste("Максимальная",Ni),b.max)
}

# №8
work.number.set("№8")

c <- subset(NEO
         , select = eval(parse(text = Ni))
         , subset = (ПОЛ == "ЖЕН" & СТЕП_РЕЛ == "СЛАБАЯ"))
c.mean <- mean(c[,1])
if (is.na(c.mean)) {
  print("Данные не заполнены корректно (присутствует NA), вычисления будет производиться без учёта неизвестных полей")
  ct <- c[,1]
  pr("Среднее арифметическое ", max(ct[!is.na(ct)]) ,2)
} else {
  pr("Среднее арифметическое ", c.mean ,2)
}

# №9
work.number.set("№9")

d <- subset(NEO
         , select = c(ЗОДИАК,eval(parse(text = Ni))))
d.var <- tapply(d[,2],d[,1],var)

pr("Зодиак с высочайшей дисперсией", names(which.max(d.var)))

# №10
work.number.set("№10")

e <- subset(NEO
         , select = eval(parse(text = Ni)))

jpeg(paste(work.number,".","Гистограмма частот ", Ni ,".jpeg",sep=""))
hist(e[,1], main = paste("Гистограмма частот",Ni)
          , xlab=Ni, ylab="Частоты")
dev.off()
