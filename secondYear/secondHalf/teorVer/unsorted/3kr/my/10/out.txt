> #!/usr/bin/Rscript
> 
> n     <- 400
> Ni    <- 'E5_Непоседливость'
> set.seed(2022010)
> load("NEO")
> encForR <- function(x){if (!is.numeric(x)) {iconv(x,to="utf8",from="cp1251")} else {x}}
> names(NEO) <- encForR(names(NEO))
> 
> for (i in 1:length(names(NEO))) {
+   NEO[i] = lapply(NEO[i],encForR)
+ }
> 
> # №10
> 
> p <- kruskal.test(NEO$E5_Непоседливость ~ NEO$СЕМ_ДОХ)$p.value
> "P-value, H1 = 'Непоседливость не зависит от семейного дохода':"
[1] "P-value, H1 = 'Непоседливость не зависит от семейного дохода':"
> round(p,3)
[1] 0.598
> if (p > 0.05) {
+     print("Принимается")
+ } else {
+     print("Не Принимается")
+ }
[1] "Принимается"
> 
