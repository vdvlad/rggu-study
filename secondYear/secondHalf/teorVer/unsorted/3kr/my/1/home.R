# перекодировка файла NEO
load("NEO")
encForR <- function(x){if (!is.numeric(x)) {iconv(x,to="utf8",from="cp1251")} else {x}}
names(NEO) <- encForR(names(NEO))

for (i in 1:length(names(NEO))) {
  NEO[i] = lapply(NEO[i],encForR)
}

# приготовления
n     <- 400
Ni    <- 'E5_Непоседливость'
set.seed(2022010)

# №1
# a)
x <- rbinom(n, size = 610, prob = 0.04)
p <- shapiro.test(x)$p.value
"P-value, H1 = 'не отл. от нормального закона':"
round(p,3)

# b)
