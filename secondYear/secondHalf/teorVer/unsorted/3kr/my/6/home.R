#!/usr/bin/Rscript

n     <- 400
Ni    <- 'E5_Непоседливость'
set.seed(2022010)
load("NEO")
encForR <- function(x){if (!is.numeric(x)) {iconv(x,to="utf8",from="cp1251")} else {x}}
names(NEO) <- encForR(names(NEO))

for (i in 1:length(names(NEO))) {
  NEO[i] = lapply(NEO[i],encForR)
}

# №6

x <- subset(NEO
           , select = E5_Непоседливость,
           , subset = (ПОЛ == "ЖЕН"))
x <- na.omit(x)[,1]
p <- shapiro.test(x)$p.value
"P-value, H1 = 'не отл. от нормального закона':"
round(p,3)
if (p > 0.05) {
    print("Принимается")
} else {
    print("Не Принимается")
}
