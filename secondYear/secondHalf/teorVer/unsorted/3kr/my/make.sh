for i in *
do
  cp $i/homework.R $i/home.R
  perl -pi -e 's/N3_Депрессивность/E5_Непоседливость/g;s/Депрессивность/Непоседливость/g;s/(n\s*<-) 330/\1 400/g;s/set.seed\(.*\)/set.seed(2022010)/g;' $i/home.R
  # sed -n -e 11,1000p $i/home.R > $i/pr.R
  # convert -size 1200x1200 xc:grey -font "Source-Code-Pro" -pointsize 12 -fill black -annotate +15+15 "@$i/pr.R" -trim +repage -bordercolor grey -border 10x10 $i/code.png
  cat $i/home.R | R --vanilla | grep "[>+\[]" > $i/out.txt
  convert -size 1200x1200 xc:grey -font "Source-Code-Pro" -pointsize 12 -fill black -annotate +15+15 "@$i/out.txt" -trim +repage -bordercolor grey -border 10x10 $i/output.png
  # convert $i/output.png -crop 0x0+0+200 $i/output.png
  # rm $i/pr.R $i/out.txt
done;
