#!/usr/bin/Rscript

n     <- 330
Ni    <- 'N3_Депрессивность'
set.seed(2022003)
load("NEO")
encForR <- function(x){if (!is.numeric(x)) {iconv(x,to="utf8",from="cp1251")} else {x}}
names(NEO) <- encForR(names(NEO))

for (i in 1:length(names(NEO))) {
  NEO[i] = lapply(NEO[i],encForR)
}

# №8

attach(NEO)
p <- wilcox.test(NEO$N3_Депрессивность[СТЕП_РЕЛ == "СИЛЬНАЯ"],
                 NEO$N3_Депрессивность[СТЕП_РЕЛ == "СЛАБАЯ"])$p.value
detach(NEO)
"P-value, H1 = 'уровень для сильной и слабой одинаков':"
round(p,3)
if (p > 0.05) {
    print("Принимается")
} else {
    print("Не Принимается")
}
