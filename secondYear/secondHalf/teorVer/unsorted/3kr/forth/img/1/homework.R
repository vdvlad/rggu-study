#!/usr/bin/Rscript

n     <- 330
Ni    <- 'N3_Депрессивность'
set.seed(2022003)
load("NEO")
encForR <- function(x){if (!is.numeric(x)) {iconv(x,to="utf8",from="cp1251")} else {x}}
names(NEO) <- encForR(names(NEO))

for (i in 1:length(names(NEO))) {
  NEO[i] = lapply(NEO[i],encForR)
}

# №1
x <- rbinom(n, size = 610, prob = 0.04)
p <- shapiro.test(x)$p.value
"P-value, H1 = 'не отл. от нормального закона':"
round(p,3)
if (p > 0.05) {
    print("Принимается")
} else {
    print("Не Принимается")
}
