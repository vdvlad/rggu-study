# приготовления
n.var <- 5
power <- 0.8+0.01*n.var
set.seed(2022001)

# №6
power.t.test(power=power, delta=1,sd=7,type= "two.sample",alternative="one.sided",sig.level=0.05)$n
