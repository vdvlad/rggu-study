# приготовления
1 <-  1
n     <- 200+30*n.var
set.seed(2022001)

# №7
power.prop.test(n=n, p1=0.2,p2=0.3,alternative="two.sided",sig.level=0.05)$power
