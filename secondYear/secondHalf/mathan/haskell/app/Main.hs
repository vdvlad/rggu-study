{-# LANGUAGE BangPatterns #-}
module Main where

type DDD = (Double,Double,Double)

trailingSpaces :: String -> String
trailingSpaces (' ':str) = trailingSpaces str
trailingSpaces str = str

-- for speed on list string is reversed
extract :: String -> [String]
extract input = reverse $ split input "" []
  where
    reverse' :: [String] -> [String]
    reverse' = reverse . filter (== "")
    split :: String -> String -> [String] -> [String]
    split []      cur !out = if (cur == "")
                             then out
                             else cur:out
    split (i:in') cur !out = if (i == ' ')
                             then if (cur == "")
                                  then split in' "" out
                                  else split in' "" (cur:out)
                             else split in' (i:cur) out

understand :: [String] -> String
understand [] = "Empty command"
understand (head':str) = case head' of
                           -- function
                           "noitcnuf" -> if (length str == 2)
                                         then show (function (rere (head str)) (rere (str !! 1)))
                                         else "Error 1"
                           -- taylor
                           "rolyat" -> if (length str == 2)
                                       then show (taylor (rere (head str)) (rere (str !! 1)))
                                       else "Error 2"
                           -- diff
                           "ffid" -> if (null str)
                                     then "Error 3"
                                     else if (head str == "tniop" && length (tail str) == 2) -- point
                                          then show (diff (point (rere (str !! 1)) (rere (str !! 2))))
                                          else if (head str == "lavretni" && length (tail str) == 4) -- interval
                                               then show (diff (interval (rere (str !! 1)) (rere (str !! 2))
                                                                        (rere (str !! 3)) (rere (str !! 4))))
                                               else "Error 4"
  where
    rere :: String -> Double
    rere x = read (reverse x) :: Double

function :: Double -> Double -> Double
function x y = y/x

taylor :: Double -> Double -> Double
taylor x y = 1 + (-(x-1) + (y-1))
               + ((x-1)^2 - (x-1)*(y-1))
               -- + 1/2*(2*(x-1)^2 - 2*(x-1)*(y-1))
               -- + 1/6*(-6*(x-1)^3 - 6*(x-1)^2*(y-1))
               + (-(x-1)^3 - (x-1)^2*(y-1))

point :: Double -> Double -> DDD
point x y = (taylor x y - function x y,x,y)

interval :: Double -> Double -> Double -> Double -> DDD
interval xL xR yL yR = seg (xL+0.01) (f xL yL,xL,yL)
  where
    f :: Double -> Double -> Double
    f x1 y1 = taylor x1 y1 - function x1 y1
    greater :: Double -> DDD -> Bool
    greater a (b,_,_) = abs(a)>abs(b)
    seg :: Double -> DDD -> DDD -- seg from segment of R
    seg xL' !max'= if (xR < xL')
                    then max'
                    else seg (xL'+0.01) (seg' xL' yL max')
    seg' :: Double -> Double -> DDD -> DDD
    seg' xcur yL' !max'= if (yR < yL')
                          then max'
                          else let h = f xcur yL'
                               in if (greater (f xcur yL') max')
                                  then seg' xcur (yL'+0.01) (h,xcur,yL')
                                  else seg' xcur (yL'+0.01) max'

diff :: (Double,Double,Double) -> String
diff (delta,x,y) = if delta > 0
           then "taylor > function in (" ++ show x ++ "," ++ show y ++ "): " ++ show delta
           else "taylor < function in (" ++ show x ++ "," ++ show y ++ "): " ++ (show . abs) delta

main = do
  putStrLn "ENTER:"
  putStrLn "┌-------------------------------------------------------------┐"
  putStrLn "| v f(x,y) = xy                                               |"
  putStrLn "| function x y                                                |"
  putStrLn "| v taylor function of f(x,y) = xy                            |"
  putStrLn "| taylor x y                                                  |"
  putStrLn "| v difference between f(x0,y0) and taylor(x0,y0) in point    |"
  putStrLn "| diff point x y                                              |"
  putStrLn "| v difference between f(x0,y0) and taylor(x0,y0) in interval |"
  putStrLn "| diff interval x_left x_right y_left y_right                 |"
  putStrLn "└-------------------------------------------------------------┘"
  putStrLn ""
  putStrLn "Print command, please."
  i <- getLine
  i1 <- return $ trailingSpaces i
  let loop i' = if ((take 3 i') == "end")
                then return ()
                else do
                       putStrLn $ understand (extract i') ++ "\n"
                       putStrLn "Print command, please."
                       i <- getLine
                       i1 <- return $ trailingSpaces i
                       loop i1
  loop i1
