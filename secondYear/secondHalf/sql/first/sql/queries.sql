-- select 1
SELECT * FROM product_accounting_view;

-- select 2
SELECT * FROM product_accounting_view where cost < 5000;

-- select 3
SELECT name_product,cost
FROM product_accounting_view where cost BETWEEN 5000 and 1000000;

-- select 4
SELECT * FROM product_accounting_view where name_product LIKE 'Немецкий%';

-- select 5
SELECT name_product, (cost*100) as Price_of_100
FROM product_accounting_view;

-- select 6
SELECT name_product, cost
FROM product_accounting_view ORDER BY cost DESC;

-- select 7
SELECT name_commodity, sum(cost*count_product)
AS Price_of_all_of_cathegory
FROM product_accounting_view GROUP BY name_commodity;

-- select 8
SELECT name_commodity, cost, count_product as Price_of_all_of_cathegory
FROM product_accounting_view where cost < 20000 and count_product > 70;

-- select 9
SELECT DISTINCT name_commodity, cost
FROM product_accounting_view ORDER BY cost;

-- select 10
SELECT DISTINCT name_commodity, cost
FROM product_accounting_view ORDER BY cost LIMIT 10 OFFSET 7;

-- delete 1
DELETE FROM product_accounting_view where cost < 5000;
SELECT * FROM product_accounting_view;

-- update 1
UPDATE product_accounting_view
SET name_commodity = 'Хозтовары'
WHERE name_commodity = 'Машины';
SELECT * FROM product_accounting_view;
