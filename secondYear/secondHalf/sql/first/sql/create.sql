DROP VIEW     product_accounting_view;
DROP TRIGGER  product_accounting_view_trig ON product_accounting_view;
DROP FUNCTION product_accounting_view_check();

DROP INDEX name_commodity_idx;
DROP TABLE product_accounting;
DROP TABLE commodity_group_table;

-- commodity_group_table
CREATE TABLE commodity_group_table (
    id             serial PRIMARY KEY
  , name_commodity varchar(100) NOT NULL UNIQUE
);
CREATE INDEX name_commodity_idx
ON commodity_group_table
USING hash
(name_commodity);

-- product_accounting
CREATE TABLE product_accounting (
    -- Номер продукта
    number_product serial PRIMARY KEY
    -- Товарная группа
  , commodity_group int NOT NULL
  , FOREIGN KEY (commodity_group) REFERENCES commodity_group_table(id)
    -- Название продукции
    -- По стандартам < 200
  , name_product varchar(200) UNIQUE
    -- Стоимость, в копейках
  , cost int NOT NULL
      check (cost > 0)
    -- Количество
  , count_product int DEFAULT 0
      check (count_product >= 0)
);

-- product_accounting_view
CREATE VIEW product_accounting_view AS
  SELECT
    number_product
  , name_commodity
  , name_product
  , cost
  , count_product
  FROM product_accounting p, commodity_group_table c
  where p.commodity_group = c.id;

-- product_accounting_view_check
CREATE FUNCTION product_accounting_view_check() RETURNS TRIGGER AS $product_accounting_view$
  BEGIN
    IF (TG_OP = 'DELETE') THEN
        DELETE FROM product_accounting
        WHERE
              number_product  = OLD.number_product
          AND commodity_group =
              (SELECT c.id
               FROM commodity_group_table c
               WHERE OLD.name_commodity = c.name_commodity)
          AND name_product    = OLD.name_product
          AND cost            = OLD.cost
          AND count_product   = OLD.count_product;
        RETURN OLD;

-- product_accounting_view_check_2
    ELSIF (TG_OP = 'UPDATE') THEN
        UPDATE product_accounting SET
            number_product  = NEW.number_product
          , name_product    = NEW.name_product
          , commodity_group =
            (SELECT c.id
             FROM commodity_group_table c
             WHERE NEW.name_commodity = c.name_commodity)
          , cost            = NEW.cost
          , count_product   = NEW.count_product
        WHERE
              number_product  = OLD.number_product
          AND name_product    = OLD.name_product
          AND commodity_group =
              (SELECT c.id
               FROM commodity_group_table c
               WHERE OLD.name_commodity = c.name_commodity)
          AND cost           = OLD.cost
          AND count_product  = OLD.count_product;
        RETURN NEW;

-- product_accounting_view_check_3
    ELSIF (TG_OP = 'INSERT') THEN
        INSERT INTO product_accounting as p SELECT
            NEW.number_product
          , c.id
          , NEW.name_product
          , NEW.cost
          , NEW.count_product
        FROM commodity_group_table c
        WHERE NEW.name_commodity = c.name_commodity
        ;
        RETURN NEW;
    END IF;
    RETURN NULL;
  END;
$product_accounting_view$ LANGUAGE plpgsql;

-- product_accounting_view_trig
CREATE TRIGGER product_accounting_view_trig
  INSTEAD OF INSERT OR UPDATE OR DELETE ON product_accounting_view
  FOR EACH ROW
  EXECUTE PROCEDURE product_accounting_view_check();
