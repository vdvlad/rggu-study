-- select 1
SELECT * FROM product_accounting_view;
 number_product | name_commodity |   name_product    | cost  | count_product 
----------------+----------------+-------------------+-------+---------------
              7 | Стройматериалы | Упаковка арматуры |    10 |           200
              8 | Стройматериалы | Бетон             |    12 |            20
              9 | Стройматериалы | Цемент            |  1000 |            20
             10 | Хозтовары      | Кухонный нож      |    30 |            20
             11 | Хозтовары      | Рюмка             |    30 |          2000
             12 | Хозтовары      | Бокал             |    30 |          2000
             13 | Хозтовары      | Ваза              |    30 |          2000
             14 | Хозтовары      | Мебель            |    30 |            20
             15 | Машины         | Bugatti           | 10000 |            20
             16 | Машины         | Porshe            | 10000 |            20
             17 | Машины         | Subaru            | 10000 |            20
             18 | Машины         | Lexus             |    10 |            20
             19 | Машины         | Шестёрка          |   499 |            20
             20 | Холодильники   | Немецкий А1       |   499 |           300
             21 | Холодильники   | Немецкий А2       |    10 |           300
             22 | Хозтовары      | Клей              |    10 |           300
             23 | Хозтовары      | Плафон            |    10 |           300
             24 | Холодильники   | Русский Б2        | 50000 |           300
             25 | Холодильники   | Русский Б3        | 50000 |           250
             26 | Холодильники   | Русский Б17       | 50000 |           250
             27 | Холодильники   | Русский Б9        | 50000 |           250
(21 rows)

-- select 2
SELECT * FROM product_accounting_view where cost < 5000;
 number_product | name_commodity |   name_product    | cost | count_product 
----------------+----------------+-------------------+------+---------------
             23 | Хозтовары      | Плафон            |   10 |           300
             22 | Хозтовары      | Клей              |   10 |           300
             14 | Хозтовары      | Мебель            |   30 |            20
             13 | Хозтовары      | Ваза              |   30 |          2000
             12 | Хозтовары      | Бокал             |   30 |          2000
             11 | Хозтовары      | Рюмка             |   30 |          2000
             10 | Хозтовары      | Кухонный нож      |   30 |            20
              9 | Стройматериалы | Цемент            | 1000 |            20
              8 | Стройматериалы | Бетон             |   12 |            20
              7 | Стройматериалы | Упаковка арматуры |   10 |           200
             19 | Машины         | Шестёрка          |  499 |            20
             18 | Машины         | Lexus             |   10 |            20
             21 | Холодильники   | Немецкий А2       |   10 |           300
             20 | Холодильники   | Немецкий А1       |  499 |           300
(14 rows)

-- select 3
SELECT name_product,cost
FROM product_accounting_view where cost BETWEEN 5000 and 1000000;
 name_product | cost  
--------------+-------
 Bugatti      | 10000
 Porshe       | 10000
 Subaru       | 10000
 Русский Б2   | 50000
 Русский Б3   | 50000
 Русский Б17  | 50000
 Русский Б9   | 50000
(7 rows)

-- select 4
SELECT * FROM product_accounting_view where name_product LIKE 'Немецкий%';
 number_product | name_commodity | name_product | cost | count_product 
----------------+----------------+--------------+------+---------------
             20 | Холодильники   | Немецкий А1  |  499 |           300
             21 | Холодильники   | Немецкий А2  |   10 |           300
(2 rows)

-- select 5
SELECT name_product, (cost*100) as Price_of_100
FROM product_accounting_view;
   name_product    | price_of_100 
-------------------+--------------
 Упаковка арматуры |         1000
 Бетон             |         1200
 Цемент            |       100000
 Кухонный нож      |         3000
 Рюмка             |         3000
 Бокал             |         3000
 Ваза              |         3000
 Мебель            |         3000
 Bugatti           |      1000000
 Porshe            |      1000000
 Subaru            |      1000000
 Lexus             |         1000
 Шестёрка          |        49900
 Немецкий А1       |        49900
 Немецкий А2       |         1000
 Клей              |         1000
 Плафон            |         1000
 Русский Б2        |      5000000
 Русский Б3        |      5000000
 Русский Б17       |      5000000
 Русский Б9        |      5000000
(21 rows)

-- select 6
SELECT name_product, cost
FROM product_accounting_view ORDER BY cost DESC;
   name_product    | cost  
-------------------+-------
 Русский Б2        | 50000
 Русский Б9        | 50000
 Русский Б17       | 50000
 Русский Б3        | 50000
 Subaru            | 10000
 Bugatti           | 10000
 Porshe            | 10000
 Цемент            |  1000
 Шестёрка          |   499
 Немецкий А1       |   499
 Мебель            |    30
 Рюмка             |    30
 Кухонный нож      |    30
 Бокал             |    30
 Ваза              |    30
 Бетон             |    12
 Клей              |    10
 Плафон            |    10
 Lexus             |    10
 Упаковка арматуры |    10
 Немецкий А2       |    10
(21 rows)

-- select 7
SELECT name_commodity, sum(cost*count_product)
AS Price_of_all_of_cathegory
FROM product_accounting_view GROUP BY name_commodity;
 name_commodity | price_of_all_of_cathegory 
----------------+---------------------------
 Машины         |                    610180
 Стройматериалы |                     22240
 Холодильники   |                  52652700
 Хозтовары      |                    187200
(4 rows)

-- select 8
SELECT name_commodity, cost, count_product as Price_of_all_of_cathegory
FROM product_accounting_view where cost < 20000 and count_product > 70;
 name_commodity | cost | price_of_all_of_cathegory 
----------------+------+---------------------------
 Хозтовары      |   10 |                       300
 Хозтовары      |   10 |                       300
 Хозтовары      |   30 |                      2000
 Хозтовары      |   30 |                      2000
 Хозтовары      |   30 |                      2000
 Стройматериалы |   10 |                       200
 Холодильники   |   10 |                       300
 Холодильники   |  499 |                       300
(8 rows)

-- select 9
SELECT DISTINCT name_commodity, cost
FROM product_accounting_view ORDER BY cost;
 name_commodity | cost  
----------------+-------
 Машины         |    10
 Стройматериалы |    10
 Хозтовары      |    10
 Холодильники   |    10
 Стройматериалы |    12
 Хозтовары      |    30
 Машины         |   499
 Холодильники   |   499
 Стройматериалы |  1000
 Машины         | 10000
 Холодильники   | 50000
(11 rows)

-- select 10
SELECT DISTINCT name_commodity, cost
FROM product_accounting_view ORDER BY cost LIMIT 10 OFFSET 7;
 name_commodity | cost  
----------------+-------
 Холодильники   |   499
 Стройматериалы |  1000
 Машины         | 10000
 Холодильники   | 50000
(4 rows)

-- delete 1
DELETE FROM product_accounting_view where cost < 5000;
DELETE 14
SELECT * FROM product_accounting_view;
 number_product | name_commodity | name_product | cost  | count_product 
----------------+----------------+--------------+-------+---------------
             15 | Машины         | Bugatti      | 10000 |            20
             16 | Машины         | Porshe       | 10000 |            20
             17 | Машины         | Subaru       | 10000 |            20
             24 | Холодильники   | Русский Б2   | 50000 |           300
             25 | Холодильники   | Русский Б3   | 50000 |           250
             26 | Холодильники   | Русский Б17  | 50000 |           250
             27 | Холодильники   | Русский Б9   | 50000 |           250
(7 rows)

-- update 1
UPDATE product_accounting_view
SET name_commodity = 'Хозтовары'
WHERE name_commodity = 'Машины';
UPDATE 3
SELECT * FROM product_accounting_view;
 number_product | name_commodity | name_product | cost  | count_product 
----------------+----------------+--------------+-------+---------------
             24 | Холодильники   | Русский Б2   | 50000 |           300
             25 | Холодильники   | Русский Б3   | 50000 |           250
             26 | Холодильники   | Русский Б17  | 50000 |           250
             27 | Холодильники   | Русский Б9   | 50000 |           250
             15 | Хозтовары      | Bugatti      | 10000 |            20
             16 | Хозтовары      | Porshe       | 10000 |            20
             17 | Хозтовары      | Subaru       | 10000 |            20
(7 rows)

