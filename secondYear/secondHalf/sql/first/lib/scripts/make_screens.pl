use 5.10.0;
use Parallel::ForkManager;

$PATH_SQL = './sql';
$PATH_TMP = './tmp';
$PATH_IMG = './img';


qx?rm $PATH_TMP/select_out.txt?;
qx?psql -U vlad -d mydb -a -f $PATH_SQL/create.sql?;
qx?psql -U vlad -d mydb -a -f $PATH_SQL/insert.sql?;
qx?psql -U vlad -d mydb -a -f $PATH_SQL/queries.sql > $PATH_TMP/queries_out.txt?;

open(my $FILE,"<", "$PATH_TMP/queries_out.txt");
my $k = 0;
my @data;
my @dataName;
my $res;
my $last = 1;
while (my $i = <$FILE>) {
  if ($i =~ /^-- ([\w \d]+)/) {
    $tmp = $1;
    $tmp =~ s/ /\\ /g;
    push(@dataName,$tmp);
    $res .= "\n" unless $res eq "";
    $k    = 1;
    $res .= $i;
  } elsif ($k == 1) {
    if ($i =~ /^\s*$/) {
      $k     = 0;
      push(@data,$res);
      $last += 1;
      $res   = "";
    } else {
      $res .= $i;
    }
  }
}
if ($res ne "") {
  push(@data,$res)
} else {
  $last--;
}
close($FILE);

my $pm = Parallel::ForkManager->new(10);
DATA_LOOP:
  for $ind(0..$last-1) {
    $pm->start and next DATA_LOOP;
    system("convert -size 1200x1200 xc:grey -font \"Anka/Coder-Regular\" -pointsize 12 -fill black -annotate +15+15 \"@data[$ind]\" -trim +repage -bordercolor grey -border 10x10 $PATH_IMG/@dataName[$ind].png&");
    $pm->finish;

  }
