#!/usr/bin/env perl

use strict;
use warnings;

use File::Basename;
use File::Spec::Functions;
use IPC::Open3;
use JSON::PP;
use Term::ANSIColor;

# Load the configuration file.

my $config_path;
my $config;

sub load_config {
    my $config_text = do {
        local $/ = undef;
        open( my $config_file, '<', $config_path )
          or die "Can't open $config_path: $!";
        <$config_file>;
    };
    $config = decode_json($config_text);
}

if ( defined $ENV{'COLORLATEX_CONFIG'} ) {
    $config_path = $ENV{'COLORLATEX_CONFIG'};
    load_config();
}
else {
    my @config_paths = (
        '../lib/scripts/colorlatex.json'
    );
    my $n = $#config_paths;
    foreach (@config_paths) {
        $config_path = $_;
        if ( !$n-- || -e $config_path ) {
            load_config();
            last;
        }
    }
}

# Get the LaTeX compiler.

$ENV{'PATH'} = "$config->{'compiler'}->{'path'}:$ENV{'PATH'}";

my $compiler_name = basename($0);
if ( $compiler_name eq 'colorlatex.pl' ) {
    $compiler_name = $ARGV[0];
    shift @ARGV;
}
else {
    $compiler_name =~ s/^color//;
}

my $compiler_path = catfile( $config->{'compiler'}->{'path'}, $compiler_name );
die "Can't find $compiler_name in PATH" if ( !-e $compiler_path );

die 'Compiler is self-referencing'
  if ( ( stat $compiler_path )[1] == ( stat $0 )[1] );

my @compiler_args = ( @{ $config->{'compiler'}->{'options'} }, @ARGV );

# Check the color mode.

my $color = "$config->{'color'}";
$color = "$ENV{'COLORLATEX_COLOR'}" if defined $ENV{'COLORLATEX_COLOR'};

die "Unsupported color mode: $color"
  if ( !grep( /^$color$/, ( 'always', 'auto', 'never' ) ) );

# If coloring is disabled, exec the LaTeX compiler.

if ( $color eq 'never' || ( $color eq 'auto' && !-t STDOUT ) ) {
    exec $compiler_name, @compiler_args or die "Can't exec";
}

# Translate colors to escape codes.

my $reset = color('reset');

foreach my $pattern ( @{ $config->{'patterns'} } ) {
    my $style = $pattern->{'style'};
    $pattern->{'color'} = color($style);
}

# Run the LaTeX compiler and colorize the output.

my $compiler_out;
my $compiler_pid = open3( '<&STDIN', $compiler_out, $compiler_out,
    $compiler_name, @compiler_args );
binmode( $compiler_out, ':bytes' );
binmode( \*STDOUT,      ':bytes' );

while ( my $line = <$compiler_out> ) {
    foreach my $pattern ( @{ $config->{'patterns'} } ) {
        my $match = $pattern->{'match'};
        my $color = $pattern->{'color'};
        $line =~ s/($match)/$color$1$reset/g;
    }
    print($line);
}

waitpid( $compiler_pid, 0 );
exit( $? >> 8 );
