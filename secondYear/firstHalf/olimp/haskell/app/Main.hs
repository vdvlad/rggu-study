{-# LANGUAGE QuasiQuotes, FlexibleContexts, BangPatterns #-}
import Prelude
import Data.Maybe (fromJust,isNothing)

import Graphics.Rendering.Chart.Easy (def,plot,points,opaque,liftEC,(.=))
import Graphics.Rendering.Chart.Easy (blue,red,magenta,cyan,setColors)
import Graphics.Rendering.Chart.Backend.Cairo (FileFormat(PNG),toFile)
import Graphics.Rendering.Chart.Backend.Cairo (FileOptions (..))
import Graphics.Rendering.Chart.Plot.Points
import Graphics.Rendering.Chart.Drawing

import Text.Regex.PCRE.Heavy (gsub,re,scan)

import Data.List.Split (splitOn)

ctg :: Double -> Double
ctg x = cos x/sin x

n157 :: Double -> Double -> IO()
n157 xtg ytg =
  do
    let k = -(xtg / ytg)
        o = ytg-k*xtg
    let func = [ (cos t, sin t)  | t <- [-pi,-pi+0.002..pi]]
        deri = [ (t,k*t+o)   | t <- [-2*pi/3,-2*pi/3+0.01..(-pi)/12]]
    toFile def "n157.png" $ do
      plot (points "Function y(x)" (func))
      plot (points "Derivated" (deri))
      plot $ liftEC $ do
        plot_points_title  .= "  O"
        plot_points_style  .= PointStyle (opaque red) (opaque red) 4 4 PointShapeCircle
        plot_points_values .= (xtg,ytg) : []

n158 :: Double -> Double -> IO()
n158 xtg ytg =
  do
    let k = -(xtg / (4*ytg))
        o = ytg-k*xtg
    let func = [ (2*cos t, sin t)  | t <- [-pi,-pi+0.002..pi]]
        deri = [ (t,k*t+o)   | t <- [-2.1,-2.097..2.1]]
    toFile def "n158.png" $ do
      plot (points "Function y(x)" (func))
      plot (points "Derivated" (deri))
      plot $ liftEC $ do
        plot_points_title  .= "  O"
        plot_points_style  .= PointStyle (opaque red) (opaque red) 4 4 PointShapeCircle
        plot_points_values .= (xtg,ytg) : []

n159 :: Double -> IO()
n159 ttg =
  do
    let k = sin ttg / (1-cos ttg)
        xtg = pi/2-1
        ytg = 1
        o = ytg-k*xtg
    let func = [ (t-sin t,1-cos t)  | t <- [-pi,-pi+0.002..pi]]
        deri = [ (t,k*t+o)   | t <- [-2.1,-2.097..2.1]]
    toFile def "n159.png" $ do
      plot (points "Function y(x)" (func))
      plot (points "Derivated" (deri))
      plot $ liftEC $ do
        plot_points_title  .= "  O"
        plot_points_style  .= PointStyle (opaque red) (opaque red) 4 4 PointShapeCircle
        plot_points_values .= (xtg,ytg) : []

n160 :: Double -> IO()
n160 ttg =
  do
    let k = -tan ttg
        xtg = (sin ttg)^3
        ytg = (cos ttg)^3
        o = ytg-k*xtg
    let func = [ ((sin t)^3,(cos t)^3)  | t <- [-pi,-pi+0.002..pi]]
        deri = [ (t,k*t+o)   | t <- [-2.1,-2.097..2.1]]
    toFile def "n160.png" $ do
      plot (points "Function y(x)" (func))
      plot (points "Derivated" (deri))
      plot $ liftEC $ do
        plot_points_title  .= "  O"
        plot_points_style  .= PointStyle (opaque red) (opaque red) 4 4 PointShapeCircle
        plot_points_values .= (xtg,ytg) : []

g :: Double
g = 9.8

n161 :: Double -> Double -> Double -> String -> IO()
n161 v0 alpha t0 name =
  do
    let k = (v0*sin alpha - g*t0)/(v0*cos alpha)
        xtg = v0*cos alpha*t0
        ytg = v0*sin alpha*t0 - g*(t0^2)/2
        o = ytg-k*xtg
    let func = [ (v0*cos alpha*t,v0*sin alpha*t - g*(t^2)/2)  | t <- [-pi,-pi+0.002..3*pi]]
        deri = [ (t,k*t+o)   | t <- [-80,-79.9..200]]
    toFile def name $ do
      plot (points "Function y(x)" (func))
      plot (points "Derivated" (deri))
      plot $ liftEC $ do
        plot_points_title  .= "  O"
        plot_points_style  .= PointStyle (opaque red) (opaque red) 4 4 PointShapeCircle
        plot_points_values .= (xtg,ytg) : []

main :: IO ()
main = do
  putStrLn "Run"
  n157 (-1/2) ((sqrt 3)/2)
  n158 1      (-(sqrt 3)/2)
  n159 (pi/2)
  n160 (pi/4)
  n161 50 (pi/3) 2 "n161a.png"
  n161 50 (pi/3) 7 "n161b.png"
