#include "StackDelim.h"
#include <iostream>

int main() {
  StackDelim s1(525);
  StackDelim s2(s1);
  std::cout << s2 << " " << s2.getNumber() << std::endl;
  std::cout << s2.getTail() << " " << s2.getTail().getNumber() << std::endl;
  std::cout << s2*s1 << " " << (s2*s1).getNumber() << std::endl;
  StackDelim s3(1525);
  std::cout << s3 << " " << s3.getNumber() << std::endl;
  std::cout << s3*s1 << " " << (s3*s1).getNumber() << std::endl;
  std::cout << (s3*s1).length() << std::endl;
  std::cout << std::endl;

  StackDelim s4(525*5);
  std::cout << (s1==s2) << std::endl;
  std::cout << (s1==s4) << std::endl;
  std::cout << (s1==s3) << std::endl;
  std::cout << std::endl;

  std::cout << (s1<s2) << std::endl;
  std::cout << (s1<s4) << std::endl;
  std::cout << (s1<s3) << std::endl;
  return 0;
}
