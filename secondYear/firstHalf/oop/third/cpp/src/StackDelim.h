#include <fstream>
#include <cmath>
#include <string>

class StackDelim {
  private:
    struct StackElem {
      StackElem* d_;
      int        n_;
    };
    StackDelim(const StackElem& d);

    StackElem* d_;

  public:
    StackDelim();
    StackDelim(int num);
    StackDelim(const StackDelim& d);

    StackDelim& operator=(const StackDelim& d);

    int getNumber() const;

    int getHead() const;
    StackDelim getTail() const;

    // include 1
    int length() const;

    StackDelim operator*(const StackDelim& d) const;
    bool operator==(const StackDelim& d) const;
    bool operator!=(const StackDelim& d) const;
    bool operator> (const StackDelim& d) const;
    bool operator< (const StackDelim& d) const;
    bool operator>=(const StackDelim& d) const;
    bool operator<=(const StackDelim& d) const;

    friend std::ostream& operator<<(std::ostream& os, const StackDelim& d);

    ~StackDelim();
};
