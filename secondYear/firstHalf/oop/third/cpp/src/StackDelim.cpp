#include "StackDelim.h"

StackDelim::StackDelim(const StackElem& d) {
  const StackElem*  sl = &d;
  StackElem** sr = &(this->d_);
  while (sl) {
    *sr       = new StackElem;
    (*sr)->n_ = sl->n_;
    sr        = &((*sr)->d_);
    sl        = sl->d_;
  }
  *sr = nullptr;
}

StackDelim::StackDelim() {
  d_ = nullptr;
}

StackDelim::StackDelim(int num) {
  StackElem* oldHead;
  d_ = nullptr;
  if (num <= 0) {
    return;
  }
  while (num % 2 == 0) {
    oldHead = d_;
    d_ = new StackElem;
    d_->d_ = oldHead;

    d_->n_  = 2;
    num    /= 2;
  }
  int sqrtNum = std::sqrt(num);
  int p = 3;
  while (num != 1 && p <= sqrtNum) {
    while (num % p == 0) {
      oldHead = d_;
      d_ = new StackElem;
      d_->d_ = oldHead;

      d_->n_  = p;
      num    /= p;
    }
    p += 2;
  }
  if (num != 1) {
    oldHead = d_;
    d_ = new StackElem;
    d_->d_ = oldHead;

    d_->n_  = num;
  }
}

StackDelim::StackDelim(const StackDelim& d) {
  const StackElem*  sl = d.d_;
  StackElem** sr = &(this->d_);
  *sr = nullptr;
  while (sl) {
    *sr       = new StackElem;
    (*sr)->n_ = sl->n_;
    sr        = &((*sr)->d_);
    sl        = sl->d_;
    *sr = nullptr;
  }
}

StackDelim& StackDelim::operator=(const StackDelim& d) {
  StackElem*  sl = d.d_;
  StackElem** sr = &(this->d_);
  while (sl) {
    *sr       = new StackElem;
    (*sr)->n_ = sl->n_;
    sr        = &((*sr)->d_);
    sl        = sl->d_;
  }
  return *this;
}

int StackDelim::getNumber() const {
  int result    = 1;
  StackElem* s = d_;
  while (s) {
    result *= s->n_;
    s = s->d_;
  }
  return result;
}

int StackDelim::getHead() const {
  if (d_) {
    return d_->n_;
  }
  return 1;
}

StackDelim StackDelim::getTail() const {
  if (this->d_ == nullptr) {
    return StackDelim();
  }
  return StackDelim(*(this->d_->d_));
}

int StackDelim::length() const {
  StackElem* s = this->d_;
  int count = 1;
  while (s) {
    s = s->d_;
    ++count;
  }
  return count;
}

StackDelim StackDelim::operator*(const StackDelim& d) const {
  StackElem* sl = this->d_;
  StackElem* sr = d.d_;

  StackDelim s;
  StackElem* oldHead;
  while (sl != nullptr && sr != nullptr) {
    if (sl->n_ <= sr->n_ ) {
      oldHead = s.d_;
      s.d_ = new StackElem;
      s.d_->d_ = oldHead;

      s.d_->n_ = sr->n_;
      sr = sr->d_;
    } else {
      oldHead = s.d_;
      s.d_ = new StackElem;
      s.d_->d_ = oldHead;

      s.d_->n_ = sl->n_;
      sl = sl->d_;
    }
  }
  while (sl != nullptr) {
    oldHead = s.d_;
    s.d_ = new StackElem;
    s.d_->d_ = oldHead;

    s.d_->n_ = sl->n_;
    sl = sl->d_;
  }
  while (sr != nullptr) {
    oldHead = s.d_;
    s.d_ = new StackElem;
    s.d_->d_ = oldHead;

    s.d_->n_ = sr->n_;
    sr = sr->d_;
  }

  // reverse
  StackDelim s0;
  sr = s.d_;
  while (sr != nullptr) {
    oldHead = s0.d_;
    s0.d_ = new StackElem;
    s0.d_->d_ = oldHead;

    s0.d_->n_ = sr->n_;
    sr = sr->d_;
  }

  return s0;
}

bool StackDelim::operator==(const StackDelim& d) const {
  StackElem* sl = this->d_;
  StackElem* sr = d.d_;
  while (sl != nullptr && sr != nullptr) {
    if (sl->n_ != sr->n_ ) {
      return false;
    }
    sl = sl->d_;
    sr = sr->d_;
  }
  return (sl == sr);
}

bool StackDelim::operator!=(const StackDelim& d) const {
  return !(*this == d);
}

bool StackDelim::operator> (const StackDelim& d) const {
  return this->getNumber() > d.getNumber();
}

bool StackDelim::operator< (const StackDelim& d) const {
  return this->getNumber() < d.getNumber();
}

bool StackDelim::operator>=(const StackDelim& d) const {
  return this->getNumber() >= d.getNumber();
}

bool StackDelim::operator<=(const StackDelim& d) const {
  return this->getNumber() <= d.getNumber();
}

StackDelim::~StackDelim() {
  if (!d_) {
    return;
  }
  StackElem* tail    = d_;
  StackElem* newHead = tail->d_;
  while (newHead) {
    delete tail;
    tail = newHead;
    newHead = tail->d_;
  }
  delete tail;
}

std::ostream& operator<<(std::ostream& os, const StackDelim& d) {
  StackDelim::StackElem* s = d.d_;
  std::string result = "";
  int rank = 0;
  int num  = 1;
  while (s) {
    if (num == s->n_) {
      ++rank;
    } else {
      if (rank > 1) {
        result = std::to_string(num)
               + std::string("^")
               + std::to_string(rank)
               + std::string("-") + result;
      } else {
        if (rank > 0) {
          result = std::to_string(num)
                 + std::string("-") + result;
        }
      }
      num  = s->n_;
      rank = 1;
    }
    s = s->d_;
  }
  if (rank > 1) {
    result = std::to_string(num)
           + std::string("^")
           + std::to_string(rank)
           + std::string("-") + result;
  } else {
    if (rank > 0) {
      result = std::to_string(num)
             + std::string("-") + result;
    }
  }
  os << "\"1-" << result << "Null\"";
  return os;
}
