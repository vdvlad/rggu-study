#!/bin/sbcl --script

(require "delim-stack" "./delim-stack.lisp")

(defparameter aDelim (make-instance 'delim :num 2112312))
(defparameter bDelim (make-instance 'delim :num 4245363))

(print (show aDelim))
(print (getNumber aDelim bDelim))
(print (delimHead aDelim bDelim))
(print (delimTail aDelim bDelim))
(print (show (delimProduct2 aDelim bDelim)))
(print (show (delimProduct aDelim bDelim aDelim)))

(terpri)

(print (delimEqual aDelim bDelim))
(print (delimEqual aDelim aDelim))
(print (delimEqual aDelim aDelim aDelim))
(print (delimEqual aDelim aDelim bDelim))

(terpri)

(print (delimGreater aDelim bDelim))
(print (delimGreater bDelim aDelim))
(print (delimGreater bDelim bDelim))
(print (delimEqLesser aDelim bDelim aDelim bDelim))

(terpri)

(defparameter cDelim (make-instance 'delim))
(print (delimHead cDelim))

(terpri)
