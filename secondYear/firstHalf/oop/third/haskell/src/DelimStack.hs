module DelimStack (genDelim,Delim(..)
                  ,getNumber,head,tail,(*.)
                  ,length) where

import Prelude hiding (head,tail,length)

data Delim a = Null | Cons a (Delim a)
  deriving (Eq)

instance Foldable Delim where
  foldr _ b Null           = b
  foldr f b c@(Cons ch cs) = let x = f ch b
                             in  foldr f x cs
  foldMap _ Null           = mempty
  foldMap m c@(Cons ch cs) = m ch <> foldMap m cs

instance (Show i,Integral i) => Show (Delim i) where
  show Null         = "Null"
  show (Cons ah as) = show' ah 1 as
    where
      show' h num Null = show'' h  num ++ "Null"
      show' h num (Cons ah as) = if (h==ah)
                                 then show'  h (num+1) as
                                 else show'' h  num ++ show' ah 1 as
      show'' h num =
        if (num == 1)
        then "\""
             ++ show h
             ++ "\"-"
        else "\""
             ++ show h
             ++ "^" ++ show num
             ++ "\"-"

instance (Ord i,Integral i) => Ord (Delim i) where
  (<=) a b = getNumber a <= getNumber b


getNumber :: (Ord i, Integral i) => Delim i -> Int
getNumber Null = 1
getNumber a    = fromIntegral $ foldl (*) 1 a

head :: Delim Int -> Int
head Null       = 1
head (Cons a b) = a

tail :: Delim Int -> Delim Int
tail Null       = Null
tail (Cons a b) = b

length :: Delim Int -> Int
length Null       = 0
length (Cons a b) = 1 + length b

-- product of two stacks with delim
(*.) :: Delim Int -> Delim Int -> Delim Int
(*.) a b = reverse' $ prod a b Null
  where
    prod :: Delim Int -> Delim Int -> Delim Int -> Delim Int
    prod Null Null c = c
    prod Null b    c = concat' b c
    prod a    Null c = concat' a c
    prod a@(Cons ah as) b@(Cons bh bs) c =
      if   ah < bh

      then prod as b  (Cons ah c)
      else prod a  bs (Cons bh c)

    concat' :: Delim Int -> Delim Int -> Delim Int
    concat' Null    b = b
    concat' (Cons ah as) b = concat' as (Cons ah b)

    reverse' :: Delim Int -> Delim Int
    reverse' a = reverse'' a Null
      where
        reverse'' :: Delim Int -> Delim Int -> Delim Int
        reverse'' Null b           = b
        reverse'' a@(Cons ah as) b = reverse'' as (Cons ah b)

genDelim :: Int -> Delim Int
genDelim num = delim num (2:[3,5..]) n
  where
    n      = floor $ sqrt $ fromIntegral num
    delim :: Int -> [Int] -> Int -> Delim Int
    delim a b@(bh : bs) c = if (a == 1)
                            then Null
                            else
                              if (bh >= c)
                              then Cons a Null
                              else
                                if (a `mod` bh == 0)
                                then Cons bh (delim (a `div` bh) b c)
                                else delim a bs c
