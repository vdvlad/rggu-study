module Main where

import Prelude
import DelimStack

main :: IO()
main = do
  let d = genDelim (foldl (*) 1 [1..15])
  let e = genDelim 21339723
  putStrLn $ show d
  putStrLn $ show $ getNumber d
  putStrLn $ show e
  putStrLn $ show $ getNumber e
  putStrLn $ show (d *. e)
  let b = genDelim (foldl (*) 1 [1..15])
  putStrLn $ show b
  putStrLn $ show $ DelimStack.length d
  putStrLn ""
  putStrLn $ show $ d == e
  putStrLn $ show $ d == b
  putStrLn ""
  putStrLn $ show $ d > e
  putStrLn $ show $ d < b
