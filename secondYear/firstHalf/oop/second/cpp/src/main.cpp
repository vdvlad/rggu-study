#include "Racio.h"
#include <iostream>

int main() {
  Racio r1(5,25);
  Racio r2(2,3);
  Racio r3(21,4);
  Racio r10(2,2,3);
  std::cout << r10 << std::endl;
  std::cout << std::endl;
  std::cout << (r2 == r1) << std::endl;
  std::cout << (r2 == r2) << std::endl;
  std::cout << std::endl;
  std::cout << r2 + r1 << std::endl;
  std::cout << r2 - 2  << std::endl;
  std::cout << 3 - r2  << std::endl;
  std::cout << std::endl;
  std::cout << r2 * r1  << std::endl;
  std::cout << r2 / r1  << std::endl;
  std::cout << std::endl;
  std::cout << (r2 > r1)  << std::endl;
  std::cout << (r2 <= r2)  << std::endl;
  std::cout << std::endl;
  std::cout << r3.wholePart() << std::endl;
  std::cout << std::endl;
  Racio r4(2.23,0.03);
  std::cout << r4 << std::endl;
  Racio r5(12.23,6.1);
  std::cout << r5 << std::endl;
  std::cout << std::endl;
  std::cout << r1.toDouble() << std::endl;
  return 0;
}
