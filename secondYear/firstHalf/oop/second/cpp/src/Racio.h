#pragma once

#include <fstream>

class Racio {
  private:
    int p_;
    int m_;
    int euclid(const int p, const int m);
  public:
    Racio();
    Racio(const int p, const int m);
    // mixed racio
    Racio(const int z, const int p , const int m);
    Racio(const double d, const double acc);
    Racio(const Racio& r);
    Racio& operator=(const Racio& r);

    int getNumerator() const;
    int getDenumerator() const;

    Racio negate() const;
    Racio swap() const;
    double toDouble() const;
    double floor() const;

    bool operator==(const Racio& r) const;
    bool operator!=(const Racio& r) const;

    bool operator> (const Racio& r) const;
    bool operator< (const Racio& r) const;
    bool operator>=(const Racio& r) const;
    bool operator<=(const Racio& r) const;

    Racio operator+(const Racio& r) const;
    Racio operator-(const Racio& r) const;

    Racio operator*(const Racio& r) const;
    Racio operator/(const Racio& r) const;

    int wholePart() const;
};

std::ostream& operator<<(std::ostream& os, const Racio& r);

Racio operator+(const Racio& r, const int& i);
Racio operator+(const int& i, const Racio& r);

Racio operator-(const Racio& r, const int& i);
Racio operator-(const int& i, const Racio& r);

Racio operator*(const Racio& r, const int& i);
Racio operator*(const int& i, const Racio& r);

Racio operator/(const Racio& r, const int& i);
Racio operator/(const int& i, const Racio& r);
