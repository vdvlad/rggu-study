#include "Racio.h"

int Racio::euclid(const int p, const int m) {
  int r = p % m;
  if (r != 0) {
    return euclid(m,r);
  }
  return m;
}

Racio::Racio() {
  p_ = 1;
  m_ = 1;
}

Racio::Racio(const int p, const int m) {
  int e = euclid(p,m);
  p_ = p/e;
  m_ = m/e;
  if (m_ < 0) {
    p_ = -p_;
    m_ = -m_;
  }
}

Racio::Racio(const int z, const int p , const int m) {
  int p1 = z*m+p;
  int e  = euclid(p,m);
  p_ = p1/e;
  m_ = m/e;
  if (m_ < 0) {
    p_ = -p_;
    m_ = -m_;
  }
}

Racio::Racio(const double d, const double acc) {
  if (acc > 1) {
    p_ = d;
    m_ = 1;
    return;
  }
  p_ = d/acc; // int
  m_ = 1/acc; // int
  int e = euclid(p_,m_);
  if (m_*e < 0) {
    p_ = -p_/e;
    m_ = -m_/e;
  } else {
    p_ = p_/e;
    m_ = m_/e;
  }
}

Racio::Racio(const Racio& r) {
  this->p_ = r.p_;
  this->m_ = r.m_;
}

Racio& Racio::operator=(const Racio& r) {
  this->p_ = r.p_;
  this->m_ = r.m_;
  return *this;
}

int Racio::getNumerator() const {
  return this->p_;
}

int Racio::getDenumerator() const {
  return this->m_;
}

std::ostream& operator<<(std::ostream& os, const Racio& r) {
  int c = r.getNumerator()/r.getDenumerator();

  Racio r2 = r - Racio(c,1);
  if (c) {
    os << c << " + " << r2.getNumerator() << "/" << r2.getDenumerator();
  } else {
    os << r2.getNumerator() << "/" << r2.getDenumerator();
  }

  return os;
}

Racio Racio::negate() const {
  return Racio(p_,-m_);
}

Racio Racio::swap() const {
  return Racio(m_,p_);
}

double Racio::floor() const {
  return ((int)p_)/m_;;
}

double Racio::toDouble() const {
  return ((double)p_)/m_;;
}

bool Racio::operator==(const Racio& r) const {
  return (this->p_ == r.p_ && this->m_ == r.m_);
}

bool Racio::operator!=(const Racio& r) const {
  return !(*this == r);
}

bool Racio::operator> (const Racio& r) const {
  return this->p_*r.m_ > r.p_*this->m_;
}

bool Racio::operator< (const Racio& r) const {
  return this->p_*r.m_ < r.p_*this->m_;
}

bool Racio::operator>=(const Racio& r) const {
  return !(*this < r);
}

bool Racio::operator<=(const Racio& r) const {
  return !(*this > r);
}

Racio Racio::operator+(const Racio& r) const {
  int p = this->p_ * r.m_ + r.p_ * this->m_;
  int m = this->m_ * r.m_;
  return Racio(p,m);
}

Racio Racio::operator-(const Racio& r) const {
  return *this + r.negate();
}

Racio Racio::operator*(const Racio& r) const {
  int p = this->p_ * r.p_;
  int m = this->m_ * r.m_;
  return Racio(p,m);
}

Racio Racio::operator/(const Racio& r) const {
  return *this * r.swap();
}

int Racio::wholePart() const {
  return (int)(p_/m_);
}

// // // //

Racio operator+(const Racio& r, const int& i) {
  int p = r.getNumerator()  + i * r.getDenumerator();
  int m = r.getDenumerator();
  return Racio(p,m);
}

Racio operator+(const int& i, const Racio& r) {
  int p = r.getNumerator()  + i * r.getDenumerator();
  int m = r.getDenumerator();
  return Racio(p,m);
}

Racio operator-(const Racio& r, const int& i) {
  return r + (-i);
}

Racio operator-(const int& i, const Racio& r) {
  return r + (-i);
}

Racio operator*(const int& i, const Racio& r) {
  return Racio(i*r.getNumerator(),r.getDenumerator());
}

Racio operator*(const Racio& r, const int& i) {
  return Racio(i*r.getNumerator(),r.getDenumerator());
}

Racio operator/(const Racio& r, const int& i) {
  return Racio(r.getNumerator(),r.getDenumerator()*i);
}

Racio operator/(const int& i, const Racio& r) {
  return Racio(i*r.getDenumerator(),r.getNumerator());
}
