mod racio;
use racio::*;

fn main() {
  let a : Racio = Racio::new(4,6);
  let b : Racio = Racio::new(2,5);
  println!("\t{0}"  ,a.show());
  println!("\t{0}"  ,(a + b).show());
  println!("\t{0}\n",(a - b).show());
  println!("\t{0}"  ,(a * b).show());
  println!("\t{0}\n",(a / b).show());
  println!("\t{0}"  ,(a == b));
  println!("\t{0}\n",(a != b));
  println!("\t{0}"  ,(a > b));
  println!("\t{0}\n",(a <= b));
  let c : Racio = Racio::new(23,5);
  println!("\t{0}"  ,c.whole_part());
}
