use std::ops::*;

#[derive(Clone,Copy)]
pub struct Racio {
  p_ : i64,
  m_ : i64,
}

fn euclid(p: i64, m: i64) -> i64 {
  let r: i64 = p%m;
  if r != 0 {
    return euclid(m,r);
  }
  return m;
}

impl Racio {
  pub fn new(p: i64, m: i64) -> Racio {
    let e:  i64 = euclid(p,m);
    let p1: i64 = p/e;
    let m1: i64 = m/e;
    if m1 < 0 {
      return Racio {p_ : -p1, m_ : -m1}
    }
    return Racio {p_ : p1, m_ : m1}
  }

  pub fn show(self) -> String {
    let slash = "/";
    return [self.p_.to_string(), slash.to_owned(), self.m_.to_string()].join("");
  }

  pub fn negate(self) -> Racio {
    return Racio {p_ : -self.p_, m_ : self.m_}
  }

  pub fn reverse(self) -> Racio {
    return Racio {p_ : self.m_, m_ : self.p_}
  }

  pub fn whole_part(self) -> i64 {
    return self.p_/self.m_;
  }
}

impl Add for Racio {
  type Output = Racio;
  fn add(self, r: Racio) -> Racio {
    let p = self.p_ * r.m_ + self.m_ * r.p_;
    let m = self.m_ * r.m_;
    return Racio::new(p,m);
  }
}

impl Sub for Racio {
  type Output = Racio;
  fn sub(self, r: Racio) -> Racio {
    return self + r.negate();
  }
}

impl Mul for Racio {
  type Output = Racio;
  fn mul(self, r: Racio) -> Racio {
    let p = self.p_ * r.p_;
    let m = self.m_ * r.m_;
    return Racio::new(p,m);
  }
}

impl Div for Racio {
  type Output = Racio;
  fn div(self, r: Racio) -> Racio {
    return self * r.reverse();
  }
}

impl PartialEq for Racio {
  fn eq(&self, r: &Racio) -> bool {
    return self.p_ == r.p_ && self.m_ == r.m_;
  }
}

impl PartialOrd for Racio {
    fn partial_cmp(&self, r: &Racio) -> Option<std::cmp::Ordering> {
      // (self.p_ * r.m_) cmp (self.m_ * r.p_)
      return Some((self.p_ * r.m_).cmp(&(self.m_ * r.p_).to_owned()));
    }
}
