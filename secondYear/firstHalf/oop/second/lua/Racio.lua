Racio = {p = 1, m = 1}

local function euclid (p, m)
  local r = p % m
  if (r ~= 0) then
    return euclid(m,r)
  end
  return m
end

function Racio:new (o)
  local p_ = o["p"]
  local m_ = o["m"]
  local e  = euclid(p_,m_)
  if m_ < 0 then
     p_ = -p_
     m_ = -m_
  end

  local o2 = { p = math.floor(p_/e)
             , m = math.floor(m_/e)}

  setmetatable(o2,self)
  self.__index = self
  return o2
end

function Racio:show ()
  return tostring(self.p) .. " % " .. tostring(self.m)
end

function Racio:negate ()
  return Racio:new{p = self.p, m = -self.m}
end

function Racio:swap ()
  return Racio:new{p = self.m, m = self.p}
end

function Racio:__add (r)
  local p_ = self.p*r.m + self.m*r.p
  local m_ = self.m*r.m
  local e  = euclid(p_,m_)
  return Racio:new{p = math.floor(p_/e)
                 , m = math.floor(m_/e)}
end

function Racio:__sub (r)
  return self + r:negate()
end

function Racio:__mul (r)
  local p_ = self.p*r.p
  local m_ = self.m*r.m
  local e  = euclid(p_,m_)
  return Racio:new{p = math.floor(p_/e)
                 , m = math.floor(m_/e)}
end

function Racio:__div (r)
  return self * r:swap()
end

function Racio:__eq (r)
  return self.p == r.p and self.m == r.m
end

function Racio:__lt (r)
  return self.p * r.m < self.m * r.p
end

function Racio:__gt (r)
  return self.p * r.m > self.m * r.p
end

function Racio:__le (r)
  return not (self > r)
end

function Racio:__ge (r)
  return not (self < r)
end

function Racio:wholePart (r)
  return math.floor(self.p/self.m)
end
