#!/usr/bin/lua

require 'Racio'

local a = Racio:new{p = 2, m = 4}
local b = Racio:new{p = 3, m = 7}
print((a + b):show())
print((a - b):show())
print()
print((a * b):show())
print((a / b):show())
print()
print(a ~= b)
print(a > b)
print(a <= b)
print()
local c = Racio:new{p = 23, m = 7}
print(c:wholePart())
