package Raci;

public class Racio {
  private int numerator_;
  private int denumerator_;

  private void setNumerator(final int numerator) {
    final int e = euclid(numerator,denumerator_);
    numerator_   = numerator/e;
    denumerator_ = denumerator_/e;
    if (denumerator_ < 0) {
      numerator_   = -numerator_;
      denumerator_ = -denumerator_;
    }
  }

  private void setDenumerator(final int denumerator) {
    final int e = euclid(numerator_,denumerator);
    numerator_   = numerator_/e;
    denumerator_ = denumerator/e;
    if (denumerator_ < 0) {
      numerator_   = -numerator_;
      denumerator_ = -denumerator_;
    }
  }

  private final int getNumerator() {
    return numerator_;
  }

  private final int getDenumerator() {
    return denumerator_;
  }

  private int euclid(final int p, final int m) {
    int r = p % m;
    if (r != 0) {
      return euclid(m,r);
    }
    return m;
  }

  public Racio(final int numerator, final int denumerator) {
    final int e = euclid(numerator,denumerator);
    numerator_   = numerator/e;
    denumerator_ = denumerator/e;
    if (denumerator_ < 0) {
      numerator_   = -numerator_;
      denumerator_ = -denumerator_;
    }
  }

  public String show() {
    return String.valueOf(numerator_)
           + "/"
           + String.valueOf(denumerator_);
  }

  public Racio sumWith(final Racio r2) {
    final int numerator, denumerator;
    numerator = numerator_   * r2.denumerator_
              + r2.numerator_* denumerator_;
    denumerator = denumerator_ * r2.denumerator_;
    return new Racio(numerator,denumerator);
  }

  public Racio negate() {
    return new Racio(numerator_,-denumerator_);
  }

  public Racio reverse() {
    return new Racio(denumerator_,numerator_);
  }

  public Racio delWith(final Racio r2) {
    return this.sumWith(r2.negate());
  }

  public Racio prodWith(final Racio r2) {
    final int numerator, denumerator;
    numerator   = numerator_   * r2.numerator_;
    denumerator = denumerator_ * r2.denumerator_;
    return new Racio(numerator,denumerator);
  }

  public Racio divWith(final Racio r2) {
    return this.prodWith(r2.reverse());
  }

  public boolean equal(final Racio r2) {
    return this.numerator_ == r2.numerator_
        && this.denumerator_ == r2.denumerator_;
  }

  public boolean notEqual(final Racio r2) {
    return !(this == r2);
  }

  public boolean more(final Racio r2) {
    return this.numerator_ * r2.denumerator_
        >  this.denumerator_ * r2.numerator_;
  }

  public boolean less(final Racio r2) {
    return this.numerator_ * r2.denumerator_
        <  this.denumerator_ * r2.numerator_;
  }

  public boolean moreOrEqual(final Racio r2) {
    return !(this.less(r2));
  }

  public boolean lessOrEqual(final Racio r2) {
    return !(this.more(r2));
  }

  public int wholePart() {
    return this.numerator_ / this.denumerator_;
  }
}
