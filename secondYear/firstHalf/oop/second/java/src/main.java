import Raci.Racio;

class Main {
  public static void main(String[] args) {
    Racio r1 = new Racio(1,6);
    Racio r2 = new Racio(1,3);
    System.out.println("Our racio " + r2.sumWith(r1).show());
    System.out.println("Our racio " + r2.delWith(r1).show());
    System.out.println("Our racio " + r2.prodWith(r1).show());
    System.out.println("Our racio " + r2.divWith(r1).show());

    System.out.println("Our racio " + r1.equal(r1));
    System.out.println("Our racio " + r1.equal(r2));

    System.out.println("Our racio " + r1.more(r2));
    System.out.println("Our racio " + r1.lessOrEqual(r1));

    Racio r3 = new Racio(100,3);
    System.out.println("Our racio " + r1.wholePart());
  }
}
