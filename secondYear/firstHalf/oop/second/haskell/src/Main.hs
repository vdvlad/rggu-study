module Main where

import Prelude
import Racio

main :: IO()
main = do
  let a = 35 % 49
      b = 1 % 3
  putStrLn $ show (a +% b)
  putStrLn $ show (a -% b)

  putStrLn $ show (a *% b)
  putStrLn $ show (a /% b)

  putStrLn $ show (a == b)
  putStrLn $ show (a /= b)

  putStrLn $ show (a > b)
  putStrLn $ show (a <= a)
  putStrLn $ show $ wholePart a
