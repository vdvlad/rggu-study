module Racio(Frak, (%), negate, swap, wholePart
            ,(+%), (-%), (*%), (/%)
            ,(==), (/=),(<),(>),(<=),(>=)) where

import Prelude hiding (negate)

data Frak = Frak Int Int

(%) :: Int -> Int -> Frak
(%) p m = let
            e  = euclid p m
            p' = div p e
            m' = div m e
          in
             if m' < 0
             then Frak (-p') (-m')
             else Frak p' m'

instance Show (Frak) where
  show (Frak p m) = show p ++ " % " ++ show m

euclid :: Int -> Int -> Int
euclid p m = let r = mod p m
             in
               if (r /= 0)
               then euclid m r
               else m

negate :: Frak -> Frak
negate (Frak p m) = Frak p (-m)

swap :: Frak -> Frak
swap (Frak p m) = Frak m p

(+%) :: Frak -> Frak -> Frak
(+%) (Frak p1 m1) (Frak p2 m2) =
  let p = p1*m2 + p2*m1
      m = m1*m2
  in
      p % m

(-%) :: Frak -> Frak -> Frak
(-%) r1 r2 = (+%) r1 (negate r2)

(*%) :: Frak -> Frak -> Frak
(*%) (Frak p1 m1) (Frak p2 m2) =
  let p = p1*p2
      m = m1*m2
  in
      p % m

(/%) :: Frak -> Frak -> Frak
(/%) r1 r2 = (*%) r1 (swap r2)

wholePart :: Frak -> Int
wholePart (Frak p m) = div p m

instance Eq (Frak) where
  (==) (Frak p1 m1) (Frak p2 m2)  = p1 == p2 && m1 == m2
  (/=) r1 r2 = not (r1 == r2)

instance Ord (Frak) where
  (>)  (Frak p1 m1) (Frak p2 m2)  = p1 * m2 > p2 * m1
  (<)  (Frak p1 m1) (Frak p2 m2)  = p1 * m2 < p2 * m1
  (>=) r1 r2 = not (r1 < r2)
  (<=) r1 r2 = not (r1 > r2)
