#import "Racio.h"

@implementation Racio

@synthesize p_;
@synthesize m_;

+(int) euclid: (int) p
       second: (int) m {
  int r = p % m;
  if (r != 0) {
    return [Racio euclid: m second: r];
  }
  return m;
}

-(id) init {
  self = [super init];
  self.p_ = 1;
  self.m_ = 1;
  return self;
}

-(NSString*) show {
  return [NSString stringWithFormat: @"%i %% %i"
                   , self.p_
                   , self.m_];
}

-(id) initWithP: (int) p
      withM:     (int) m {
  self = [super init];
  int e = [Racio euclid: p second: m];
  self.p_ = p/e;
  self.m_ = m/e;
  if (self.m_ < 0) {
    self.p_ = -self.p_;
    self.m_ = -self.m_;
  }
  return self;
}

-(Racio*) negate {
  return [[Racio alloc] initWithP: self.p_ withM: (-self.m_)];
}

-(Racio*) swap {
  return [[Racio alloc] initWithP: self.m_ withM: self.p_];
}

-(Racio*) sumWith: (Racio*) r {
  int p = self.p_ * r.m_ + self.m_ * r.p_;
  int m = self.m_ * r.m_;
  return [[Racio alloc] initWithP: p withM: m];
}

-(Racio*) delWith: (Racio*) r {
  return [self sumWith: [r negate]];
}

-(Racio*) prodWith: (Racio*) r {
  int p = self.p_ * r.p_;
  int m = self.m_ * r.m_;
  return [[Racio alloc] initWithP: p withM: m];
}

-(Racio*) divWith: (Racio*) r {
  return [self prodWith: [r swap]];
}

-(bool) equal: (Racio*) r {
  return self.p_ == r.p_ && self.m_ == r.m_;
}

-(bool) notEqual: (Racio*) r {
  return !(self == r);
}

-(bool) more: (Racio*) r {
  return self.p_ * r.m_ > self.m_ * r.p_;
}

-(bool) less: (Racio*) r {
  return self.p_ * r.m_ < self.m_ * r.p_;
}

-(bool) moreOrEqual: (Racio*) r {
  return ![self less: r];
}

-(bool) lessOrEqual: (Racio*) r {
  return ![self more: r];
}

-(int) wholePart {
  return (int)(self.p_/self.m_);
}

@end
