#import <Foundation/Foundation.h>
#import "Racio.h"

int main() {
  @autoreleasepool {
    Racio* a = [[Racio alloc] initWithP: 6 withM: 9];
    Racio* b = [[Racio alloc] initWithP: 4 withM: 7];
    NSLog(@"%@",     [[a sumWith: b] show]);
    NSLog(@"%@",     [[a delWith: b] show]);
    NSLog(@"%@",     [[a prodWith: b] show]);
    NSLog(@"%@",     [[a divWith: b] show]);
    NSLog(@"%d",     [a equal: b]);
    NSLog(@"%d\n\n", [b equal: b]);
    NSLog(@"%d",     [a more: b]);
    NSLog(@"%d",     [a moreOrEqual: a]);
    Racio* c = [[Racio alloc] initWithP: 40 withM: 7];
    NSLog(@"%d",     [c wholePart]);
  }
  return 0;
}
