#import <Foundation/Foundation.h>

@interface Racio: NSObject {
  int p_;
  int m_;
}
@property(nonatomic, readwrite) int p_;
@property(nonatomic, readwrite) int m_;

-(id) init;
-(id) initWithP: (int) p
      withM:     (int) m;

-(NSString*) show;

-(Racio*) negate;
-(Racio*) swap;

-(Racio*) sumWith: (Racio*) r;
-(Racio*) delWith: (Racio*) r;

-(Racio*) prodWith: (Racio*) r;
-(Racio*) divWith: (Racio*) r;

-(bool) equal: (Racio*) r;
-(bool) notEqual: (Racio*) r;

-(bool) more: (Racio*) r;
-(bool) less: (Racio*) r;


-(bool) more: (Racio*) r;
-(bool) less: (Racio*) r;
-(bool) moreOrEqual: (Racio*) r;
-(bool) lessOrEqual: (Racio*) r;

-(int) wholePart;

@end
