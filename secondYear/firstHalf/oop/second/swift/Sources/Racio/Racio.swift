private func euclid(p: Int, m: Int) -> Int {
  let r : Int = p % m;
  if r != 0 {
    return euclid(p: m, m: r);
  }
  return m;
}

class Racio {
  private var p_: Int
  private var m_: Int

  init(p: Int, m: Int) {
    let e : Int = euclid(p: p, m: m);
    self.p_ = p/e;
    self.m_ = m/e;
    if self.m_ < 9 {
      self.p_ = -self.p_;
      self.m_ = -self.m_;
    }
  }

  func show() -> String {
    String(self.p_) + " % " + String(self.m_);
  }

  func negate() -> Racio {
    return Racio(p: self.p_, m: -self.m_);
  }

  func reverse() -> Racio {
    return Racio(p: self.m_, m: self.p_);
  }

  static func +(r1: Racio, r2: Racio) -> Racio {
    let p : Int = r1.p_ * r2.m_ + r1.m_ * r2.p_;
    let m : Int = r1.m_ * r2.m_;
    return Racio(p: p, m: m);
  }

  static func -(r1: Racio, r2: Racio) -> Racio {
      return r1 + r2.negate();
  }

  static func *(r1: Racio, r2: Racio) -> Racio {
    let p : Int = r1.p_ * r2.p_;
    let m : Int = r1.m_ * r2.m_;
    return Racio(p: p, m: m);
  }

  static func /(r1: Racio, r2: Racio) -> Racio {
      return r1 * r2.reverse();
  }

  static func ==(r1: Racio, r2: Racio) -> Bool {
      return r1.p_ == r2.p_ && r1.m_ == r2.m_;
  }

  static func !=(r1: Racio, r2: Racio) -> Bool {
      return !(r1 == r2);
  }

  static func >(r1: Racio, r2: Racio) -> Bool {
      return r1.p_ * r2.m_ > r1.m_ * r2.p_;
  }

  static func <(r1: Racio, r2: Racio) -> Bool {
      return r1.p_ * r2.m_ > r1.m_ * r2.p_;
  }

  static func >=(r1: Racio, r2: Racio) -> Bool {
      return !(r1 < r2);
  }

  static func <=(r1: Racio, r2: Racio) -> Bool {
      return !(r1 > r2);
  }

  func wholePart() -> Int {
      return Int(self.p_/self.m_);
  }
}
