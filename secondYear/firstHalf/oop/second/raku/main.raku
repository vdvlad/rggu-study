#!/usr/bin/rakudo
use lib ".";
use Racio;

my Frak $a = Frak.new(:p(2),:m(4));
my Frak $b = Frak.new(:p(3),:m(4));

say $a.show();
say ($a + $b +$a).show();
say "";

say $a.del($b).show();
say $a.prod($b).show();
say $a.div($b).show();
say "";

say $a.equal($b);
say ($a != $b);
say "";

say ($a >= $b);
say ($a < $b);

my Frak $c = Frak.new(:p(31),:m(4));
say ($c.wholePart());
