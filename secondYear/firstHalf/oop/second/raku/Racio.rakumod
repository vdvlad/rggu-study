unit module Racio;

sub euclid(Int $p, Int $m --> Int) {
    my Int $r = $p % $m;
    if ($r != 0) {
        return euclid($m,$r);
    }
    return $m;
}

class Frak is export {
    has Int $.p is readonly;
    has Int $.m is readonly;

    submethod BUILD(:$p, :$m) {
        my Int $e = euclid($p,$m);
        $!p = $p div $e;
        $!m = $m div $e;
        if ($!m < 0) {
          $!p = -$!p;
          $!m = -$!m;
        }
    }

    method show() {
        return $!p ~ " % " ~ $!m;
    }

    method negate() {
        return Frak.new(:p($!p), :m(-$!m));
    }

    method swap() {
        return Frak.new(:p($!m), :m($!p));
    }

    method sum(Frak $f) {
        return Frak.new(
            :p($!p * $f.m + $!m * $f.p)
          , :m($!m * $f.m)
        );
    }

    method del(Frak $f) {
        return self.sum($f.negate());
    }

    method prod(Frak $f) {
        return Frak.new(
            :p($!p * $f.p)
          , :m($!m * $f.m)
        );
    }

    method div(Frak $f) {
        return self.prod($f.swap());
    }

    method equal(Frak $f) {
        return $!p == $f.p && $!m == $f.m;
    }

    method notEqual(Frak $f) {
        return !(self.equal($f));
    }

    method more(Frak $f) {
        return $!p * $f.m > $!m * $f.p;
    }

    method less(Frak $f) {
        return $!p * $f.m < $!m * $f.p;
    }

    method moreOrEqual(Frak $f) {
        return !(self.less($f));
    }

    method lessOrEqual(Frak $f) {
        return !(self.more($f));
    }

    method wholePart() {
        return floor($!p / $!m);
    }
}

multi sub infix:<+>(Frak $f1, Frak $f2 --> Frak) is export {
    $f1.sum($f2);
}

multi sub infix:<->(Frak $f1, Frak $f2 --> Frak) is export {
    $f1.del($f2);
}

multi sub infix:<*>(Frak $f1, Frak $f2 --> Frak) is export {
    $f1.prod($f2);
}

multi sub infix:</>(Frak $f1, Frak $f2 --> Frak) is export {
    $f1.div($f2);
}

multi sub infix:<==>(Frak $f1, Frak $f2 --> Bool) is export {
    $f1.equal($f2);
}

multi sub infix:<!=>(Frak $f1, Frak $f2 --> Bool) is export {
    $f1.notEqual($f2);
}

multi sub infix:«>»(Frak $f1, Frak $f2 --> Bool) is export {
    $f1.more($f2);
}

multi sub infix:«<»(Frak $f1, Frak $f2 --> Bool) is export {
    $f1.less($f2);
}

multi sub infix:«>=»(Frak $f1, Frak $f2 --> Bool) is export {
    $f1.moreOrEqual($f2);
}

multi sub infix:«<=»(Frak $f1, Frak $f2 --> Bool) is export {
    $f1.lessOrEqual($f2);
}
