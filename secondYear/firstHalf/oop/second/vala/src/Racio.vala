class Racio {
	private int p_;
	private int m_;

	public Racio(int p, int m) {
		int e = euclid(p,m);
		p_ = p/e;
		m_ = m/e;
		if (p_ < 0) {
		  p_ = -p_;
		  m_ = -m_;
		}
	}

	private int euclid(int p, int m) {
		int r = p % m;
		if (r != 0) {
			return euclid(m,r);
		}
		return m;
	}

	public Racio negate() {
		return new Racio(p_,-m_);
	}

	public Racio swap() {
		return new Racio(m_,p_);
	}

	public string show() {
		return "".concat(p_.to_string(), " % ", m_.to_string());
	}

	public Racio sumWith(Racio r) {
		int p = this.p_ * r.m_ + this.m_ * r.p_;
		int m = this.m_ * r.m_;
		return new Racio(p, m);
	}

	public Racio delWith(Racio r) {
		return this.sumWith(r.negate());
	}

	public Racio prodWith(Racio r) {
		int p = this.p_ * r.p_;
		int m = this.m_ * r.m_;
		return new Racio(p, m);
	}

	public Racio divWith(Racio r) {
		return this.prodWith(r.swap());
	}

	public bool equal(Racio r) {
		return this.p_ == r.p_ && this.m_ == r.m_;
	}

	public bool notEqual(Racio r) {
		return !(this.equal(r));
	}

	public bool more(Racio r) {
		return this.p_ * r.m_ > this.m_ * r.p_;
	}

	public bool less(Racio r) {
		return this.p_ * r.m_ < this.m_ * r.p_;
	}

	public bool moreOrEqual(Racio r) {
		return !(this.less(r));
	}

	public bool lessOrEqual(Racio r) {
		return !(this.more(r));
	}

	public int wholePart() {
		return (int)(this.p_/this.m_);
	}
}
