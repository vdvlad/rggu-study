int main() {
	Racio a = new Racio(2,3);
	Racio b = new Racio(4,7);
    stdout.printf("%s\n", a.sumWith(b).show());
    stdout.printf("%s\n", a.delWith(b).show());
    stdout.printf("\n");
    stdout.printf("%s\n", a.prodWith(b).show());
    stdout.printf("%s\n", a.divWith(b).show());
    stdout.printf("\n");
    stdout.printf("%s\n", a.equal(b).to_string());
    stdout.printf("%s\n", b.equal(b).to_string());
    stdout.printf("%s\n", b.notEqual(b).to_string());
    stdout.printf("\n");
    stdout.printf("%s\n", a.more(b).to_string());
    stdout.printf("%s\n", b.more(b).to_string());
    stdout.printf("%s\n", b.moreOrEqual(b).to_string());
    stdout.printf("\n");
	Racio c = new Racio(40,7);
    stdout.printf("%s\n", c.wholePart().to_string());
    return 0;
}