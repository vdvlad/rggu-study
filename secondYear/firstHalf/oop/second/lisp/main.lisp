#!/bin/sbcl --script

(require "ratio" "./ratio.lisp")

(defparameter aRat (make-instance 'ratio2 :numerator 21 :denumerator 49))
(defparameter bRat (make-instance 'ratio2 :denumerator 3))

(print (describe (sumR aRat bRat)))
(print (describe (sumR 2 bRat)))
(print (describe (sumR 2 2 3 4 bRat)))
(print (describe (subR 2 2 3 4 bRat)))
(print (describe (proR aRat bRat)))
(print (describe (divR 2 2 3 4 bRat)))

(print (equalR aRat aRat))
(print (equalR bRat bRat aRat aRat))
(print (notEqualR aRat bRat))
(terpri)

(print (moreR aRat aRat bRat))
(print (moreOrEqualR aRat aRat bRat))
(print (moreOrEqualR aRat aRat bRat))
(terpri)

(defparameter aRat (make-instance 'ratio2 :numerator 21 :denumerator 4))
(print (wholePartR aRat))
(print (wholePartRList aRat aRat bRat))

(terpri)
