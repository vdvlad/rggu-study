(provide 'ratio)

;; p = qm + r
(defmethod euclid (p m)
  (let ((r (rem p m)))
    (if (= r 0)
        m
        (euclid m r))))

;; p/m; p,m in N
(defclass ratio2 ()
  ((p :type integer :initarg :p)
   (m :type integer :initarg :m)))

(defmethod initialize-instance
  :around ((r ratio2) &key (numerator 1) (denumerator 1))
  (let ((e     (euclid numerator denumerator)))
    (let ((num (/ numerator e))
          (denum (/ denumerator e)))
      (if (< denum 0)
       (progn
          (call-next-method r :p (- num))
          (call-next-method r :m (- denum)))
       (progn
          (call-next-method r :p num)
          (call-next-method r :m denum))
       )
    )
  )
)

(defun sumR2 (r1 r2)
  (cond
    ((and (typep r1 'ratio2) (typep r1 'ratio2))
     (make-instance 'ratio2
                    :numerator   (+ (* (slot-value r1 'p) (slot-value r2 'm))
                                    (* (slot-value r2 'p) (slot-value r1 'm)))
                    :denumerator (* (slot-value r1 'm) (slot-value r2 'm))))
    ((typep r1 'ratio2)
     (make-instance 'ratio2
                    :numerator   (+ (* r2 (slot-value r1 'm)) (slot-value r1 'p))
                    :denumerator (slot-value r1 'm)))
    ((typep r2 'ratio2)
     (make-instance 'ratio2
                    :numerator   (+ (* r1 (slot-value r2 'm)) (slot-value r2 'p))
                    :denumerator (slot-value r2 'm)))
    (t (+ r1 r2))
  )
)
(defun sumR (&rest r)
  (reduce #'sumR2 (cons 0 r)))

(defun negateR (r)
  (if (typep r 'ratio2)
      (make-instance 'ratio2
                     :numerator   (- (slot-value r 'p))
                     :denumerator (slot-value r 'm))
      (-r)
  )
)

;; substruction
(defun subR2 (r1 r2)
  (sumR2 r1 (negateR r2)))
(defun subR (r1 &rest r)
  (subR2 r1 (reduce #'sumR2 (cons 0 r))))

;; production
(defun proR2 (r1 r2)
  (cond
    ((and (typep r1 'ratio2) (typep r1 'ratio2))
     (make-instance 'ratio2
                    :numerator   (* (slot-value r1 'p) (slot-value r2 'p))
                    :denumerator (* (slot-value r1 'm) (slot-value r2 'm))))
    ((typep r1 'ratio2)
     (make-instance 'ratio2
                    :numerator   (* r2 (slot-value r1 'p))
                    :denumerator (slot-value r1 'm)))
    ((typep r2 'ratio2)
     (make-instance 'ratio2
                    :numerator   (* r1 (slot-value r2 'p))
                    :denumerator (slot-value r2 'm)))
    (t (* r1 r2))
  )
)
(defun proR (&rest r)
  (reduce #'proR2 (cons 1 r)))

;; p/m -> m/p
(defun swapR (r)
  (if (typep r 'ratio2)
      (make-instance 'ratio2
                     :numerator   (slot-value r 'm)
                     :denumerator (slot-value r 'p))
      (/ 1 r)
  )
)

;; divine
(defun divR2 (r1 r2)
  (proR2 r1 (swapR r2)))
(defun divR (r1 &rest r)
  (divR2 r1 (reduce #'proR2 (cons 1 r))))

;; equal; more or equal 2 arguments
(defun equalR2 (r1 r2)
  (and (= (slot-value r1 'p) (slot-value r2 'p))
       (= (slot-value r1 'm) (slot-value r2 'm))))
(defun equalR (r1 r2 &rest r)
  (if (not (null r))
      (and (equalR2 r1 r2) (apply 'equalR (cons r1 r)))
      (and (equalR2 r1 r2))))

;; not equal; more or equal 2 arguments
(defun notEqualR2 (r1 r2)
  (or (/= (slot-value r1 'p) (slot-value r2 'p))
      (/= (slot-value r1 'm) (slot-value r2 'm))))
(defun notEqualR (r1 r2 &rest r)
  (not
    (if (not (null r))
        (and (equalR2 r1 r2) (apply 'equalR (cons r1 r)))
        (and (equalR2 r1 r2)))))

;; more then another; more or equal 2 arguments
(defun moreR2 (r1 r2)
  (> (* (slot-value r1 'p) (slot-value r2 'm))
     (* (slot-value r2 'p) (slot-value r1 'm))))
(defun moreR (r1 r2 &rest r)
  (if (not (null r))
      (and (moreR2 r1 r2) (apply 'moreR (cons r1 r)))
      (and (moreR2 r1 r2))))

;; less then another; more or equal 2 arguments
(defun lessR2 (r1 r2)
  (< (* (slot-value r1 'p) (slot-value r2 'm))
     (* (slot-value r2 'p) (slot-value r1 'm))))
(defun lessR (r1 r2 &rest r)
  (if (not (null r))
      (and (lessR2 r1 r2) (apply 'lessR (cons r1 r)))
      (and (lessR2 r1 r2))))

;; more or equal then another; more or equal 2 arguments
(defun moreOrEqualR2 (r1 r2)
  (not (lessR2 r1 r2)))
(defun moreOrEqualR (r1 r2 &rest r)
  (if (null r)
      (and (moreOrEqualR2 r1 r2) (apply 'moreOrEqualR (cons r1 r)))
      (and (moreOrEqualR2 r1 r2))))

;; less or equal then another; more or equal 2 arguments
(defun lessOrEqualR2 (r1 r2)
  (not (moreR2 r1 r2)))
(defun lessOrEqualR (r1 r2 &rest r)
  (if (null r)
      (and (lessOrEqualR2 r1 r2) (apply 'lessOrEqualR (cons r1 r)))
      (and (lessOrEqualR2 r1 r2))))

;; extract whole part
(defun wholePartR (r)
  (floor (slot-value r 'p) (slot-value r 'm)))
(defun wholePartRList (&rest r)
  (map 'list #'wholePartR r))
