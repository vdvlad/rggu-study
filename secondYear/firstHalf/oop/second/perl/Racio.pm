#!/usr/bin/perl
use 5.010;
package Racio;

sub euclid {
    $self = shift;
    my $p = shift;
    my $m = shift;

    return 1 if $m == 0;
    my $r = $p % $m;

    if ($r != 0) {
        return $self->euclid($m,$r);
    }
    return $m;
}

sub new {
    my($class) = shift;
    my $e = euclid($p,$m);
    my $self = {
        _p => shift,
        _m => shift
    };

    bless $self, $class;

    my $e = $self->euclid(
        $self->{_p}
      , $self->{_m});
    $self->{_p} /= $e;
    $self->{_m} /= $e;
    return $self;
}

sub show {
    $self = shift;
    return $self->{_p} . " % " . $self->{_m};
}

sub negate {
    my $self = shift;
    return new Racio($self->{_p}, -$self->{_m});
}

sub swap {
    my $self = shift;
    return new Racio($self->{_m}, $self->{_p});
}

sub sumWith {
    my $r1 = shift;
    my $r2 = shift;
    my $p;
    my $m;
    while (defined $r2) {
        $p  = $r1->{_p} * $r2->{_m} + $r2->{_p} * $r1->{_m};
        $m  = $r1->{_m} * $r2->{_m};
        $r2 = shift;
        $r1 = new Racio($p,$m);
    }

    return $r1;
}

sub delWith {
    my $r1 = shift;
    my $r2 = shift;
    my $r3 = shift;
    while (defined $r3) {
        $r2 = $r2->sumWith($r3);
        $r3 = shift;
    }
    return $r1->sumWith($r2->negate());
}

sub prodWith {
    my $r1 = shift;
    my $r2 = shift;
    my $p;
    my $m;
    while (defined $r2) {
        $p  = $r1->{_p} * $r2->{_p};
        $m  = $r1->{_m} * $r2->{_m};
        $r2 = shift;
        $r1 = new Racio($p,$m);
    }
    return new Racio($p,$m);
}

sub divWith {
    my $r1 = shift;
    my $r2 = shift;
    my $r3 = shift;
    while (defined $r3) {
        $r2 = $r2->prodWith($r3);
        $r3 = shift;
    }
    return $r2->prodWith($r1->swap());
}

sub equal {
    my $r1     = shift;
    my $r2     = shift;
    my $result = 1;
    while (defined $r2) {
        $result*= $r1->{_p} == $r2->{_p}
               && $r1->{_m} == $r2->{_m};
        $r2 = shift;
    }
    return $result;
}

sub notEqual {
    my $r1     = shift;
    my $r2     = shift;
    my $result = 1;
    while (defined $r2) {
        $result*= $r1->{_p} == $r2->{_p}
               && $r1->{_m} == $r2->{_m};
        $r2 = shift;
    }
    return !$result;
}

sub more {
    my $r1     = shift;
    my $r2     = shift;
    my $result = 1;
    while (defined $r2) {
        $result*= $r1->{_p} * $r2->{_m}
                > $r1->{_m} * $r2->{_p};
        $r2 = shift;
    }
    return $result;
}

sub less {
    my $r1     = shift;
    my $r2     = shift;
    my $result = 1;
    while (defined $r2) {
        $result*= $r1->{_p} * $r2->{_m}
                < $r1->{_m} * $r2->{_p};
        $r2 = shift;
    }
    return $result;
}

sub moreOrEqual {
    my $r1     = shift;
    my $r2     = shift;
    my $result = 1;
    while (defined $r2) {
        $result*= !($r1->less($r2));
        $r2 = shift;
    }
    return $result;
}

sub lessOrEqual {
    my $r1     = shift;
    my $r2     = shift;
    my $result = 1;
    while (defined $r2) {
        $result*= !($r1->more($r2));
        $r2 = shift;
    }
    return $result;
}

sub wholePart {
    my $r     = shift;
    return int($r->{_p}/$r->{_m});
}

1;
