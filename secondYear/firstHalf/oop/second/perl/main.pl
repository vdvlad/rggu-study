#!/usr/bin/perl
use 5.010;
use lib ".";
use Racio;

my $a = new Racio(35,49);
my $b = new Racio(2,3);

say $b->sumWith($a)->show();
say $b->delWith($a)->show();

say;

say $b->prodWith($a)->show();
say $b->divWith($a)->show();

say;

say $b->equal($b);
say $b->notEqual($a);

say;

say $b->more($a);
say $b->moreOrEqual($b);

my $c = new Racio(20,3);
say $c->wholePart();
