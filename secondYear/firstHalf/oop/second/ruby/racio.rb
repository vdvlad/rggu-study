class Racio
  attr_accessor:p, :m

  def initialize(p,m)
    e  = euclid(p,m)
    @p = p.div(e)
    @m = m.div(e)
    if (@m < 0) then
      @p = -@p
      @m = -@m
    end
  end

  def euclid(p,m)
    e = p % m
    if e != 0 then
      return euclid(m,e)
    else
      return m
    end
  end
  private :euclid

  def to_s
    return @p.to_s + "/" + @m.to_s
  end

  def negate
    return Racio.new(-self.p,self.m)
  end

  def reverse
    return Racio.new(self.m,self.p)
  end

  def +(r)
    p = self.p*r.m + r.p*self.m
    m = self.m*r.m
    return Racio.new(p,m)
  end

  def -(r)
    return self + r.negate
  end

  def *(r)
    p = self.p*r.p
    m = self.m*r.m
    return Racio.new(p,m)
  end

  def /(r)
    return self * r.reverse
  end

  def ==(r)
    return self.p == r.p && self.m == r.m
  end

  def !=(r)
    return !(self == r)
  end

  def >(r)
    return self.p*r.m > r.p*self.m
  end

  def <(r)
    return self.p*r.m < r.p*self.m
  end

  def <=(r)
    return !(self > r)
  end

  def >=(r)
    return !(self < r)
  end

  def wholePart()
    return self.p.div(self.m)
  end
end
