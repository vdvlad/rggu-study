#ifndef RACIO
#define RACIO

#include <stdio.h>

typedef struct {
  int p;
  int m;
} Racio;

Racio init (int p, int m);
int euclid (int p, int m);

Racio negate (Racio r);
Racio swap (Racio r);

Racio sum (Racio r1, Racio r2);
// subtraction
Racio sub (Racio r1, Racio r2);
Racio prod (Racio r1, Racio r2);
// division
Racio divR (Racio r1, Racio r2);

void output (Racio r);

int equal (Racio r1, Racio r2);
int notEqual (Racio r1, Racio r2);

int more (Racio r1, Racio r2);
int less (Racio r1, Racio r2);
int moreOrEqual (Racio r1, Racio r2);
int lessOrEqual (Racio r1, Racio r2);

int wholePart (Racio r);

#endif // RACIO
