#include <stdio.h>
#include "racio.h"

int main() {
  Racio a = init(2,5);
  output(a);
  printf("\n");
  Racio b = init(21,49);
  output(b);
  printf("\n");
  output(sum(b,a));
  printf("\n");

  printf("%d %d\n", equal(b,b), equal(b,a));
  printf("\n");

  a = init(2,6);
  b = init(-2,4);
  printf("%d %d\n", more(b,a), less(b,a));
  return 0;
}
