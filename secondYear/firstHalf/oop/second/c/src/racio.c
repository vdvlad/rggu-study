#include <stdio.h>
#include "racio.h"

int euclid (int p, int m) {
  int r = p % m;
  return (r) ? euclid(m,r) : m;
}

inline Racio init (int p, int m) {
  Racio r;
  int e = euclid(p,m);
  r.p = p/e;
  r.m = m/e;
  if (r.m < 0) {
    r.p = -r.p;
    r.m = -r.m;
  }
  return r;
}

Racio sum (Racio r1, Racio r2) {
  int p = r1.p*r2.m + r2.p*r1.m;
  int m = r1.m*r2.m;
  return init(p,m);
}

inline Racio negate (Racio r) {
  Racio n;
  n.p = -r.p;
  n.m = r.m;
  return n;
}

inline Racio sub (Racio r1, Racio r2) {
  return sum(r1,negate(r2));
}

Racio swap (Racio r) {
  Racio n;
  n.p = r.m;
  n.m = r.p;
  return n;
}

Racio prod (Racio r1, Racio r2) {
  int p = r1.p*r2.p;
  int m = r1.m*r2.m;
  return init(p,m);
}

inline Racio div (Racio r1, Racio r2) {
  return prod(r1, swap(r2));
}

void output (Racio r) {
  printf("%d/%d", r.p, r.m);
}

inline int equal (Racio r1, Racio r2) {
  return r1.p == r2.p && r1.m == r2.m;
}

inline int notEqual (Racio r1, Racio r2) {
  return !(equal(r1,r2));
}

inline int more (Racio r1, Racio r2) {
  return (r1.p*r2.m)>(r2.p*r1.m);
}

inline int less (Racio r1, Racio r2) {
  return (r1.p*r2.m)<(r2.p*r1.m);
}

int moreOrEqual (Racio r1, Racio r2) {
  return !less(r1,r2);
}

int lessOrEqual (Racio r1, Racio r2) {
  return !more(r1,r2);
}

int wholePart (Racio r) {
  return (int)(r.p/r.m);
}
