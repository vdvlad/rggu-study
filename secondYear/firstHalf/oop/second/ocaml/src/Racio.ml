type racio = {p : int; m : int};;

module Racio = struct
  let rec euclid p m =
    let r = p mod m in
      if (r != 0)
      then euclid m r
      else m

  let init p' m' =
    let e   = euclid p' m' in
    let p'' = p'/e         in
    let m'' = m'/e         in
      if (m'' < 0)
      then {p = -p''; m = -m''}
      else {p =  p''; m =  m''}

  let negate r = {p = -r.p; m = r.m}
  let swap   r = {p =  r.m; m = r.p}

  let show r   = (string_of_int r.p) ^ " % " ^ (string_of_int r.m)

  let sum r1 r2 = init (r1.p*r2.m+r2.p*r1.m) (r1.m*r2.m)
  let del r1 r2 = sum r1 (negate r2)

  let prod r1 r2  = init (r1.p*r2.p) (r1.m*r2.m)
  let div  r1 r2  = prod r1 (swap r2)

  let equal     r1 r2 = r1.p == r2.p && r1.m == r2.m
  let notEqual  r1 r2 = not (equal r1 r2)

  let more r1 r2 = r1.p * r2.m > r2.p * r1.m
  let less r1 r2 = r1.p * r2.m < r2.p * r1.m

  let moreOrEqual r1 r2 = not (less r1 r2)
  let lessOrEqual r1 r2 = not (more r1 r2)

  let wholePart r = r.p/r.m
end;;
