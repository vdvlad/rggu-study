open Racio

let main() =
  let a = Racio.init 2 3 in
  let b = Racio.init 35 49 in
  print_endline (Racio.show(Racio.sum a b));
  print_endline (Racio.show(Racio.del a b) ^ "\n");

  print_endline (Racio.show(Racio.prod a b));
  print_endline (Racio.show(Racio.div a b));

  print_endline (Bool.to_string (Racio.equal a b));
  print_endline (Bool.to_string (Racio.equal b b) ^ "\n");

  print_endline (Bool.to_string (Racio.more a b));
  print_endline (Bool.to_string (Racio.moreOrEqual b b) ^ "\n");

  let c = Racio.init 35 2 in
  print_endline (Int.to_string (Racio.wholePart c));;

main();;
