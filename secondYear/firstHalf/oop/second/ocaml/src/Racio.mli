type racio = {p : int; m : int}

module Racio :
  sig
    val init : int -> int -> racio

    val negate : racio -> racio
    val swap   : racio -> racio

    val show : racio -> string

    val sum : racio -> racio -> racio
    val del : racio -> racio -> racio

    val prod : racio -> racio -> racio
    val div  : racio -> racio -> racio

    val equal    : racio -> racio -> bool
    val notEqual : racio -> racio -> bool

    val more     : racio -> racio -> bool
    val less     : racio -> racio -> bool

    val moreOrEqual   : racio -> racio -> bool
    val lessOrEqual   : racio -> racio -> bool

    val wholePart : racio -> int
  end;;
