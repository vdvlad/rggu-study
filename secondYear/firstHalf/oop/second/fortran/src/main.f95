program Fractional

use class_Racio;
  type(Racio) :: r1,r2,r3

  call r1%init(1,6)
  call r2%init(2,3)
  r3 = r2 + r1
  write (*,*) r3%getNumerator()
  write (*,*) r3%getDenumerator()

  r3 = r2 - r1
  write (*,*) r3%getNumerator()
  write (*,*) r3%getDenumerator()

  r3 = r2 / r1
  write (*,*) r3%show()

  write (*,*) r1 == r2
  write (*,*) r2 == r2

  write (*,*) r1 >= r2
  write (*,*) r2 <= r2

  write (*,*) r2%wholePart()

end program
