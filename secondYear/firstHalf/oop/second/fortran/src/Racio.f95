module class_Racio
  implicit none
  private

  type, public :: Racio
    Integer :: numerator_, denumerator_
  contains
    procedure :: init => racio_init
    procedure :: setNumerator => racio_setNumerator
    procedure :: setDenumerator => racio_setDenumerator
    procedure :: getNumerator => racio_getNumerator
    procedure :: getDenumerator => racio_getDenumerator

    procedure :: negate  => racio_negate
    procedure :: reverse => racio_reverse

    procedure       :: add => racio_add
    generic, public :: operator(+) => add
    procedure       :: del => racio_del
    generic, public :: operator(-) => del

    procedure :: show => racio_show

    procedure       :: prod => racio_prod
    generic, public :: operator(*) => prod
    procedure       :: div => racio_div
    generic, public :: operator(/) => div

    procedure       :: equal => racio_equal
    generic, public :: operator(==) => equal
    procedure       :: notEqual => racio_notEqual
    generic, public :: operator(/=) => notEqual

    procedure       :: more => racio_more
    generic, public :: operator(>) => more
    procedure       :: less => racio_less
    generic, public :: operator(<) => less

    procedure       :: moreOrEqual => racio_moreOrEqual
    generic, public :: operator(>=) => moreOrEqual
    procedure       :: lessOrEqual => racio_lessOrEqual
    generic, public :: operator(<=) => lessOrEqual

    procedure :: wholePart => racio_wholePart
    end type Racio

    contains

    subroutine racio_init (this, numerator, denumerator)
      Integer     :: numerator, denumerator
      Integer     :: e
      class(Racio) :: this
      e = euclid(numerator, denumerator)
      this%numerator_   = numerator / e
      this%denumerator_ = denumerator / e
      if (this%denumerator_ < 0) then
        this%numerator_   = this%numerator_
        this%denumerator_ = this%denumerator_
      end if
    end subroutine

    recursive function euclid (p, m) result (eu)
      Integer     :: p,m,r
      Integer     :: eu
      r = mod(p,m)
      if (r /= 0) then
        eu = euclid(m,r)
      else
        eu = m
      end if
    end function

    subroutine racio_setNumerator (this, numerator)
      Integer     :: numerator
      Integer     :: e
      class(Racio) :: this
      e = euclid(numerator, this%denumerator_)
      this%numerator_   = numerator / e
      this%denumerator_ = this%denumerator_ / e
      if (this%denumerator_ < 0) then
        this%numerator_   = this%numerator_
        this%denumerator_ = this%denumerator_
      end if
    end subroutine

    subroutine racio_setDenumerator (this, denumerator)
      Integer     :: denumerator
      Integer     :: e
      class(Racio) :: this
      e = euclid(this%numerator_, denumerator)
      this%denumerator_ = denumerator / e
      this%numerator_   = this%numerator_ / e
      if (this%denumerator_ < 0) then
        this%numerator_   = this%numerator_
        this%denumerator_ = this%denumerator_
      end if
    end subroutine

    Integer function racio_getNumerator(this)
      class(Racio) :: this
      racio_getNumerator = this%numerator_
    end function

    Integer function racio_getDenumerator(this)
      class(Racio) :: this
      racio_getDenumerator = this%denumerator_
    end function

    function racio_negate(this)
      class(Racio), intent(in) :: this
      type(Racio) :: racio_negate
      racio_negate = Racio(this%numerator_, -this%denumerator_)
    end function

    function racio_reverse(this)
      class(Racio), intent(in) :: this
      type(Racio) :: racio_reverse
      racio_reverse = Racio(this%denumerator_, this%numerator_)
    end function

    function racio_add(this, that)
      class(Racio), intent(in) :: this, that
      type(Racio)  :: racio_add
      Integer      :: numerator, denumerator
      numerator = this%numerator_*that%denumerator_ &
                + that%numerator_*this%denumerator_
      denumerator = this%denumerator_ * that%denumerator_

      call racio_init(racio_add,numerator, denumerator)
    end function

    function racio_del(this, that)
      class(Racio), intent(in) :: this, that
      type(Racio) :: racio_del
      racio_del = this + that%negate()
    end function

    function itoa(i) result(res)
      character(:),allocatable :: res
      integer,intent(in) :: i
      character(range(i)+2) :: tmp
      write(tmp,'(i0)') i
      res = trim(tmp)
    end function

    function racio_show(this)
      class(Racio), intent(in) :: this
      character(:),allocatable :: racio_show
      racio_show =  itoa(this%numerator_) // "/" // itoa(this%denumerator_)
    end function

    function racio_prod(this, that)
      class(Racio), intent(in) :: this, that
      type(Racio)  :: racio_prod
      Integer      :: numerator, denumerator
      numerator   = this%numerator_   * that%numerator_
      denumerator = this%denumerator_ * that%denumerator_

      call racio_init(racio_prod,numerator, denumerator)
    end function

    function racio_div(this, that)
      class(Racio), intent(in) :: this, that
      type(Racio) :: racio_div
      racio_div = this * that%reverse()
    end function

    function racio_equal(this, that)
      class(Racio), intent(in) :: this, that
      Logical :: racio_equal
      racio_equal = this%numerator_   == that%numerator_ &
              .and. this%denumerator_ == that%denumerator_
    end function

    function racio_notEqual(this, that)
      class(Racio), intent(in) :: this, that
      Logical :: racio_notEqual
      racio_notEqual = .not. racio_equal(this,that)
    end function

    function racio_more(this, that)
      class(Racio), intent(in) :: this, that
      Logical :: racio_more
      racio_more = this%numerator_ * that%denumerator_ &
                 > that%numerator_ * this%denumerator_
    end function

    function racio_less(this, that)
      class(Racio), intent(in) :: this, that
      Logical :: racio_less
      racio_less = this%numerator_ * that%denumerator_ &
                 < that%numerator_ * this%denumerator_
    end function

    function racio_moreOrEqual(this, that)
      class(Racio), intent(in) :: this, that
      Logical :: racio_moreOrEqual
      racio_moreOrEqual = .not. (this < that)
    end function

    function racio_lessOrEqual(this, that)
      class(Racio), intent(in) :: this, that
      Logical :: racio_lessOrEqual
      racio_lessOrEqual = .not. (this > that)
    end function

    function racio_wholePart(this)
      class(Racio), intent(in) :: this
      Integer :: racio_wholePart
      racio_wholePart = this%numerator_ / this%denumerator_
    end function

    end module
