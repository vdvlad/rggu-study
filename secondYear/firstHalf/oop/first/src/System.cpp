#include "System.h"
#include <iostream>

double System::square(double x) {
  return x*x;
}

void System::concat(std::vector<PlacedObject> v1, std::vector<PlacedObject> v2, bool (System::*iF)(const PlacedObject& po)) {
  v1.resize(v1.size()+v2.size());
  for (auto j: v2) {
    if ((this->*iF)(j)) {
      v1.push_back(j);
    }
  }
}

bool System::calcSizeStar(const PlacedObject& po) {
  for (auto i: planets_ ) {
    if ((std::get<0>(i)).getRadius() >= std::get<0>(po).getRadius()) return false;
  }
  for (auto i: asteroids_ ) {
    if (std::get<0>(i).getRadius() >= std::get<0>(po).getRadius()) return false;
  }
  return true;
}

bool System::calcSizePlanet(const PlacedObject& po) {
  if (std::get<0>(stars_[0]).getRadius() <= std::get<0>(po).getRadius()) return false;
  if (starsNumbers_ == 2
      && (std::get<0>(stars_[1]).getRadius() <= std::get<0>(po).getRadius()))
    return false;
  return true;
}

bool System::calcDist(const PlacedObject& po1,const PlacedObject& po2) {
  return (square(std::get<1>(po1)-(std::get<1>(po2)))
         +square(std::get<2>(po1)-(std::get<2>(po2)))
         > std::get<0>(po1).getRadius() + std::get<0>(po2).getRadius());
}

bool System::calcAllKernel(const PlacedObject& po) {
  if (!calcDist(stars_[0],po)
      || std::get<0>(stars_[0]).getName() == std::get<0>(po).getName()
      || std::get<0>(stars_[0]) == std::get<0>((po))) return false;
  if (starsNumbers_ == 2
      && (!calcDist(stars_[1],po)
        || std::get<0>(stars_[1]).getName() == std::get<0>(po).getName()
        || std::get<0>(stars_[1]) == std::get<0>(po))) return false;
  for (auto i: planets_ ) {
    if (!calcDist(i,po)
        || std::get<0>(i).getName() == std::get<0>(po).getName()
        || std::get<0>(i) == std::get<0>(po)) return false;
    for (auto j: std::get<0>(i).getSputniks()) {
      if (   j.getName() == std::get<0>(po).getName()
          || j == std::get<0>(po)) return false;
    }
  }
  for (auto i: asteroids_ ) {
    if (!calcDist(i,po)
        || std::get<0>(i).getName() == std::get<0>(po).getName()
        || std::get<0>(i) == std::get<0>(po)) return false;
  }
  return true;
}

System::System() {
  starsNumbers_ = 1;
  Atmosphere a;
  Object s(minSizeStar()+1,minTempStar()+1,minTempStar()+1,a);
  stars_  = new PlacedObject[1];
  *stars_ = std::make_tuple(s,0,0,0);
}

System::System(const Object& star) {
  starsNumbers_ = 1;
  stars_  = new PlacedObject[1];
  if (star.getLife() == false
    && star.getTmin() > minTempStar()
    && star.getRadius() > minSizeStar()) {
      *stars_ = std::make_tuple(star,0,0,0);
      return;
  }
  Atmosphere a;
  Object s(minSizeStar()+1,minTempStar()+1,minTempStar()+1,a);
  *stars_ = std::make_tuple(s,0,0,0);
}

System::System(const PlacedObject* stars, int num) {
  double x,y;
  double speed= std::get<3>(*stars);
  if (stars != nullptr) {
    if (num == 1 && speed == 0) {
      starsNumbers_ = 1;
      stars_ = new PlacedObject[1];
      *stars_ = stars[0];
      return;
      }
    if (num == 2 && speed > 0) {
      stars_ = new PlacedObject[2];
      *stars_ = stars[0];
      if (!calcDist(stars[1],stars_[0])) {
        return;
      }
      stars_[1] = stars[1];
      starsNumbers_ = 2;
      return;
    }
  }
  x=0,y=0,speed=0;
  starsNumbers_ = 1;
  Atmosphere a;
  Object s(minSizeStar()+1,minTempStar()+1,minTempStar()+1,a);
  stars_  = new PlacedObject[1];
  *stars_ = std::make_tuple(s,x,y,0);
  return;
}

System& System::operator=(const System& sys){
  this->starsNumbers_ = sys.starsNumbers_;
  if (this->stars_ == nullptr) { delete [] stars_;}
  this->stars_  = new PlacedObject[sys.starsNumbers_];
  for (int i=1; i<= sys.starsNumbers_; ++i) {
    this->stars_[i] = sys.stars_[i];
  }
  this->planets_ = sys.planets_;
  this->asteroids_ = sys.asteroids_;
  return *this;
}

bool System::operator==(const System& sys) const{
  if (this->starsNumbers_ != sys.starsNumbers_) return false;
  for (int i=1; i<= sys.starsNumbers_; ++i) {
    if (this->stars_[i] != sys.stars_[i]) return false;
  }
  if ( this->planets_   == sys.planets_
    && this->asteroids_ == sys.asteroids_)
    return true;
  return false;
}

std::ostream& operator<<(std::ostream& os, const System& sys){
  PlacedObject* stars;
  int num;
  std::tie (stars, num) = sys.getStars();
  os << "There are a system:" << std::endl;
  os << "Stars: \"" << std::get<0>(stars[0]).getName();
  if (num == 2) {
    os << "\" and \"" << std::get<0>(stars[1]).getName();
  }
  os << "\"." << std::endl;
  os << "Planets: " << sys.getPlanets().size() << "\"" << std::endl;
  os << "Asteroids: " << sys.getAsteroids().size() << std::endl;
  return os;
}

void System::setStar2(const Object& star, double x, double y, double speed) {
  if (speed < 0) return;
  PlacedObject* starsNew  = new PlacedObject[2];
  starsNew[0] = std::make_tuple(
                  std::get<0>(stars_[0]),
                  std::get<1>(stars_[0]),
                  std::get<2>(stars_[0]),
                  speed);
  starsNew[1] = std::make_tuple(star,x,y,speed);
  if ( calcSizeStar(starsNew[1])
    && star.getLife() == false
    && star.getTmin() > minTempStar()
    && star.getRadius() > minSizeStar()
    && calcAllKernel(starsNew[1])) {
      starsNumbers_ = 2;
      delete [] stars_;
      stars_ = starsNew;
      return;
  }
  delete [] starsNew;
}

void System::setPlanet(const Object& planet, double x, double y, double speed) {
  planets_.clear();
  auto i = std::make_tuple(planet,x,y,speed);
  if (calcAll(i)) {
    planets_.push_back(i);
  }
}

void System::setPlanets(const std::vector<PlacedObject>& planets) {
  planets_.clear();
  concat(planets_, planets, &System::calcAll);
}

void System::setAsteroid(const Object& asteroid, double x, double y, double speed) {
  asteroids_.clear();
  auto i = std::make_tuple(asteroid,x,y,speed);
  if (calcAll(i)) {
    asteroids_.push_back(i);
    std::get<0>(asteroids_[asteroids_.size()-1]).setSputniks(std::vector<Object>());
  }
}

void System::setAsteroids(const std::vector<PlacedObject>& asteroids) {
  asteroids_.clear();
  concat(asteroids_, asteroids, &System::calcAll);
  for (auto i : asteroids_) {
    std::get<0>(i).setSputniks(std::vector<Object>());
  }
}

System::~System() {
  delete [] stars_;
}

void System::changeStar(int num, const Object& star, double x, double y, double speed=0) {
  if (speed < 0) return;
  int anum;
  if (num==1) {
    if (speed != 0 && starsNumbers_) return;
    anum = 0;
  }
  else if (num == 0) anum = 1;
  else return;
  PlacedObject* starsNew  = new PlacedObject[2];
  starsNew[anum] = std::make_tuple(
                  std::get<0>(stars_[anum]),
                  std::get<1>(stars_[anum]),
                  std::get<2>(stars_[anum]),
                  speed);
  starsNew[num] = std::make_tuple(star,x,y,speed);
  if (star.getLife() == false
    && star.getTmin() > minTempStar()
    && star.getRadius() > minSizeStar()
    && calcSizeStar(starsNew[num])
    && calcAllKernel(starsNew[num])) {
      starsNumbers_ = 2;
      delete [] stars_;
      stars_ = starsNew;
      return;
  }
  delete [] starsNew;
}

void System::addPlanet(const PlacedObject& planet){
  if (calcAll(planet)) {
    planets_.push_back(planet);
  }
}

void System::addPlanets(const std::vector<PlacedObject>& planets){
  concat(planets_, planets, &System::calcAll);
}

void System::addAsteroid(const PlacedObject& asteroid){
  if (calcAll(asteroid)) {
    asteroids_.push_back(asteroid);
    std::get<0>(asteroids_[asteroids_.size()-1]).setSputniks(std::vector<Object>());
  }
}

void System::addAsteroids(const std::vector<PlacedObject>& asteroids){
  size_t j = asteroids_.size();
  concat(asteroids_, asteroids, &System::calcAll);
  for (; j<asteroids_.size(); ++j) {
    std::get<0>(asteroids_[j]).setSputniks(std::vector<Object>());
  }
}


const std::tuple<PlacedObject*,int> System::getStars() const{
  return std::make_tuple(stars_, starsNumbers_);
}

const std::vector<PlacedObject>& System::getPlanets() const{
  return planets_;
}

const std::vector<PlacedObject>& System::getAsteroids() const{
  return asteroids_;
}
