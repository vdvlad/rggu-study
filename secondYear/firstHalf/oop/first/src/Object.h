#pragma once
#include "Atmosphere.h"

#include <string>
#include <vector>

#include <fstream>

class Object {
  protected:
    double radius_;
    // temperature, kelvin
    double tMin_;
    double tMax_;

    Atmosphere atm_;

    std::string name_;
    std::vector<Object> sputnik_;

    bool life_;

    // check proportion of arguments in range
    bool range(const double& a, const double& b) const;
  public:
    Object();
    // atm is optional
    Object(double radius, double tMin, double tMax, Atmosphere atm);
    Object(const Object& obj);

    Object& operator=(const Object& obj);
    bool operator==(const Object& obj) const;

    bool isLike(const Object& obj) const;

    void setRadius(const double& radius);
    void setTemperature(const double& tMin,const double& tMax);
    void setAtmosophere(const Atmosphere& atm);
    void setName(const std::string& name);
    void setSputniks(const std::vector<Object>& sat);
    void setLife(const bool& life);

    const double& getRadius() const;
    const double& getTmin() const;
    const double& getTmax() const;
    const Atmosphere& getAtmosophere() const;
    const std::string& getName() const;
    const std::vector<Object>& getSputniks() const;
    const bool& getLife() const;

    void addSputnik(const Object& sat);
    void addSputniks(const std::vector<Object>& sat);

    bool isAtmosphere() const;

    friend std::ostream& operator<<(std::ostream& os, const Object& obj);
};
