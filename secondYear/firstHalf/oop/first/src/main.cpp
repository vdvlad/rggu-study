#include <iostream>
#include <tuple>
#include "Atmosphere.h"
#include "Object.h"
#include "System.h"

int main() {
  Atmosphere a(2,"test",12);
  Object* o = new Object(10.0,2.0,3.2,a);
  Object o1(2.0,2.0,7.2,a);
  Object o2(1.0,3.0,3.2,a);
  Object o3(11.0,3.0,3.2,a);
  // o->addSatellite(o1);
  // o->addSatellite(o2);
  // o->addSatellite(o2);

  Object h(10000000, 20000, 30000, a);
  h.setName("Star");
  System s(h);
  o1.setName("H2");
  o2.setName("H3");
  o3.setName("H3");
  s.addPlanet(std::make_tuple(*o,20000000,3000000,2));
  s.addPlanet(std::make_tuple(o1,200000,200000,10));
  s.addPlanet(std::make_tuple(o2,-200000,200000,10));
  s.addPlanet(std::make_tuple(o3,200000,-200000,10));
  // s.addPlanet(std::make_tuple(*o,200,30,2));
  std::cout << *o << std::endl;
  std::cout << s << std::endl;
  std::cout << std::get<0>(s.getPlanets()[0]) << std::endl;
  std::cout << std::get<0>(s.getPlanets()[1]) << std::endl;
  std::cout << std::get<0>(s.getPlanets()[2]) << std::endl;
  std::cout << std::get<0>(s.getPlanets()[3]) << std::endl;

  delete o;

  return 0;
}
