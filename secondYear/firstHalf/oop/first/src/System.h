#pragma once

#include <vector>
#include <tuple>

#include <algorithm>

#include "Object.h"

// object, x, y, speed
typedef std::tuple<Object, double, double, double> PlacedObject;

class System {
  private:
    // https://futurism.com/the-smallest-possible-size-for-a-star
    // http://www.csun.edu/~boregan/astrolab/manual/unit080.htm
    PlacedObject* stars_;
    int starsNumbers_;
    std::vector<PlacedObject> planets_;
    std::vector<PlacedObject> asteroids_;

    double square(double x);
    void concat(std::vector<PlacedObject> v1, std::vector<PlacedObject> v2, bool (System::*iF)(const PlacedObject& po));
    bool calcSizeStar(const PlacedObject& po);
    bool calcSizePlanet(const PlacedObject& po);
    bool calcDist(const PlacedObject& po1,const PlacedObject& po2);
    bool calcAllKernel(const PlacedObject& po);
    inline bool calcAll(const PlacedObject& po) {return calcAllKernel(po) && calcSizePlanet(po);};

  public:
    System();
    System(const Object& star);
    System(const PlacedObject* stars, int num);

    System& operator=(const System& sys);
    bool operator==(const System& sys) const;
    friend std::ostream& operator<<(std::ostream& os, const System& sys);

    void setStar2(const Object& star, double x, double y, double speed);
    void setPlanet(const Object& planet, double x, double y, double speed);
    void setPlanets(const std::vector<PlacedObject>& planets);
    void setAsteroid(const Object& asteroid, double x, double y, double speed);
    void setAsteroids(const std::vector<PlacedObject>& asteroid);

    // from 0 to 1
    void changeStar(int num, const Object& star, double x, double y, double speed);
    void addPlanet(const PlacedObject& planet);
    void addPlanets(const std::vector<PlacedObject>& planets);
    void addAsteroid(const PlacedObject& asteroid);
    void addAsteroids(const std::vector<PlacedObject>& asteroids);

    const std::tuple<PlacedObject*,int> getStars() const;
    const std::vector<PlacedObject>& getPlanets() const;
    const std::vector<PlacedObject>& getAsteroids() const;

    inline double minSizeStar() const {return 1'391'000/2;}
    inline double minTempStar() const {return 3000;}

    ~System();
};
