#include "Atmosphere.h"

Atmosphere::Atmosphere() {
  gaseVolume_ = 0;
  maxPercent_ = 100;
}

Atmosphere::Atmosphere(const double& gaseVolume, const std::string& gasName, const double gasPercent) {
  gaseVolume_ = gaseVolume;
  if (gasPercent < 100 && gasPercent >=0) {
    gases_[gasName] = gasPercent;
    maxPercent_ = 100 - gasPercent;
  } else if (gasPercent > 100) {
    gases_[gasName] = 100;
    maxPercent_     = 0;
  }
}

Atmosphere::Atmosphere(const double& gaseVolume, const std::unordered_map<std::string,double>& gases){
  if (gaseVolume > 0) {
    gaseVolume_ = gaseVolume;
  } else {
    gaseVolume_ = 0;
  }

  maxPercent_ = 100;
  for (auto& [name,count]: gases) {
    if (maxPercent_ > count) {
      gases_[name] = count;
      maxPercent_ -= count;
    }
    else {
      gases_[name] = maxPercent_;
      return;
    }
  }
}

Atmosphere::Atmosphere(const Atmosphere& atm){
  this->gaseVolume_ = atm.gaseVolume_;
  this->gases_      = atm.gases_;
  this->maxPercent_ = atm.maxPercent_;
}

Atmosphere& Atmosphere::operator=(const Atmosphere& atm){
  this->gaseVolume_ = atm.gaseVolume_;
  this->gases_      = atm.gases_;
  this->maxPercent_ = atm.maxPercent_;
  return *this;
}

bool Atmosphere::operator==(const Atmosphere& atm) const {
  if (this->gases_.size() != atm.gases_.size());
  for (auto& [name,percent] : atm.gases_) {
    double a = abs((*this).gases_.at(name) / percent);
    if (a < 0.9) return false;
    if (a > 1.1) return false;
  }
  return true;
}

const double Atmosphere::operator[](const std::string& name) {
  return gases_[name];
}

std::ostream& operator<<(std::ostream& os, const Atmosphere& atm){
  os << "The atmosphere containt from:" << std::endl;
  for (auto& [name,count]: atm.getGases()) {
    os << "\tGas \"" << name << "\" with " << count << "% of all gases." << std::endl;
  }
  return os;
}

void Atmosphere::clearGaseVolume() {
  gaseVolume_ = 0;
  gases_.clear();
}

void Atmosphere::setGaseVolume(const double& a){
  if (a > 0) {
    gaseVolume_ = a;
  } else {
    gaseVolume_ = 0;
  }
}

void Atmosphere::setGaseVolume2(const double& a){
  double scale = gaseVolume_/a;
  if (scale > 1) return;
  for (auto& [name,count]: gases_) {
    gases_[name] = count*scale;
  }
  gaseVolume_ = a;
  maxPercent_ = a - gaseVolume_;
}

double Atmosphere::getGaseVolume() const {
  return gaseVolume_;
}

void Atmosphere::setGases(const std::unordered_map<std::string,double>& gases){
  gases_.clear();
  maxPercent_ = 100;
  for (auto& [name,count]: gases) {
    if (maxPercent_ > count) {
      gases_[name] = count;
      maxPercent_ -= count;
    }
    else {
      gases_[name] = maxPercent_;
      return;
    }
  }
}

void Atmosphere::addGases(const std::unordered_map<std::string,double>& gases){
  for (auto& [name,count]: gases) {
    if (maxPercent_ > count) {
      gases_[name] = count;
      maxPercent_ -= count;
    }
    else {
      gases_[name] = maxPercent_;
      return;
    }
  }
}

void Atmosphere::addGas(const std::string& gasName, const double& gasePercent){
  if (maxPercent_ + gasePercent < 100) {
    gases_[gasName] = gasePercent;
    maxPercent_ -= gasePercent;
    return;
  }
  gases_[gasName] = maxPercent_;
  maxPercent_     = 0;
}

const std::unordered_map<std::string,double>& Atmosphere::getGases() const{
  return gases_;
}
