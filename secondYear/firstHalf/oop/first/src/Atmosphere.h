#pragma once

#include <string>
#include <unordered_map>

#include <iterator>
#include <fstream>

class Atmosphere {
  private:
    double gaseVolume_;
    double maxPercent_;
    // unordered_map of pair<name,percent>
    std::unordered_map<std::string,double> gases_;
  public:
    Atmosphere();
    Atmosphere(const double& gaseVolume, const std::string& gasName, const double gasPercent);
    Atmosphere(const double& gaseVolume, const std::unordered_map<std::string,double>& gases);
    Atmosphere(const Atmosphere& atm);

    Atmosphere& operator=(const Atmosphere& atm);
    // is equal percent?
    bool operator==(const Atmosphere& atm) const;
    const double operator[](const std::string& name);
    friend std::ostream& operator<<(std::ostream& os, const Atmosphere& atm);

    void clearGaseVolume();
    // percents save
    void   setGaseVolume(const double& a);
    // with "free" space, only more then older
    void   setGaseVolume2(const double& a);
    double getGaseVolume() const;

    void   setGases(const std::unordered_map<std::string,double>& gases);
    void   addGases(const std::unordered_map<std::string,double>& gases);
    void   addGas(const std::string& gasName, const double& gasePercent);
    const std::unordered_map<std::string,double>& getGases() const;
};
