#include "Object.h"

bool Object::range(const double& a, const double& b) const {
  double c = a/b;
  return (0.9 > c && c < 1.1);
}

Object::Object(){
  radius_ = 1;
  tMin_   = 100;
  tMax_   = 100;
  name_   = "";
  life_   = false;
}

Object::Object(double radius, double tMin, double tMax, Atmosphere atm=Atmosphere()){
  if (radius > 0) {
    radius_  = radius;
  } else radius_ = abs(radius);

  if (tMin > 0) {
    tMin_   = tMin;
  } else tMin_ = 0;
  if (tMax >= tMin) {
    tMax_   = tMax;
  } else tMax_ = 0;
  atm_ = atm;
  name_   = "";
  life_   = false;
}

Object::Object(const Object& obj){
  this->radius_    = obj.radius_;
  this->tMin_      = obj.tMin_;
  this->tMax_      = obj.tMax_;
  this->atm_       = obj.atm_;
  this->sputnik_ = obj.sputnik_;
  this->name_      = obj.name_;
  this->life_      = obj.life_;
}

Object& Object::operator=(const Object& obj){
  this->radius_    = obj.radius_;
  this->tMin_      = obj.tMin_;
  this->tMax_      = obj.tMax_;
  this->atm_       = obj.atm_;
  this->sputnik_ = obj.sputnik_;
  this->name_      = obj.name_;
  this->life_      = obj.life_;
  return *this;
}

bool Object::operator==(const Object& obj) const {
  return (
      this->radius_    == obj.radius_
  &&  this->tMin_      == obj.tMin_
  &&  this->tMax_      == obj.tMax_
  &&  this->atm_       == obj.atm_
  &&  this->sputnik_ == obj.sputnik_
  &&  this->name_      == obj.name_
  &&  this->life_      == obj.life_);
}

bool Object::isLike(const Object& obj) const {
  return (
        range(this->radius_, obj.radius_)
    &&  range(this->tMin_, obj.tMin_)
    &&  range(this->tMax_, obj.tMax_)
    &&  this->atm_ == obj.atm_
    &&  range(this->sputnik_.size(), obj.sputnik_.size()));
}

void Object::setRadius(const double& radius){
  if (radius > 0) {
    radius_  = radius;
  }
}

void Object::setTemperature(const double& tMin,const double& tMax){
  if (tMin > 0) {
    tMin_   = tMin;
  }
  if (tMax >= tMin) {
    tMax_   = tMax;
  }
}

void Object::setName(const std::string& name) {
  name_ = name;
}

void Object::setAtmosophere(const Atmosphere& atm){
  atm_ = atm;
}

void Object::setSputniks(const std::vector<Object>& sat) {
  sputnik_.clear();
  size_t j=0;
  for (auto& i : sat) {
    if (this->getRadius() > i.getRadius()) {
      for (; j< sputnik_.size() && !(i==sputnik_[j]) ; ++j);
      if (j == sputnik_.size()) {
        sputnik_.push_back(i);
        sputnik_[sputnik_.size()-1].setSputniks(std::vector<Object>());
      }
    }
  }
}

const double& Object::getRadius() const {
  return radius_;
}

const double& Object::getTmin() const {
  return tMin_;
}

const double& Object::getTmax() const {
  return tMax_;
}

const Atmosphere& Object::getAtmosophere() const {
  return atm_;
}

const std::string& Object::getName() const {
  return name_;
}

const std::vector<Object>& Object::getSputniks() const {
  return sputnik_;
}

const bool& Object::getLife() const {
  return life_;
}

std::ostream& operator<<(std::ostream& os, const Object& obj) {
  os << "Characteristic of object";
  if (obj.name_ != "") {
    os << ", named \"" << obj.name_ << "\":" << std::endl;
  } else {
    os << ":" << std::endl;
  }
  os << "Radius is " << obj.radius_ << std::endl;
  os << "Temperature is " << obj.tMin_ << "-" << obj.tMax_ << "K" << std::endl;
  os << "Count of sputniks: " << obj.sputnik_.size() << std::endl;

  return os;
}

void Object::addSputnik(const Object& sat) {
  if (this->getRadius() > sat.getRadius()) {
    size_t j=0;
    for (; j< sputnik_.size() && !(sat==sputnik_[j]) ; ++j);
    if (j == sputnik_.size()) {
      sputnik_.push_back(sat);
    }
  }
}

void Object::addSputniks(const std::vector<Object>& sat) {
  size_t j=0;
  for (auto& i : sat) {
    if (this->getRadius() > i.getRadius()) {
      for (; j< sputnik_.size() && !(i==sputnik_[j]) ; ++j);
      if (j == sputnik_.size()) {
        sputnik_.push_back(i);
        sputnik_[sputnik_.size()-1].setSputniks(std::vector<Object>());
      }
    }
  }
}

bool Object::isAtmosphere() const {
  return atm_.getGaseVolume() > 0;
}
